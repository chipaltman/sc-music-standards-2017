
Artistic Processes: Connecting- I can connect musical ideas and works to personal experience, careers,
culture, history, and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Benchmark
CM.C NL.8 I can
recognize
musical
selections
from some
cultures and
time
periods.

Benchmark
CM.C NM.8 I can identify
musical
selections
from a
specific
culture and a
historical
time period.

Benchmark
CM.C NH.8 I can identify
musical
selections
from multiple
cultures
and/or
historical
time periods.

Benchmark
CM.C IL.8 I can examine
relationships
among
musical
selections
from multiple
cultures and/or
historical time
periods.

Indicator
CM.C NL.8.1 I can
recognize
that all
cultures
perform
music.

Indicator
CM.C NM.8.1 I can
recognize
similar
elements of
music in a
specific
culture.

Indicator
CM.P NH.8.1 I can identify
similar
elements of
music in
different
cultures.

Indicator
CM.C IL.8.1 I can examine
music from
multiple
cultures and
time periods.

Benchmark
CM.C IM.8 I can
research the
role of
music
within a
specific
culture or
historical
time period
and present
what I
discovered.
Indicator
CM.C IM.8.1 I can
research a
specific
culture/time
period and
perform a
song from
that
culture/time
period.

Benchmark
CM.C IH.8 I can modify
a musical
work using
characteristi
cs from a
culture or
time period.

Benchmark
CM.C AL.8 I can
examine
contemporary
musical
works to
determine the
influence of
historical and
cultural
traditions.

Benchmark
CM.C AM.8 I can analyze
a diverse
repertoire of
music from a
cultural or
historical
time period.

Benchmark
CM.C AH.8 I can examine
and perform
music based
on historical
and cultural
contributions.

Indicator
CM.C IH.8.1 I can change
a musical
work using
the
elements of
music from
a culture or
time period.

Indicator
CM.C AL.8.1 I can explain
specific
cultural and
historical
traditions and
infuse these
ideas into my
music.

Indicator
CM.C AM.8.1 I can select
musical
elements in
contemporar
y music that
reflect
cultural and
historical
influences.

Indicator
CM.C AH.8.1 I can use
historical and
cultural
contributions
to justify my
musical
choices.

Page 149

Anchor Standard 9: I can relate music to other arts disciplines, content areas and career path choices.
Benchmark
CM.C NL.9 I can
explore
choral music
concepts
among arts
disciplines
other
content
areas and
related
careers.

Benchmark
CM.C NM.9 I can
recognize
choral music
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
CM.C NH.9 I can apply
choral music
concepts to
arts
disciplines,
other content
areas, and
related
careers.

Benchmark
CM.C IL.9 I can explore a
range of skills
shared among
arts
disciplines,
other content
areas and how
they can be
applied to a
career in
music.

Benchmark
CM.C IM.9 I can
recognize
specific
skills shared
among arts
disciplines,
other
content
areas and
how they
can be
applied to a
career in
music.

Indicator
CM.C NL.9.1
Indicator
CM.C NM.9.1
Indicator
CM.C NH.9.1
Indicator
CM.C IL.9.1
Indicator
CM.C IM.9.1
I can
identify the
relationship
between
music and
another
subject in
my school.

I can
demonstrate
a relationship
between
music and
another
subject in my
school.

I can
demonstrate
and describe
the
relationship
between
music and a
concept from
another
subject in my
school.

I can apply
music
concepts to
other arts
disciplines and
content areas.

I can
examine the
relationship
between
music and
specific
content from
another arts
discipline
and content
area.

Benchmark
CM.C IH.9 I can
analyze the
tools,
concepts,
and
materials
used among
arts
disciplines,
other
content
areas and
how they
are used in
music
careers.
Indicator
CM.C IH.9.1
Benchmark
CM.C AL.9 I can apply
concepts
among arts
disciplines
and other
content areas
to choral
music and
analyze how
my interests
and skills
will prepare
me for a
career.

Benchmark
CM.C AM.9 I can explain
how
economic
conditions,
cultural
values and
location
influence
music and the
need for
music related
careers.

Benchmark
CM.C AH.9 I can research
societal
political and
cultural issues
as they relate
to other arts
and content
areas and
apply to my
role as a
musician.

Indicator
CM.C AL.9.1
Indicator
CM.C AM.9.1
Indicator
CM.C AH.9.1
I can apply
concepts
from other
arts
disciplines
and content
areas to my
music.

I can explain
ideas from
other arts
disciplines
and content
areas through
music.

I can explain
how my
artistic
choices are
influenced by
cultural and
social values.

I can analyze
complex ideas
that influence
my artistic
perspective
and creative
work.

Page 150

Indicator
CM.C NL.9.2
Indicator
CM.C NM.9.2
Indicator
CM.C NH.9.2
I can
identify and
discuss
examples of
musicians in
my
community.

I can identify
life skills
necessary for
a music
career.

I can identify
specific
careers in
music.

Indicator
CM.C IL.9.2 I can
demonstrate
and describe
the skills
needed for
careers in
music.

Indicator
CM.C IM.9.2 I can
examine the
educational
requirement
s needed for
a variety of
careers in
music.

Indicator
CM.C IH.9.2 I can
research
skills
needed for
various
music
careers.

Indicator
CM.C AL.9.2
Indicator
CM.C AM.9.2
Indicator
CM.C AH.9.2
I can describe
traditional
and emerging
careers in
music.

I can pursue
opportunities
that will lead
me to a
career in
music.

I can research
my personal
career choices
in the arts.

Page 151

Artistic Processes: Connecting - I can connect musical ideas and works to personal experience, careers,
culture, history and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Novice
Low
Benchmark
CM.P NL.8
I can recognize musical selections from some
cultures and time periods.

Novice
Mid
Benchmark
CM.P NM.8
I can identify musical selections from a
specific culture and a historical time period.

Indicator
CM.P NL.8.1
I can recognize that all cultures perform
music.
Sample Learning Targets

Indicator
CM.P NM.8.1
I can recognize similar elements of music in
a specific culture.
Sample Learning Targets

I can sing a patriotic song.




I can recognize historical/ cultural
events in my community where choral
music is performed.
I can...



I can recognize that call and
response is a type of form in
African-American music.



I can recognize syncopated patterns in
Latin music.



Novice
High
Benchmark
CM.P NH.8
I can identify musical selections from
multiple cultures and/or historical time
periods.
Indicator
CM.P NH.8.1
I can identify similar elements of music in
different cultures.
Sample Learning Targets


I can sing a song in AB form from
America, such as “Yankee Doodle,
and find a similar AB form in a song
from Australia, such as “Waltzing
Matilda.”



I can use technology (YouTube
recordings and choral websites) to
identify different choral genres.



I can...

I can...

Page 165

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Novice
Low
Benchmark
CM.P NL.9
I can explore choral music concepts among
arts disciplines, other content areas, and
related careers.
Indicator
CM.C NL.9.1
I can identify the relationship between music
and another subject in my school.

Novice
Mid
Benchmark
CM.P NM.9
I can recognize choral music concepts among
arts disciplines, other content areas, and
related careers.
Indicator
CM.C NM.9.1
I can demonstrate a relationship between
music and another subject in my school.

Sample Learning Targets

Sample Learning Targets






I can sing high and low pitches in
“Star Light, Star Bright,” and I can
point to high and low images in Van
Gogh’s Starry Night.
I can identify rhyming words in a
song.



I can count in beats of four while
moving to folk songs in 4/4 meter.



I can describe the relationship
between sound waves and pitch.



I can…

Novice
High
Benchmark
CM.P NH.9
I can apply choral music concepts to arts
disciplines, other content areas, and related
careers.
Indicator
CM.C NH.9.1
I can demonstrate and describe the
relationship between music and a concept
from another subject in my school.
Sample Learning Targets


I can research the history of the
Underground Railroad and discuss
how this history is reflected in the
African-American song, “Follow the
Drinking Gourd.”



I can find repeated rhythms
(ostinati) in a song, and find repeated
stanzas in a poem.



I can…

I can...

Page 166

Indicator
CM.C NL.9.2
I can identify and discuss examples of
musicians in my community.

Indicator
CM.C NM.9.2
I can identify life skills necessary for a music
career.

Indicator
CM.C NH.9.2
I can identify specific careers in music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets







I can discuss the sounds/songs I hear
at a football game.



I can describe proper performer and
audience behavior for a concert.

I can list places where I have seen
musicians perform.



I can work with others to improve my
performance.

I can identify musicians in my
community (choir director, church
singer).




I can…



I can use an internet search engine to
locate arts businesses who employ
musicians, (such as theaters, music
stores, university arts departments,
churches).



I describe music careers of
community members.



I can…

I can...

Page 167
