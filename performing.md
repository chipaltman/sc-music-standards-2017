# Artistic Process: Performing

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.

Benchmark
CM.P NL.3
Benchmark
CM.P NM.3
Benchmark
CM.P NH.3
Benchmark
CM.P IL.3
Benchmark
CM.P IM.3
Benchmark
CM.P IH.3
Benchmark
CM.P AL.3
Benchmark
CM.P AM.3
Benchmark
CM.P AH.3
I can
produce a
steady, free
tone on a
comfortable
pitch.

I can produce
a steady, free
tone within a
limited
range.

I can produce
a steady, free
tone while
singing in
tune.

I can produce
a centered
tone in a
comfortable
tessitura.

I can
produce a
centered
tone in some
tessituras
specific to
my vocal
range.

I can
produce a
centered
tone in most
tessituras
specific to
my vocal
range.

I can produce
a welldeveloped
tone in all
tessituras
specific to
my vocal
range.

Indicator
CM.P NL.3.1 I can sing
some simple
patterns
alone and
with others.

Indicator
CM.P NM.3.1 I can identify
and sing in
my head and
chest voices.

Indicator
CM.P NH.3.1
Indicator
CM.P IL.3.1 I can sing with
a resonant,
centered, and
free tone in
harmony.

Indicator
CM.PIM.3.1
Indicator
CM.P IM.3.1 I can sing
2-3 part
songs with
centered
tone
quality, in
tune, while
demonstrati
ng dynamic
changes.

Indicator
CM.PAL.3.1 I can
consistently
produce a
welldeveloped,
vibrant tone
across the
entire range
of my voice.
Indicator

CM.PAM.3.1 I can sing
with a welldeveloped
tone, some 34 part songs,
demonstratin
g balance
and
intonation,
by adjusting
my voice to
conductor’s
cues.

I can sing
alone and
within a 3-4
part
ensemble,
singing with
welldeveloped
tone quality
while
maintaining
balance and
intonation.

I can adjust
tone
color/timbre
in response to
stylistic
demands and
the musical
needs of an
ensemble.
Indicator
CM.P AH.3.1 I can
manipulate the
tone quality
of my voice to
reflect the
stylistic
demands of a
piece of
music.

I can blend
my voice
with others
singing in
tune in my
head voice.

I can sing
my assigned
part in tune
with
appropriate
tone
quality,
resonance
and vocal
timbre.

Page 139

Indicator
CM.P NL.3.2 I can
demonstrate
correct
singing
posture.

Indicator
CM.P NM.3.2 I can sing
songs based
on the
pentatonic
scale.

Indicator
CM.P NH.3.2 I can sing in
tune with
breath
support.

Indicator
CM.P IL.3.2 I can sing in
tune my
assigned part
with clear
tone quality,
using breath
control and
correct
posture.

Indicator
CM.P IM.3.2 I can sing
with a
centered
tone and a
steady
tempo.

Indicator
CM.P IH.3.2
Indicator
CM.P AL.3.2
Indicator
CM.P AM.3.2
Indicator
CM.P AH.3.2
I can sing
2-3 part
songs with
centered
tone
quality, in
tune, while
demonstrat
ing
articulation
changes.

I can sing
with a welldeveloped
tone,
incorporating
all musical
symbols,
tempo and
expressive
indications.

I can sing
with welldeveloped
tone quality
and increased
vocal
technique.

I can sing in a
variety of
languages
with welldeveloped
tone quality,
making
needed
adjustments in
vocal
technique.

Benchmark
CM.P AM.4 I can sing
with
increased
fluency and
expression in
small and
large
ensembles a
varied
repertoire/
genre of
choral music.

Benchmark
CM.P AH.4 I can sing with
increased
fluency and
expression
from memory
varied
repertoire/
genres of
choral music.

Anchor Standard 4: I can perform with technical accuracy and expression.
Benchmark
CM.P NL.4 I can speak,
chant, sing,
and move to
demonstrate
awareness
of beat.

Benchmark
CM.P NM.4 I can speak,
chant, sing
and move to
demonstrate
awareness of
beat, tempo,
dynamics,
and melodic
direction.

Benchmark
CM.P NH.4 I can sing
expressively,
alone or in
groups,
matching
dynamic
levels and
responding to
the cues of a
conductor.

Benchmark
CM.P IL.4 I can sing
expressively
with
appropriate
dynamics and
phrasing.

Benchmark
CM.P IM.4 I can sing
expressively
with
appropriate
dynamics,
phrasing,
and
interpretation.

Benchmark
CM.P IH.4 I can sing
while
interpreting
my
conductor’s
cues in
order to
perform
with
expression
and
technical
accuracy.

Benchmark
CM.P AL.4 I can sing
with
increased
fluency and
expression a
varied
repertoire/
genre of
choral music.

Page 140

Indicator
CM.P NL.4.1 I can speak,
chant to the
beat.

Indicator
CM.P NM.4.1 I can
demonstrate
different
tempo
markings
when singing
and moving
to the beat.

Indicator
CM.P NH.4.1 I can
demonstrate
dynamic
levels in
response to a
conductor.

Indicator
CM.P IL.4.1 I can sing,
observing a
variety of
dynamic
markings in
songs.

Indicator
CM.P IM.4.1 I can
interpret a
conductor’s
dynamic
and
phrasing
cues when
singing.

Indicator
CM.P IH.4.1 I can
interpret a
conductor’s
gesture with
rhythmic
and melodic
precision.

Indicator
CM.P AL.4.1 I can sing
with
rhythmic
and melodic
precision
music from
diverse
genres.

Indicator
CM.P AM.4.1 I can
interpret a
conductor’s
gesture in a
varied
repertoire of
music.

Indicator
CM.P AH.4.1 I can enhance
the expressive
quality of my
performance
through
singing from
memory.

// mark

Indicator
CM.P NL.4.2 I can sing
and move to
the beat.

Indicator
CM.P NM.4.2 I can
demonstrate
dynamic
levels when
singing and
moving to the
beat.

Indicator
CM.P NH.4.2 I can respond
to a
conductor’s
gradual
dynamic
cues when
singing.

Indicator
CM.P IL.4.2 I can sing,
observing
phrasing
suggestions
and markings
in music.

Indicator
CM.P IM.4.2 I can sing,
observing
phrasing
markings
and
breathing
appropriately alone and
in groups.

Indicator
CM.P IH.4.2 I can
interpret a
conductor’s
dynamic,
articulation
, and
phrasing
cues.

Indicator
CM.P AL.4.2 I can sing
observing
dynamics,
articulation,
and
phrasing, in
the style of
the music.

Indicator
CM.P AM.4.2 I can
interpret a
conductor’s
gestures
appropriate
to the genre.

Indicator
CM.P AH.4.2 I can sing a
cappella
vocal
selections
from memory.

Page 141

Anchor Standard 5: I can perform using music notation.
Benchmark
CM.P NL.5 I can
identify
music
notation
symbols
representing
simple
familiar
tonal and
rhythm
patterns
and tunes.

Benchmark
CM.P NM.5 I can read
and perform
tonal and
rhythmic
patterns
using music
notation.

Benchmark
CM.P NH.5 I can read
and perform
simple
unfamiliar
and familiar
songs using
music
notation.

Benchmark
CM.P IL.5 I can identify
music
notation,
symbols
representing
an expanded
set of tonal,
rhythmic,
technical, and
expressive
ideas.

Benchmark
CM.P IM.5 I can
perform at
sight simple
unfamiliar
musical
works.

Benchmark
CM.P IH.5 I can use a
system to
fluently
sight-read
moderately
complex
melodies in
treble and
bass clefs.

Benchmark
CM.P AL.5 I can perform
at sight
complex
unfamiliar
musical
works with
accuracy.

Benchmark
CM.P AM.5 I can perform
at sight
complex
unfamiliar
musical
works with
accuracy and
appropriate
expression/
interpretation.

Benchmark
CM.P AH.5 I can perform
at sight
complex
unfamiliar
musical works
with accuracy,
appropriate
expression/
interpretation and
fluency.

Page 142

Indicator
CM.P NL.5.1 I can use
nontraditional
notation to
identify
pitches in a
clef.

Indicator
CM.P NM.5.1 I can sing
tonal
patterns
using a sightreading
system.

Indicator
CM.P NH.5.1 I can perform
simple
unfamiliar
rhythm
patterns
using music
notation.

Indicator
CM.P IL.5.1 I can identify
sharps, flats,
naturals, and
simple key
signatures.

Indicator
CM.P IM.5.1 I can
perform at
sight simple
unfamiliar
musical
works with
accurate
pitches.

Indicator
CM.P NL.5.2 I can
identify
note values.

Indicator
CM.P NM.5.2 I can
identify basic
time
signatures.

Indicator
CM.P NH.5.2 I can perform
simple
unfamiliar
tonal
patterns
using music
notation.

Indicator
CM.P IL.5.2 I can sightread stepwise
tonic (do, re,
mi, fa, so)
patterns and
simple meter
based (2/4,
3/4, 4/4)
rhythmic
patterns.

Indicator
CM.P IM.5.2 I can sight
read using
reading
systems
such as taka-di-mi,
Gordon,
count
singing, and
neutral
syllables to
unfamiliar
melodies
with tonic
triad skips.

Indicator
CM.P IH.5.1 I can
perform at
sight
moderately
complex
unfamiliar
musical
works with
accurate
pitches.
Indicator
CM.P IH.5.2 I can notate
intermediate
note values
and time
signatures.

Indicator
CM.P AL.5.1 I can perform
at sight
complex
unfamiliar
musical
works with
accurate
pitches

Indicator
CM.P AM.5.1 I can perform
at sight
complex
unfamiliar
musical
works with
correction
articulation.

Indicator
CM.P AH.5.1 I can perform
at sight
complex
unfamiliar
works with
fluency.

Indicator
CM.P AL.5.2 I can sight
read using
multiple
reading
systems (taka-di-mi and
Gordon,
count
singing,
neutral
syllables) in
my music.

Indicator
CM.P AM.5.2 I can
identify
advanced
note values
and time
signatures
that represent
smaller beat
subdivisions
in my music.

Indicator
CM.P AH.5.2 I can notate
advanced
values and
time
signatures
that represent
syncopation
and smaller
beat
subdivisions
in my music.

Page 143

Indicator
CM.P NL.5.3 I can
identify
simple
familiar
rhythm
patterns
with
corresponding
notation.

Indicator
CM.P NM.5.3 I can sing
using eighth,
quarter, half
and whole
notes and
rests.

Indicator
CM.P NL.5.4
Indicator
CM.P NM.5.4
I can
identify
simple
familiar
tonal
patterns
with
corresponding
notation.

I can sing a
variety of
tempos in
music.

Indicator
CM.PNH.5.3 I can sing in
unison and
simple 2part music.

Indicator
CM.P NH.5.4
I can sing
simple
patterns in
multiple
tonalities.

Indicator
CM.P IL.5.3 I can identify
advanced note
values and
time
signatures
that represent
syncopation
and smaller
beat
subdivisions
in my music.

Indicator
CM.P IM.5.3 I can apply
basic tempo
markings in
my music.

Indicator
CM.P IH.5.3 I can apply
intermediate
tempo
markings in
my music.

Indicator
CM.P IL.5.4
Indicator
CM.P IM.5.4 I can apply
expressive
markings in
my music.

I can identify
expressive
markings in
my music.

Indicator
CM.P AL.5.3 Indicator
CM.PAM.5.3 Indicator
CM P AH.5.3 I can justify
the use of
advanced
tempo
markings in
my music.

I can identify
the use of
advanced
tempo
markings in
my music.

I can analyze
the use of
advanced
tempo
markings in
my music.

Indicator
CM.P IH.5.4
Indicator
CM.P AL.5.4
Indicator
CM.P AM.5.4
Indicator
CM.P AH.5.4
I can apply
advanced
expressive
markings in
my music.

I can identify
technical,
expressive,
and formal
markings in
my music.

I can analyze
the technical,
expressive,
and formal
markings in
my music.

I can justify
the technical,
expressive,
and formal
markings in
my music.

Page 144

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.
Novice
Low
Benchmark
CM.P NL.3 I can produce a steady, free tone on a
comfortable pitch.
Indicator
CM.P NL.3.1 I can sing some simple patterns alone and
with others.
Sample Learning Targets

Novice
Mid
Benchmark
CM.P NM.3 I can produce a steady, free tone within a
limited range.
Indicator
CM.P NM.3.1 I can identify and sing in my head and chest
voices.
Sample Learning Targets



I can echo sing some sol-mi patterns.



I can sing sirens, sighs, lip trills.



I can sing the response in a call and
response song.



I can sing a major scale.




I can match pitch when I sing a
simple song.

I can sing vocal warm-ups moving
by half-steps.

I can sing through a simple phrase without
taking a breath.
 I can sing on pitch high/low.




I can identify head and chest voice
by listening to performance examples.



I can...

Novice
High
Benchmark
CM.P NH.3 I can produce a steady, free tone while singing
in tune.
Indicator
CM.P NH.3.1 I can blend my voice with others singing in tune in my head voice.
Sample Learning Targets


I can produce a light, clear tone while
singing different dynamic levels. (Ex.
Not shouting when you sing forte).



I can sing with a lifted soft palate.



I can…

I can…

Page 156

Indicator
CM.P NL.3.2 I can demonstrate correct singing posture.
Sample Learning Targets






I can stand with knees slightly bent,
feet shoulder width apart, my ribcage
lifted, relaxed shoulders, and chin
parallel with the floor.
I can sit with both feet on the floor,
on the edge of my chair, my ribcage
lifted, relaxed shoulders, and chin
parallel with the floor.

Indicator
CM.P NM.3.2 I can sing songs based on the pentatonic
scale.
Sample Learning Targets


I can sing in unison a folk song based
on the pentatonic scale.



I can sing vocal warm-ups based on
the pentatonic scale.



I can...

Indicator
CM.P NH.3.2 I can sing in tune with breath support.
Sample Learning Targets


I can sing with soft palate lifted.



I can take a low diaphragmatic
breath.



I can…

I can…

Anchor Standard 4: I can perform with technical accuracy and expression.
Novice
Low
Benchmark
CM.P NL.4 I can speak, chant, sing and move to
demonstrate awareness of beat.

Novice
Mid
Benchmark
CM.P NM.4 I can speak, chant, sing, and move to
demonstrate awareness of beat, tempo,
dynamics, and melodic direction.

Novice
High
Benchmark
CM.P NH.4 I can sing expressively, alone or in groups,
matching dynamic levels and responding to
the cues of a conductor.

Page 157

Indicator
CM.P NL.4.1 I can speak, chant to the beat.
Sample Learning Targets


I can play chanting games.



I can echo chant patterns.



I can patsch the beat while I chant.



I can...

Indicator
CM.P NL.4.2 I can sing and move to the beat.
Sample Learning Targets


I can play traditional singing games.



I can skip, hop, and jump to the beat.



I can...

Indicator
CM.P NM.4.1 I can demonstrate different tempo markings
when singing and moving to the beat.
Sample Learning Targets

Indicator
CM.P NH.4.1 I can demonstrate dynamic levels in response
to a conductor.
Sample Learning Targets



I can demonstrate adagio in a chant
while walking to the beat.



I can sing forte in response to a
conductor’s gesture.



I can demonstrate allegro in singing
while walking to the beat.



I can sing piano in a large ensemble
in response to a conductor’s gesture.



I can...



I can describe dynamics by symbol
located in my music score.



I can...

Indicator
CM.P NM.4.2 I can demonstrate dynamic levels when
singing and moving to the beat.
Sample Learning Targets

Indicator
CM.P NH.4.2 I can respond to a conductor’s gradual
dynamic cues when singing.
Sample Learning Targets



I can sing forte while patsching the
beat.



I can respond to my conductor’s cues
by singing a crescendo.



I can sing a crescendo while
patsching the beat.



I can respond to my conductor’s curs
by singing a decrescendo.



I can…



I can...

Page 158

Anchor Standard 5: I can perform using music notation.
Novice
Low
Benchmark
CM.P NL.5 I can identify music notation symbols
representing simple familiar tonal and
rhythm patterns and tunes.
Indicator
CM.P NL.5.1 I can use non-traditional notation to identify
pitches in a clef.
Sample Learning Targets

Novice
Mid
Benchmark
CM.P NM.5 I can read and perform tonal and rhythmic
patterns using music notation.

Novice
High
Benchmark
CM.P NH.5 I can read and perform simple unfamiliar and
familiar songs using music notation.

Indicator
CM.P NM.5.1 I can sing tonal patterns using a sightreading system.
Sample Learning Targets

Indicator
CM.P NH.5.1
I can perform simple unfamiliar rhythm
patterns using music notation.
Sample Learning Targets



I can use pictures/objects to identify
high and low pitches.



I can sing music examples using
solfège and Kodaly.



I can use my hands to identify so and
mi.



I can sing so, mi, and la in a music
example.

I can...



I can sing so, mi, and do in a music
example.



I can...





I can chant an unfamiliar four-beat
rhythm pattern presented in
notation, in a familiar meter.



I can clap an unfamiliar four-beat
rhythm pattern presented in notation,
in a familiar meter.



I can...

Page 159

Indicator
CM.P NL.5.2
I can identify note values.

Indicator
CM.P NM.5.2
I can identify basic time meters.

Sample Learning Targets

Sample Learning Targets



I can identify quarter, eighth, half,
and whole notes in a familiar song.



I can sing music examples in simple
meter(2/4, 3/4 and 4/4)



I can identify strong and weak beats
in music.



I can identify the meter signature of a
familiar song.





Indicator
CM.P NM.5.3
I can sing using eighth, quarter, half and
whole notes and rests.
Sample Learning Targets



I can match the correct notation with
a familiar four-beat rhythm pattern
presented to me aurally.



I can perform a familiar four-beat
rhythm pattern presented in
notation.



I can select the correct notation to
represent a familiar four-beat rhythm
pattern presented aurally.



I can perform a familiar four-beat
tonal pattern presented in notation.



I can match the correct notation with
a familiar rhythm presented to me
aurally.



I can...





I can perform so, mi, and la tonal
patterns presented in notation, in a
familiar tonality.



I can perform so, mi, and do tonal
patterns presented in notation, in a
familiar tonality.



I can...

I can...

I can...

Indicator
CM.P NL.5.3
I can identify simple familiar rhythm
patterns with corresponding notation.
Sample Learning Targets

Indicator
CM.P NH.5.2
I can perform simple unfamiliar tonal
patterns using music notation.
Sample Learning Targets

Indicator
CM.P NH.5.3
I can sing in unison and simple 2-part
music.
Sample Learning Targets
I can sing a simple folk song in unison with
and without accompaniment.
 I can sing in a round or canon.


I can...

I can...

Page 160

Indicator
CM.P NL.5.4
I can identify simple familiar tonal patterns
with corresponding notation.
Sample Learning Targets


I can match the correct notation with
a familiar three-pitch tonal pattern
presented to me aurally.



I can select the correct notation to
represent a familiar three-pitch tonal
pattern presented to me aurally.



Indicator
CM.P NM.5.4
I can sing a variety of tempos in music.
Sample Learning Targets
I can perform accelerando and ritardando in a
song.
I can perform sudden tempo changes in a
song.
 I can...

Indicator
CM.P NH.5.4 I can sing simple patterns in multiple
tonalities.
Sample Learning Targets


I can sing pitches in major and
minor.



I can sing pitches in pentatonic
patterns.



I can...

I can...
