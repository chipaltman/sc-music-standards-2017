# College and Career Ready Standards for Choral Music Proficiency

_South Carolina Department of Education_

_Columbia, South Carolina_

_2017_

Page 133

## Choral Music Introduction

In writing the 2017 South Carolina Choral Music Standards, our goal was to bridge the 2010 South Carolina Choral Music Standards
with the 2014 National Core Arts Standards for Music to create a simplified, relevant document for teachers and students to use in the
Choral Music classroom. The purpose of this document and the “I can” language is to enable the teacher to become the facilitator of
goals for the student using benchmarks to set achievable goals and to self-assess to take ownership of their learning.
Choral students come to us from a variety of musical backgrounds and experiences. A freshman high school choral classroom may
consist of students who perform at novice levels as well as students who perform at advanced levels. Moving from a grade-level based
model to a proficiency-based model allows teachers to meet students at their individual ability level to differentiate learning most
effectively. Many choral teachers are also teachers of general or instrumental music. For simplified planning, we have chosen to
streamline the wording of several standards, benchmarks, and indicators with the other music areas. The sample learning targets are
specific to Choral Music. Our hope is that the 2017 South Carolina Choral Music Standards will not only be a valuable resource for the
teacher as a facilitator, but also for the learner to be actively engaged in his or her educational goals.

Page 134

##Choral Music Standards

Intermediate Choral Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new ideas.
Anchor Standard 1:I can arrange and compose music.
Intermediate
Low
Benchmark
CM.CR IL.1
I can arrange a short song for my voice.

Intermediate
Mid
Benchmark
CM.CR IM.1
I can arrange a short song for two voices,
using harmony.

Indicator
CM.CR IL.1.1
I can create a simple tune (monophonic
melody) without accompaniment, within
specified guidelines.
Sample Learning Targets

Indicator
CM.CR IM.1.1
I can develop a simple tune with
accompanying parts (homophonic work).




I can create an eight measure melody
using pitches from the pentatonic
scale.
I can create an eight measure melody
using the pitches do, re, mi, fa, so.

Sample Learning Targets


I can create a melodic ostinato
pattern to accompany my melody.



I can create a rhythmic
accompaniment on a percussion
instrument to accompany my
melody.

I can...


Intermediate
High
Benchmark
CM.CR IH.1
I can arrange a short song for an ensemble,
demonstrating an understanding of voicing
and texture.
Indicator
CM.CR IH.1.1
I can combine different voices to create
various tone colors in my arrangement.


Sample Learning Targets



I can arrange a song for SA voices.



I can arrange a song for SAB voices.



I can...

I can...

Page 168

Indicator
CM.CR IL.1.2
I can create a melody using rhythms that are
appropriate for the time signature.

Indicator
CM.CR IM.1.2
I can develop an original arrangement of a
traditional canon or round.

Indicator
CM.CR IH.1.2
I can experiment with changes in tone color,
creating variety and contrast through a
combination of different voices.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can embellish my short song using
pitches from tonic triad and quarter
and eighth notes/rests.



I can create an original arrangement
of a canon for a small ensemble of 2
primary voice parts.



I can compare and contrast a SSA and
SAB arrangements of the same
song.



I can create a melody in duple meter.



I can write an original round that ends
on a tonic triad.





I can create a melody in triple meter.





I can compare and contrast SATB and
TTBB arrangements of the same
song.

I can...

I can...



I can...

Indicator
CM.CR.1 IL.3
I can develop a melody using pitches that are
appropriate for the tonality.

Indicator
CM.CR IM.1.3
I can develop my song using I, IV, and V
chord progressions.

Indicator
CM.CR IH.1.3
I can experiment with non-chord tones and
chord progressions.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can write an eight-measure melody
using pitches from a major scale.







I can provide an eight-measure
accompaniment using I and V chord
progressions.

I can write an eight-measure melody
using pitches from a minor scale.



I can write a melody over a I, IV, V
chord progression.

I can…



I can…



I can manipulate tones to create a
plagal cadence and a perfect
authentic cadence.



I can use notation software to
arrange phrases and basic chord
progressions.



I can…

Page 169

Anchor Standard 2: I can improvise music.
Intermediate
Low
Benchmark
CM.CR IL.2
I can improvise simple rhythmic patterns
within a given meter.
Indicator
CM.CR IL.2.1
I can improvise my own simple rhythmic
pattern using a neutral syllable.
Sample Learning Targets

Intermediate
Mid
Benchmark
CM.CR IM.2
I can improvise simple tonal patterns within
a given tonality.
Indicator
CM.CR IM.2.1
I can improvise my own simple tonal
patterns on a neutral syllable.
Sample Learning Targets



I can improvise a four-beat rhythm
pattern while patsching a steady
beat in 4/4 meter.



I can improvise a three-note tonic
triad pattern in a major key on a
neutral syllable.



I can improvise a six-beat rhythm
pattern on a neutral syllable while
patsching a steady beat in ¾ meter.



I can improvise a three-note
dominant triad pattern in a major
key on a neutral syllable.

I can...



I can improvise a three-note tonic
triad pattern in a minor key on a
neutral syllable.



I can...



Intermediate
High
Benchmark
CM.CR IH.2
I can improvise simple melodic phrases.
Indicator
CM.CR IH.2.1
I can identify chord changes to improvise a
short melody.
Sample Learning Targets


I can identify different chord
patterns on a staff line.



I can write a I, IV, V chord
progression using notation.



I can...

Page 170

Indicator
CM.CR IL.2.2
I can improvise my own simple rhythm
patterns using ta-ka-di-mi or a counting
system.
Sample Learning Targets

Indicator
CM.CR IM.2.2
I can improvise my own simple tonal
patterns using solfege.
Sample Learning Targets

Indicator
CM.CR IH.2.2
I can improvise simple melodic phrases that
correspond with chord progressions in an
unfamiliar song.
Sample Learning Targets



I can improvise a four-beat rhythm
pattern using ta-ka-di-mi while
patsching a steady beat in 4/4 meter.



I can improvise a three-note tonic
triad pattern in a major key on
solfege syllables.



I can improvise a melodic phrase
over a given chord progression in a
major tonality.



I can improvise a six-beat rhythm
pattern using ta-ka-di-mi while
patsching a steady beat in ¾ meter.



I can improvise a three-note
dominant triad pattern in a major
key on solfege syllables.



I can improvise a melodic phrase
over a given chord progression in a
minor tonality.



I can...



I can improvise a three-note tonic
triad pattern in a minor key on
solfege syllables.



I can use electronic musical tools to
mix or arrange music within a given
chord progression.



I can...



I can...

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.
Intermediate
Low
Benchmark
CM.P IL.3
I can produce a centered tone in a comfortable
tessitura.

Intermediate
Mid
Benchmark
CM.P IM.3
I can produce a centered tone in some
tessituras specific to my vocal range.

Intermediate
High
Benchmark
CM.P IH.3
I can produce a centered tone in most
tessituras specific to my vocal range.

Page 171

Indicator
CM.P IL.3.1
I can sing with a resonant, centered, and free
tone in harmony.


I can match pitch when I sing a
simple round.



I can sing a 2-part song with a
centered tone.





I can sing a 2-part song with blended
vowel formation between the voice
parts.

Indicator
CM.PIM.3.1 I can sing my assigned part in tune with
appropriate tone quality, resonance and
vocal timbre.
 I can sing ostinati, partner songs,
rounds and two-part music while
maintaining proper vowel
formation, and head voice.




I can…


I can sing my assigned part in a
cappella music in rehearsal and
performance settings.
I can sing ostinati, partner songs,
rounds and two-part music with
clear tone quality.







I can sing the alto part in a SSA
arrangement with clear tone quality,
breath control, and correct posture.
I can sing the tenor part in a TTB
arrangement with clear tone quality,
breath control, and correct posture.

I can sing two- and three-part songs with
centered tone quality, in tune, while
demonstrating dynamic changes.
 I can sing two- and three-part music,
maintaining correct intonation,
breath support, and vocal timbre.


I can demonstrate appropriate diction
by articulating clarity of consonants
and purity of vowels.



I can sing forte without shouting.



I can…

I can….

Indicator
CM.P IL.3.2
I can sing in tune my assigned part with clear
tone quality, using breath control and
correct posture.
Sample Learning Targets

Indicator
CM.P IM.3.1
Indicator
CM.P IM.3.2
I can sing with a centered tone and a steady
tempo.
Sample Learning Targets
 I can perform with a centered tone
while singing allegro.

Indicator
CM.P IH.3.2
I can sing two- and three-part songs with
centered tone quality, in tune, while
demonstrating articulation changes.
Sample Learning Targets
 I can sing staccato in a two part song
with a centered tone quality.



I can perform with a centered tone
while singing largo.



I can perform an accent in a three-part
song with a centered tone quality.



I can…



I can…

I can…
Page 172

Anchor Standard 4: I can perform with technical accuracy and expression.
Intermediate
Low
Benchmark
CM.P IL.4
I can sing expressively with appropriate
dynamics and phrasing.

Intermediate
Mid
Benchmark
CM.P IM.4
I can sing expressively with appropriate
dynamics, phrasing, and interpretation.

Indicator
CM.P IL.4.1
I can sing, observing a variety of dynamic
markings in songs.
 I can sing a crescendo and a
decrescendo notated in music.

Indicator
CM.P IM.4.1
I can interpret a conductor’s dynamic and
phrasing cues when singing.
 I can observe my conductor’s cue to
lift and breathe in a phrase of music.




I can sing a sforzando.
I can...

Indicator
CM.P IL.4.2
I can sing, observing phrasing suggestions
and markings in music.
Sample Learning Targets




I can observe my conductor’s cue to
observe a fermata.

Intermediate
High
Benchmark
CM.P IH.4
I can sing while interpreting my conductor’s
cues in order to perform with expression and
technical accuracy.
Indicator
CM.P IH.4.1
I can interpret a conductor’s gesture with
rhythmic and melodic precision.
 I can interpret my conductor’s cues
to sing with accurate rhythmic
division and subdivision of beat.


I can interpret my conductor’s
gesture to balance other voice parts
in my ensemble.



I can.

I can...

Indicator
CM.P IM.4.2
I can sing, observing phrasing markings and
breathing appropriately alone and in groups.
Sample Learning Targets

Indicator
CM.P IH.4.2
I can interpret a conductor’s dynamic,
articulation, and phrasing cues.
Sample Learning Targets



I can observe legato markings in
music.



I can observe a breath mark when
notated in my music.



I can observe my director’s cues to
sing with marcato.



I can observe a tenuto marking in
music.
I can...



I can stagger breathe in a long phrase
with other singers in my section.
I can…



I can sing phrases of irregular length
by following my conductor's cues.





Page 173

Anchor Standard 5: I can perform using music notation.
Intermediate
Low
Benchmark
CM.P IL.5
I can identify music notation symbols
representing an expanded set of tonal,
rhythmic, technical, and expressive ideas.

Intermediate
Mid
Benchmark
CM.P IM.5
I can perform at sight simple unfamiliar
musical works.

Intermediate
High
Benchmark
CM.P IH.5
I can use a system to fluently sight-read
moderately complex melodies in treble and
bass clefs.

Indicator
CM.P IL.5.1
I can identify sharps, flats, naturals, and
simple key signatures.

Indicator
CM.P IM.5.1
I can perform at sight simple unfamiliar
musical works with accurate pitches.

Indicator
CM.P IH.5.1
I can perform at sight moderately complex
unfamiliar musical works with accurate
pitches.
 I can read a three-part music score.



I can identify basic key signatures
and locate do on the staff.



I can identify accidentals in my
score.



I can...



I can read a two-part music score.



I can use solfege to read two-part
songs including skips of tonic triad.



I can...



I can use solfege to read three-part
music including skips of tonic and
dominant triads.



I can...

Page 174

Indicator
CM.P IL.5.2
I can sight-read stepwise tonic (do, re, mi, fa,
so) patterns and simple meter based (2/4/,
3/4/, 4/4) rhythmic patterns.
Sample Learning Targets

Indicator
CM.P IM.5.2
I can sight read using reading systems such
as ta-ka-di-mi, Gordon, count singing, and
neutral syllables to unfamiliar melodies with
tonic triad skips.
Sample Learning Targets



I can use numbers, rhythm
syllables, or count singing to sightread rhythms for my voice part.



I can use numbers, tonal syllables,
or count singing to sight-read
pitches for my voice part.



I can read tonal patterns including
tonic triad skips using solfege.

I can...



I can...





I can read rhythm patterns including
basic divided beat in my music using
reading systems (ta-ka-di-mi,
Gordon syllables etc.).

Indicator
CM.P IH.5.2
I can notate intermediate note values and
time signatures.

Sample Learning Targets



I can notate rhythm including
extension dots in music notation
software.
I can notate rhythm patterns using
compound meter in music notation
software.



I can...

Indicator
CM.P IL.5.3
I can identify advanced note values and time
signatures that represent syncopation and
smaller beat subdivisions in my music.

Indicator
CM.P IM.5.3
I can apply basic tempo markings in my
music.

Indicator
CM.P IH.5.3
I can apply intermediate tempo markings in
my music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify and count syncopated
rhythm patterns.



I can read a rhythmic passage and
apply the term adagio.



I can perform a rhythmic passage
and apply the term prestissimo.



I can identify and count sixteenth
note patterns.



I can sing a melodic phrase and apply
the term moderato.



I can perform a melodic phrase and
apply the term con brio.



I can...



I can...



I can...
Page 175

Indicator
CM.P IL.5.4
I can identify expressive markings in my
music.
Sample Learning Targets

Indicator
CM.P IM.5.4
I can apply expressive markings in my
music.
Sample Learning Targets

Indicator
CM.P IH.5.4
I can apply advanced expressive markings in
my music.
Sample Learning Targets



I can label dolce markings in my
music.



I can sing a melodic passage with
dolce expression.



I can sing a melodic passage with bel
canto expression.



I can label cantible markings in my
music.



I can sing cantible in a melodic
passage.



I can sing a melodic passage with
gracioso expression.



I can...



I can...



I can…

Artistic Processes: Responding - I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Intermediate
Low
Benchmark
CM.R IL.6
I can describe how the elements of music are
used to communicate ideas and evoke
emotional responses in others and myself.

Intermediate
Mid
Benchmark
CM.R IM.6
I can identify and explain how the elements
of music are used in a variety of genres to
determine my personal preferences.

Intermediate
High
Benchmark
CM.R IH.6
I can evaluate a performance and offer
constructive suggestions for improvement
using provided criteria.

Page 176

Indicator
CM.R IL.6.1
I can explain how music elements are used to
communicate ideas.

Sample Learning Targets

Indicator
CM.R IM.6.1
I can identify how the melody, harmony,
rhythm, timbre, texture, form, and
expressive elements are different in varying
genres of music.
Sample Learning Targets



I can explain why a composer chose
to use certain dynamic and tempo
markings in a piece of music.



I can describe how harmony is
different in jazz music as opposed to
hip-hop music.



I can explain why the Armed Forces
Medley is in 4/4 time.



I can describe how the use of the
elements of music can be used to
determine a specific genre.



I can explain why a sea chantey or
work song has a strong driving beat.



I can...



Indicator
CM.R IH.6.1
I can identify advanced musical symbols, key
signatures, and complex meter.

Sample Learning Targets


I can identify articulation marks,
ornaments.



I can identify key signatures,
accidentals, as they appear in music.
I can...



I can…
Indicator
CM.R IL.6.2

Indicator
CM.R IM.6.2

I can describe how the elements of music
affect the mood of a song.

I can describe common elements found in
various genres of music.

Sample Learning Targets

Sample Learning Targets



I can talk about the effect of a minor
melody on my emotional response to
a piece of music.



I can find a 12-bar blues progression
in a Rock and Roll song from the
1950s.



I can explain why a lullaby is sung
softly and slowly.



I can find examples of Improvisation
in bluegrass and rap music.



I can...



I can…

Indicator
CM.R IH.6.2
I can explain why advanced musical symbols,
key signatures, and complex meter are used
in music.
Sample Learning Targets


I can explain how tonality is used to
affect the mood of a song.



I can explain how meter and
modulation is used to affect the
mood of a song.



I can…
Page 177

Indicator
CM.R IL.6.3

Indicator
CM.R IM.6.3

Indicator
CM.R IH.6.3

I can use the elements of music to describe
my emotional response to a music
performance.

I can use the elements of music to describe
why I like particular genres.

I can use the elements of music to offer
suggestions for improvement.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets





I can discuss vocal timbre and how it
affects the mood of a song.

I can critically listen to music
performances and use the elements of
music to describe my opinion.
I can…



I can critically listen to different
genres of music and use the elements
of music to explain which genres are
my favorite.



I can compare and contrast two
genres of music.



I can…



I can listen to a recording of an
ensemble and critique vocal
technique.



I can use a recording device to
complete a self-critique.
I can…

Anchor Standard 7: I can evaluate music.
Intermediate
Low
Benchmark
CM.R IL.7
I can describe the quality of music
performances using provided criteria.

Intermediate
Mid
Benchmark
CM.R IM.7
I can describe my evaluation of a performance
to others.

Intermediate
High
Benchmark
CM.  IH.7
I can describe the quality of my performances
and my compositions.

Page 178

Indicator
CM.R IL.7.1
I can define all the elements of music.
Sample Learning Targets


I can name all of the elements of
music.



I can provide specific details about
the different elements of music.



Indicator
CM.R IM.7.1
I can identify specific criteria I use when I
critique others’ performances.
Sample Learning Targets


I can identify criteria to create a
rubric for my performance and the
performance of others.



I can discuss with others which
specific characteristics of their
performance needed improvement.

I can...


Indicator
CM.P IL.7.2
I can describe the characteristics of a quality
performance using musical terms.
Sample Learning Targets


I can listen to music performances to
determine if an ensemble maintained
good intonation.



I can listen to music performances to
determine if an ensemble maintained
good balance and blend.



I can...

Indicator
CM.R IH.7.1
I can compare my performance to the
performance of others.
Sample Learning Targets


I can compare my own performance
to the performance of others using
rubric-based feedback from an
adjudicator at a county, region, or
state festival.



I can use a recording provided by an
adjudicator at a county, region, or
state festival to compare my
performance to the performance of
others.



I can...

I can...

Indicator
CM.P IL.7.2
I can use the elements of music to evaluate a
composition.
Sample Learning Targets

Indicator
CM.P IH.7.2
I can use the elements of music to evaluate
my performance or the performance of others.
Sample Learning Targets



I can select criteria for a rubric to
assess a choral composition.



I can use a rubric to evaluate my own
performance.



I can use the elements of music to
describe the anticipated level of
difficulty of a choral composition.



I can use a rubric to evaluate the
performance of others and provide
feedback. I can...



I can…...

Page 179

Artistic Processes: Connecting - I can connect musical ideas and works to personal experience, careers,
culture, history, and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Intermediate
Low
Benchmark
CM.C IL.8
I can examine relationships among musical
selections from multiple cultures and/or
historical time periods.
Indicator
CM.P IL.8.1
I can identify similarities and differences in
music from multiple cultures and time
periods.
Sample Learning Targets






I can compare and contrast two songs
from different time periods, such as
tight harmonies in “Boogie Woogie
Bugle Boy,” by the Andrews Sisters
in the 1940s, to single melodic line in
“Big Yellow Taxi,” by Joni Mitchell
in the 1970s.
I can compare and contrast a classical
piece from an oratorio with a modern
octavo written by a contemporary
composer.
I can use music terms to compare the
stylistic differences in a classical
choir and modern choir.

Intermediate
Mid
Benchmark
CM.C IM.8
I can research the role of music within a
specific culture or historical time period and
present what I discovered.
Indicator
CM.P IM.8.1
I can research a specific culture/ time period
and perform a song from that culture/time
period.
Sample Learning Targets


I can research a composer in music
history and perform his/her work in
the style pertaining to that time
period.



I can research the role of the “Star
Spangled Banner” in various settings
and time periods since it was adopted
as our National Anthem in 1931.



I can read an informational text and
perform a song from that time period.



I can…

Intermediate
High
Benchmark
CM.C IH.8
I can modify a musical work using
characteristics from a culture or time period.
Indicator
CM.P IH.8.1
I can change a musical work using the
elements of music from a culture or time
period.
Sample Learning Targets


I can change the interpretation of a
Bach Chorale by adding elements of
jazz music.



I can change the interpretation of a
spiritual by adding elements of a
classical style.



I can...

Page 180

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Intermediate
Low
Benchmark
CM.C IL.9
I can explore a range of skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Intermediate
Mid
Benchmark
CM.C IM.9
I can recognize specific skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Indicator
CM.C IL.9.1
I can apply music concepts to other arts
disciplines and content areas.
Sample Learning Targets


I can compare and contrast the
elements of music to the elements
and principles of art.



I can perform a choral piece based on
a piece of creative writing, such as
“Inscription of Hope,” and then
change the dynamics of the piece..



Indicator
CM.C IM.9.1
I can examine the relationship between music
and specific content from another arts
discipline and content area.
Sample Learning Targets


I can sing two- and three-part
harmony, and create ways to display
layering or thicker texture in arts
classes (for example, making a quilt
square for a class project).



I can examine the role of music in
theatre.



I can examine the role of music in
dance.



I can...

I can...

Intermediate
High
Benchmark
CM.C IH.9
I can analyze the tools, concepts, and
materials used among arts disciplines, other
content areas and how they are used in music
careers.
Indicator
CM.C IH.9.1
I can apply concepts from other arts
disciplines and content areas to my music.
Sample Learning Targets


I can use the elements of theatre to
add expressive qualities to my
performance of a Broadway song.
I can use the elements of dance to
choreograph a movement sequence
for a choral work.



I can...

Page 181

Indicator
CM.C IL.9.2
I can demonstrate and describe the skills
needed for careers in music.

Indicator
CM.C IM.9.2
I can examine the educational requirements
needed for a variety of careers in music.

Indicator
CM.C IH.9.2
I can research skills needed for various music
careers.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can research skills needed for a
variety of music careers.



I can investigate and report about
music careers in SC.



I can name careers in music and
research the requirements for the
chosen career.



I can interview a person in an arts
field to discover the music skills
needed for that job.



I can examine the requirements of a
music producer.



I can research topics about careers in
music that interest me.



I can identify college degree
programs for music therapy.



I can…



I can…

I can...

Page 182

Advanced Choral Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new ideas.
Anchor Standard 1: I can arrange and compose music.
Advanced
Low
Benchmark
CM.CR AL.1
I can describe how I use melody, rhythm,
and harmony to compose or arrange a work
for a specific purpose.
Indicator
CM.CR AL.1.1
I can arrange melodic themes for specific
purposes, using arrangement and
compositional techniques.
Sample Learning Targets


I can use diminution, retrograde,
fragmentation, augmentation,
permutation to arrange a
composition.



I can arrange a melody for a specific
holiday.



I can…

Advanced
Mid
Benchmark
CM.CR AM.1
I can collaborate with others to compose or
arrange a musical work for a specific
purpose.
Indicator
CM.CR AM.1.1
I can sing in ensembles, working with others
to develop ideas as we compose or arrange a
composition.
Sample Learning Targets


I can work with others to create a
multi-movement work.



I can use technology to collaborate
with team members while
composing/arranging a composition



Advanced
High
Benchmark
CM.CR AH.1
I can compose short, original musical ideas
and works using all the elements of music
for a specific purpose.
Indicator
CM.CR AH.1.1
I can create musical ideas and works using
chord progressions and modulations.
Sample Learning Targets


I can compose a four-part choral
piece in the style of a Bach Chorale
utilizing appropriate cadences and
chord progressions.



I compose in sonata form
transforming a piece through
exposition, development and
recapitulation.



I can...

I can...

Page 183

Indicator
CM.CR AL.1.2
I can use compositional techniques to
compose works in a given musical form.

Indicator
CM.CR AM.1.2
I can work with others to analyze
arrangements and original compositions for
improvements.

Indicator
CM.CR AH.1.2
I can use characteristic forms of music to
create a choral composition for a specific
purpose.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can explain common music forms,
(for example: verse-refrain, AB,
ABA,).



I can use the circle of fifths to
compose a work in complementary
keys.







I can...


Indicator
CM.CR AL.1.3
I can compose short compositions in major
and minor keys.
Sample Learning Targets

I can use a recording to analyze the
arrangements and original
compositions of my peers for
improvement.
I can use multiple recordings of my
composition, in order to compare and
contrast qualities of the performance
areas that need improvement.



I can use verse and refrain form to
compose a traditional hymn.



I can use ABA form to compose a
popular-contemporary style song.



I can...

I can....

Indicator
CM.CR AM.1.3
I can compose an original composition in
four-part chorale style.
Sample Learning Targets

Indicator
CM.CR.1 AH.3
I can compose a choral composition with a
variety of expressive devices.
Sample Learning Targets



I can use a poem to create a sixteenmeasure composition in a major key.



I can write an SATB arrangement of a
traditional folk song.



I can use tempo to enhance
expression in a composition.



I can write a sixteen-measure
composition in a minor key to
accompany a dance.



I can use traditional voice leading
composition techniques.



I can use dynamics to enhance
expression in a composition.





I can …



I can…

I can use articulation to enhance
expression in a composition.



I can…
Page 184

Anchor Standard 2: I can improvise music.
Advanced
Low
Benchmark
CM.CR AL.2
I can perform a brief improvisation given a
chord progression and meter.

Advanced
Mid
Benchmark
CM.CR AM.2
I can perform an improvisation given a
motive, chord progression and meter.

Advanced
High
Benchmark
CM.CR AH.2
I can perform an extended improvisation
with freedom and expression featuring
motivic development within a given tonality,
meter, and style.

Indicator
CM.CR AL.2.1
I can improvise a short passage using only a
chord progression.

Indicator
CM.CR AM.2.1
I can perform an improvisation on a given
motive.

Indicator
CM.CR AM.2.1
I can improvise an extended unaccompanied
solo within a given tonality, meter, and
style.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can use passing tones, and other
non-harmonic tones to improvise a
short passage over a given chord
progression in major tonality.



I can use rhythmic variation,
including augmentation and
diminution, to improvise on a given
motive.




I can improvise a cadenza
I can incorporate a composer’s
melodic and rhythmic motives into
my performance.



I can use passing tones, and other
non-harmonic tones to improvise a
short passage over a given chord
progression in minor tonality.



I can use passing tones and use nonharmonic tones to improvise on a
given motive.



I can...





I can...

I can...

Page 185

Indicator
CM.CR AL.2.2
I can improvise a short passage in an
established meter.

Indicator
CM.CR AM.2.2
I can improvise an extended passage using
only a chord progression.

Indicator
CM.CR AH.2.2
I can improvise freely within a given
tonality, meter, and style, responding to aural
cues from other members of an ensemble.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can use rhythmic variations to
improvise a solo in 6/8 meter.



I can use melodic variations to
improvise a solo in cut-time.



I can…



I can use rhythmic variations,
passing tones, and other onharmonic tones to improvise an
extended passage given a chord
progression.



I can improvise an extended melody
over a repeated chord progression.



I can use rhythmic variations to
improvise a solo in duple (such as 2/4
or 4/4) and triple (such as 6/8 meter).



I can...



I can improvise a solo incorporating
and responding to musical
gestures/ideas as performed by
another member of my ensemble.



I can use electronic musical tools to
mix or arrange or improvise music
within a given chord progression.



I can...

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.
Advanced
Low
Benchmark
CM.P AL.3
I can produce a well-developed tone in all
tessituras specific to my vocal range.

Advanced
Mid
Benchmark
CM.P AM.3
I can consistently produce a well-developed,
vibrant tone across the entire range of my
voice.

Advanced
High
Benchmark
CM.P AH.3
I can adjust tone color/timbre in response to
stylistic demands and the musical needs of an
ensemble.
Page 186

Indicator
CM.P AL.3.1
I can sing with a well-developed tone, some
three- and four-part songs, demonstrating
balance and intonation, by adjusting my
voice to conductor’s cues.
Sample Learning Targets

Indicator
CM.P AM.3.1
I can sing alone and within a three- and fourpart ensemble, singing with well-developed
tone quality while maintaining balance and
intonation.
Sample Learning Targets



I can sing “Hallelujah” by Leonard
Cohen maintaining correct balance
and intonation, making adjustments
as needed.



I can sing in an ensemble “City
Called Heaven” by Moses Hogan
giving expression while maintaining
sensitivity to the vocal solo line.



I can sing in a quartet while
maintaining balance and intonation
within the ensemble.



I can prepare a vocal solo for a
festival, scholarship audition or
competitive event.



I can...



I can...

Indicator
CM.P AL.3.2
I can sing with a well-developed tone,
incorporating all musical symbols, tempo
and expressive indications.
Sample Learning Targets




I can sing with a well-developed tone,
while singing “Praise His Holy
Name” by Keith Hampton
maintaining technique, dynamics,
and tempo, adjusting as needed.
I can perform Mozart’s Requiem with
an orchestra while observing the
expressive indications of the Classical
style. I can...

Indicator
CM.P AH.3.1
I can manipulate the tone quality of my voice
to reflect the stylistic demands of a piece of
music.
Sample Learning Targets





I can sing “O Fortuna” from Orff’s
“Carmina Burana” making
adjustments in tone quality as
needed.
I can sing “Sleep” by Eric Whitacre
and demonstrate straight, pure tone
singing.
I can...

Indicator
CM.P AM.3.2
I can sing with well-developed tone quality
and increased vocal technique.
Sample Learning Targets


I can sing in an ensemble “Elijah
Rock” arranged by Emerson with
well-developed tone, making
adjustments as needed.



I can adjust my tone quality to match
the stylistic demands of an aria or
jazz ballad.



Indicator
CM.P AH.3.2
I can sing in a variety of languages with welldeveloped tone quality, making needed
adjustments in vocal technique.
Sample Learning Targets


I can sing “Esto les Digo” by Kinley
Lange using correct vocal technique
with and without accompaniment.



I can perform a German art song.



I can...

I can...
Page 187

Anchor Standard 4: I can perform with technical accuracy and expression.
Advanced
Low
Benchmark
CM.P AL.4
I can sing with increased fluency and
expression a varied repertoire/genre of
choral music.
Indicator
CM.P AL.4.1
I can sing with rhythmic and melodic
precision music from diverse genres.
Sample Learning Targets


I can apply swing style to a piece of
jazz music.



I can sing polyphonic entrances in
music with accuracy.



Advanced
Mid
Benchmark
CM.P AM.4
I can sing with increased fluency and
expression in small and large ensembles a
varied repertoire/genre of choral music.
Indicator
CM.P AM.4.1
I can interpret a conductor’s gesture in a
varied repertoire of music.
Sample Learning Targets


I can interpret the phrasing gesture
of my conductor in a traditional
madrigal.



I can...


Advanced
High
Benchmark
CM.P AH.4
I can sing with increased fluency and
expression from memory varied
repertoire/genres of choral music.
Indicator
CM.P AH.4.1
I can enhance the expressive quality of my
performance through singing from memory.
Sample Learning Targets


I can interpret the gestures of my
conductor while performing a South
African Traditional Folk song from
memory as an ensemble singer.

I can interpret the dynamic gesture
of my conductor in sforzando at the
end of a spiritual.



I can perform a recitative from the
Classical period.

I can…



I can...

Page 188

Indicator
CM.P AL.4.2
I can sing observing dynamics, articulation,
phrasing, the style of the music.
Sample Learning Targets

Indicator
CM.P AM.4.2
I can interpret a conductor’s gestures
appropriate to the genre.
Sample Learning Targets

Indicator
CM.P AH.4.2
I can sing a cappella vocal selections from
memory.
Sample Learning Targets



I can perform a Classical piece of
music with appropriate dynamics,
articulations and phrases.



I can interpret the gestures of my
conductor that are specific to jazz
music as an ensemble singer.



I can memorize the technical demands
of an a Capella piece of music as an
ensemble singer.



I can perform a Baroque piece of
music with appropriate dynamics,
articulation and phrases.



I can interpret the gestures of my
conductor to balance my voice part
in a tone cluster of pitches.



I can use a recording device to assess
myself singing an a Capella selection
from memory.



I can...



I can…



I can...

Anchor Standard 5: I can perform using music notation.
Advanced
Low
Benchmark
CM.P AL.5
I can perform at sight complex unfamiliar
musical works with accuracy.

Advanced
Mid
Benchmark
CM.P AM.5
I can perform at sight complex unfamiliar
musical works with accuracy and appropriate
expression/interpretation.

Advanced
High
Benchmark
CM.P AH.5
I can perform at sight complex unfamiliar
musical works with accuracy, appropriate
expression/interpretation, and fluency.

Page 189

Indicator
CM.P AL.5.1
I can perform at sight complex unfamiliar
musical works with accurate pitches.

Indicator
CM.P AM.5.1
I can perform at sight complex unfamiliar
musical works with correction articulation.

Indicator
CM.P AH.5.1
I can perform at sight complex unfamiliar
works with fluency.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can sight-sing an assigned part of
the Schubert Mass in G with accurate
pitches and rhythms.



I can sight-sing an assigned part of
the Rutter Gloria with accurate
pitches and rhythms.







I can perform my voice part in a
SSAATTBB piece of choral music.



I can independently perform my part
in the Faure Requiem with accuracy.



I can perform a complex line of vocal
percussion.



I can follow the direction of the stems
to determine my vocal line in a closed
score.



I can...



I can...

I can...

Indicator
CM.P AL.5.2
I can sight read using multiple reading
systems (ta-ka-di-mi and Gordon, count
singing, neutral syllables) in music.
Sample Learning Targets




I can read rhythmic patterns
including subdivision of the beat in
music using reading systems
(takadimi, Gordon syllables etc.).

Indicator
CM.P AM.5.2
I can identify advanced note values and time
signatures that represent smaller beat
subdivisions in my music.
Sample Learning Targets


I can perform with accuracy
rhythmic patterns including
compound meter,



I can perform with accuracy
subdivision of beat in music using
reading systems.



I can...

I can read rhythmic patterns including
borrowed division of beat in music
using reading systems.

Indicator
CM.P AH.5.2
I can notate advanced values and time
signatures that represent syncopation and
smaller beat subdivisions in my music.
Sample Learning Targets



I can notate advanced rhythmic and
tonal patterns using music notation
software.
I can notate syncopation and smaller
beat subdivisions using music
notation software.



I can...

I can...

Page 190

Indicator
CM.P AL.5.3 I can identify the use of advanced tempo
markings in my music.
Sample Learning Targets

Indicator
CM.PAM.5.3 I can analyze the use of advanced tempo
markings in my music.
Sample Learning Targets

Indicator
CM P AH.5.3
I can justify the use of advanced tempo
markings in my music.
Sample Learning Targets



I can identify a composer's selection
of a specific tempo marking in a
piece of music.



I can analyze composer's selection of
a specific tempo marking in a piece
of music.



I can justify composer's selection of a
specific tempo marking in a piece of
music.



I can define advanced tempo
markings in a choral work.





I can justify the use of advanced
tempo markings in a choral work.



I can analyze advanced tempo
markings to apply to my performance
of a choral work.

I can...



I can...



I can...

Indicator
CM.P AL.5.4
I can identify technical, expressive, and
formal markings in my music.
Sample Learning Targets


I can identify “Over the Rainbow” as
AABA form and the effect on
performance expression.



I can identify the need for
embellishments in the return of the A
section of a Da Capo aria.



I can...

Indicator
CM.P AM.5.4
I can analyze the technical, expressive, and
formal markings in my music.
Sample Learning Targets


Indicator
CM.P AH.5.4
I can justify the technical, expressive, and
formal markings in my music.
Sample Learning Targets


I can perform the Faure Requiem with
appropriate performance expression.



I can explain the use of fugal form in
the Bach Magnificat and the effect on
performance expression
I can apply embellishments in the
return of the A section of a Da Capo
aria.



I can justify and perform
embellishments in the return of the A
section of a Da Capo aria.



I can…



I can...

Page 191

Artistic Processes: Responding - I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Advanced
Low
Benchmark
CM.R AL.6
I can analyze a composition or performance
and offer constructive suggestions for
improvement using provided criteria.
Indicator
CM.R AL.6.1
I can identify forms used in varying cultures
and historical periods.
Sample Learning Targets


I can explain the components of
sonata form.






Advanced
Mid
Benchmark
CM.R AM.6
I can analyze and critique compositions and
performances from a variety of genres,
cultures and time periods using personally
developed criteria.
Indicator
CM.R AM.6.1

Advanced
High
Benchmark
CM.R AH.6
I can justify my criteria for evaluating music
works and performances based on personal
and collaborative research.

I can describe characteristics of a variety of
musical forms.
Sample Learning Targets

I can justify my interpretation of a musical
work based on the elements of music.
Sample Learning Targets



I can explain the characteristics of
musical forms and how they are
similar and different.

I can describe how sonata form
differs in symphonic works versus
instrumental works.



I can describe how a particular form
appears in different genres of music.

I can describe how various forms
have evolved over time.



I can compare and contrast
characteristics of a variety of musical
forms and describe the way these
forms appear in compositions from
varying genres and styles.

I can...



I can...

Indicator
CM.R AH.6.1



I can identify harmonic structure of
the music I hear and perform.



I can describe how harmonic
structure changes the mood of a piece
of music.



I can identify specific performance
decisions of different performers.



I can describe how performance
decisions highlight the form and
harmonic structure set forth by the
composer.



I can...
Page 192

Indicator
CM.R AL.6.2
I can describe stylistic qualities of music from
different cultures and time periods.
Sample Learning Targets


I can listen to music from different
time periods and describe the
differences in their styles.



I can identify an instrumental,
expressive, and tonal quality that
makes music from a specific culture
unique.
I can list qualities of music from
various historical periods.




I can identify key signature changes and
modulations in relation to form.
Sample Learning Targets


I can explain how modulations affect
harmonic structure.



I can describe how a modulation
bridges between sections in sonata
form.



I can explain how modulations and
tonality unify a musical work.



I can...

Indicator
CM.R AH.6.2
I can justify the performance decisions in a
variety of musical works.
Sample Learning Targets



I can justify my performance
decisions based on my analysis of the
elements of music and their use in
the appropriate historical period.
I can create a presentation based on
my analysis of the elements of music
and their use in a specific historical
period.



I can...

I can...

Indicator
CM.R AL.6.3
I can describe stylistic qualities of music from
different historical periods and cultures and
offer suggestions for improvement of my
performance.
Sample Learning Targets


I can describe stylistic qualities of
music from the Medieval time period.



I can describe stylistic qualities of
music from the Irish culture.



Indicator
CM.R AM.6.2

I can…

Indicator
CM.R AM.6.3
I can describe stylistic qualities of music from
different historical periods and how it applies
to my instrument.
Sample Learning Targets

Indicator
CM.R AH.6.3
I can justify my evaluation of musical works
from different historical periods and cultures
based on my personal and collaborative
research.
Sample Learning Targets



I can apply stylistic qualities of music
from the Medieval time period to my
performance



I can justify performance choices in a
Medieval chant based on my personal
and collaborative research.



I can apply stylistic qualities of Irish
music to my performance.





I can…



I can justify my performance choices
in an Irish song based on my personal
and collaborative research.
I can...
Page 193

Anchor Standard 7: I can evaluate music.
Advanced
Low
Benchmark
CM.R AL.7
I can analyze performances and
compositions, offering suggestions for
improvement using provided criteria.
Indicator
CM.R AL.7.1

Advanced
Mid
Benchmark
CM.R AM.7
I can analyze and critique compositions and
performances using personally-developed
criteria.
Indicator
CM.R AM.7.1

Advanced
High
Benchmark
CM.R AH.7
I can justify my criteria for evaluating musical
works and performances based on personal
and collaborative research.
Indicator
CM.R AH.7.1

I can communicate feedback for personal
performances and compositions.
Sample Learning Targets

I can analyze personal compositions and
provide criteria for improvement.
Sample Learning Targets

I can explain criteria used for evaluation.
Sample Learning Targets



I can listen to my recorded
performance using a technological
device.



I can provide criteria for improvement
based on composition rules by
developing a self-assessment rubric.



I can develop criteria for the
evaluation of music works and
performances.



I can complete a rubric and evaluate
my own composition.







I can explain how the criteria for the
evaluation of music works and
performances were developed.

I can formulate constructive feedback
for my own personal performances
using the elements of music as the
basis for criteria of evaluation.

I can participate in a Skype or
FaceTime with members of my choir,
interacting with another choir and
critiquing each other’s performance
of a similar song.



I can...





I can…

I can...

Page 194

Indicator
CM.P AL.7.2
I can present my evaluation of a formal or
informal performance.
Sample Learning Targets

Indicator
CM.P AM.7.2
I can analyze performances and provide
criteria for improvement.
Sample Learning Targets



I can evaluate and present feedback
on the performance of others.





I can evaluate and present feedback
on the compositions of others.





I can formulate feedback rubric for
the performance of others using the
elements of music as the basis for
criteria of evaluation.





I can analyze personal performances
and those of others using a
technological device.
I can provide criteria for improvement
of my personal performances and
those of others by creating a rubric
based on the elements of music.

Indicator
CM.P AH.7.2
I can justify artistic decisions used in
compositions and performances.
Sample Learning Targets


I can determine what artistic elements
are used in a music composition.



I can discuss why the artistic elements
are important to that particular piece
of music.



I can...

I can...

I can...

Artistic Processes: Connecting - I can connect musical ideas and works to personal experience, careers,
culture, history, and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Advanced
Low
Benchmark
CM.C AL.8
I can examine contemporary musical works to
determine the influence of historical and
cultural traditions.

Advanced
Mid
Benchmark
CM.C AM.8
I can analyze a diverse repertoire of music
from a cultural or historical time period.

Advanced
High
Benchmark
CM.C AH.8
I can examine and perform music based on
historical and cultural contributions.

Page 195

Indicator
CM.C AL.8.1
I can explain specific cultural and historical
traditions and infuse these ideas into my
music.
Sample Learning Targets

Indicator
CM.C AM.8.1
I can select musical elements in
contemporary music that reflect cultural and
historical influences.
Sample Learning Targets



I can recognize historical and cultural
influences in George Frederic
Handel’s classical music.



I can research the Classical elements
of music found in John Rutter’s “For
the Beauty of the Earth.”



I can scat several measures of a jazz
piece and discuss why I made specific
choices in my improvisation.





I can listen for specific music
elements in a choral work and decide
which composer and/or time period
the piece represents.

I can…



I can…

Indicator
CM.C AH.8.1
I can use historical and cultural contributions
to justify my musical choices.
Sample Learning Targets


I can justify why a particular octavo
based on an American folk song
(such as “Shenandoah”) is often
chosen for All-State competition
(using musical terms such as rhythm,
melody, dynamics, form, tempo,
etc.)



I can justify the historical and cultural
contributions of the American
Spiritual.



I can...

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Advanced
Low
Benchmark
CM.C AL.9
I can apply concepts among arts disciplines
and other content areas to choral music and
analyze how my interests and skills will
prepare me for a career.

Advanced
Mid
Benchmark
CM.C AM.9
I can explain how economic conditions,
cultural values and location influence music
and the need for music related careers.

Advanced
High
Benchmark
CM.C AH.9
I can research societal political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a musician.

Page 196

Indicator
CM.C AL.9.1
I can explain ideas from other arts disciplines
and content areas through music.
Sample Learning Targets

Indicator
CM.C AM.9.1
. I can explain how my artistic choices are
influenced by cultural and social values.
Sample Learning Targets



I can analyze the lyrics of a choral
work and find a painting or drawing
which reflects the lyrics.



I can explain the connections between
the Vietnam War, political/social
unrest and the music of the era.



I can research the Flamenco and the
use of castanets in the dance and
performance of “El Vito” by Jodi
Jensen.



I can explain the connections between
the poetry of Langston Hughes, the
art of William H. Johnson and Jazz
music.

I can…





I can...

Indicator
CM.C AH.9.1
I can analyze complex ideas that influence my
artistic perspective and creative work.
Sample Learning Targets


I can analyze conflict and resolution
during different time periods of the
United States and the effect on
popular music.



I can analyze the effect of mass
genocide and the performance of the
piece “Prayer of the Children” by
Kurt Bestor.



I can…

Page 197

Indicator
CM.C AL.9.2

Indicator
CM.C AM.9.2

Indicator
CM.C AH.9.2

I can describe traditional and emerging
careers in music.

I can pursue opportunities that will lead me to
a career in music.

I can research my personal career choices in
the arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify the skills, training, and
education necessary to pursue a career
in music education.



I can conduct a survey comparing the
number of arts careers available in
rural, suburban and urban areas.



I can identify the skills, training and
education necessary to pursue a career
in music therapy.



I can job shadow a professional in the
music business industry.





I can...


I can identify college programs in
choral music and compare them to
other music degrees.



I can conduct a survey, (using my
digital device), comparing the number
of arts careers available in a particular
region of a country to arts careers
available in my community.



I can personally interview or
Skype/FaceTime local and national
leaders in the music industry.



I can…

I can…

Page 198

Choral Music Glossary
A Cappella Choral or vocal music performed without instrumental accompaniment.
AB Binary Form The most basic musical form in which two contrasting sections are present.
ABA Ternary Form A basic musical form consisting of three sections (A, B, and A), the third section being virtually identical to the
first. If it is exactly identical, the third section often is not written out, the performer simply being directed to repeat the first
section (usually marked da capo or D.C.), as in the da capo aria and minuet or scherzo with trio.
Accelerando Gradual increase of speed.
Accidentals A note of a pitch (or pitch class) that is not a member of the scale or mode indicated by the most recently applied key
signature, including the sharp (♯), flat (♭), and natural (♮) symbols.
Accompaniment The additional but subordinate music used to support a melodic line.
Adagio Slow, leisurely, or solemn tempo.
Allegro Bright, cheerful or lively tempo.
Alto Second-highest vocal range.
Andante Moving along, flowing, at a walking pace.
Aria A long, accompanied song for a solo voice, typically one in an opera or oratorio.
Arrangement/Arrange Composition based on existing music (e.g., scoring for voices not used in the original piece, adding a
percussion part to the original.
Articulation The manner or style in which the notes in a piece of music are sung.
Augmentation A melody, theme, or motif is presented in longer note-values than were previously used.
Aural By ear; without reference to or memorization of written music.
Page 199

Baroque A genre of classical music of c. 1600-1750 which included composers such as Bach, Handel, and Vivaldi and has a heavy use
of counterpoint and polyphony and conveyed drama and elaborate ornamentation.
Bass Lowest vocal range.
Bass Clef A symbol located at the beginning of a staff to indicate the pitches of the notes places on the lines and spaces below middle
C.
Beat A main accent or rhythmic unit in music.
Bel Canto A lyrical style of operatic singing using a full rich broad tone and smooth phrasing.
Blend (Vowel Alignment) The combination of voices in group singing so that individual performers are indistinguishable.
Blues A kind of jazz that evolved from the music of African-Americans, especially work songs and spirituals, in the early 20th century.
Breath Support Efficient and appropriate use of the breath stream for phonation.
Cadence A sequence of notes or chords comprising the close of a musical phrase.
Cadenza A virtuoso solo passage inserted into a movement in a concerto or other work, typically near the end.
Call and Response The alternation of musical phrases between groups of musicians.
Canon/Round A composition for two or more voices in which one voice enters after another in exact imitation of the first.
Cantabile In a smooth, singing style.
Chant To recite musically.
Choir A group of singers who usually sing in parts with several voices on each part.
Chorale A musical composition (or part of one) consisting of or resembling a harmonized version of a simple, stately hymn tune.
Chord Three or more pitches sounded simultaneously or functioning as if sounded simultaneously.
Chord Progression (or harmonic progression)A series of musical chords.
Page 200

Circle of Fifths The relationship among the 12 tones of the chromatic scale, their corresponding key signatures, and the associated
major and minor keys.
Classical (1) Music written in the European tradition during a period lasting approximately from 1750 to 1830. (2) Homophonic
texture, or an obvious melody with accompaniment. Melodies that tend to be almost voice-like and singable, allowing
composers to actually replace singers as the focus of the music.
Con Brio Performed with vigor.
Closed Score A musical score in which two or more parts are put on the same staff.
Complementary Keys Keys sharing many common tones.
Compose To create a musical work or idea.
Composer A person who creates a musical work or idea.
Composition A musical work.
Compound Meter Beats are divided into three notes.
Concerto A musical composition for a solo instrument or instruments accompanied by an orchestra, especially one conceived on a
relatively large scale.
Contemporary Music that can be understood as belonging to the period that started in the mid-1970s to early 1990s, which includes
modernist, postmodern, neo-romantic, and pluralist music. However, the term may also be employed in a broader sense to refer
to all post-1945 musical forms.
Count-Singing A choral rehearsal technique that involves singing the correct pitches, but replacing the lyrics with each note's position
within a measure.
Crescendo A gradual increase in the loudness of a sound or section of music.
Cut Time A meter with two half-note beats per measure.
Decrescendo A gradual decrease in the loudness of a sound or section of music.
Page 201

Diaphragmatic Breath Abdominal breathing, belly breathing or deep breathing; breathing that is done by contracting the diaphragm, a
muscle located horizontally between the thoracic cavity and abdominal cavity. Air enters the lungs and the belly expands during
this type of breathing.
Diction Enunciation. The clarity with which words are spoken or sung.
Diminution A melody, theme, or motifs presented in shorter note-values than were previously used.
Dolce Played in a tender, adoring manner; to play sweetly with a light touch.
Duple Meter A primary division of 2 beats to the bar, usually indicated by 2 and multiples (simple) or 6 and multiples (compound) in
the upper figure of the time signature.
Dynamics The loudness or softness of music.
Eighth A note/rest having the time duration of one eighth of the time duration of a whole.
Elements of Music The fundamental characteristics that make up a piece of music: rhythm, dynamics, pitch, harmony, tone color,
texture, form.
Embellishments A group of notes or a single note added to a basic melody as ornamentation.
Ensemble A group of musicians that perform as a unit. 2-part-ensemble is divided into two parts, frequently SA.3-part-ensemble is
divided into three parts, frequently SSA or SAB4-part-ensemble is divided into four parts, frequently SATB
Expression The art of playing or singing music with emotional communication. The elements of music that comprise expression
include dynamic indications, such as forte or piano, phrasing, differing qualities of timbre and articulation, color, intensity,
energy and excitement.
Fermata A symbol of musical notation indicating that the note should be prolonged beyond its normal duration or note value would
indicate.
Folk Song A song originating among the people of a country or area, passed by oral tradition from one singer or generation to the next,
often existing in several versions, and marked generally by simple, modal melody and stanzaic, narrative verse.
Page 202

Four-part chorale style Music written for four voices (SATB).
Form The structure or organization of a musical phrase or composition.
Forte (f) Loud.
Fortissimo (ff) Very loud.
Fragmentation The use of fragments or the "division of a musical idea (gesture, motive, theme, etc.) into segments." It is used in tonal
and atonal music, and is a common method of localized development and closure.
Fugue (fugal form) A contrapuntal composition in which a short melody or phrase (the subject) is introduced by one part and
successively taken up by others and developed by interweaving the parts.
Genre A type or style of music; an established form of musical composition such as a ballad, concerto, folk music, lullaby, march and
spiritual.
Grazioso Played in a graceful, smooth, or elegant in style.
Half A note/rest that has half the duration of a whole.
Harmony/Harmonic (1) The pattern of intervals and chords in a composition. (2) The ways in which chords and intervals are related
to one another and the ways in which one interval or chord can be connected to another. Adjective form, harmonic.
Head Voice A clear, open tone that resonates in the head and not in the throat or chest.
Historical/Cultural Context Music containing characteristics popular of a particular time-period or geographical region.
Improvisation/Improvise The creation of music in the course of performance. Verb form, improvise.
Interpretation Decoding motivations behind musical structures and the ways in which listeners and performers understand musical
works and practices.
Interval The distance between two pitches.
Intonation The proper production of a musical tone so that it is played or sung in tune.
Page 203

Jazz A type of music of black American origin characterized by improvisation, syncopation, and usually a regular or forceful rhythm,
emerging at the beginning of the 20th century.
Key Signature The sharp, flat, or natural signs placed at the beginning of a staff indicating the tonality of the composition.
Kodaly Hand signals are used to show tonal relationships. The moveable “do” is practiced. The musical material emphasized is the
mother-tongue folksong. Uses rhythm syllable "Ta" for quarter notes and "Ti-Ti" for eighth notes.
Legato In a smooth, flowing manner, without breaks between notes.
Loop A repeating section of sound material. Short sections of material can be repeated to create ostinato patterns.
Major A mode based on a scale consisting of a series of whole steps except for half steps between the third & fourth and seventh &
eighth degrees.
Marcato Played with emphasis.
Mass A choral composition that sets the invariable portions of the Eucharistic liturgy to music.
Melodic A repetitive pattern that can be used with any scale.
Melodic Direction The quality of movement of a melody, including nearness or farness of successive pitches or notes in a melody.
Melody A succession of tones comprised of mode, rhythm, and pitches so arranged as to achieve musical shape, being perceived as a
unity by the mind.
Meter The way beats of music are grouped, often in sets of two or three.
Mezzo Forte (mf) Medium loud.
Mezzo Piano (mp) Medium soft.
Minor A scale having half steps between the second and third, fifth and sixth, and seventh and eighth degrees, with whole steps for the
other intervals.
Modulation Changing from one key (tonic, or tonal center) to another.
Page 204

Motive A short tune or musical figure that characterizes and unifies a composition. It can be of any length but is usually only a few
notes long. A motive can be a melodic, harmonic or rhythmic pattern that is easily recognizable throughout the composition.
Musical Idea Musical fragment or succession of notes.
Non-Traditional Notation A word, place, character, or object regarded as typifying or representing something.
Notation/Notate A system used for writing down music showing aspects of music tones such as the tones to be sounded (pitch), the
time in (dynamics) at which the tone should be played. Verb form, notate.
Note A symbol used to represent the duration of a sound and, when placed on a music staff, to also indicate the pitch of the sound.
Octavo A generic term for sheet music (typically in the form of a booklet) containing a short choral work.
OpenScore A musical choral or orchestral score in which each part has a staff to itself.
Oratorio A large-scale musical work for orchestra and voices, typically a narrative on a religious theme, performed without the use of
costumes, scenery, or action.
Ostinati- Short music patterns that are repeated persistently throughout a performance, composition, or a section of one. (Singular
form, ostinato.)
Partner Songs-Two or more different songs that are performed at the same time to create harmony.
Passage A musical idea that may or may not be complete or independent.
Patsch Patting either the left, right, or both thighs with the hands.
Pattern A repetitive sequence.
Pentatonic A scale made up of five tones (usually do, re, mi, so and la).
Percussion Family of instruments in which sound arises from the striking of materials with sticks, hammers, or the hands.
Permutation Any ordering of the elements of a set.
Phrase A division or section of a musical line, somewhat comparable to a clause or sentence in language.
Page 205

Phrasing (1) A short musical idea similar to a sentence in spoken language; also a style of performance that gives shape to the musical
phrases. (2) The grouping of consecutive melodic notes, both in their composition and performance.
Pianissimo (pp) Very soft.
Piano (p) Soft.
Pitch (1) The property of a musical tone that is determined by the frequency of the sound waves creating it. (2) The highness or
lowness of a tone.
Plagal Cadence A chord progression where the subdominant chord is followed by the tonic chord (IV-I).
Posture The position of the body for singing. Chin parallel to the floor, shoulders back and down with chest held high. Abdomen flat
and firm, held in an expandable position. Hands relaxed and still at the sides. Knees flexibly loose and never locked. Feet flat on
the floor and shoulder-width apart. Weight of the body should be balanced on both feet and body held slightly forward.
Prestissimo Very fast.
Quarter (1)A note/rest having the time duration of one fourth of the time duration of a whole. (2)An ensemble of four performers.
Range The scope of notes that a voice can produce.
Recapitulation A part of a movement (especially one in sonata form) in which themes from the exposition are restated.
Recitative Musical declamation of the kind usual in the narrative and dialogue parts of opera and oratorio, sung in the rhythm of
ordinary speech with many words on the same note.
Repeat Reiteration of a tone at the same pitch level.
Repertoire A selection of musical pieces that an ensemble or performer knows or is prepared to perform.
Rest A symbol standing for a measured break in the sound with a defined duration.
Retrograde Reverses the order of the motive's pitches: what was the first pitch becomes the last, and vice versa.
Rhythm The systematic arrangement of musical sounds, principally according to duration and periodic stress.
Page 206

Rhythmic A set of beats and rests that defines the tempo and pace of a musical piece.
Ritardando Gradual decrease of speed.
Scale A group of notes (or pitch-classes) arranged sequentially, rising or falling.
Score A written or printed representation of a musical work.
Section A complete, but not independent musical idea.
Sforzando With sudden emphasis.
Sixteenth A note having one sixteenth the time value of a whole note.
Skip/Leap Any interval larger than a whole tone or whole step.
Soft Palate The fleshy, flexible part toward the back of the roof of the mouth. Lifting the soft palate reduces nasality in singing and
produces a more open tone.
Solfege A system that uses distinct syllables to identify the various notes of a scale:do, re, mi, fa, so, la, ti, do.
Solo A single performer or a passage that is to be performed by a single performer.
Sonata A composition for an instrumental soloist, often with a piano accompaniment, typically in several movements with one or more
in sonata form.
Soprano Highest vocal range.
Spiritual A religious folk song of African-American origin.
Staccato A dot above the note indicating that the note thus marked should be shortened to half its written length, the second half
replaced with silence.
Step Dynamics Phrases or sections of music increase or decrease volume by steps in a piece of music (pp-p-mf-f).
Step An interval of a second.
Page 207

Style The composer’s manner of treating the various elements that make up a composition as well as the performer’s manner of
presenting the composition.
Syllables/Sight-Reading System A method of musical training involving both ear training and sight singing.
Syncopation To put stress on a normally unstressed beat.
Takadimi The beat is always called ta. Insimple meters, the division and subdivision are always ta-di and ta-ka-di-mi. Any note value
can be the beat, depending on thetime signature. In compound meters (wherein the beat is generally notated withdotted notes),
the division and subdivision are always ta-ki-da and ta-va-ki-di-da-ma.
Tempo (1) A steady succession of units of rhythm; the beat. (2) The speed at which a piece of music is performed or is written to be
performed.
Tenor A singing voice between baritone and alto, the highest of the ordinary adult male range.
Tenuto A note or chord held for its full time value or slightly more
Tessitura The general range of a melody or voice part; specifically, the part of the register in which most of the tones of a melody or
voice part lie.
Texture The number and relationship of musical lines in a composition.
Time (Meter) Signature Notation to specify how many beats (pulses) are to be contained in each bar and which note value is to be
given one beat.
Tonality The use of a central note, call the tonic, around which the other tonal material of a composition is built and to which the music
returns for a sense of rest and finality.
Tone Cluster A musical chord comprising at least three adjacent tones in a scale.
Tone Color/Timbre/Quality(1) The blend of overtones that distinguish a note played on a flute, for example, from the same note
played on a violin. (2) The distinctive tone quality of a particular musical instrument or voice. (3) the character of musical tones
with reference to their richness or perfection.
Page 208

Tonic Triad A chord of three notes, the lowest note being the tonic of the key, the middle note being the third tone of the key, and the
top note being the fifth tone of the key.
Traditional Notation Music written on one or more staves, using traditional note symbols and clefs to indicate pitch locations and
durations.
Transpose To reproduce in a different key, by raising or lowering in pitch.
Treble Clef A symbol located at the beginning of a staff to indicate the pitches of the notes placed on the lines and spaces above
middle C.
Trio An ensemble of three performers.
Triplets Three notes of equal length that are performed in the duration of two notes of equal length.
Triple Meter A primary division of 3 beats to the bar, usually indicated by 3 (simple) or 9 (compound) in the upper figure of the time
signature.
Unison Two or more musical parts sounding the same pitch or at an octave interval.
Variations Formal technique where material is repeated in an altered form. The changes may involve harmony, melody, counterpoint,
rhythm, timbre, orchestration, or any combination of these.
Verse and Refrain The verse section of the song is the section in which different sets of words are sung to the same repeated melody
and contrasts with a refrain, where the words and melody are both repeated.
Vocal Inflection Alteration in pitch or tone of the voice.
Vocalist A singer, typically one who regularly performs with a jazz or pop group.
Vocalize A vocal exercise that is sung without words, typically using different vowel sounds.
Vocal Score Music score of a vocal or choral composition written for orchestral accompaniment, such as an oratorio or cantata. In a
piano-vocal score, the vocal parts are written out in full, but the accompaniment is reduced and adapted for keyboard (usually
piano).
Page 209

Vocal Skills/Technique The abilities that allow a musician or group of musicians to perform with a refined degree of phrasing,
dynamics and style.
Vocal Sound Source Sound created by a voice.
Voicing The manner in which one distributes, or spaces, notes and chords among the various voice parts.
Whole Note The longest note/rest duration in music.

Page 210

References
Arizona Department of Education. (2006). Arizona Music Standards-2006 [PDF document]. Retrieved from
https://www.azed.gov/standards-practices/files/2011/09/music.pdf
Colorado Department of Education. (2009). Music Academic Standards. Retrieved from
http://www2.cde.state.co.us/scripts/allstandards/COStandards.asp?stid=5&glid=0&pgcid=60
Georgia Department of Education. (2009). Georgia Performance Standards Fine Arts – Music Education [PDF document]. Retrieved
from https://www.georgiastandards.org/standards/gps%20support%20docs/fine-arts-music-gps.pdf
Kelly, S., Williams, D., Oliverio, J., Zack, J., Greenberg, E., Millwater, D., . . . Gordon, M. (2014) RTT Performing Fine Arts
Assessment Project. Retrieved fromhttps://cfaefl.org/assessmentproject/InnerPage.aspx?ID=6
Mississippi Department of Education. (2003). Mississippi Visual and Performing Arts Framework. Retrieved from
http://www.mde.k12.ms.us/docs/curriculum-and-instructions-library/music.pdf?sfvrsn=2
Missoula County Public Schools. Advanced Chamber Ensemble 10, 11, 12 [PDF document]. Retrieved from
http://www.mcpsmt.org/cms/lib03/MT01001940/Centricity/domain/825/music/Advanced%20Chamber%20Ensemble.pdf
National Association of Music Education. (2014). Music Standards Ensemble [PDF document]. Retrieved from
http://www.nafme.org/wp-content/files/2014/11/2014-Music-Standards-Ensemble-Strand.pdf
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
O’Connor, K. (2016) A Practical Guide to Vocal Development. Retrieved from http://singwise.com/cgibin/main.pl?section=articles&doc=TipsForPracticingSinging
Overstreet, J., & Ware, M. (2009, Feb 12). Rehearsal Strategies for High School Choirs [PDF document]. Retrieved from
http://foundationformusiceducation.org/wp-content/uploads/2010/08/Rehearsal-Strategies-For-High-School-Choirs.pdf
Public Schools of North Carolina State Board of Education: (North Carolina Arts Education, 2010). Arts Education Instructional
Support Tools. Retrieved from http://ances.ncdpi.wikispaces.net/K-12+Instructional+Tools
Page 211

Public Schools of North Carolina State Board of Education. (n.d.). North Carolina Essential Standards [PDF document]. Retrieved
from http://www.dpi.state.nc.us/docs/acre/standards/new-standards/arts/music/9-12.pdf
Public Schools of North Carolina State Board of Education. (n.d.). North Carolina Essential Standards [PDF document]. Retrieved
from http://www.dpi.state.nc.us/docs/acre/standards/new-standards/arts/music/k-8.pdf
South Carolina American Choral Directors Association. (2016). Elementary, Middle, High School All State Choir. Retrieved from
http://www.scacda.com/
South Carolina Department of Education. (2010). South Carolina Academic Standards for General Music. Retrieved from
http://ed.sc.gov/scdoe/assets/file/agency/ccr/Standards-Learning/documents/AcademicStandards2010DraftGeneralMusic.pdf
South Carolina Department of Education. South Carolina Academic Standards for the Visual and Performing Arts [PDF document].
Retrieved from https://ed.sc.gov/scdoe/assets/file/agency/ccr/StandardsLearning/documents/AcademicStandardsforChoralMusic.pdf
South Carolina Department of Education. (2015). South Carolina College- and Career-Ready Standards for English Language Arts.
Retrieved from http://ed.sc.gov/scdoe/assets/file/programsservices/59/documents/ELA2015SCCCRStandards.pdf
South Carolina Music Educators Association. (2016). High School Choral Performance Assessment. Retrieved from
http://www.scmea.net/divisions/choral-division/hscpa/
State Education Agency Directors of Arts Education: (Music Technology Strand, 2014). National Core Arts Standards. Retrieved from
http://www.nationalartsstandards.org

Page 212

South Carolina
College- and Career-Ready Standards for
Design Proficiency

South Carolina Department of Education
Columbia, South Carolina
2017
Page 92

Design
Introduction
Design is all around us and permeates every aspect of our lives. From waking up and deciding what to wear to making choices about
our environment, purchases, and recreation, we interact with the work of designers. The design fields include, but are not limited to,
Communication Design, Environmental Design, Experiential Design, and Object Design.
Functionality and aesthetics are two concepts that determine how we use a design and what we see in a design. For example, the
science of a bridge (function) must also be aesthetically pleasing for its environment. The design process guides students to experience
this interface by proceeding through a sequence of steps to find solutions for a design challenge. These steps include the following:
defining the design challenge, conducting research, brainstorming solutions, constructing a prototype, presenting a design solution to a
sample target group, receiving feedback, reflecting on the feedback, and making improvements on the prototype.
Students are guided through the design process to make creative and considerate decisions concerning the interaction of function and
aesthetics toward constructing a well-crafted prototype. The process requires that students present their design solution/prototype,
explain their thought processes, and receive feedback from stakeholders. This feedback allows students to analyze and reflect upon
their work in order to make thoughtful revisions toward improvement.
The design standards are organized in steps that parallel the design process. Students move through the standards, as shaped by the
design process, by working independently and collaboratively with others in order to reach an aesthetically-effective and functional
outcome.
Students are immersed cognitively when involved in the design process. The use of skills such as communication, creativity, critical
thinking, and problem solving are truly embodied in their work. Teaching through design reaches diverse learners who are able to
approach design thinking from their own personal perspectives and abilities.
These design standards are written to be applicable across all content areas. Traditionally considered under visual arts, problem solving
through design thinking may be applied to their artistic work but, just as importantly, it also may be used for project work in other
disciplines. Effective practices will be employed in all student work as a result of studying the South Carolina College and Career
Ready Standards for Design Proficiency.
Page 93

Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
De.CR
NL.1
I can
recognize
design
questions.

Benchmark
De.CR
NM.1
I can
recognize
how design
questions are
used to solve
problems.

Benchmark
De.CR
NH.1
I can answer
design
challenge
questions.

Benchmark
De.CR
IL.1
I can work
with a team
to answer
design
challenge
questions.

Benchmark
De.CR
IM.1
I can work
with a team
from a given
list to
identify and
describe a
design
challenge to
develop.

Benchmark
De.CR
IH.1
I can work
with a team
from a given
list of design
challenges
and select
one to
describe.

Benchmark
De.CR
AL.1
I can work
with a team
to conceive
many design
challenge
possibilities
relating to a
certain topic.

Benchmark
De.CR
AM.1
I can work
with a team
to conceive
many design
challenge
possibilities.

Benchmark
De.CR
AH.1
I can work on
my own to
conceive
many design
challenge
possibilities.

Page 94

Indicator
De.CR
NL.1.1
I can answer
the design
challenge
questions
who, what,
and where, in
order to
define the
design
challenge.

Indicator
De.CR
NM.1.1
I can answer
the design
challenge
questions
who, what,
when, and
where in
order to
define the
design
challenge.

Indicator
De.CR
NH.1.1
I can answer
the design
challenge
questions
who, what,
when, where,
why, and
how in order
to define the
design
challenge.

Indicator
De.CR
IL.1.1
I can work
with a team
to answer the
design
challenge
questions
who, what,
when, where,
why, and
how to
define the
design
challenge.

Indicator
De.CR
IM.1.1
I can work
with a team
to select a
design
challenge
from a given
list using
criteria to
answer the
design
challenge
questions
and define
the challenge.

Indicator
De.CR
IH.1.1
I can work in
a team to
discuss
design
challenges
from a given
list and select
one to define
from answers
to the design
challenge
questions.

Indicator
De.CR
AL.1.1
I can work
with a team
using design
thinking
strategies to
list several
design
challenge
options about
a topic and
select one to
define.

Indicator
De.CR
AM.1.1
I can work
with a team
using design
thinking
strategies to
list many
design
challenge
possibilities
and prioritize
to select one
to define.

Indicator
De.CR
AH.1.1
I can use
design
thinking
strategies to
list many
design
challenge
possibilities
and prioritize
to select one
to define.

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Benchmark
De.CR
NL.2
I can
recognize
research
methods.

Benchmark
De.CR
NM.2
I can
recognize
how
research is
used to solve
a design
problem.

Benchmark
De.CR
NH.2
I can apply
research
methods.

Benchmark
De.CR
IL.2
I can work
with a team
to research
aspects of
the design
challenge.

Benchmark
De.CR
IM.2
I can work
with a team
to research
and describe
aspects of
the design
challenge.

Benchmark
De.CR
IH.2
I can work
with a team
to explain
why
researched
aspects of
the design
challenge are
needed.

Benchmark
De.CR
AL.2
I can work
with a team
to analyze the
aspects of
the design
challenge.

Benchmark
De.CR
AM.2
I can work
independentl
y or with a
team to
evaluate the
parts of the
design
challenge.

Benchmark
De.CR
AH.2
I can lead a
discussion to
evaluate the
parts of the
design
challenge.

Page 95

Indicator
De.CR
NL.2.1
I can use a
research
method to
investigate
the design
challenge.

Indicator
De.CR
NM.2.1
I can use
research
methods to
investigate
the design
challenge.

Indicator
De.CR
NH.2.1
I can use a
variety of
methods to
investigate
the design
challenge.

Indicator
De.CR
IL.2.1
I can work
with a team
to identify
necessary
information
for the
design
challenge.

Indicator
De.CR
IM.2.1
I can
communicate
my research
to the team.

Indicator
De.CR
IH.2.1
I can work
with a team
to prioritize
research
from the
individual
team
members.

Indicator
De.CR
AL.2.1
I can
examine my
research and
report the
connections
of that
information
with the
team.

Indicator
De.CR
AM.2.1
I can work
with a team
to determine
the
importance
of the
research
from the
team
members.

Indicator
De.CR
AH.2.1
I can guide
my team in
determining
the
importance
of the
research
from the
team
members.

Benchmark
De.CR
AM.3
I can work
independentl
y or with a
team to
evaluate the
usable design
solutions to
the challenge.

Benchmark
De.CR
AH.3
I can lead a
discussion to
evaluate the
usable design
solutions to
the challenge.

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Benchmark
De.CR
NL.3
I can
recognize
design
thinking.

Benchmark
De.CR
NM.3
I can
recognize
how design
thinking is
used to solve
a design
problem.

Benchmark
De.CR
NH.3
I can apply
design
thinking
strategies.

Benchmark
De.CR
IL.3
I can work
with a team
using design
thinking
strategies to
generate
ideas for
design
solutions to
the challenge.

Benchmark
De.CR
IM.3
I can work
with a team
using design
thinking
strategies to
generate
some usable
design
solutions to
the challenge.

Benchmark
De.CR
IH.3
I can work
with a team
using design
thinking
strategies to
generate
many usable
design
solutions to
the challenge.

Benchmark
De.CR
AL.3
I can work
with a team
to analyze
usable design
solutions to
the challenge.

Page 96

Indicator
De.CR
NL.3.1
I can use a
design
thinking
strategy to
list possible
design
solutions to
the challenge.

Indicator
De.CR
NM.3.1
I can use
more than
one design
thinking
strategy to
list possible
design
solutions to
the challenge.

Indicator
De.CR
NH.3.1
I can use a
variety of
design
thinking
strategies to
list possible
design
solutions to
the challenge.

Indicator
De.CR
IL.3.1
I can work
with a team
using a
variety of
design
thinking
strategies to
list possible
design
solutions
without
judgement.

Indicator
De.CR
IM.3.1
I can work
with a team
to turn ideas
into possible
design
solution
concepts.

Indicator
De.CR
IH.3.1
I can work
with a team
to determine
which design
solutions
effectively
meet the
challenge
criteria.

Indicator
De.CR
AL.3.1
I can
examine,
discuss, and
select
possible
design
solutions to
best address
the challenge.

Indicator
De.CR
AM.3.1
I can work
with a team
to develop
criteria to
determine the
value of the
usable design
solutions to
the challenge.

Indicator
De.CR
AH.3.1
I can guide
my team in
determining
the value of
the usable
design
solutions to
the challenge.

Benchmark
De.CR
IH.4
I can work
with a team
to create a
prototype to
solve a
design
challenge.

Benchmark
De.CR
AL.4
I can work
with a team
to create a
prototype
that solves
multiple
aspects of a
design
challenge.

Benchmark
De.CR
AM.4
I can work
with a team
to create a
prototype
that solves all
aspects of a
design
challenge
functionally
and
aesthetically.

Benchmark
De.CR
AH.4
I can use
sophisticated
materials,
techniques,
and processes
to create the
most viable
prototype.

Anchor Standard 4: I can create an original prototype.
Benchmark
De.CR
NL.4
I can
recognize a
prototype.

Benchmark
De.CR
NM.4
I can
recognize
how a
prototype is
used to solve
a design
challenge.

Benchmark
De.CR
NH.4
I can explore
materials,
techniques
and processes
to create a
prototype.

Benchmark
De.CR
IL.4
I can work
with a team
to make a
prototype
that
represents a
solution to a
design
challenge.

Benchmark
De.CR
IM.4
I can work
with a team
to make
multiple
prototypes
that represent
various
solutions to a
design
challenge.

Page 97

Indicator
De.CR
NL.4.1
I can explore
using
physical
models,
space
models,
interactions,
and
storytelling
as
prototypes.

Indicator
De.CR
NM.4.1
I can use
strategies to
create a twodimensional
drawing or a
threedimensional
model of a
design
solution.

Indicator
De.CR
NH.4.1
I can use
basic
materials and
techniques
to develop a
model of my
design ideas.

Indicator
De.CR
IL.4.1
I can work
with a team
to make a
prototype to
experience
the design
challenge
criteria.

Indicator
De.CR
IM.4.1
I can work
with a team
to make
prototypes
to experience
the design
challenge
criteria.

Indicator
De.CR
IH.4.1
I can work
with a team
to make a
prototype
that
addresses
functional
aspects and
aesthetics.

Indicator
De.CR
AL.4.1
I can work
with a team
to select
materials,
techniques,
and processes
to create a
prototype.

Indicator
De.CR
AM.4.1
I can work
with a team
to select and
apply the best
materials,
techniques,
and processes
to create a
prototype.

Indicator
De.CR
AH.4.1
I can select
and apply
professional
materials,
techniques,
and processes
to create a
prototype.

Benchmark
De.P
AL.5
I can work
with a team
to prepare
and deliver a
presentation
to a sample
target group.

Benchmark
De.P
AM.5
I can work
with a team
to develop a
wellprepared,
aesthetically
pleasing
presentation
for a sample
target group
that includes
community
business
leaders or
professionals
in the field.

Benchmark
De.P
AH.5
I can develop
a wellprepared,
aesthetically
pleasing
presentation
for a sample
target group
that includes
professionals
and business
leaders in my
community.

Artistic Processes: Presenting-I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Benchmark
De.P
NL.5
I can share
my design
with a small
group.

Benchmark
De.P
NM.5
I can identify
how a design
presentation
is used to
solve a
design
challenge.

Benchmark
De.P
NH.5
I can present
my design
solution to a
design
challenge.

Benchmark
De.P
IL.5
I can work
with a team
to present our
design
solution to a
challenge.

Benchmark
De.P
IM.5
I can work
with a team
to select an
approach to
present our
design
solution to a
challenge.

Benchmark
De.P
IH.5
I can work
with a team
to prepare
and deliver a
presentation
that has
defined
criteria.

Page 98

Indicator
De.P
NL.5.1
I can share
my
prototype
and answer
simple
questions
about the
design
solution.

Indicator
De.P
NM.5.1
I can explain
the design
challenge
and my
design
solution.

Indicator
De.P
NH.5.1
I can present
my design
solution to
the challenge
using a
visual.

Indicator
De.P
IL.5.1
I can work
with a team
to present our
design
solution to
the challenge
using one or
more visuals.

Indicator
De.P
IM.5.1
I can work
with a team
to select an
approach
using
technology
for the
design
solution
presentation.

Indicator
De.P
IH.5.1
I can work
with a team
to create a
presentation
that includes
specific
criteria and
delivers
required
information
concerning
the design
challenge
and design
solution.

Indicator
De.P
AL.5.1
I can work in
a team to
present our
design
solution to a
group of
possible
users/consum
ers for
feedback.

Indicator
De.P
AM.5.1
I can work in
a team to
present our
design
solution to a
sample target
group that
includes
community
business
leaders and
professionals
in a related
field for
feedback.

Indicator
De.P
AH.5.1
I can present
our design
solution to a
sample target
audience that
includes
professionals
and business
leaders in a
related field
for feedback.

Page 99

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Benchmark
De.R
NL.6

Benchmark
De.R
NM.6

Benchmark
De.R
NH.6

Benchmark
De.R
IL.6

I can
recognize
how
reflection is
necessary in
the design
process.

I can
recognize
that revision
is necessary
in the design
process.

I can
encourage
feedback to
my design
and the
designs of
others by
asking and
answering
questions.

I can reflect
on and
provide
feedback to a
design
solution.

Indicator
De.R
NL.6.1
I can identify
the strengths
of my design
and designs
of others.

Indicator
De.R
NM.6.1
I can identify
areas of my
design and
the designs
of others that
need
improvement
.

Indicator
De.R
NH.6.1
I can prepare
some
questions for
feedback to
help me
revise my
design.

Indicator
De.R
IL.6.1
I can work
with a team
to record
feedback and
summarize
design
solution
recommendat
ions.

Benchmark
De.R
IM.6

Benchmark
De.R
IH.6

Benchmark
De.R
AL.6

Benchmark
De.R
AM.6

Benchmark
De.R
AH.6

I can
interpret
feedback
from my
peers to
revise our
design
solution.

I can work
with a team
to analyze
and explain
the steps of
the design
solution
revision.

I can work
with a team
to retest our
revised
design
solution and
analyze the
results.

I can
facilitate the
repetition of
the design
process to
revise and
retest the
design
solution.

Indicator
De.R
IM.6.1
I can work
with a team
to list and
prioritize
feedback to
improve our
design
solution.

Indicator
De.R
IH.6.1
I can work
with a team
to plan and
develop the
steps to
improve our
design
solution.

Indicator
De.R
AL.6.1
I can work
with a team
to improve
the
functionality
of our design
solution and
record the
results of the
modifications

I can work
with a team
to explain
future
improvement
s and repeat
the design
process to
revise and
retest the
design
solution.
Indicator
De.R
AM.6.1
I can work
with a team
to repeat the
design
process as
necessary to
improve the
design
solution.

Indicator
De.R
AH.6.1
I can guide
and frame
questions to
facilitate the
design
process to
improve a
design
solution.

Page 100

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Benchmark
De.C
NL.7
I can
recognize
some
examples of
design found
in my home
and
community.

Benchmark
De.C
NM.7
I can
recognize
differences in
designs
found in my
home and
community.

Benchmark
De.C
NH.7
I can describe
differences in
designs from
various
cultures
throughout
history.

Benchmark
De.C
IL.7
I can identify
improvements
or changes in
designs found
in various
cultures and
time periods.

Indicator
De.C
NL.7.1
I can find and
name some
designs
(object
environment
al,
communicati
on, or
experiential)
around me.

Indicator
De.C
NM.7.1
I can name
some
different
design
materials and
methods of
construction.

Indicator
De.C
NH.7.1
I can
compare how
designs are
different in
various
cultures
throughout
history.

Indicator
De.C
IL.7.1
I can compare
design
similarities
and
differences
among
different
cultures and
time periods.

Benchmark
De.C
IM.7
I can describe
why
improvements or
changes were
made in
designs
found in
various
cultures and
time periods.
Indicator
De.C
IM.7.1
I can explain
the possible
reasons
improvement
s and/or
changes were
made in a
design
through
different
cultures and
time periods.

Benchmark
De.C
IH.7
I can
analyze a
variety of
design
works from
different
cultures and
time
periods.

Benchmark
De.C
AL.7
I can
examine past
design works
to determine
their
influence on
present
designs.

Benchmark
De.C
AM.7
I can work
with a team
to analyze the
influence of
past design
works on
present
design
challenges.

Benchmark
De.C
AH.7
I can evaluate
my design
solution to
determine the
effective use
of past
design
works.

Indicator
De.C
IH.7.1
I can
recognize
patterns in
design
choices and
make
connections
to the
developmen
t of design
through
different
cultures and
time
periods.

Indicator
De.C
AL.7.1
I can find and
compare how
choices from
a current
design reflect
influences of
past design
solutions.

Indicator
De.C
AM.7.1
I can work
with a team
to explain
how the
designer's
choices on
the current
design
challenge
reflect
influences of
design
solutions
from the past.

Indicator
De.C
AH.7.1
I can assess
my design
choices and
relate them to
past design
influences.

Page 101

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Benchmark
De.C
NL.8
I can explore
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
NM.8
I can
recognize
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
NH.8
I can apply
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
IL.8
I can explore
a range of
skills shared
among arts
disciplines,
other content
areas and
how they can
be applied in
a design
career.

Benchmark
De.C
IM.8
I can
recognize
specific skills
shared
among arts
disciplines,
other content
areas and
how they can
be applied in
a design
career.

Indicator
De.C
NL.8.1
I can connect
design with
objects in my
home and
school.

Indicator
De.C
NM.8.1
I can
recognize
that design
exists in all
arts
disciplines
and other
content areas.

Indicator
De.C
NH.8.1
I can use
design
concepts in
other subjects
in my school.

Indicator
De.C
IL.8.1
I can
investigate a
range of
skills used in
various
design
careers, arts
disciplines,
and content
areas.

Indicator
De.C
IM.8.1
I can name
design skills
used in
various arts
disciplines
and content
areas and
relate these
skills to a
career in
design.

Benchmark
De.C
IH.8
I can analyze
the tools,
concepts, and
materials
used among
arts
disciplines,
other content
areas and
how they are
used in a
design
career.
Indicator
De.C
IH.8.1
I can
investigate
tools,
concepts and
materials
used in other
arts
disciplines
and content
areas.

Benchmark
De.C
AL.8
I can apply
concepts
among arts
disciplines
and other
content areas
to design and
analyze how
my interests
and skills
will prepare
me for a
career.
Indicator
De.C
AL.8.1
I can use
concepts
found in
various arts
disciplines
and other
content areas
in a design
work.

Benchmark
De.C
AM.8
I can explain
how
economic
conditions,
cultural
values, and
location
influence
design and
the need for
design
related
careers.
Indicator
De.C
AM.8.1
I can describe
how
economic
conditions,
cultural
values, and
geographic
locations
affect design
and design
careers.

Benchmark
De.C
AH.8
I can research
societal,
political, and
cultural
issues as they
relate to other
arts and
content areas
and apply to
my role as a
designer.

Indicator
De.C
AH.8.1
I can
examine the
importance
of the work
of a designer
in issues that
relate to a
global
society.

Page 102

Indicator
De.C
NL.8.2
I can
recognize
that people
have careers
in design.

Indicator
De.C
NM.8.2
I can identify
design
businesses
and careers in
my
community.

Indicator
De.C
NH.8.2
I can identify
ways design
thinking is
used in other
careers or
vocations.

Page 103

Novice Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Novice
Low
Benchmark
De.CR
NL.1
I can recognize design questions.

Novice
High
Benchmark
De.CR
NH.1
I can answer design challenge questions.

Indicator
De.CR
NL.1.1
I can answer the design challenge questions
who, what, and where, in order to define the
design challenge.

Novice
Mid
Benchmark
De.CR
NM.1
I can recognize how design questions are
used to solve problems.
Indicator
De.CR
NM.1.1
I can answer the design challenge questions
who, what, when, and where in order to define
the design challenge.

Sample Learning Targets
 I can answer “who” the design
challenge impacts.

Sample Learning Targets
 I can answer “when” the design
challenge will occur.

Sample Learning Targets
 I can answer “why” the design
challenge is needed.



I can answer “what” the design
challenge is for.



I can answer “where” the design
challenge will be impacted.





I can use design questions to
recognize how to define a design
challenge.



I can…

Indicator
De.CR
NH.1.1
I can answer the design challenge questions
who, what, when, where, why, and how in
order to define the design challenge.



I can answer “how” the design
challenge will be implemented.



I can…

I can…

Page 104

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Novice
Low
Benchmark
De.CR
NL.2
I can recognize research methods.

Novice
High
Benchmark
De.CR
NH.2
I can apply research methods.

Indicator
De.CR
NL.2.1
I can use a research method to investigate the
design challenge.

Novice
Mid
Benchmark
De.CR
NM.2
I can recognize how research is used to solve
a design problem.
Indicator
De.CR
NM.2.1
I can use research methods to investigate the
design challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can observe (using the five senses)
the existing designs.



I can observe an object in use.




Indicator
De.CR
NH.2.1
I can use a variety of methods to investigate
the design challenge.



I can use more than one of the
following: observation, printed
materials, technology, and/or
interviewing.



I can use a variety of the following
research methods: observation,
printed materials, technology, and/or
interviewing.

I can see and feel the parts of a
design object.



I can use printed materials to learn
about an object.



I can…



I can interview others for research
information.

I can observe, sketch, or record
(photography, video) an object to
show what I’ve learned about the
design object.





I can…

I can interview individuals with
experience with an object to
determine possible aspects to
redesign.



I can…

Page 105

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Novice
Low
Benchmark
De.CR
NM.3
NL.3
I can recognize design thinking.

Novice
Mid
Benchmark
De.CR
I can recognize how design thinking is used
to solve a design problem.

Novice
High
Benchmark
De.CR
NH.3
I can apply design thinking strategies.

Indicator
De.CR
NL.3.1
I can use a design thinking strategy to list
possible design solutions to the challenge.

Indicator
De.CR
NM.3.1
I can use more than one design thinking
strategy to list possible design solutions to
the challenge.

Indicator
De.CR
NH.3.1
I can use a variety of design thinking
strategies to list possible design solutions to
the challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can name possible solutions.



I can stay on topic to randomly call
out ideas for possible design
solutions.



I can…



I can use more than one of the
following: list aloud, popcorn
brainstorming, passing
brainstorming to provide possible
solutions.



I can determine which solutions can
be used in the design challenge.



I can provide a visual or drawing to
explain my idea.



I can create questions rather than
ideas to inspire further thinking.



I can organize my ideas using mind
maps.



I can listen to others and participate in
one conversation at a time to provide
possible design solutions.



I can…



I can…

Page 106

Anchor Standard 4: I can create an original prototype.
Novice
Low
Benchmark
De.CR
NL.4
I can recognize a prototype.

Novice
Mid
Benchmark
De.CR
NM.4
I can recognize how a prototype is used to
solve a design challenge.

Novice
High
Benchmark
De.CR
NH.4
I can explore materials, techniques and
processes to create a prototype.

Indicator
De.CR
NL.4.1
I can explore using physical models, space
models, interactions, and storytelling as
prototypes.

Indicator
De.CR
NM.4.1
I can use strategies to create a twodimensional drawing or a three-dimensional
model of a design solution.

Indicator
De.CR
NH.4.1
I can use basic materials and techniques to
develop a model of my design ideas.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can role play to act out solutions to
a design challenge.



I can use clay or other materials to
create a model of a new cup design.



I can explore space models with
geometric forms in a given area.



I can draw a new logo design.



I can…



I can use my words to tell about my
design idea.



I can…



I can work with a team to explore and
select the most appropriate materials
to build/compose the prototype.



I can work with a team to explore and
select the most appropriate
techniques and processes to
build/compose the prototype.



I can…

Page 107

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Novice
Low
Benchmark
De.P
NL.5
I can share my design with a small group.

Novice
Mid
Benchmark
De.P
NM.5
I can identify how a design presentation is
used to solve a design challenge.

Novice
High
Benchmark
De.P
NH.5
I can present my design solution to a design
challenge.

Indicator
De.P
NL.5.1
I can share my prototype and answer simple
questions about the design solution.

Indicator
De.P
NM.5.1
I can explain the design challenge and my
design solution.

Indicator
De.P
NH.5.1
I can present my design solution to the
challenge using a visual.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can show my prototype to my
peers.



I can explain the “who, what, where”
of the design challenge.



I can draw my ideas to present my
design challenge solution.



I can answer questions about the
“what” of the design solution.



I can explain the “when, and how” of
the design challenge.





I can ...



I can create a presentation board to
help explain my design challenge
solution.

I can ...



I can...

Page 108

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Novice
Low
Benchmark
De.R
NL.6

Novice
Mid
Benchmark
De.R
NM.6

Novice
High
Benchmark
De.R
NH.6

I can recognize how reflection is necessary in
the design process.

I can recognize that revision is necessary in
the design process.

I can encourage feedback to my design and
the designs of others by asking and answering
questions.

Indicator
De.R
NL.6.1
I can identify the strengths of my design and
designs of others.

Indicator
De.R
NM.6.1
I can identify areas of my design and the
designs of others that need improvement.

Indicator
De.R
NH.6.1
I can prepare some questions for
feedback to help me revise my design.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can listen and respond to the
opinions of others.



I can work with others to list possible
improvements to our solution.



I can ask what new materials could be
used in a design solution.



I can list the positive comments about
my design.



I can list changes I would make to my
design solution.



I can ask simple questions about a
design solution.



I can...



I can...



I can ask questions about who needs
the design.



I can...

Page 109

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Novice
Low
Benchmark
De.C
NL.7
I can recognize some examples of design
found in my home and community.

Novice
Mid
Benchmark
De.C
NM.7
I can recognize differences in designs found
in my home and community.

Novice
High
Benchmark
De.C
NH.7
I can describe differences in designs from
various cultures throughout history.

Indicator
De.C
NL.7.1
I can find and name some designs (object
environmental, communication, or
experiential) around me.
Sample Learning Targets

Indicator
De.C
NM.7.1
I can name some different design materials
and methods of construction.

Indicator
De.C
NH.7.1
I can compare how designs are different in
various cultures throughout history.

Sample Learning Targets

Sample Learning Targets



I can find and name some design
objects that I use every day.



I can identify the methods used for
communication design.



I can group designs that have similar
styles, subject, or media.



I can find and name some
environmental designs in my school
and community.



I can discuss the materials used in an
environmental design.







I can identify common characteristics
within a design from different styles,
periods, and cultures.

I can recognize the use of
communication design in
newspapers, billboards, and
commercials.

I can describe how a design was
made.



I can…



I can recognize the use of
experiential design in play grounds,
video games and amusement parks.



I can…



I can…

Page 110

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Novice
Low
Benchmark
De.C
NL.8
I can explore design thinking in arts
disciplines, other content areas, and related
careers.

Novice
Mid
Benchmark
De.C
NM.8
I can recognize design thinking in arts
disciplines, other content areas, and related
careers.

Novice
High
Benchmark
De.C
NH.8
I can apply design thinking in arts disciplines,
other content areas, and related careers.

Indicator
De.C
NL.8.1
I can explore how design exists in all arts
disciplines and other content areas.
Sample Learning Targets

Indicator
De.C
NM.8.1
I can recognize that design exists in all arts
disciplines and other content areas.
Sample Learning Targets

Indicator
De.C
NH.8.1
I can use design concepts in other subjects in
my school.
Sample Learning Targets



I can name designed objects in my
home and classroom.



I can identify ways design is used in
my community.



I can use the design process to solve
problems in other subjects.



I can talk about design choices found
in my home and classroom.



I can draw designs used in my
community.





I can draw examples of everyday
designs.



I can use design thinking to
brainstorm multiple solutions in
other subjects.

I can…



I can…



I can…

Page 111

Indicator
De.C
NL.8.2
I can recognize that people have careers in
design.

Indicator
De.C
NM.8.2
I can identify design businesses and careers in
my community.

Indicator
De.C
NH.8.2
I can identify ways design thinking is used in
other careers or vocations.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can recognize that musicians are
designers.



I can identify businesses in my
community that hire designers.



I can identify how design thinking is
used in business and industry.



I can recognize that buildings are
designed by architects.



I can identify where and how
designers impact my community.





I can recognize that choreographers
are designers.



I can locate design companies in my
community.

I can identify design thinking skills
that are used in education and service
organizations.



I can…

I can…





I can…

Page 112

Intermediate Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Intermediate
Low
Benchmark
De.CR
IL.1
I can work with a team to answer design
challenge questions.

Intermediate
High
Benchmark
De.CR
IH.1
I can work with a team from a given list of
design challenges and select one to describe.

Indicator
De.CR
IL.1.1
I can work with a team to answer the design
challenge questions who, what, when, where,
why, and how in order to define the design
challenge

Intermediate
Mid
Benchmark
De.CR
IM.1
I can work with a team from a given list to
identify and describe a design challenge to
develop.
Indicator
De.CR
IM.1.1
I can work with a team to select a design
challenge from a given list using certain
criteria and answer the design challenge
questions to define the challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can communicate and listen to others
when answering the design challenge
questions.



I can work with a team to choose a
design challenge based on its
importance to me and my community.



I can record information from the
group’s discussion.



I can work with a team to choose a
design challenge based on the need
for improvement to how it looks and
how it works.



I can...


I can…

Indicator
De.CR
IH.1.1
I can work in a team to discuss design
challenges from a given list and select one to
define from answers to the design challenge
questions.



I can work with a team to compare
and contrast the design challenge
options and select one based on their
importance to me and my community.



I can work with a team to compare
and contrast the design challenge
options and select one based on their
need for improvement to how it looks
and how it works.
I can…



Page 113

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Intermediate
Low
Benchmark
De.CR
IL.2
I can work with a team to research aspects of
the design challenge.

Intermediate
Mid
Benchmark
De.CR
IM.2
I can work with a team to research and
describe aspects of the design challenge.

Intermediate
High
Benchmark
De.CR
IH.2
I can work with a team to explain why
researched aspects of the design challenge
are needed.

Indicator
De.CR
IL.2.1
I can work with a team to identify necessary
information for the design challenge.

Indicator
De.CR
IM.2.1
I can communicate my research to the team.

Indicator
De.CR
IH.2.1
I can work with a team to prioritize research
from the individual team members.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify with a team what
information is necessary about the
existing design.



I can use visuals, technology,
demonstrations, and/or descriptions,
to report the research.



I can work with others to select the
best research methods to gather
necessary information.



I can discuss the research with others.



I can use printed materials to present
necessary information.



I can demonstrate the existing
function of a design.



I can work with others to create a
survey and/or use technology to learn
about a design.



I can list the research from the team
members.



I can work with others to identify the
most significant research.

Page 114

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Intermediate
Low
Benchmark
De.CR
IL.3
I can work with a team using design thinking
strategies to generate ideas for design
solutions to the challenge.
Indicator
De.CR
IL.3.1
I can work with a team using a variety of
design thinking strategies to list possible
design solutions without judgement.

Intermediate
Mid
Benchmark
De.CR
IM.3
I can work with a team using design
thinking strategies to generate some usable
design solutions to the challenge.
Indicator
De.CR
IM.3.1
I can work with a team to turn ideas into
possible design solution concepts.

Intermediate
High
Benchmark
De.CR
IH.3
I can work with a team using design
thinking strategies to generate many usable
design solutions to the challenge.
Indicator
De.CR
IH.3.1
I can work with a team to determine which
design solutions effectively meet the
challenge criteria.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can work with a team using a
variety of the following: list aloud,
popcorn brainstorming, passing
brainstorming, questioning
brainstorming, webbing, mind
mapping to provide possible
solutions.



I can build on the ideas of others in
creating possible solutions.



I can work with a team to determine
which solutions can be used in the
design challenge.



I can…



I can contribute my ideas concerning
usable solutions.



I can respond to others’ ideas
concerning usable solutions.



I can…



I can work with others to prioritize
choices concerning effective
solutions.



I can work with others to select
possible solutions.



I can…

Page 115

Anchor Standard 4: I can create an original prototype.
Intermediate
Low
Benchmark
De.CR
IL.4
I can work with a team to make a prototype
that represents a solution to a design
challenge.
Indicator
De.CR
IL.4.1
I can work with a team to make a prototype
to experience the design challenge criteria.

Intermediate
Mid
Benchmark
De.CR
IM.4
I can work with a team to make multiple
prototypes that represent various solutions to
a design challenge.
Indicator
De.CR
IM.4.1
I can work with a team to make prototypes
to experience the design challenge criteria.

Sample Learning Targets

Sample Learning Targets



I can work with others to create a
prototype that allows a concept to be
experienced.



I can create a simple prototype that is
made quickly and inexpensively to
experience feedback early and often.



I can…







I can work with others to create
multiple prototypes concerning one
design challenge that allow a concept
to be experienced.
I can work with a team to create
multiple simple prototypes that are
made quickly and inexpensively.

Intermediate
High
Benchmark
De.CR
IH.4
I can work with a team to create a prototype
to solve a design challenge.
Indicator
De.CR
IH.4.1
I can work with a team to make a prototype
that addresses functional aspects and
aesthetics.
Sample Learning Targets


I can work with a team to determine
the functionality of the prototype.



I can work with a team to improve
the functionality of the prototype to
address many aspects.



I can make a prototype that uses the
elements and/or principles of the arts
disciplines.



I can…

I can…

Page 116

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Intermediate
Low
Benchmark
De.P
IL.5
I can work with a team to present our design
solution to a challenge.

Intermediate
Mid
Benchmark
De.P
IM.5
I can work with a team to select an approach
to present our design solution to a challenge.

Intermediate
High
Benchmark
De.P
IH.5
I can work with a team to prepare and deliver
a presentation that has defined criteria.

Indicator
De.P
IL.5.1
I can work with a team to present our design
solution to the challenge using one or more
visuals.

Indicator
De.P
IM.5.1
I can work with a team to select an approach
using technology for the design solution
presentation.

Sample Learning Targets

Sample Learning Targets

Indicator
De.P
IH.5.1
I can work with a team to create a
presentation that includes specific criteria
and delivers required information concerning
the design challenge and design solution.
Sample Learning Targets



I can work with a team to prepare one
or more visuals such as photographs,
drawings, diagrams, charts, and 3D
examples to present our design
solution.



I can work with others to create a
slideshow presentation.



I can work with others to create a
webpage to present a design solution.



I can work with a team to explain the
“who, what, when, where, why, and
how” of the design challenge.



I can work with others to combine
still photos and videos to present a
design solution.



I can work with a team to explain the
“who, what, when, where, why, and
how” of the design solution.



I can ...





I can work in a team to prepare a
presentation that includes specific
criteria such as a title, infographics,
text, graphics, and/or media.



I can work with a team to prepare a
presentation that includes required
information such as the goal,
identified population, challenge
statement, key aspects, data, and
design solution.



I can…

I can…
Page 117

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Intermediate
Low
Benchmark
De.R
IL.6

Intermediate
Mid
Benchmark
De.R
IM.6

Intermediate
High
Benchmark
De.R
IH.6

I can reflect on and provide feedback to a
design solution.

I can interpret feedback from my peers to
revise our design solution

I can work with a team to analyze and explain
the steps of the design solution revision.

Indicator
De.R
IL.6.1
I can work with a team to record feedback
and summarize design solution
recommendations.

Indicator
De.R
IM.6.1
I can work with a team to list and prioritize
feedback to improve our design solution.

Indicator
De.R
IH.6.1
I can work with a team to plan and develop
the steps to improve our design solution.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can record feedback about our
design in my journal.



I can explain some of the solutions
presented as feedback to the group.





I can work with others to make a list
of the most important improvements
that need to be made to the design
solution.



I can...



I can work with others to review
feedback to determine next steps in
the revision process.



I can work with others to make
changes to our prototype that
improves our solution.



I can record my improvement ideas
for a design solution.



I can...

I can...

Page 118

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Intermediate
Low
Benchmark
De.C
IL.7
I can identify improvements or changes in
designs found in various cultures and time
periods.
Indicator
De.C
IL.7.1
I can compare design similarities and
differences among different cultures and time
periods.
Sample Learning Targets

Intermediate
Mid
Benchmark
De.C
IM.7
I can describe why improvements or changes
were made in designs found in various
cultures and time periods.
Indicator
De.C
IM.7.1
I can explain the possible reasons
improvements and/or changes were made in a
design through different cultures and time
periods.
Sample Learning Targets

Intermediate
High
Benchmark
De.C
IH.7
I can analyze a variety of design works from
different cultures and time periods.
Indicator
De.C
IH.7.1
I can recognize patterns in design choices and
make connections to the development of
design through different cultures and time
periods.
Sample Learning Targets



I can write a description about
characteristics of a specific design
style, period, or culture.



I can explain the possible reasons a
chair design evolved through cultures
and time periods.



I can make connections between
design choices on chairs from
different cultures and time periods.



I can compare changes in the designs
of furniture from other cultures over
time.



I can explain the possible reasons a
simple tool changed through cultures
and time periods.



I can make connections between
design choices on furniture from
different cultures and time periods.



I can…



I can…



I can…

Page 119

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Intermediate
Low
Benchmark
De.C
IL.8
I can explore a range of skills shared among
arts disciplines, other content areas and how
they can be applied in a design career.

Intermediate
Mid
Benchmark
De.C
IM.8
I can recognize specific skills shared among
arts disciplines, other content areas and how
they can be applied in a design career.

Indicator
De.C
IL.8.1
I can investigate a range of skills used in
various design careers, arts disciplines, and
content areas.
Sample Learning Targets

Indicator
De.C
IM.8.1
I can name design skills used in various arts
disciplines and content areas and relate these
skills to a career in design.
Sample Learning Targets



I can recognize skills that are specific
to a career in design.



I can research design careers.



I can pick and write about my favorite
design career.



I can list things that are designed by
people with a specific career in
design.



I can match a design product to a
design career.



I can list specific skills needed for a
design career.



I can…



I can…

Intermediate
High
Benchmark
De.C
IH.8
I can analyze the tools, concepts, and
materials used among arts disciplines, other
content areas and how they are used in a
design career.
Indicator
De.C
IH.8.1
I can investigate tools, concepts and materials
used in other arts disciplines and content
areas.
Sample Learning Targets


I can recognize skills that are specific
to design careers that are attained in
other arts disciplines and content
areas.



I can discuss costs of using different
materials to create the same design.



I can…

Page 120

Advanced Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Advanced
Low
Benchmark
De.CR
AL.1
I can work with a team to conceive many
design challenge possibilities relating to a
certain topic.
Indicator
De.CR
AL.1.1
I can work with a team using design thinking
strategies to list several design challenge
possibilities about a topic and select one to
define.
Sample Learning Targets

Advanced
Mid
Benchmark
De.CR
AM.1
I can work with a team to conceive many
design challenge possibilities.

Advanced
High
Benchmark
De.CR
AH.1
I can work on my own to conceive many
design challenge possibilities.

Indicator
De.CR
AM.1.1
I can work with a team using design thinking
strategies to list many design challenge
possibilities and prioritize to select one to
define.
Sample Learning Targets

Indicator
De.CR
AH.1.1
I can use design thinking strategies to list
many design challenge possibilities and
prioritize to select one to define.



I can work with a team to
brainstorm by randomly calling out
ideas.



I can work with a team to
brainstorm by creating questions
rather than ideas to inspire further
thinking.



I can work with a team to use visual
diagrams to organize information and
ideas. I can…

I can work with a team to compare
and contrast the design challenge
options and select one to define.



I can…





I can work with a team using a
variety of the following: list aloud,
popcorn brainstorming,
questioning brainstorming,
webbing, mind mapping to provide
many possible design challenges.

Sample Learning Targets


I can use a variety of the following:
list aloud, popcorn brainstorming,
questioning brainstorming,
webbing, mind mapping to provide
many possible design challenges.



I can compare and contrast the design
challenge options and select one to
define.



I can…
Page 121

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Advanced
Low
Benchmark
De.CR
AL.2
I can work with a team to analyze the aspects
of the design challenge.
Indicator
De.CR
AL.2.1
I can examine my research and report the
connections of that information with the
team.
Sample Learning Targets


I can review with a team the
research from multiple sources.



I can report the connections among
the data to my team.



Advanced
Mid
Benchmark
De.CR
AM.2
I can work independently or with a team to
evaluate the parts of the design challenge.
Indicator
De.CR
AM.2.1
I can work with a team to determine the
importance of the research from the team
members.
Sample Learning Targets


I can work with others to determine
the importance of the production and
cost improvement needed.



I can work with others to determine
the importance of the aesthetic
improvement needed.

I can…


I can work with others to determine
the importance functional
improvement needed.



I can…

Advanced
High
Benchmark
De.CR
AH.2
I can lead a discussion to evaluate the parts of
the design challenge.
Indicator
De.CR
AH.2.1
I can guide my team in determining the
importance of the research from the team
members.
Sample Learning Targets


I can present findings from research
that supports the need for aesthetic,
production, and/or functional
improvements.



I can justify the need for a new design
or redesign concept.



I can…

Page 122

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Advanced
Low
Benchmark
De.CR
AL.3
I can work with a team to analyze usable
design solutions to the challenge.
Indicator
De.CR
AL.3.1
I can examine, discuss, and select possible
design solutions to best address the
challenge.
Sample Learning Targets


I can review and discuss connections
among the possible solutions.



I can work with others to combine
parts of design solution ideas to
solve the design challenge.



I can…

Advanced
Mid
Benchmark
De.CR
AM.3
I can work independently or with a team to
evaluate the usable design solutions to the
challenge.
Indicator
De.CR
AM.3.1
I can work with a team to develop criteria to
determine the value of the usable design
solutions to the challenge.
Sample Learning Targets


I can work with others to list criteria
such as time, cost, functionality,
aesthetics, etc.



I can work with others to prioritize
design solutions based on chosen
criteria.





I can work with a team to reach a
consensus concerning the most viable
solutions to the design challenge.

Advanced
High
Benchmark
De.CR
AH.3
I can lead a discussion to evaluate the usable
design solutions to the challenge.
Indicator
De.CR
AH.3.1
I can guide my team in determining the value
of the usable design solutions to the
challenge.
Sample Learning Targets


I can lead a discussion to determine
the criteria.



I can lead a discussion that reaches a
consensus concerning the most viable
solutions to the design challenge.



I can justify how the solutions
effectively address the identified
needs.



I can…

I can…

Page 123

Anchor Standard 4: I can create an original prototype.
Advanced
Low
Benchmark
De.CR
AL.4
I can work with a team to create a prototype
that solves multiple aspects of a design
challenge.
Indicator
De.CR
AL.4.1
I can work with a team to select materials,
techniques, and processes to create a
prototype.
Sample Learning Targets






I can work with a team to select the
most appropriate materials to
build/compose the prototype from
those explored.
I can work with a team to select the
most appropriate techniques and
processes to build/compose the
prototype from those explored.

Advanced
Mid
Benchmark
De.CR
AM.4
I can work with a team to create a prototype
that solves all aspects of a design challenge
functionally and aesthetically.
Indicator
De.CR
AM.4.1
I can work with a team to select and apply the
best materials, techniques, and processes to
create a prototype.
Sample Learning Targets


I can work with a team to apply the
best materials to build/compose the
prototype from those explored.



I can work with a team to apply the
best techniques and processes to
build/compose the prototype from
those explored.



Advanced
High
Benchmark
De.CR
AH.4
I can use sophisticated materials, techniques,
and processes to create the most viable
prototype.
Indicator
De.CR
AH.4.1
I can select and apply professional materials,
techniques, and processes to create a
prototype.
Sample Learning Targets


I can apply professional materials to
build/compose the prototype.



I can apply professional techniques
and processes to build/compose the
prototype.



I can…

I can…

I can…

Page 124

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Advanced
Low
Benchmark
De.P
AL.5
I can work with a team to prepare and deliver
a presentation to a sample target group.

Indicator
De.P
AL.5.1
I can work in a team to present our design
solution to a group of possible
users/consumers for feedback.
Sample Learning Targets


I can work in a team and ask
questions of the target group so I can
effectively get the feedback.



I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, with a team to attain
feedback from the sample group.



Advanced
Mid
Benchmark
De.P
AM.5
I can work with a team to develop a wellprepared, aesthetically pleasing presentation
for a sample target group that includes
community business leaders or professionals
in the field.
Indicator
De.P
AM.5.1
I can work in a team to present our design
solution to a sample target group that includes
community business leaders and professionals
in a related field for feedback.
Sample Learning Targets




I can…


I can work in a team and ask
questions of the target group with
professionals so I can effectively get
the feedback.
I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, with a team to attain
feedback from the sample group with
business leaders in my community.

Advanced
High
Benchmark
De.P
AH.5
I can develop a well-prepared, aesthetically
pleasing presentation for a sample target
group that includes professionals and business
leaders in my community.
Indicator
De.P
AH.5.1
I can present our design solution to a sample
target audience that includes professionals and
business leaders in a related field for
feedback.
Sample Learning Targets


I can ask questions of the target group
with professionals so I can effectively
get the feedback.



I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, to attain feedback from the
sample group with professionals.



I can…

I can…

Page 125

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Advanced
Low
Benchmark
De.R
AL.6

Advanced
Mid
Benchmark
De.R
AM.6

Advanced
High
Benchmark
De.R
AH.6

I can work with a team to retest our revised
design solution and analyze the results.

I can work with a team to explain future
improvements and repeat the design process
to revise and retest the design solution.

I can facilitate the repetition of the design
process to revise and retest the design
solution.

Indicator
De.R
AL.6.1
I can work with a team to improve the
functionality of our design solution and
record the results of the modifications.

Indicator
De.R
AM.6.1
I can work with a team to repeat the design
process as necessary to improve the design
solution.

Indicator
De.R
AH.6.1
I can guide and frame questions to facilitate
the design process to improve a design
solution.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets




I can work with others to make
improvements to the prototype’s
functionality.
I can chart the progress of our
revisions to help my team improve
the functionality of the design.



I can....




I can retest my solution and revise as
many times as necessary to achieve
the most effective solution.
I can...



I can lead a class discussion on how
to revise a design challenge.



I can form questions to lead the
reflection process.



I can...

Page 126

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Advanced
Low
Benchmark
De.C
AL.7
I can examine past design works to determine
their influence on present designs.

Advanced
Mid
Benchmark
De.C
AM.7
I can work with a team to analyze the
influence of past design works on present
design challenges.

Advanced
High
Benchmark
De.C
AH.7
I can evaluate my design solution to
determine the effective use of past design
works.

Indicator
De.C
AL.7.1
I can find and compare how choices from a
current design reflect influences of past
design solutions.

Indicator
De.C
AM.7.1
I can work with a team to explain how the
designer's choices on the current design
challenge reflect influences of design
solutions from the past.

Indicator
De.C
AH.7.1
I can assess my design choices and relate
them to past design influences.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify influences from
previous designs in a current design
solution.



I can work with others to identify
influences from previous designs in a
current design solution.



I can defend my interpretations of
how different styles, periods, and
cultures have influenced my designs.



I can explain how specific past
designs are reflected in a current
design.



I can work with others to explain how
specific past designs are reflected in a
current design.



I can debate my choices made in my
designs that are influenced by
different styles, periods, and cultures.



I can…



I can…



I can…

Page 127

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Advanced
Low
Benchmark
De.C
AL.8
I can apply concepts among arts disciplines
and other content areas to design and analyze
how my interests and skills will prepare me
for a career.

Advanced
Mid
Benchmark
De.C
AM.8
I can explain how economic conditions,
cultural values, and location influence design
and the need for design related careers.

Advanced
High
Benchmark
De.C
AH.8
I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a designer.

Indicator
De.C
AL.8.1
I can use concepts found in various arts
disciplines and other content areas in a design
work.
Sample Learning Targets

Indicator
De.C
AM.8.1
I can describe how economic conditions,
cultural values, and geographic locations
affect design and design careers.
Sample Learning Targets

Indicator
De.C
AH.8.1
I can examine the importance of the work of a
designer in issues that relate to a global
society.
Sample Learning Targets



I can use the elements and/or
principles of arts disciplines in a
current design work.



I can discuss the relationships
between the designer and other
careers.



I can use concepts found in dance in a
current design work.





I can research processes of other
careers to determine how design
affects it.

I can…



I can justify community investment in
design.



I can…



I can defend the impact of design
careers within a society.



I can promote the intrinsic value of
design to individuals and society



I can find an important design
problem in another country and create
a design solution to help.



I can…

Page 128

Design Glossary
Aesthetics Concerned with appearance or the appreciation of beauty.
Artistic Processes The way the brain and the body make art and define the link between art making and the learner.
Aspects A particular part or parts of the design challenge.
Assess To estimate or evaluate the value of information researched.
Beta Testing Using a prototype to receive feedback from a sample target group.
Brainstorm A step in the problem solving process to producing an idea or several ideas. Ex. Popcorn Brainstorming, Passing
Brainstorming, Questioning Brainstorming.
Communication Design Design directed towards making connections between people. Ex. Graphic design, packaging design, web
design, etc.
Craftsmanship A degree or level of skill involved in creating a craft or work of art.
Define (a design challenge) Answering the design challenge questions to provide a clear description of what the design challenge is.
Design An outline, sketch, plan, model, or prototype of a solution to be developed or constructed that considers aesthetic decisions.
See definitions of Object Design, Environmental Design, Communication Design, and Experiential Design.
Design Challenge A design problem or a design issue defined as having the need to be altered, changed, or created in a particular way
to solve.
Design Challenge Questions Basic questions used to gather information concerning a design problem: Who, What, Where, When,
Why, and How. The answers to these questions define the design challenge.
Design Problem A specific design aspect or issue regarded as needing to be dealt with, overcome, or changed.
Design Process A process designed to identify a specific design problem, research the problem, create a solution to the problem, and
present the solution to the problem.
Page 129

Design Solution A means of solving a design problem.
Design Thinking To use one's mind to apply the process of design.
Design Thinking Strategies Methods or procedures used to brainstorm ideas or reason the process of design. Ex: Mind Maps, Concept
Maps, Webbings, Electronic Brainstorms, etc.
Environmental Design Design of surroundings or conditions in which a person, place, or thing interacts. Ex. Interior design,
playground design, community planning, etc.
Experiential Design Design based on personal interactivity or experiences; sometimes referred to as interactive design. Ex. Design
parades, design festivals, design theme parks, etc.
Feedback A reaction or response to a particular design problem or design solution.
Functional Referring to a design having a special activity, purpose, or task.
Interactions A person or a group of persons interacting with a prototype.Investigate To examine, research, or inquire the design
problem or aspects of the design problem in order to create a solution.
Mind Mapping A visual diagram used to organize information and ideas. It starts with a single idea, written or drawn in the center of a
blank page, to which associated words or ideas are added, continuing to associate the words and ideas.
Object Design Design of a material thing often related to industrial and product design. Ex. Design of tools, toys, cars, etc.
Passing Brainstorming A brainstorming technique in which individuals convey ideas one after another building upon the ideas of
others in a group.
Physical Model A three dimensional replication or copy of a prototype
Popcorn Brainstorming A brainstorming technique in which individuals freely state ideas in a group.
Presentation An activity in which an individual or a team shows, describes, or explains a design solution to a group of people.
Prototype A two-dimensional product or three-dimensional model of a design solution. See definitions of physical models, space
models, interactions, and storytelling.
Page 130

Questioning Brainstorming A brainstorming technique in which individuals generate questions in a group that may later be explored.
Research Investigating the design challenge determining the who, what, where, when, why, and how using a variety of research
methods; surveys, observations, interviews, experiments, internet, encyclopedias, newspapers, magazines, etc.
Sample Learning Target A broad lesson learning scenario.
Space Model a 2D or 3D replication or copy within which all things move
Standard Principle that is used as a basis for judgment.
Storytelling The use of words to describe the function or purpose of a prototype
Team A group organized to meet specific goals.
Techniques The use of tools and materials in unique ways that are specific to the designer and the medium.
Webbing Is a brainstorming technique that provides a visual structure or framework for idea development and can assist with
organizing and prioritizing information.

Page 131

References
Alfonso N. (2009, December 2). ABC Nightline - IDEO Shopping Cart [Online Video]. Retrieved from
https://www.youtube.com/watch?v=M66ZU2PCIcM
Lerman, L. &Borstel, J., (2003). Liz Lerman’s critical response process, a method of for getting useful feedback on anything you make,
from dance to dessert. Liz Lerman Dance Exchange.
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
Thayer School of Engineering at Dartmouth. (2012, March 28). IDEO workshop part four: prototyping [Online Video]. Retrieved from
https://www.youtube.com/watch?v=Rbjej4A6oRk
Thayer School of Engineering at Dartmouth. (2012, March 28). IDEO workshop part three: brainstorm [Online Video]. Retrieved from
https://www.youtube.com/watch?v=Ocb1bonXWc8
VandeZande, R. (2016). Art and Design Education [PowerPoint slides]. Retrieved from
https://drive.google.com/a/kent.edu/file/d/0B7Etk0esSRy2NEVMQl9sUVM0bFE/view?usp=sharing
VandeZande, R. (2011). Design education supports social responsibility and the economy. Arts Education Policy Review, 112(1), 2634.

Page 132

South Carolina
College- and Career-Ready Standards for
Media Arts Proficiency

South Carolina Department of Education
Columbia, South Carolina
2017
Page 375

Media Arts
Introduction
Education systems in the United States have long recognized the need for national standards to provide the basis for a common
curriculum and academic programs throughout the country. The South Carolina academic standards for Media Arts are emerging to
new levels in South Carolina’s academic standards for the Visual and Performing Arts. Studies in Media Arts utilize many of the
elements and principles from other arts disciplines while creating a body of work that reflects its own list of developing elements and
principles. Original media artworks reflect the aesthetics that are embedded in the visual and performing arts.
Our students are increasingly using media as a source of communication and networking. It is imperative that our educational system
and its constituents remain current with the trends and technologies that accompany the use of media. This includes appropriate use of
media and the ability to interpret Media Arts productions both socially and professionally to nurture the 21st century learner. The new
21st century skills movement (www.21stcenturyskills.org) specifically references media literacy as one of the skills all students need to
be attractive to employers in this new century. This also aligns with the skills for the South Carolina profile of the high school graduate.
Teachers should understand that these standards need to be reinforced throughout these proficiency levels as the students use more
advanced tools and media applications as well as more complex terminology and concepts.
Studies in Media Arts function as a components of an overall school curriculum that addresses the role of diversity role in the
classroom. Therefore, a school’s Media Arts curriculum should include sequential Media Arts courses as well as specialized courses.
Media Arts can include courses in animation; film studies; graphic design; sound design and recording; digital photography; digital
painting/illustration; and social media/web presence. Programs of study are designed to expose the student to a variety of future career
opportunities while making them globally aware of the importance that Media Arts plays in the classroom and beyond.

Page 376

Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can use past, current and emerging technology tools, procedures, and processes to
create a variety of media artworks in a safe and responsible manner.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.CR
NL.1

Benchmark
MA.CR
NM.1

Benchmark
MA.CR
NH.1

Benchmark
MA.CR
IL.1

Benchmark
MA.CR
IM.1

Benchmark
MA.CR
IH.1

Benchmark
MA.CR
AL.1

Benchmark
MA.CR
AM.1

Benchmark
MA.CR
AH.1

I can
recognize
technology
tools,
procedures
and processes
and use them
in a safe and
responsible
manner to
make media
artworks.

I can identify
a technology
tool,
procedure,
and process to
make still
pictures,
moving
pictures, or
digital audio.

I can identify
multiple
technology
processes to
make still
pictures,
moving
pictures, or
digital audio.

I can explain
and use a
technology
tool,
procedure and
process to
convey
meaning in
media
artwork.

I can explain
and use
multiple
technology
tools,
procedures
and processes
to convey
meaning in
media
artwork.

I can apply the
most
appropriate
technology
tool, procedure
and process to
convey a
message to
make a media
artwork.

I can apply
some
effective
technology
tools,
procedures
and processes
to convey a
message to
make a
variety of
media
artworks.

I can
manipulate
multiple
technology
tools,
procedures
and processes
to convey
messages to
make a
variety of
media
artworks in
the most
effective way.

I can use and
justify the
most effective
technology
tools,
procedures
and processes
to make a
variety of
still, moving,
and/or digital
audio images
to convey
meaning
using
personal
voice.

Page 377

Indicator
MA.CR
NL.1.1
I can safely
and
responsibly
show the
parts of a
technology
tool used to
make media
arts.

Indicator
MA.CR
NM.1.1
I can safely
and
responsibly
identify and
use parts of
some
technology
tools used to
make media
arts.

Indicator
MA.CR
NH.1.1

Indicator
MA.CR
IL.1.1

Indicator
MA.CR
IM.1.1

Indicator
MA.CR
IH.1.1

Indicator
MA.CR
AL.1.1

Indicator
MA.CR
AM.1.1

Indicator
MA.CR
AH.1.1

I can safely
and
responsibly
identify and
use multiple
technology
tools to make
media arts.

I can explain
and safely use
a technology
tool to
convey
meaning in
media arts.

I can explain
and safely use
multiple
technology
tools to
convey
meaning in
media arts.

I can choose
some
appropriate
technology
tools to convey
a message
while making a
media artwork
in a safe and
responsible
manner.

I can choose
multiple
effective
technology
tools to
convey a
message
while making
a variety of
media
artworks in a
safe and
responsible
manner.

I can
manipulate
multiple
technology
tools to
convey
messages to
make a
variety of
media
artworks in
the most
effective way
in a safe and
responsible
manner.

I can justify
the most
effective
technology
tools to make
a variety of
media
artworks that
convey
meaning
using
personal
voice in a safe
and
responsible
manner.

Page 378

Indicator
MA.CR
NL.1.2

Indicator
MA.CR
NM.1.2

Indicator
MA.CR
NH.1.2

Indicator
MA.CR
IL.1.2

Indicator
MA.CR
IM.1.2

Indicator
MA.CR
IH.1.2

Indicator
MA.CR
AL.1.2

Indicator
MA.CR
AM.1.2

Indicator
MA.CR
AH.1.2

I can follow
the steps of
some
technology
procedures
and processes
to make
media
artworks.

I can identify
the steps of a
technology
procedure and
process to
make media
artworks.

I can identify
the steps of
multiple
technology
procedures
and processes
to make
media
artworks.

I can explain
the steps of a
technology
procedure and
process to
convey
meaning in
media arts.

I can explain
the steps of
multiple
technology
procedures
and processes
to convey
meaning in
media arts.

I can choose the
appropriate
technology
procedure to
convey a
message while
making a media
artwork.

I can choose
some
effective
technology
procedures
and processes
to convey a
message
while making
a variety of
media
artworks.

I can
manipulate
multiple
technology
procedures
and processes
to convey
messages to
make a
variety of
media
artworks in
the most
effective way.

I can justify
the most
effective
technology
procedures
and processes
to make a
variety of
media
artworks that
convey
meaning
using
personal
voice.

Page 379

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles.
Benchmark
MA.CR
NL.2

Benchmark
MA.CR
NM.2

Benchmark
MA.CR
NH.2

Benchmark
MA.CR
IL.2

Benchmark
MA.CR
IM.2

Benchmark
MA.CR
IH.2

Benchmark
MA.CR
AL.2

Benchmark
MA.CR
AM.2

Benchmark
MA.CR
AH.2

I can
recognize and
explore some
elements and
principles in
media arts.

I can combine
elements and
principles of
media arts to
make media
artwork.

I can
communicate
meaning in
my work by
selecting and
arranging
elements and
principles of
media arts.

I can apply
elements and
principles of
media arts to
revise my
work.

I can analyze
and apply the
elements and
principles of
media arts as
a response to
an artistic
problem.

I can analyze
my media
artwork
through a
critique and
refine my
work based on
given criteria.

I can create,
refine, and
communicate
ideas based on
the elements
and principles
of media arts
to complete a
variety of
media
artworks.

I can
document and
justify the
planning and
development
of a media
artwork from
the inception
of the idea to
completion.

I can create a
body of work
in a variety of
media art
forms that
explore
personal
themes, ideas,
or concepts.

Indicator
MA.CR
NL.2.1

Indicator
MA.CR
NM.2.1

Indicator
MA.CR
NH.2.1

Indicator
MA.CR
IL.2.1

Indicator
MA.CR
IM.2.1

Indicator
MA.CR
IH.2.1

Indicator
MA.CR
AL.2.1

Indicator
MA.CR
AM.2.1

Indicator
MA.CR
AH.2.1

I can
recognize
some
elements or
principles of
media arts to
communicate
an idea.

I can combine
elements and
principles of
media arts
using multiple
media
techniques.

I can change
the meaning
of a media
artwork using
different
elements or
principles.

I can identify
improvement
s needed in
my media
artwork and
explore
strategies to
strengthen the
intended
meaning.

I can explain
how multiple
elements or
principles of
media arts are
used to
convey
meaning in
media
artworks.

I can
participate in a
formal critique
to revise my
artwork.

I can apply
organizational
strategies that
communicate a
personal
meaning,
theme, idea, or
concept.

I can create a
process folio
to document
the planning
of a media
artwork.

I can explain
and defend
the choices I
made to
communicate
my artistic
ideas across
multiples
media
artworks.
Page 380

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content, ideas,
skills, and media works for display.
Benchmark
MA.P
NL.3

Benchmark
MA.P
NM.3

Benchmark
MA.P
NH.3

Benchmark
MA.P
IL.3

Benchmark
MA.P
IM.3

Benchmark
MA.P
IH.3

Benchmark
MA.P
AL.3

Benchmark
MA.P
AM.3

Benchmark
MA.P
AH.3

I can identify
media
artworks as
communication.

I can explain
ways media
artworks are
presented.

I can consider
audience
response
when
discussing
how media
artworks are
presented.

I can identify
a target
audience for
presentation
of my media
artwork.

I can identify
and choose
multiple
formats used
in presenting
media
artworks for a
target
audience.

I can compare
presentation
formats for
different
media
artworks and
its intended
audience.

I can present
media
artworks
considering
combinations
of formats and
target
audience.

I can analyze
and interpret
the
effectiveness
of a media
arts
presentation
for an
intended
audience.

I can promote
and present
media
artworks for
intentional
impacts
through a
variety of
contexts such
as markets
and venues.

Indicator
MA.P
IH.3.1

Indicator
MA.P
AL.3.1

Indicator
MA.P
AM.3.1

Indicator
MA.P
AH.3.1

I can select my
intended
audience and
choose
multiple media
formats to get
the most
views.

I can evaluate
the
effectiveness
of virtual and
physical
presentations
of a media
artwork.

I can create a
media plan
(funding,
distribution,
and viewing)
to promote
my media
artwork.

Indicator
MA.P
NL.3.1

Indicator
MA.P
NM.3.1

Indicator
MA.P
NH.3.1

Indicator
MA.P
IL.3.1

Indicator
MA.P
IM.3.1

I can present
a media
artwork to an
audience.

I can identify
venues
appropriate
for still and
moving
images in
media
artworks.

I can examine
how audience
response
varies
depending on
how media
artwork is
presented.

I can identify
multiple ways
to share my
work through
different
media outlets.

I can choose
proper format
for my media
artwork.

I can choose
the most
effective
media format
for a select
audience.

Page 381

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Benchmark
MA.R
NL.4

Benchmark
MA.R
NM.4

Benchmark
MA.R
NH.4

Benchmark
MA.R
IL.4

Benchmark
MA.R
IM.4

Benchmark
MA.R
IH.4

Benchmark
MA.R
AL.4

Benchmark
MA.R
AM.4

Benchmark
MA.R
AH.4

I can explore
message and
purpose in
my media
artworks and
the work of
others.

I can identify
some
messages and
purposes in
media
artworks.

I can identify
the messages
and purposes,
in my media
artworks and
the work of
others.

I can explain
the messages
and purposes
in media
artworks.

I can analyze
and describe
reactions and
interpretation
s of messages
and purposes
in a variety of
media
artworks.

I can
investigate
personal and
group
intentions
about
messages and
purposes in
media
artworks.

I can discuss
and analyze
the message
and purpose in
a variety of
media
artworks.

I can analyze
the message
and intent of
a variety of
media
artworks.

I can justify
the message,
intent, and
impact of
diverse media
artworks,
considering
complex
factors of
context and
bias.

Page 382

Indicator
MA.R
NL.4.1

Indicator
MA.R
NM.4.1

Indicator
MA.R
NH.4.1

Indicator
MA.R
IL.4.1

Indicator
MA.R
IM.4.1

Indicator
MA.R
IH.4.1

Indicator
MA.R
AL.4.1

Indicator
MA.R
AM.4.1

Indicator
MA.R
AH.4.1

I can describe
parts of a
media
artwork.

I can identify
how media
artworks are
put together.

I can identify
the subject,
composition,
and media
arts elements
and
principles
for a variety
of media
artworks.

I can explain
how to use
the elements
and
principles of
media art to
compose a
media
artwork.

I can show
the
similarities
and
differences in
how media
artworks are
organized by
the elements
and
principles.

I can rephrase
ways in which
a variety of
media
artworks
organize
criteria.

I can analyze
the
organization of
the elements
and principles
of media
artworks.

I can critique
how the
composition
characteristics
in multiple
media
artworks work
together.

I can justify
the
organizational
choices made
by media
artists.

Indicator
MA.R
NL.4.2

Indicator
MA.R
NM.4.2

Indicator
MA.R
NH.4.2

Indicator
MA.R
IL.4.2

Indicator
MA.R
IM.4.2

Indicator
MA.R
IH.4.2

Indicator
MA.R
AL.4.2

Indicator
MA.R
AM.4.2

Indicator
MA.R
AH.4.2

I can describe
my thoughts
about
messages in
media
artworks.

I can identify
ideas, issues,
and/or
experiences
presented in
the messages
of media
artworks.

I can explain
the
techniques
used in
different
media
artworks that
reflect
varying
messages and
points of
view.

I can explore
the language,
tone, and
point of view
used in media
texts to
influence
meaning and
interpretation
of messages.

I can analyze
the
effectiveness
of a
presentation
and treatment
of messages in
media artwork.

I can interpret
the qualities
of and
relationships
between the
components,
style, and
message
communicate
d by media
artworks and
artists.

I can justify
my
interpretation
of language,
tone, and
point of view
of the
message in a
media
artwork.

I can name a
message in
media
artworks.

I can
investigate
increasingly
complex
messages in
media
artworks.

Page 383

Indicator
MA.R
NL.4.3
I can name a
purpose of
some media
artworks.

Indicator
MA.R
NL.4.4
I can make a
statement
about my
media
artwork.

Indicator
MA.R
NM.4.3

Indicator
MA.R
NH.4.3

Indicator
MA.R
IL.4.3

Indicator
MA.R
IM.4.3

Indicator
MA.R
IH.4.3

Indicator
MA.R
AL.4.3

Indicator
MA.R
AM.4.3

Indicator
MA.R
AH.4.3

I can identify
the purpose of
a media
artwork.

I can identify
the purpose
and audience
of a media
artwork.

I can explain
that different
media can
produce
artworks that
have the same
purpose.

I can
investigate
increasingly
complex
techniques
that artists use
to convey
purpose in
media
artwork.

I can find and
interpret data
to explore
multiple
differences in
the purpose of
media artwork.

I can analyze
formal and
informal
situations, the
effectiveness
of
presentation,
and treatment
of media to
convey the
purpose.

I can analyze
and interpret
the qualities
of
relationships
between the
components,
style,
message, and
how they
relate to the
purpose.

I can justify
my
interpretation
and
explanation of
the purpose of
multiple
media
artwork.

Indicator
MA.R
NM.4.4

Indicator
MA.R
NH.4.4

Indicator
MA.R
IL.4.4

Indicator
MA.R
IM.4.4

Indicator
MA.R
IH.4.4

Indicator
MA.R
AL.4.4

Indicator
MA.R
AM.4.4

Indicator
MA.R
AH.4.4

I can describe
multiple
elements and
principles of
media art in
my work.

I can identify
elements and
principals of
media arts in
artist’s
statements.

I can develop
an artist’s
statement that
describes
media arts
criteria and
intent of my
work.

I can develop
an artist’s
statement that
merges
personal
influences
with intent
and media arts
criteria for my
work.

I can develop
an artist’s
statement that
identifies
common
themes in
personal
influences,
intent and
media arts
criteria for
work.

I can justify
my choices of
criteria,
cultural
influences,
personal
experiences,
to create my
own voice in
my artist
statement.

I can describe I can
my media
recognize an
artwork.
element
and/or
principle of
media art in
my work.

Page 384

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Benchmark
MA.C
NL.5

Benchmark
MA.C
NM.5

Benchmark
MA.C
NH.5

Benchmark
MA.C
IL.5

Benchmark
MA.C
IM.5

Benchmark
MA.C
IH.5

Benchmark
MA.C
AL.5

Benchmark
MA.C
AM.5

Benchmark
MA.C
AH.5

I can explore
ideas that
connect
media arts to
different
cultures and
time periods.

I can
recognize
some
examples of
media arts
found in
different
cultures and
time periods.

I can identify
media arts
used for
different
purposes in
various
cultures and
time periods.

I can analyze
a variety of
media
artworks from
different
cultures and
time periods.

I can describe
why different
media
artworks are
used for
different
purposes in
various
cultures and
time periods.

I can analyze
similarities
and
differences in
media
artworks
among
different
cultures and
time periods.

I can examine
past media arts
works to
determine their
influence on
media today.

I can explain
the influence
of past media
arts works
throughout
different time
periods and
how that
reflects on
media today.

I can evaluate
media arts
works from
the past and
apply the
most effective
ones to my
work and the
work of
others.

Page 385

Indicator
MA.C
NL.5.1

Indicator
MA.C
NM.5.1

Indicator
MA.C
NH.5.1

Indicator
MA.C
IL.5.1

Indicator
MA.C
IM.5.1

Indicator
MA.C
IH.5.1

Indicator
MA.C
AL.5.1

Indicator
MA.C
AM.5.1

I can
recognize
ideas that
connect
media arts to
history,
cultures, and
the world.

I can relate to
ideas that
connect
media arts to
history,
cultures, and
the world.

I can show
how ideas
connect
media arts to
history,
cultures, and
the world.

I can explain
how ideas
connect
media arts to
history,
cultures, and
the world.

I can
compare and
contrast how
to connect
media arts
ideas to
history,
cultures, and
the world.

I can interpret
how media
arts ideas
connect to
history,
cultures, and
the world.

I can
participate in
formal and
informal
situations
relating to how
media art
connects to
history,
cultures, and
the world.

I can examine
the
relationship
between
media arts,
history,
cultures, and
the world.

Indicator
MA.C
AH.5.1
I can justify
the
relationship
between
media arts,
history,
cultures, and
the world.

Page 386

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Benchmark
MA.C
NL.6

Benchmark
MA.C
NM.6

Benchmark
MA.C
NH.6

Benchmark
MA.C
IL.6

Benchmark
MA.C
IM.6

Benchmark
MA.C
IH.6

Benchmark
MA.C
AL.6

Benchmark
MA.C
AM.6

Benchmark
MA.C
AH.6

I can explore
media arts
concepts
among other
arts
disciplines,
content areas,
and related
careers.

I can
recognize a
media arts
concept
among other
arts
disciplines,
content areas,
and related
careers.

I can apply
media arts
concepts
among other
arts
disciplines,
content areas,
and related
careers.

I can explore
a range of
media arts
skills shared
among other
arts
disciplines,
content areas,
and careers.

I can
recognize
specific
media arts
skills shared
among other
arts
disciplines,
content areas,
and careers.

I can analyze
the media arts
tools,
concepts, and
materials used
among other
arts
disciplines,
content areas,
and careers.

I can apply
media arts
concepts to
other arts
disciplines and
content areas
to prepare me
for a career.

I can research
aspects of
media arts
careers to
influence my
career path.

I can analyze
complex ideas
from other
arts
disciplines
and content
areas to
inspire my
creative work
and evaluate
its impact on
my artistic
perspective.

Page 387

Indicator
MA.C
NL.6.1

Indicator
MA.C
NM.6.1

Indicator
MA.C
NH.6.1

I can identify
a relationship
between
media arts
and another
subject in my
school.

I can
demonstrate a
relationship
between
media arts
and another
subject in my
school.

I can
demonstrate
and describe
the
relationship
between
media arts
and a concept
from another
subject in my
school.

Indicator
MA.C
NL.6.2

Indicator
MA.C
NM.6.2

Indicator
MA.C
NH.6.2

I can identify
different
types of
media arts
that interest
me.

I can identify
and
demonstrate
the skills used
to make
media
artwork that
interests me.

I can describe
specific
careers in
media arts.

Indicator
MA.C
IL.6.1
I can apply
media arts
concepts to
other arts
disciplines
and content
areas.

Indicator
MA.C
IL.6.2
I can
demonstrate
and describe
the skills
needed for
careers in
media arts.

Indicator
MA.C
IM.6.1

Indicator
MA.C
IH.6.1

Indicator
MA.C
AL.6.1

Indicator
MA.C
AM.6.1

I can examine
the
relationship
between
media arts
and specific
content from
another arts
discipline and
content area.

I can apply
concepts from
other arts
disciplines and
content areas
to my media
artwork.

I can explain
ideas from
other arts
disciplines and
content areas
through media
arts.

Indicator
MA.C
IM.6.2

Indicator
MA.C
IH.6.2

Indicator
MA.C
AL.6.2

Indicator
MA.C
AM.6.2

Indicator
MA.C
AH.6.2

I can identify
specific skills
required for
various
careers in
media arts.

I can research
topics about
careers in
media arts that
interest me.

I can identify
the skills,
training, and
education
necessary to
pursue a career
in media arts
that interests
me.

I can pursue
opportunities
that will lead
me to a career
in media arts.

I can
demonstrate
skills
necessary for
a career in
media arts.

I can explain
how
economic
conditions,
cultural
values, and
location
influence
media arts
and the need
for related
careers.

Indicator
MA.C
AH.6.1
I can research
societal,
political, and
cultural issues
as they relate
to other arts
and content
areas and
apply to my
role as a
media artist.

Page 388

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Benchmark
MA.C
NL.7

Benchmark
MA.C
NM.7

I can imitate
digital
citizenship
when I am
online.

I can identify
digital
citizenship
when I am
online.

Benchmark
MA.C
NH.7
I can show
digital
citizenship
when I am
online.

Benchmark
MA.C
IL.7
I can model
and explain
aspects of
digital
citizenship
when I am
online.

Benchmark
MA.C
IM.7

Benchmark
MA.C
IH.7

Benchmark
MA.C
AL.7

I can describe
different
aspects of
digital
citizenship
when I am
online.

I can interpret
different
aspects of
digital
citizenship
when I am
online.

I can
participate in
formal and
informal
situations to
discuss and
demonstrate
digital
citizenship
when I am
online.

Benchmark
MA.C
AM.7

Benchmark
MA.C
AH.7

I can analyze
and identify
the
appropriate
digital
citizenship
strategy to use
when I am
online.

I can justify
my choice of
digital
citizenship
strategy to
use when I
am online.

Page 389

Indicator
MA.C
NL.7.1
Internet
Safety

Indicator
MA.C
NM.7.1
Internet
Safety

Indicator
MA.C
NH.7.1
Internet
Safety

Indicator
MA.C
IL.7.1
Internet
Safety

I can explore
the internet
safely and
responsibly
when logging
on to my
device.

I can identify
several safe
ways to
search topics
on the
internet.

I can share
with others
how to safely
search for
information
on the
internet.

I can explain
and
demonstrate
several safe
and reliable
ways to
search for
accurate
information
on the
internet.

Indicator
MA.C
IM.7.1
Internet
Safety
I can
collaborate
with other
students in
various safe
and reliable
ways to
search for
information
on the
internet.

Indicator
MA.C
IH.7.1
Internet
Safety

Indicator
MA.C
AL.7.1
Internet Safety

Indicator
MA.C
AM.7.1
Internet
Safety

Indicator
MA.C
AH.7.1
Internet
Safety

I can identify
predictable
situation when
using the
internet.

I can
participate in
formal and
informal
situations
when
collaborating
with others
and can model
appropriate
and positive
etiquette.

I can analyze
various ways
to use digital
citizenship to
collaborate
with the
world in an
appropriate
and positive
way.

I can compile
a selection of
information
that is found
on the
internet and
how helps me
justify my
own voice as
an artist.

Page 390

Indicator
MA.C
NL.7.2
Digital
Footprint
Privacy

Indicator
MA.C
NM.7.2
Digital
Footprint
Privacy

Indicator
MA.C
NH.7.2
Digital
Footprint
Privacy

Indicator
MA.C
IL.7.2
Digital
Footprint
Privacy

Indicator
MA.C
IM.7.2
Digital
Footprint
Privacy

Indicator
MA.C
IH.7.2
Digital
Footprint
Privacy

Indicator
MA.C
AL.7.2
Digital
Footprint
Privacy

Indicator
MA.C
AM.7.2
Digital
Footprint
Privacy

Indicator
MA.C
AH.7.2
Digital
Footprint
Privacy

I can explore
how to post
safely on the
internet.

I can identify
several safe
online
platforms to
post on the
internet.

I can share
various ways
to post safely
on the
internet.

I can explain
and model
how to post
safely on the
internet.

I can analyze
various ways
to post safely
on the
internet.

I can
investigate
several ways
that
information on
the internet is
not safe or
responsible
and ways to
respond to
these
problems.

I can
participate in
formal and
informal
situations
when
collaborating
with others to
post safely on
the internet.

I can
investigate
various ways
to post safely
on the internet
and know the
difference
between a
positive and a
negative post.

I can justify
my choice of
what I post on
the internet to
interact with
the world in
an appropriate
and positive
way.

Page 391

Indicator
MA.C
NL.7.3
Copyright

Indicator
MA.C
NM.7.3
Copyright

Indicator
MA.C
NH.7.3
Copyright

Indicator
MA.C
IL.7.3
Copyright

Indicator
MA.C
IM.7.3
Copyright

Indicator
MA.C
IH.7.3
Copyright

I can identify
that a media
artwork has
an owner.

I can find the
owner of a
media
artwork on
the internet.

I can credit
the owner of
media
artwork on
the internet
when I intend
to use it.

I can explain
and model the
use of media
artwork that
is owned by
another artist,
and can
demonstrate
my
responsibilities and rights
when using
the work for
educational
purposes.

I can identify
media
artwork that
is owned by
another artist,
and can
demonstrate
my
responsibilities and rights
when using
the work for
educational or
personal
purposes.

I can handle
unexpected
situations with
copyright and
fair use rules
as it applies to
my artwork,
performance,
or
presentation.

Indicator
MA.C
AL.7.3
Copyright

I can
participate in
formal and
informal
situations
when
collaborating
with others to
discuss
copyright laws
that apply to a
media artwork.

Indicator
MA.C
AM.7.3
Copyright

Indicator
MA.C
AH.7.3
Copyright

I can analyze
and
synthesize
various ways
that copyright
laws apply to
my work and
the work of
others.

I can justify
my choice of
how I use
copyright
law to protect
my work and
the work of
others.

Page 392

Novice Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new artistic ideas and work.
Anchor Standard 1: I can use past, current and emerging technology tools, procedures, and processes to
create a variety of media artworks in a safe and responsible manner.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.CR
NL.1

Benchmark
MA.CR
NM.1

Benchmark
MA.CR
NH.1

I can recognize technology tools, procedures
and processes and use them in a safe and
responsible manner to make media artworks.

I can identify a technology tool, procedure,
and process to make still pictures, moving
pictures, or digital audio.

I can identify multiple technology processes to
make still pictures, moving pictures, or digital
audio.

Page 393

Indicator
MA.CR
NL.1.1

Indicator
MA.CR
NM.1.1

Indicator
MA.CR
NH.1.1

I can safely and responsibly show the parts of
a technology tool used to make media arts.

I can safely and responsibly identify and use
parts of some technology tools used to make
media arts.

I can safely and responsibly identify and use
multiple technology tools to make media arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can name some parts of a camera.

• I can label the parts of a camera.

• I can name the differences between a
computer, tablet, and a smartphone.

• I can follow the safety procedures when
using a media tool.

• I can pick from a list of tools which tool
would be the best to take a photograph that
tells a story about what makes me happy.

• I can...

• I can secure an iPad into a stand safely.

• I can identify where external components
are entered into a device.

• I can...

• I can...

Page 394

Indicator
MA.CR
NL.1.2

Indicator
MA.CR
NM.1.2

Indicator
MA.CR
NH.1.2

I can follow the steps of some technology
procedures and processes to make media
artworks.

I can identify the steps of a technology
procedure and process to make media
artworks.

I can identify the steps of multiple technology
procedures and processes to make media
artworks.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can take a photograph.

• I can take a photograph that is in focus.

• I can record my voice.

• I can take picture to tell a story.

• I can make an instructional video on how to
take a photograph. (The video will include
still and moving pictures.)

• I can record a video.

• I can video someone teaching a lesson.

• I can...

• I can...

• I can integrate still and moving images into
an iMovie trailer.
• I can...

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles of media arts.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.CR
NL.2

Benchmark
MA.CR
NM.2

Benchmark
MA.CR
NH.2

I can recognize and explore some elements
and principles in media arts.

I can combine elements and principles of
media arts to make media artwork.

I can communicate meaning in my work by
selecting and arranging elements and
principles of media arts.

Page 395

Indicator
MA.CR
NL.2.1

Indicator
MA.CR
NM.2.1

Indicator
MA.CR
NH.2.1

I can recognize some elements or principles
of media arts to communicate an idea.

I can combine elements and principles of
media arts using multiple media techniques.

I can change the meaning of a media artwork
using different elements or principles.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify when the music changes mood • I can use sound and moving images to tell a
in a film.
story.

• I can change the font and color on a
magazine cover to create emphasis.

• I can explore different music choices for a
video.

• I can change the speed and camera angle of
an animation.

• I can use lighting and contrast in a photo to
convey mood.

• I can explore how different fonts are used on • I can...
magazine covers.

• I can...

• I can ...

Page 396

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content, ideas,
skills, and media works for display.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.P
NL.3

Benchmark
MA.P
NM.3

Benchmark
MA.P
NH.3

I can identify media artworks as
communication.

I can explain ways media artworks are
presented.

I can consider audience response when
discussing how media arts works are
presented.

Page 397

Indicator
MA.P
NL.3.1

Indicator
MA.P
NM.3.1

Indicator
MA.P
NH.3.1

I can present a media artwork to an audience.

I can identify venues appropriate for still and
moving images in media artworks.

I can examine how audience response varies
depending on how media artwork is presented.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can present photography as a media
artwork.

• I can name places to show my work.

• I can identify how children respond
differently to a cereal commercial than
adults.

• I can present film as moving pictures to
create meaning.
• I can present posters and brochures as
advertisements.
• I can...

• I can identify where a still image is more
appropriate to use than a moving image.
• I can identify two places to post a video.
• I can…

• I can identify how a movie trailer and a
movie poster target audience for different
purposes.
• I can examine how different ages of people
chose different formats to view media.
• I can...

Page 398

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.R
NL.4

Benchmark
MA.R
NM.4

Benchmark
MA.R
NH.4

I can explore message and purpose in my
media artworks and the work of others.

I can identify some messages and purposes in
media artworks.

I can identify the messages and purposes, in
my media artworks and the work of others.

Page 399

Indicator
MA.R
NL.4.1

Indicator
MA.R
NM.4.1

Indicator
MA.R
NH.4.1

I can describe parts of a media artwork.

I can identify how media artworks are put
together.

I can identify the subject, composition, and
media arts elements and principles for a
variety of media artworks.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify a type of media artwork
(video, podcast, animation, etc.)

• I can name color as an element of a media
artwork.

• I can describe how different camera angles
are used in a film.

• I can name the subject in a photograph.

• I can tell that a photograph only shows
value, as an element of art, because it is
black and white.

• I can describe the rule of thirds and how it is
used in more than one media art form.

• I can identify color, size, font, and space
choices in a media artwork.
• I can...

• I can define an element and a principle of
media art in a short film or advertisement.
• I can…

• I can recognize how lighting is used to
change the mood or intent of the film.
• I can recognize how costume choices are
used to convey meaning.
• I can...

Page 400

Indicator
MA.R
NL.4.2

Indicator
MA.R
NM.4.2

Indicator
MA.R
NH.4.2

I can name a message in media artworks.

I can describe my thoughts about messages in
media artworks.

I can identify ideas, issues, and/or experiences
presented in the messages of media artworks.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify a message in a commercial.

• I can tell my thoughts about the message in a • I can explain my thoughts about the ideas,
movie, video, etc.
issues and experiences shown on a TV
episode and movie.

• I can identify the message in a print
advertisement.
• I can identify the plot in a movie.
• I can…

• I can explain for whom a media artwork
message was created.
• I can...

• I can explain my thoughts about ideas,
issues and experiences shown in an
advertisement and photograph.
• I can explain my thoughts about the
effectiveness of an advertisement or film.
• I can…

Page 401

Indicator
MA.R
NL.4.3

Indicator
MA.R
NM.4.3

Indicator
MA.R
NH.4.3

I can name a purpose of some media artworks. I can identify the purpose of a media artwork.

I can identify the purpose and audience of a
media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can predict the message of a media artwork • I can describe the preferences of an artist
based on an image.
that makes a video blog.
•
• I can describe the preferences of a radio
• I can identify the choices made by a
broadcaster.
choreographer in music video.
• I can explain the choices made by a
filmmaker when making a movie.
• I can...

• I can identify how a filmmaker uses light,
sound, costume, and setting to convey a
purpose.

• I can explain why different age groups may
respond differently to an internet meme.
• I can explain how different age groups may
respond differently to a movie or TV show.
• I can explain how people from different
backgrounds would react to a video game.
• I can...

• I can…

Page 402

Indicator
MA.R
NL.4.4

Indicator
MA.R
NM.4.4

Indicator
MA.R
NH.4.4

I can make a statement about my media
artwork.

I can describe my media artwork.

I can recognize an element and/or principle
of media art in my work.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can name the subject of my media
artwork.

• I can explain what inspired me to make my
artwork.

• I can explain some compositional elements
in my media artwork.

• I can name the setting of my media artwork.

• I can explain how I made my media
artwork.

• I can write a title for my work.

• I can...

• I can...

• I can...

• I can explain how setting, color, lighting,
etc., are used in my work.

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.C
NL.5

Benchmark
MA.C
NM.5

Benchmark
MA.C
NH.5

I can explore ideas that connect media arts to
different cultures and time periods.

I can recognize some examples of media arts
found in different cultures and time periods.

I can identify media arts used for different
purposes in various cultures and time periods.

Page 403

Indicator
MA.C
NL.5.1

Indicator
MA.C
NM.5.1

Indicator
MA.C
NH.5.1

I can recognize ideas that connect media arts
to history, cultures, and the world.

I can relate to ideas that connect media arts to
history, cultures, and the world.

I can show how ideas connect media arts to
history, cultures, and the world.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can watch a commercial and recognize that • I discuss ideas that connect to different
cultures are different based on clothing,
cultures other than my own by watching a
language or environment.
video, newsbyte, or commercial.
• I can choose a book that shows differences
in cultures and/or time periods.

• I can discuss ideas that connect to my
classmates from different cultures after
viewing current news reports.

• I can write about the connections to another
culture when looking at a media artwork.
• I can present a film that connects to my
family history.
• I can...

• I can...
• I can make an infographic about different
cultures and historical figures.
• I can...

Page 404

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.C
NL.6

Benchmark
MA.C
NM.6

Benchmark
MA.C
NH.6

I can explore media arts concepts among other I can recognize a media arts concept among
arts disciplines, content areas, and related
other arts disciplines, content areas, and
careers.
related careers.

I can apply media arts concepts among other
arts disciplines, content areas, and related
careers.

Page 405

Indicator
MA.C
NL.6.1

Indicator
MA.C
NM.6.1

Indicator
MA.C
NH.6.1

I can identify a relationship between media
arts and another subject in my school.

I can demonstrate a relationship between
media arts and another subject in my school.

I can demonstrate and describe the relationship
between media arts and a concept from
another subject in my school.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can gather an example and show how a
media art and a fine art can be the same, or
similar.

• I can show and describe the relationship
between a media art and fine art in a media
presentation.

• I can show how ideas connect media arts
and fine arts by creating a media
presentation. (Portraits)

• I can find ways that line can be shown in
visual art, dance, and media art.

• I can look at three commercials and tell what • I can show how ideas connect line in visual
other areas are connected within the
art, dance, and media art.
commercial such as E*TRADE’s baby,
Doritos and Clorox.
• I can make a picture, advertisement or short
video that uses another discipline.
• I can…
• I can...

• I can name another discipline used in a
video or picture conveying a message to an
audience.
• I can...

Page 406

Indicator
MA.C
NL.6.2

Indicator
MA.C
NM.6.2

Indicator
MA.C
NH.6.2

I can identify different types of media arts that I can identify and demonstrate the skills used
interest me.
to make a media artwork that interests me.

I can describe specific careers in media arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify the media I use at home or
school.

• I can name the skills used to make a
magazine layout.

• I can document the purpose of a director on
a film.

• I can identify the media I use for different
purposes.

• I can name the skills used to make a music
video.

• I can research roles in video game and music
video production.

• I can...

• I can name the skill used to make a
commercial or video game.

• I can research the role of a sound engineer.

• I can name the skills used to create a
podcast.
• I can...

• I can review movie credits to see all the
careers needed to make a movie.
• I can...

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.C
NL.7

Benchmark
MA.C
NM.7

Benchmark
MA.C
NH.7

I can imitate digital citizenship when I am
online.

I can identify digital citizenship when I am
online.

I can show digital citizenship when I am
online.

Page 407

Indicator
MA.C
NL.7.1
Internet Safety

Indicator
MA.C
NM.7.1
Internet Safety

Indicator
MA.C
NH.7.1
Internet Safety

I can explore the internet safely and
responsibly when logging on to my device.

I can identify several safe ways to search
topics on the internet.

I can share with others how to safely search
for information on the internet.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can know where to find my password.

• I can remember my password, log onto a
computer and use a computer application
with my student account.

• I can share with others how to safely log in
to my computer and protect my login in and
personal information.

• I can log in to my electronic device.
• I can follow acceptable use policies at my
school, home, or in public.

• I can create a bookmark for a website on my • I can share a document safely and
browser.
responsibly on the internet within a group of
my peers.

• I can...

• I can download an approved application.

• I can...

• I can...

Page 408

Indicator
MA.C
NL.7.2
Digital Footprint
Privacy

Indicator
MA.C
NM.7.2
Digital Footprint
Privacy

Indicator
MA.C
NH.7.2
Digital Footprint
Privacy

I can explore how to post safely on the
internet.

I can identify several safe online platforms to
post on the internet.

I can share various ways to post safely on the
internet

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can give examples of positive ways to post • I can explore what information is
thoughts and ideas on the internet.
appropriate to post online.

• I can share visual examples of good ways to
post on the internet.

• I can post images while protecting my
identity and the identity of others.

• I can follow acceptable use policies for
posting online.

• I can...

• I can...

• I can...

Page 409

Indicator
MA.C
NL.7.3
Copyright

Indicator
MA.C
NM.7.3
Copyright

Indicator
MA.C
NH.7.3
Copyright

I can identify that a media artwork has an
owner.

I can find the owner of a media artwork on the
internet.

I can credit the owner of media artwork on the
internet when I intend to use it.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can recognize a watermark.

• I can locate the watermark on a photograph. • I can tag a photo with the owner.

• I can recognize the credits on a film.

I can use correct spelling and vocabulary it
search topics.

• I can create credits when making an iMovie
trailer.

• I can identify safe search engines and
databases.

• I can help a peer safely find a video on the
internet.

• I can locate the credits for a video on a
website.

• I can work with other to search for
information on a group project.

• I can find headers and footers to check facts
on a website.

• I can...

• I can safely search for soundbites to use in
my media artwork.
• I can safely search for photographs taken by
a famous photographer on the internet.
• I can...

• I can...

Page 410

Intermediate Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new artistic ideas and work.
Anchor Standard 1: I can use past, current, and emerging technology tools, procedures and processes to
create a variety of media artworks in a safe and responsible manner.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.CR
IL.1

Benchmark
MA.CR
IM.1

Benchmark
MA.CR
IH.1

I can explain and use a technology tool,
procedure and process to convey meaning in
media artwork.

I can explain and use multiple technology
tools, procedures and processes to convey
meaning in media artwork.

I can apply the most appropriate technology
tool, procedure and process to convey a
message to make a media artwork.

Page 411

Indicator
MA.CR
IL.1.1

Indicator
MA.CR
IM.1.1

Indicator
MA.CR
IH.1.1

I can explain and safely use a technology tool
to convey meaning in media arts

I can explain and safely use multiple
technology tools to convey meaning in media
arts.

I can choose some appropriate technology
tools to convey a message while making a
media artwork in a safe and responsible
manner.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can make a media artwork about my family • I can make a media artwork about my family • I can edit a photograph to illustrate a stance
history.
history with interviews incorporated from
on a political issue.
family members.
• I can document a day in my life.
• I can select Gifs that animate my positions
• I can document a day in my life and
on politics.
focusing on a specific theme that tells a
• I can...
story.
• I can...
• I can...

Page 412

Indicator
MA.CR
IL.1.2

Indicator
MA.CR
IM.1.2

Indicator
MA.CR
IH.1.2

I can explain the steps of a technology
procedure and process to convey meaning in
media arts.

I can explain the steps of multiple technology
procedures and processes to convey meaning
in media arts.

I can choose the appropriate technology
procedure to convey a message while making
a media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can create a vlog about a social issue that
demonstrates that I know how to shoot a
video with sound.

• I can take a self-portrait photograph to
convey personal meaning.

• I can create a PSA choosing the best tools
and process to help my school.

• I can record my voice to make a vlog and
tell a story about a personal experience.

• I can make a voice over to use for daily
announcements.

• I can...

• I can combine music and sound to add under
still pictures.

• I can make a short interview about
admirable character traits to demonstrate
POV and sound.
• I can...

• I can...

Page 413

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.CR
IL.2

Benchmark
MA.CR
IM.2

Benchmark
MA.CR
IH.2

I can apply elements and principles of media
arts to revise my work.

Indicator
MA.CR
IL.2.1

I can analyze and apply the elements and
principles of media arts as a response to an
artistic problem.

Indicator
MA.CR
IM.2.1

I can analyze my media artwork through a
critique and refine my work based on given
criteria.

Indicator
MA.CR
IH.2.1

I can identify improvements needed in my
media artwork and explore strategies to
strengthen the intended meaning.

I can explain how multiple elements or
principles of media arts are used to convey
meaning in media artworks.

I can participate in a formal critique to revise
my artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how lighting can change a
photograph’s mood.

• I can explain how background music and the • I can make and post a video blog on my
speaker's tone of voice can affect meaning
process of making my film for others.
in a podcast.
• I can explain why point of view is important
• I can describe and make suggestions about a
when making a film.
• I can explain how editing and pacing can
media artwork free of personal judgment
change the rhythm of a commercial.
based on the elements and principles of
media arts.
• I can...
• I can...
• I can…
Page 414

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content, ideas,
skills, and media works for display.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.P
IL.3

Benchmark
MA.P
IM.3

Benchmark
MA.P
IH.3

I can identify a target audience for
presentation of my media artwork.

I can identify and choose multiple formats
used in presenting media artworks for a target
audience.

Indicator
MA.P
IL.3.1

I can compare presentation formats for
different media artworks and its intended
audience.

Indicator
MA.P
IM.3.1

Indicator
MA.P
IH.3.1

I can identify multiple ways to share my work
through different media outlets.

I can choose proper format for my media
artwork.

I can choose the most effective media format
for a select audience.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can post a photograph on social media.

• I can reduce the file size of a video for better • I can choose whether to upload my film to
streaming.
YouTube or Vimeo for impact.
•
• I can change the resolution of my
• I can decide whether I want to post my filers
photograph for better printing.
digitally or printed for distribution.

• I can upload my story as a podcast.
• I can upload a video to YouTube.
• I can...

• I can change the resolution of a film to be
projected for a large screen.

• I can...

• I can...
Page 415

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.R
IL.4

Benchmark
MA.R
IM.4

Benchmark
MA.R
IH.4

I can explain the messages and purposes in
media artworks.

I can analyze and describe reactions and
interpretations of messages and purposes in a
variety of media artworks.

Indicator
MA.R
IL.4.1

I can investigate personal and group intentions
about messages and purposes in media
artworks.

Indicator
MA.R
IM.4.1

Indicator
MA.R
IH.4.1

I can explain how to use the elements and
principles of media art to compose a media
artwork.

I can show the similarities and differences in
how media artworks are organized by the
elements and principles.

I can rephrase ways in which varieties of
media artworks organize criteria.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how separate things such as
framing and angles can change the film.

• I can see how film and video can be similar
in terms of image style but very different
when it comes to file size.

• I can look at a propaganda poster and remix
that using a new subject.

• I can...

• I can…

• I can…

Page 416

Indicator
MA.R
IL.4.2

Indicator
MA.R
IM.4.2

Indicator
MA.R
IH.4.2

I can explain the techniques used in different
media artworks that reflect varying messages
and points of view.

I can investigate increasingly complex
messages in media artworks.

I can explore the language, tone, and point of
view used in media texts to influence meaning
and interpretation of messages.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can relate how camera angles are
connected to the perception of the message
in a film.

• I can share what a director’s point of view
and message is in a film.

• I can analyze how point of view can
influence the audience of a news story.

• I can explain the difference in target
audience of a viral video and a full length
feature film.

• I can analyze how a director's personal
beliefs can influence their final product in a
documentary.

• I can explain the different target audience of
a meme and an ad campaign.

• I can explain how personal views can
influence an audience member’s reaction to
a commercial.

• I can identify various artistic techniques that
are used in advertising to convey a specific
message for a specific group of people.
• I can describe the main target audience of a
movie, or television show, based on the
message.
• I can…

• I can…

• I can...

Page 417

Indicator
MA.R
IL.4.3

Indicator
MA.R
IM.4.3

Indicator
MA.R
IH.4.3

I can explain that different media can produce
artworks that have the same purpose.

I can investigate increasingly complex
I can find and interpret data to explore
techniques that artists use to convey purpose in multiple differences in the purpose of media
media artwork.
artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify the way color is used in print
media.

• I can identify various lighting techniques in
a work of media art and how those
techniques influence the audience reaction.

• I can examine how an artist’s choice of
music in a short film. can influence the
audience.

• I can identify various sound techniques in a
work of media art and explain why they
were used.

• I can examine how the use of a particular
color on a meme can influence an audience.

• I can identify how text size and placement
on magazine covers can sway my opinion.
• I can...

• I can see advertisements are changed
depending on the target audience's location.

• I can...

• I can…

Page 418

Indicator
MA.R
IL.4.4

Indicator
MA.R
IM.4.4

Indicator
MA.R
IH.4.4

I can describe multiple elements and
principles of media art in my work.

I can identify elements and principles of
media arts in artist statements.

I can develop an artist’s statement that
describes media arts criteria and intent of my
work.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can write an artist statement that describes
and interprets and element or principle of
art.

• I can create a blog that describes, interprets
and analyzes my artwork.

• I can journal daily about my process and
purpose of creating artwork in class.

• I can write an artist statement that describes
how color is used in my media artwork.

• I can talk about an artist statement that
describes how quadrants are used to create
emotions in my media artwork.

• I can create a blog that describes, interprets
and analyzes my artwork.

• I can write an artist statement that describes
how line creates movement in my media
artwork.

• I can talk about an artist statement that
describes how angles are used in my media
artwork.

• I can..

• I can...

• I can...

Page 419

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.C
IL.5

Benchmark
MA.C
IM.5

Benchmark
MA.C
IH.5

I can analyze a variety of media artworks from I can describe why different media artworks
different cultures and time periods.
are used for different purposes in various
cultures and time periods.

I can analyze similarities and differences in
media artworks among different cultures and
time periods.

Page 420

Indicator
MA.C
IL.5.1

Indicator
MA.C
IM.5.1

Indicator
MA.C
IH.5.1

I can explain how ideas connect media arts to
history, cultures and the world.

I can compare and contrast how to connect
media arts ideas to history, cultures and the
world.

I can interpret how media arts ideas connect to
history, cultures, and the world.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can watch a commercial and talk about
how advertisements from different parts of
the world look.

• I can discuss how different advertisements
from different parts of the world look.

• I can watch 3 different Coca Cola
commercials and make connections to other
cultures and global advertising.

• I can watch a music video and discuss how
dancing styles change for different cultures.
• I can...

• I can compare and contrast the clothing in
music videos from around the world.
• I can...

• I can look at a video of or go to Disney
World and talk about the impact the ride
“It’s a Small World” has as a means of
teaching me more about culture.
• I can...

Page 421

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.C
IL.6

Benchmark
MA.C
IM.6

Benchmark
MA.C
IH.6

I can explore a range of media arts skills
shared among other arts disciplines, content
areas, and careers.

I can recognize specific media arts skills
shared among other arts disciplines, content
areas, and careers.

I can analyze media arts tools, concepts, and
materials used among other arts disciplines
content areas, and careers.

Page 422

Indicator
MA.C
IL.6.1

Indicator
MA.C
IM.6.1

Indicator
MA.C
IH.6.1

I can apply media arts concepts to other arts
disciplines and content areas.

I can examine the relationship between media
arts and specific content from another arts
discipline and content area.

I can apply concepts from other arts
disciplines and content areas to my media
artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how ideas connect media arts
and fine arts by creating a media
presentation.

• I can compare and contrast a media art with • I can interpret how the idea of line is used in
a fine art to discover the similarities and
painting and photography.
differences between the two.
• I can interpret the use of line and movement
• I can take a painting and create a media
in dance and photography to find similarities
artwork that represents the idea conveyed in
to create a short film.
the painting. i.e. “Off the and onto the
stage.”
• I can make connections to mathematics and
science using video games to enhance the
• I can look through magazine and cut out
playability of the game.
print ads that contain elements of math and
English.
• I can...

• I can explain how line connects media arts
to visual art and dance.
• I can talk about symbolism used in English
that is seen in print ads or websites to get a
message across to a target audience.
• I can...

• I can...

Page 423

Indicator
MA.C
IL.6.2

Indicator
MA.C
IM.6.2

Indicator
MA.C
IH.6.2

I can demonstrate and describe the skills
needed for careers in media arts.

I can identify specific skills required for
various careers in media arts.

I can research topics about careers in media
arts that interest me.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can define careers needed when making
my media artwork.

• I can discuss and assign a variety of jobs and • I can research and write what my job
responsibilities needed when making a
responsibilities and title is in making an
comic book (ie. illustrator, colorists, inker,
infomercial and explain my contributions to
etc.).
the work.

• I can describe the skills needed to be a
cinematographer.
• I can...

• I can identify the differences in skills needed • I can research and write what skills I would
for broadcast journalism and
need to work as a radio announcer.
photojournalism.
• I can...
• I can...

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.C
IL.7

Benchmark
MA.C
IM.7

Benchmark
MA.C
IH.7

I can model and explain aspects of digital
citizenship when I am online.

I can describe different aspects of digital
citizenship when I am online.

I can interpret different aspects of digital
citizenship when I am online.

Page 424

Indicator
MA.C
IL.7.1
Internet Safety

Indicator
MA.C
IM.7.1
Internet Safety

Indicator
MA.C
IH.7.1
Internet
Safety

I can explain and demonstrate several safe and I can collaborate with other students in various I can identify predictable situation when using
reliable ways to search for accurate
safe and reliable ways to search for
the internet.
information on the internet.
information on the internet.

Sample Learning Targets

Sample Learning Targets

• I can create a presentation that explains how • I can safely and responsibly work with
to keep my password information secure.
others online to create list of rules and steps
on how to protect my personal information.
• I can use my personal secure information to
create an account on an educational website. • I can identify ways to manage my online
information, safety, and collaborate with
other students on the internet in a safe and
• I can...
responsible way.

Sample Learning Targets
• . I can identify spam e-mail and delete it
from my account.
• I can recognize and report cyber bullying in
an online chatroom.
• I can...

• I can identify predictable situations that
might arise when I am searching for
information on the internet.
• I can...

Page 425

Indicator
MA.C
IL.7.2
Digital Footprint
Privacy

Indicator
MA.C
IM.7.2
Digital Footprint
Privacy

Indicator
MA.C
IH.7.2
Digital Footprint
Privacy

I can explain and model how to post safely on
the internet.

I can analyze various ways to post safely on
the internet.

I can investigate several ways that information
on the internet is not safe or responsible and
ways to respond to these problems.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can create a presentation that shows how to • I can compare and contrast different ways
post safely on the internet.
for students to post on the internet and how
to post in a constructive way.
• I can describe procedures to protect my
identity and the identity of others.
• I can describe multiple ways I can have an
online presence.
• I can...
• I can...

• I can show examples of why it is important
to post positive and constructive comments
on social media.
• I can...

Page 426

Indicator
MA.C
IL.7.3
Copyright

Indicator
MA.C
IM.7.3
Copyright

Indicator
MA.C
IH.7.3
Copyright

I can explain and model the use of media
artwork that is owned by another artist, and
can demonstrate my responsibilities and rights
when using the work for educational purposes.

I can identify media artwork that is owned by I can handle unexpected situations with
another artist, and can demonstrate my
copyright and fair use rules as it applies to
responsibilities and rights when using the work my artwork, performance, or presentation.
for educational or personal purposes.

Sample Learning Targets

Sample Learning Targets

• I can create a poster about the copyright
laws as they apply to photography.

• I can report how to attribute copyright to an • I can explain that as a creator of an original
artist in a media presentation.
piece of art I can: make copies of my work,
distribute copies of my work, or
perform/display my work publicly, or make
• I can compare and contrast primary,
derivative works.
secondary and tertiary sources of
information to decide which one best fits the
needs of my media project.
• I can make a PSA presentation that
demonstrates what to do when a problem
arises when searching for information on the
• I can...
internet.

• I can explain and demonstrate several ways
to search for a particular media form on the
internet.
• I can...

Sample Learning Targets

• I can...

Page 427

Advanced Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new artistic ideas and work.
Anchor Standard 1: I can use past, current and emerging technology tools, procedures, and processes to
create a variety of media artworks in a safe and responsible manner.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.CR
AL.1

Benchmark
MA.CR
AM.1

Benchmark
MA.CR
AH.1

I can apply some effective technology tools,
I can manipulate multiple technology tools,
procedures and processes to convey a message procedures and processes to convey messages
to make a variety of media artworks.
to make a variety of media artworks in the
most effective way.

I can use and justify the most effective
technology tools, procedures and processes to
make a variety of still, moving, and/or digital
audio images to convey meaning using
personal voice.

Page 428

Indicator
MA.CR
AL.1.1

Indicator
MA.CR
AM.1.1

Indicator
MA.CR
AH.1.1

I can choose multiple effective technology
tools to convey a message while making a
variety of media artworks in a safe and
responsible manner.

I can manipulate multiple technology tools to
convey messages to make a variety of media
artworks in the most effective way in a safe
and responsible manner.

I can justify the most effective technology
tools to make a variety of media artworks that
convey meaning using personal voice in a safe
and responsible manner.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can make a media artwork about my
family history.

• I can make a media artwork about my
family history with interviews
incorporated from family members.

• I can edit a photograph to illustrate a
stance on a political issue.

• I can document a day in my life.
• I can...

• I can document a day in my life and
focusing on a specific theme that tells a
story.

• I can create a moving film collage to
demonstrate a timeline.
• I can...

• I can...

Page 429

Indicator
MA.CR
AL.1.2

Indicator
MA.CR
AM.1.2

Indicator
MA.CR
AH.1.2

I can choose some effective technology
procedures and processes to convey a
message while making a variety of media
artworks.

I can manipulate multiple technology
procedures and processes to convey
messages to make a variety of media
artworks in the most effective way.

I can justify the most effective technology
procedures and processes to make a
variety of media artworks that convey
meaning using personal voice.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can create a vlog about a social issue
that demonstrates that I know how to
shoot a video with sound.

• I can take a self-portrait photograph to
convey personal meaning.

• I can create a PSA choosing the best
tools and process to help my school.

• I can record my voice to make a vlog
and tell a story about a personal
experience.

• I can make a voice over to use for daily
announcements.

• I can make a short interview about
admirable character traits to demonstrate
POV and sound.
• I can...

• I can...

• I can combine music and sound to add
under still pictures.
• I can...

Page 430

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.CR
AL.2

Benchmark
MA.CR
AM.2

Benchmark
MA.CR
AH.2

I can document and justify the planning
and development of a media artwork from
the inception of the idea to completion.

I can create a body of work in a variety of
media art forms that explore personal
themes, ideas, or concepts.

I can create, refine, and communicate
ideas based on the elements and
principles of media arts to complete a
variety of media artworks.

Page 431

Indicator
MA.CR
AL.2.1

Indicator
MA.CR
AM.2.1

Indicator
MA.CR
AH.2.1

I can apply organizational strategies that
communicate a personal meaning, theme,
idea, or concept.

I can create a process folio to document
the planning of a media artwork.

I can explain and defend the choices I
made to communicate my artistic ideas
across multiple media artworks. .

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how lighting can change a
photograph’s mood.

• I can explain how background music and • I can make and post a video blog on my
the speaker's tone of voice can affect
process of making my film for others.
meaning in a podcast.
• I can describe and make suggestions
• I can explain how editing and pacing can
about a media artwork free of personal
change the rhythm of a commercial.
judgment based on the elements and
principles of media arts.
• I can...
• I can…

• I can explain why point of view is
important when making a film.
• I can...

Page 432

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content,
ideas, skills, and media works for display.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.P
AL.3

Benchmark
MA.P
AM.3

Benchmark
MA.P
AH.3

I can present media artworks considering
combinations of formats and target
audience.

I can analyze and interpret the
effectiveness of a media arts presentation
for an intended audience.

I can promote and present media artworks
for intentional impacts through a variety of
contexts such as markets and venues.

Page 433

Indicator
MA.P
AL.3.1

Indicator
MA.P
AM.3.1

Indicator
MA.P
AH.3.1

I can select my intended audience and
choose multiple media formats to get the
most views.

I can evaluate the effectiveness of virtual
and physical presentations of a media
artwork.

I can create a media plan (funding,
distribution, and viewing) to promote my
media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can post a photograph on social media.

• I can reduce the file size of a video for
better streaming.

• I can choose whether to upload my film
to YouTube or Vimeo for impact.

• I can change the resolution of my
photograph for better printing.

• I can decide whether I want to post my
filers digitally or printed for distribution.

• I can change the resolution of a film to
be projected for a large screen.

• I can...

• I can upload my story as a podcast.
• I can upload a video to YouTube.
• I can...

• I can...

Page 434

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.R
AL.4

Benchmark
MA.R
AM.4

Benchmark
MA.R
AH.4

I can discuss and analyze the message and
purpose in a variety of media artworks.

I can analyze the message and intent of a
variety of media artworks.

I can justify the message, intent and
impacts of diverse media artworks,
considering complex factors of context
and bias.

Page 435

Indicator
MA.R
AL.4.1

Indicator
MA.R
AM.4.1

Indicator
MA.R
AH.4.1

I can analyze the organization of the
elements and principals of media
artworks.

I can critique how the composition
characteristics in multiple media artworks
work together.

I can justify the organizational choices
made by media artist.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how separate things such as • I can see how film and video can be
• I can look at a propaganda poster and
framing and angles can change the film.
similar in terms of image style but very
remix that using a new subject.
different when it comes to file size.
• I can explain how color theory themes
• I can create a series of podcast that
can change the emotion in a film.
• I can see how film and video can be
follow a similar format and style.
similar in terms of image style but very
different when it comes to point of view. • I can…
• I can...
• I can compare a infomercial’s use of
color to the color in a printed advertising
image.
• I can…

Page 436

Indicator
MA.R
AL.4.2

Indicator
MA.R
AM.4.2

Indicator
MA.R
AH.4.2

I can analyze the effectiveness of a
presentation and treatment of messages in
media artwork.

I can interpret the qualities of and
relationships between the components,
style, and message communicated by
media artworks and artists.

I can justify my interpretation of language,
tone, and point of view of the message in a
media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can relate how camera angles are
connected to the perception of the
message in a film.

• I can share what a director’s point of
view and message is in a film.

• I can analyze how point of view can
influence the audience of a news story.

• I can explain the difference in target
audience of a viral video and a full
length feature film.

• I can analyze how a director's personal
beliefs can influence their final product
in a documentary.

• I can identify various artistic techniques
that are used in advertising to convey a
specific message for a specific group of
people.
• I can describe the main target audience
of a movie, or television show, based on
the message.

• I can explain the different target
• I can explain how personal views can
audience of a meme and an ad campaign.
influence an audience member’s reaction
to a commercial.
• I can…
• I can...

• I can…

Page 437

Indicator
MA.R
AL.4.3

Indicator
MA.R
AM.4.3

Indicator
MA.R
AH.4.3

I can analyze formal and informal
situations, the effectiveness of
presentation, and treatment of media to
convey the purpose.

I can analyze and interpret the qualities of I can justify my interpretation and
relationships between the components,
explanation of the purpose of multiple
style, message, and how they relate to the media artworks.
purpose.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify the way color is used in
print media.

• I can identify various lighting techniques • I can examine how an artist’s choice of
in a work of media art and how those
music in a short film can influence the
techniques influence the audience
audience.
reaction.
• I can identify how text size and
placement on magazine covers can sway
• I can examine how the use of a particular
my opinion.
• I can identify various sound techniques
color on a meme can influence an
in a work of media art and explain why
audience.
they were used.
• I can...
• I can...
• I can see advertisements are changed
depending on the target audience's
location.
• I can…

Page 438

Indicator
MA.R
AL.4.4

Indicator
MA.R
AM.4.4

Indicator
MA.R
AH.4.4

I can develop an artist’s statement that merges
personal influences with intent and media arts
criteria for my work.

I can develop an artist’s statement that
identifies common themes in personal
influences, intent and media arts criteria for
work.

I can justify my choices of criteria, cultural
influences, personal experiences, to create my
own voice in my artist statement.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can write an artist statement that
• I can create a blog that describes,
describes and interprets personal choices
common themes in a series of films on
when using of lighting angles and
school culture.
shadows in a video game.
• I can write an artist statement about
• I can write an artist statement that
specific intent used in a radio
describes and interprets the use emphasis
announcement on religious views.
in costumes designs to express intent for
a dance video.
• I can…
• I can…

• I can validate my choices through an
artist statement on expressing my
personal voice on making a film about
persons’ with disabilities.
• I can defend my cultural influences in an
artist statement for a contest for
International Day.
• I can...

Page 439

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.C
AL.5

Benchmark
MA.C
AM.5

Benchmark
MA.C
AH.5

I can examine past media arts works to
determine their influence on media today.

I can explain the influence of past media
arts works throughout different time
periods and how that reflects on media
today.

I can evaluate media arts works from the
past and apply the most effective ones to
my work and the work of others.

Page 440

Indicator
MA.C
AL.5.1

Indicator
MA.C
AM.5.1

Indicator
MA.C
AH.5.1

I can participate in formal and informal
I can examine the relationship between
I can justify the relationship between
situations relating to how a media art
media arts, history, cultures, and the world. media arts, history, cultures, and the
connects to history, cultures and the world.
world.

Sample Learning Targets

Sample Learning Targets

• I can make a short documentary on an
African Drumming group that relates
native music to our country.

• I can make digital print advertisements
• I can make a short film project based on
based on a series of provided samples
a cultural theme identified by the
from the past, and use to define how
student. Students create three
different cultures will respond to the ads.
advertisements, then present these three
“ads” to the class and describe and
explain their connections.
• I can create an animation short reflecting
cultures from another country through
environment and action.
• I can make a presentation that connects
similar and different international
policies during different presidencies.
• I can...

• I can make a propaganda poster that
focuses on human rights and its changes
throughout history.
• I can...

Sample Learning Targets

• I can...

Page 441

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.C
AL.6

Benchmark
MA.C
AM.6

Benchmark
MA.C
AH.6

I can apply media arts concepts to other arts
I can research aspects of media arts careers to
disciplines and content areas to prepare me for influence my career path.
a career.

Indicator
MA.C
AL.6.1

Indicator
MA.C
AM.6.1

I can analyze complex ideas from other arts
disciplines and content areas to inspire my
creative work and evaluate its impact on my
artistic perspective.
Indicator
MA.C
AH.6.1

I can explain ideas from other arts disciplines
and content areas through media arts.

I can explain how economic conditions,
cultural values, and location influence media
arts and the need for related careers.

I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a media artist.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can make a video project using dance
themes from painter Jonathan Green.

• I can make a video of a music (band,
orchestra, choral) concert using more
than one camera and editing to produce
one work to be shared with the school
and community.

• I can make an interdisciplinary project
that to present to an organization in the
community or within the school.

• I can make a video project that
demonstrates how lines are similar in
media arts, visual arts, and dance.
• I can...

• I can make a video of dance using the
elements of earth, air, fire and water.

• I can make a short film that reflects
similarities and differences between
media arts and other disciplines with
regard to fundamental concepts. I can…
Page 442

Indicator
MA.C
AL.6.2

Indicator
MA.C
AM.6.2

Indicator
MA.C
AH.6.2

I can identify the skills, training, and
education necessary to pursue a career in
media arts that interests me.

I can pursue opportunities that will lead
me to a career in media arts.

I can demonstrate skills necessary for a
career in media arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can serve in a variety of roles such as
• I can research classes and workshops needed • I can take on an internship in the
to develop specific media arts techniques
director, camera operator, editor, sound
community in a media arts career
and
skills.
engineer, teleprompter and gaffer when
capacity.
making a short film to explore various
• I can participate in media arts opportunities
roles and skills related to filmmaking.
in my community.
• I can get an entry-level part time job
working in different media fields, such
• I can use my portfolio of work to
as a color flatter for the comic industry.
• I can...
identify skills that I am interested in
pursuing as career.
• I can...
• I can…

Page 443

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.C
AL.7

Benchmark
MA.C
AM.7

Benchmark
MA.C
AH.7

I can participate in formal and informal
situations to discuss and demonstrate
digital citizenship when I am online.

I can analyze and identify the appropriate
digital citizenship strategy to use when I
am online.

Indicator
MA.C
AL.7.1
Internet Safety

I can justify my choice of digital
citizenship strategy to use when I am
online.

Indicator
MA.C
AM.7.1
Internet Safety

Indicator
MA.C
AH.7.1
Internet Safety

I can participate in formal and informal
situations when collaborating with others
and can model appropriate and positive
netiquette.

I can analyze various ways to use digital
citizenship to collaborate with the world
in an appropriate and positive way.

I can compile a selection of information
that is found on the internet and how
helps me justify my own voice as an
artist.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can discuss and critique internet safety
and model how to use it in a safe and
responsible manner online.

• I can devise an internet safety plan for
other students to follow when they are
online in school.

• I can interact with my peers in an online
critique of an artwork, and justify my
constructive criticism.

• I can...

• I can...

• I can...

Page 444

Indicator
MA.C
AL.7.2
Digital Footprint
Privacy

Indicator
MA.C
AM.7.2
Digital Footprint
Privacy

Indicator
MA.C
AH.7.2
Digital Footprint
Privacy

I can participate in formal and informal
situations when collaborating with others
to post safely on the internet.

I can investigate various ways to post
safely on the internet and know the
difference between a positive and a
negative post.

I can justify my choice of what I post on
the internet to interact with the world in an
appropriate and positive way.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can participate on an online critique of
other student artwork in a positive and
constructive manner.

• I can create a constructive and positive
response to a blog post of another
student.

• I can communicate online in an
appropriate and positive on my webpage.

• I can...

• I can...

• I can create a blog and communicate
about global issues in a positive and
constructive way.
• I can...

Page 445

Indicator
MA.C
AL.7.3
Copyright

Indicator
MA.C
AM.7.3
Copyright

Indicator
MA.C
AH.7.3
Copyright

I can participate in formal and informal
situations when collaborating with others
to discuss copyright laws that apply to a
media artwork.

I can analyze and synthesize various ways
that copyright laws apply to my work and
the work of others.

I can justify my choice of how I use
copyright law to protect my work and the
work of others.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can take a stance in a debate about why • I can create a presentation about the
we should have copyright law.
similarities and differences between
photography and video copyright law.
• I can look at three websites with
information about the same story and
• I can create a blog that discusses how to
decide which information is valid,
use, understand and synthesize
reliable, and unbiased.
information found on different websites.
• I can...

• I can...

• I can demonstrate how I followed
copyright law in the creating of ideas
and media artwork.
• I can appraise information found on the
internet and find appropriate information
that validates and justifies my choice of
imagery, text, and sound in my artwork.
• I can...

Page 446

Media Arts Glossary
Artist Statement An artist statement lets you convey the reasoning behind your work-- why you chose a particular subject matter, why
you work in a certain medium, etc. And further, a well-written statement shows the relationship of you to your artwork, and
helps creates a connection with the viewer that will make your work (and your name) more memorable.
Attention Principle of directing perception through sensory and conceptual impact.
Balance Principle of the equitable and/or dynamic distribution of items in a media arts composition or structure for aesthetic meaning,
as in a visual frame, or within game architecture.
Codes and Conventions Codes are systems of signs put together (usually in a sequence) to create meaning. These systems may be
verbal, visual, non-verbal, or aural (e.g., sound effects, music). Visual media may use a number of technical codes such as
camera angles, framing, composition, and lighting to convey a particular attitude to a subject. Conventions are the commonly
accepted or generally known methods of doing something. Codes and conventions are used together in the study and
examination of a specific media genre. The camera angles used in a film, for example, should be studied in terms of the way
camera angles are conventionally used in the particular type of film.
Components The discrete portions and aspects of media artworks, including: elements, principles, processes, parts, assemblies, etc.,
such as: light, sound, space, time, shot, clip, scene, sequence, movie, narrative, lighting, cinematography, interactivity, etc.
Composition Principle of arrangement and balancing of components of a work for meaning and message.
Constraints Limitations on what is possible, both real and perceived.
Continuity The maintenance of uninterrupted flow, continuous action or self-consistent detail. across the various scenes or components
of a media artwork, i.e. game components, branding, movie timeline, series, etc.
Context The situation surrounding the creation or experience of media artworks that influences the work, artist or audience. This can
include how, where, and when media experiences take place, as well as additional internal and external factors (personal,
societal, cultural, historical, physical, virtual, economic, systemic, etc.)
Contrast Principle of using the difference between items, such as elements, qualities and components, to mutually complement them.
Page 447

Convention An established, common, or predictable rule, method, or practice within media arts production, such as the notion of a
‘hero’ in storytelling.
Copyright The exclusive right to make copies, license, and otherwise exploit a produced work.
Criteria The elements and principles students use to design their work
Digital Citizenship A standard of behavior with regard to the appropriate use of technology. A set of ethical and social norms that
oppose the misuse and abuse of technology.
Digital Identity How one is presented, perceived and recorded online, including personal and collective information and sites, ecommunications, commercial tracking, etc.
Divergent Thinking Unique, original, uncommon, idiosyncratic ideas; thinking “outside of the box.”
Design Thinking A cognitive methodology that promotes innovative problem solving through the prototyping and testing process
commonly used in design.
Elements of Media Arts Include but not limited to, light, sound, time, POV, performance, framing, narrative, and editing.
Emphasis Principle of giving greater compositional strength to a particular element or component in a media artwork.
Ethics Moral guidelines and philosophical principles for determining appropriate behavior within media arts environments.
Exaggeration Principle of pushing a media arts element or component into an extreme for provocation, attention, contrast, as seen in
character, voice, mood, message, etc.
Experiential Design Area of media arts wherein interactive, immersive spaces and activities are created for the user; associated with
entertainment design.
Fairness Complying with appropriate, ethical and equitable rules and guidelines.
Fair Use Permits limited use of copyrighted material without acquiring permission from the rights holders, including commentary,
search engines, criticism, etc.
Force Principle of energy or amplitude within an element, such as the speed and impact of a character’s motion
Page 448

Generative Methods Various inventive techniques for creating new ideas and models, such as brainstorming, play, open exploration,
experimentation, inverting assumptions, rule bending, etc.
Heterogeneity How an artwork can be made up of many distinct experiences and parts that are independent however when placed
together bring deeper meaning. Ex. installation that includes recorded, sounds, images, and performances.
Hybridization Principle of combining two existing media forms to create new and original forms, such as merging theatre and
multimedia.
Information Literacy Skills The abilities necessary to access, utilize, and critically evaluate the products of the mass media, including
an informed understanding of the nature of the media and the methods they employ.
Intent Purpose behind making a media art work whether personal or analyzed through the work made by others.
Interactivity A diverse range of articulating capabilities between media arts components, such as user, audience, sensory elements,
etc., that allow for inputs and outputs of responsive connectivity via sensors, triggers, interfaces, etc., and may be used to obtain
data, commands, or information and may relay immediate feedback, or other communications; contains unique sets of aesthetic
principles.
Judgement The ability to make informed and cognizant decisions regarding a media artwork, especially in the critique process.
Juxtaposition Placing greatly contrasting items together for effect.
Legal The legislated parameters and protocols of media arts systems, including user agreements, publicity releases, copyright, etc.
Manage Audience Experience The act of designing and forming user sensory episodes through multi-sensory captivation, such as
using sequences of moving image and sound to maintain and carry the viewer’s attention, or constructing thematic spaces in
virtual or experiential design
Markets The various commercial and informational channels and forums for media artworks, such as T.V., radio, internet, fine arts,
non-profit, communications, etc.
Meaning The formulation of significance and purposefulness in media artworks.

Page 449

Media Arts Contexts The diverse locations and circumstances of media arts, including its markets, networks, technologies and
vocations.
Media Environments Spaces, contexts and situations where media artworks are produced and experienced, such as in theaters,
production studios and online.
Media Literacy A series of communication competencies, including the ability to access, analyze, evaluate, and communicate
information in a variety of forms, including print and nonprint messages.
Media Messages The various artistic, emotional, expressive, prosaic, commercial, utilitarian and informational communications of
media artworks.
Media Texts Aural, print, graphic, and electronic communications with a public audience. Such texts often involve numerous people in
their construction and are usually shaped by the technology used in their production. Media texts include papers and magazines,
television, video and film, radio, computer software, and the Internet.
Medium Material used to create the art piece and determine the nature of the final work.Ex.film, digital imaging, web design.
Message Media messages contain “texts” and “subtexts.” The text is the actual words, pictures and/or sounds in a media message. The
subtext is the hidden and underlying meaning of the message. Media messages reflect the values and viewpoints of media
makers.
Modeling or Concept Modeling Creating a digital or physical representation or sketch of an idea, usually for testing; prototyping.
Movement Principle of motion of diverse items within media artworks.
Multimodal Perception The coordinated and synchronized integration of multiple sensory systems (vision, touch, auditory, etc.) in
media artworks.
Multimedia Theatre The combination of live theatre elements and digital media (sound, projections, video, etc.) into a unified
production for a live audience.
Narrative Structure The framework for a story, usually consisting of an arc of beginning, conflict, and resolution
Netiquette The correct or acceptable way of communicating on the Internet.
Page 450

Personal Aesthetic An individually formed, idiosyncratic style or manner of expressing oneself; an artist’s “voice.”
Perspective Principle pertaining to the method of three-dimensional rendering, point-of-view, and angle of composition.
Point of View The position from which something or someone is observed; the position of the narrator in relation to the story, as
indicated by the narrator's outlook from which the events are depicted and by the attitude toward the characters.
Positioning The principle of placement or arrangement.
Principles Media Arts Principles that include but not limited to, interactivity, heterogeneity, hybridization, medium, and temporality.
Production Processes The diverse processes, procedures, or steps used to carry out the construction of a media artwork, such as
prototyping, playtesting, and architecture construction in game design.
Prototyping Creating a testable version, sketch or model of a media artwork, such as a game, character, website, application, etc.
Representation Media representations are the ways in which the media portrays particular groups, communities, experiences, ideas, or
topics from a particular ideological or value perspective.
Resisting Closure Delaying completion of an idea, process, or production, or persistently extending the process of refinement, towards
greater creative solutions or technical perfection
Responsive Use of Failure Incorporating errors towards persistent improvement of an idea, technique, process or product
Rules The laws, or guidelines for appropriate behavior; protocols.
Safety Maintaining proper behavior for the welfare of self and others in handling equipment and interacting with media arts
environments and groups.
Soft Skills Diverse organizational and management skills, useful to employment, such as collaboration, planning, adaptability,
communication, etc.
Stylistic Convention A common, familiar, or even “formulaic” presentation form, style, technique, or construct, such as the use of
tension building techniques in a suspense film, for example.

Page 451

Systemic Communications Socially or technologically organized and higher-order media arts communications such as networked
multimedia; television formats and broadcasts; “viral” videos; social multimedia (e.g. “vine” videos); remixes; transmedia, etc.
System(s) The complex and diverse technological structures and contexts for media arts production, funding, distribution, viewing, and
archiving.
Technological The mechanical aspects and contexts of media arts production, including hardware, software, networks, code, etc.
Temporality How the passage of time can change one’s interpretation of an artwork or one’s ability to witness the artwork.
Tone Principle of “color,” “texture,” or “feel,” of a media arts element or component, as for sound, lighting, mood, sequence, etc.
Transdisciplinary Production Accessing multiple disciplines during the conception and production processes of media creation, and
using new connections or ideas that emerge to inform the work.
Transmedia Production Communicating a narrative and/or theme over multiple media platforms, while adapting the style and
structure of each story component to the unique qualities of the platforms.
Virtual Channels Network based presentation platforms such as: Youtube, Vimeo, Deviantart, etc.
Virtual Worlds Online, digital, or synthetic environments (e.g. Minecraft, Second Life).
Vocational The workforce aspects and contexts of media arts.

Page 452

References
Artist Statement | Art School Resources - ArtStudy.org. N.p., n.d. Web. 05 Feb. 2017.
Baker, F. (n.d.). Media Literacy Clearinghouse. Retrieved from http://frankwbaker.com/
Baker, F. W. (2010). Introduction to Media Arts (SCDE Visual & Performing Arts: 2010). Retrieved from
http://www.frankwbaker.com/media_literacy_supportdoc.htm
Boles, D. (1994). The language of media literacy: A glossary of terms. Mediacy, 16, (3). Retrieved from
http://www.mediaawareness.ca/english/resources/educational/teaching_backgrounders/media_
literacy/glossary_media_literacy.cfm
CI5472 Teaching Film, Television, and Media." CI5472 Teaching Film, Television, and Media. N.p., n.d. Web. 05 Feb. 2017.
Common Sense Media. (2016). Common Sense Education: Digital Citizenship. Retrieved from
https://www.commonsensemedia.org/educators/digital-citizenship
Elements and Principles of Media Art. St. Rosemary Educational Institution. (2016, July 18). Retrieved from
http://schoolworkhelper.net/elements-and-principles-of-media-art/
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
"Reading Between the Lines: Media Literacy." Wellness, Alcohol and Violence Education (WAVE). N.p., n.d. Web. 05 Feb. 2017.
South Carolina Department of Education. (2010). South Carolina Academic Standards for Media Arts. Retrieved from
http://ed.sc.gov/scdoe/assets/file/agency/ccr/Standards-Learning/documents/AcademicStandardsforMediaArts.pdf

Page 453


