# Instrumental Music
## Introduction

The new South Carolina College- and Career-Ready Standards for Instrumental Music Proficiency were written for the learner.
Learners begin new music experiences at different ages and progress toward music proficiency at different rates. The amount of quality
time spent in each course of study is a determining factor in the proficiency level that learners will reach. Learners at similar ages
frequently demonstrate varying proficiency levels. The continuum permits flexibility in acknowledging that students will
developmentally progress through the artistic processes at varying degrees over time.
The 2017 Instrumental Music Standards align with the 2014 National Core Arts Standards for Music and contain some language and
content from the 2010 Instrumental Music Standards. The 2017 standards were written within four artistic processes: creating,
responding, performing, and connecting. Within the artistic processes, the document outlines benchmarks, indicators, and sample
OHDUQLQJ WDUJHWV IHDWXULQJ ³, cDQ´ VWDWHPHQWV designed to place the learner LQ WKH GULYHU¶V VHDW RI WKHLU OHDUQLQJ SURFHVV These standards
cover Band and Orchestra. Standards related to performance contain Indicators and Sample Learnings Targets that are Band and
Orchestra specific.
The ultimate goal of the 2017 Instrumental Music Standards is to provide a document that is learner centered, meeting individual
educational needs and instilling a lifelong appreciation for music.
