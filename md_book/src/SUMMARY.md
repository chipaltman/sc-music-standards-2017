# College- and Career-Ready Standards Music Proficiency (2017)

[Foreword](./foreword.md)
- [Design](./design/intro.md)
- [Choral Music](./choral/intro.md)
- [General Music](./general/intro.md)
- [Instrumental Music](./instrumental/intro.md)
