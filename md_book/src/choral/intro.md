# Choral Music
## Introduction

In writing the 2017 South Carolina Choral Music Standards, our goal was to bridge the 2010 South Carolina Choral Music Standards with the 2014 National Core Arts Standards for Music to create a simplified, relevant document for teachers and students to use in the Choral Music classroom.
The purpose of this document and the “I can” language is to enable the teacher to become the facilitator of goals for the student using benchmarks to set achievable goals and to self-assess to take ownership of their learning.
Choral students come to us from a variety of musical backgrounds and experiences.
A freshman high school choral classroom may consist of students who perform at novice levels as well as students who perform at advanced levels.
Moving from a grade-level based model to a proficiency-based model allows teachers to meet students at their individual ability level to differentiate learning most
effectively.
Many choral teachers are also teachers of general or instrumental music.
For simplified planning, we have chosen to streamline the wording of several standards, benchmarks, and indicators with the other music areas.
The sample learning targets are specific to Choral Music.
Our hope is that the 2017 South Carolina Choral Music Standards will not only be a valuable resource for the teacher as a facilitator, but also for the learner to be actively engaged in his or her educational goals.
