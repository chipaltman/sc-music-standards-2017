# Design
## Introduction

Design is all around us and permeates every aspect of our lives. From waking up and deciding what to wear to making choices about
our environment, purchases, and recreation, we interact with the work of designers. The design fields include, but are not limited to,
Communication Design, Environmental Design, Experiential Design, and Object Design.
Functionality and aesthetics are two concepts that determine how we use a design and what we see in a design. For example, the
science of a bridge (function) must also be aesthetically pleasing for its environment. The design process guides students to experience
this interface by proceeding through a sequence of steps to find solutions for a design challenge. These steps include the following:
defining the design challenge, conducting research, brainstorming solutions, constructing a prototype, presenting a design solution to a
sample target group, receiving feedback, reflecting on the feedback, and making improvements on the prototype.
Students are guided through the design process to make creative and considerate decisions concerning the interaction of function and
aesthetics toward constructing a well-crafted prototype. The process requires that students present their design solution/prototype,
explain their thought processes, and receive feedback from stakeholders. This feedback allows students to analyze and reflect upon
their work in order to make thoughtful revisions toward improvement.
The design standards are organized in steps that parallel the design process. Students move through the standards, as shaped by the
design process, by working independently and collaboratively with others in order to reach an aesthetically-effective and functional
outcome.
Students are immersed cognitively when involved in the design process. The use of skills such as communication, creativity, critical
thinking, and problem solving are truly embodied in their work. Teaching through design reaches diverse learners who are able to
approach design thinking from their own personal perspectives and abilities.
These design standards are written to be applicable across all content areas. Traditionally considered under visual arts, problem solving
through design thinking may be applied to their artistic work but, just as importantly, it also may be used for project work in other
disciplines. Effective practices will be employed in all student work as a result of studying the South Carolina College and Career
Ready Standards for Design Proficiency.
