# Artistic Processes: Creating
> _I can use the elements of music to communicate new musical ideas._ — Artistic Process, Creating

## Anchor Standard 1:
> _I can arrange and compose music_ — Anchor Standard 1

### Novice

### Intermediate

### Advanced

### Low
### Mid
### High

Benchmark

CM.CR

+ NL.1 _I can notate simple rhythmic patterns using a defined selection of note values._
+ NM.1 _I can notate simple melodic patterns using a defined selection of pitches._
+ NH.1 _I can notate musical ideas using musical symbols to represent pitch and rhythm._
+ IL.1 _I can arrange a short song for my voice._
+ IM.1 _I can arrange a short song for two voices, using harmony._
+ IH.1 _I can arrange a short song for an ensemble, demonstrating an understanding of voicing and texture._
+ AL.1 _I can describe how I use melody, rhythm, and harmony to compose or arrange a work for a specific purpose._
+ AM.1 _I can collaborate with others to compose or arrange a musical work for a specific purpose._
+ AH.1 _I can compose short, original musical ideas and works using all the elements of music for a specific purpose._

Indicator

CM.CR

NL.1.1 I can recognize long and short sounds and identify simple rhythms from notation.
NM.1.1 I can create and recognize high and low sounds to represent some pitches.
NH.1.1 I can sing a variety of pitches and rhythms and label a music staff with clef and metric symbols.
IL.1.1 I can create a simple tune (monopho-nic melody) without accompaniment, within specified guidelines.
IM.1.1 I can develop a simple tune with accompanying parts (homophonic work).
IH.1.1 I can combine different voices to create various tone colors in my arrangement.
AL.1.1 I can arrange melodic themes for specific purposes, using arrangement and compositional techniques.
AM.1.1 I can sing in ensembles, working with themes for others to develop ideas as we compose or arrange a composition.
AH.1.1 I can create musical ideas and works using chord progressions and modulations.







