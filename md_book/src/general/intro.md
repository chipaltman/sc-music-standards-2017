# General Music
## Introduction

The new South Carolina Visual and Performing Arts Standards provide proficiency levels for student expectations. It should be noted
that the nature of general music within a proficiency model demands a balance of each of the artistic processes to provide the South
Carolina student with a comprehensive arts education in music. It should also be noted that further study in choral and instrumental
music is essential to cRPSOHWH RQH¶V PXVLF HGXFDWLRQ H[SHULHQFH DV ZHOO DV VWXG\ LQ PXVLF WKHRU\ FRPSRVLWLRQ DQG PXVLF KLVWRU\ This
proficiency approach to learning music provides a continuum of artistic processes and skills that assist to prepare students from a
novice (beginning) level to college- and career-ready (advanced high) level and builds state and national capacity to improve
knowledge and competencies of future adults as musicians and consumers. Equally, it answers the growing need for the critical skills of
higher order thinking and cultural competencies for relationship building in a safe and authentic way²a keystone for success in global
endeavors and diverse social environments.
Given the research of the importance and contributions of music to the cognitive development (MacDonald, Kreutz, & Mitchell, 2012),
and the positive correlations for student growth (Catterall, 2009), music is a necessary subject for the 21st century. Given the economic
impact of the arts in South Carolina, music should also be viewed as a viable career option IRU 6RXWK &DUROLQD¶V VWXGHQWV +RZHYHU
learners begin new music experiences at different ages and progress toward proficiency at different rates. While the amount of quality
time spent in each course of study is a determining factor in the proficiency level that learners will reach, learners at similar ages still
frequently demonstrate varying proficiency levels. Acknowledging this continuum permits flexibility in how students will
developmentally progress through the artistic processes. The South Carolina Standards for General Music outlines the progression of
learner skills, which makes LW HDVLHU IRU WHDFKHUV WR LGHQWLI\ D OHDUQHU¶V VNLOO OHYHO DQG WR GLIIHUHQWLDWH OHDUQLQJ IRU DOO OHDUQHUV
Demonstrating proficiency in music has potential benefits for learners. Musical knowledge and skill proficiency can be documented
through a variety of assessments and skills to be transferred directly to a career path. Colleges and universities often require at least one
credit of music for entrance. Research supports that prolonged and sustained study in the arts produces not only better artists and
critical thinkers, but also greater contributors to society as a whole (Catterall, 2009).
The rationale for the creation of this document stems from the need to provide a more transparent, learner friendly document that
clearly describes benchmarks for learners at various stages, provide pathways for meeting these benchmarks, and suggest possible
Page 214

strategies for a learning approach. Its intent is to recognize that everyone can learn music, to motivate learning, to increase achievement
through goal setting and self-assessment, and to facilitate building functional knowledge of the role of music in a global society. The
artistic processes, standards, benchmarks, indicators, and learning targets are meant to guide learning and should be shared with
learners and made available to parents and other stakeholders.
The four artistic processes of Creating, Responding, Performing, and Connecting provide the framework for the standards that
represent a comprehensive K±12 course of study and are interconnected and aligned with the National Core Arts Standards. This
document also acknowledges the varied resources available throughout the state and provides possible strategies to meet the standard at
each level of proficiency. The indicators can be viewed as units of study that support the standards. It should be noted that the learning
targets are examples of appropriate lesson material to address the indicators leading to the benchmarks and are meant to serve as
possible suggestions for the construction of lessons. The use of technology as a strategic tool through musical apps, digital recorders,
computers, interactive boards, stereos, keyboards, phones, and Smart TV devices can enhance learning, increase engagement, and
should be a part of the instructional toolbox for addressing the general music standards. The standards document helps motivate
learning by showing how to set achievable goals, self-assess, and chaUW SURJUHVV E\ XVLQJ ³, FDQ´ VWDWHPHQWV WKDW IDFLOLWDWH WKLV SURFHVV
Learners take ownership of their individual development as a musician. The document guides the facilitation of music learning toward
more functional, interactive, and culturally diverse processes. It provides examples of learning targets that can measure student growth
at a defined proficiency level and describes the standard and indicator in terms of individual lessons. This document provides a clearer
understanding of what learners need to know and be able to do to move from one level to the next. The ultimate goal of general music
is to provide foundational support for the development of each student as a musician and leads them to continued participation in choral
and/or instrumental music, as well as expanding their knowledge and interest as composers, theorists, and consumers of music.
