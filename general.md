South Carolina
College- and Career-Ready Standards for
General Music Proficiency

South Carolina Department of Education
Columbia, South Carolina
2017
Page 213

General Music
Introduction
The new South Carolina Visual and Performing Arts Standards provide proficiency levels for student expectations. It should be noted
that the nature of general music within a proficiency model demands a balance of each of the artistic processes to provide the South
Carolina student with a comprehensive arts education in music. It should also be noted that further study in choral and instrumental
music is essential to complete one’s music education experience, as well as study in music theory, composition, and music history. This
proficiency approach to learning music provides a continuum of artistic processes and skills that assist to prepare students from a
novice (beginning) level to college- and career-ready (advanced high) level and builds state and national capacity to improve
knowledge and competencies of future adults as musicians and consumers. Equally, it answers the growing need for the critical skills of
higher order thinking and cultural competencies for relationship building in a safe and authentic way—a keystone for success in global
endeavors and diverse social environments.
Given the research of the importance and contributions of music to the cognitive development (MacDonald, Kreutz, & Mitchell, 2012),
and the positive correlations for student growth (Catterall, 2009), music is a necessary subject for the 21st century. Given the economic
impact of the arts in South Carolina, music should also be viewed as a viable career option for South Carolina’s students. However,
learners begin new music experiences at different ages and progress toward proficiency at different rates. While the amount of quality
time spent in each course of study is a determining factor in the proficiency level that learners will reach, learners at similar ages still
frequently demonstrate varying proficiency levels. Acknowledging this continuum permits flexibility in how students will
developmentally progress through the artistic processes. The South Carolina Standards for General Music outlines the progression of
learner skills, which makes it easier for teachers to identify a learner’s skill level and to differentiate learning for all learners.
Demonstrating proficiency in music has potential benefits for learners. Musical knowledge and skill proficiency can be documented
through a variety of assessments and skills to be transferred directly to a career path. Colleges and universities often require at least one
credit of music for entrance. Research supports that prolonged and sustained study in the arts produces not only better artists and
critical thinkers, but also greater contributors to society as a whole (Catterall, 2009).
The rationale for the creation of this document stems from the need to provide a more transparent, learner friendly document that
clearly describes benchmarks for learners at various stages, provide pathways for meeting these benchmarks, and suggest possible
Page 214

strategies for a learning approach. Its intent is to recognize that everyone can learn music, to motivate learning, to increase achievement
through goal setting and self-assessment, and to facilitate building functional knowledge of the role of music in a global society. The
artistic processes, standards, benchmarks, indicators, and learning targets are meant to guide learning and should be shared with
learners and made available to parents and other stakeholders.
The four artistic processes of Creating, Responding, Performing, and Connecting provide the framework for the standards that
represent a comprehensive K–12 course of study and are interconnected and aligned with the National Core Arts Standards. This
document also acknowledges the varied resources available throughout the state and provides possible strategies to meet the standard at
each level of proficiency. The indicators can be viewed as units of study that support the standards. It should be noted that the learning
targets are examples of appropriate lesson material to address the indicators leading to the benchmarks and are meant to serve as
possible suggestions for the construction of lessons. The use of technology as a strategic tool through musical apps, digital recorders,
computers, interactive boards, stereos, keyboards, phones, and Smart TV devices can enhance learning, increase engagement, and
should be a part of the instructional toolbox for addressing the general music standards. The standards document helps motivate
learning by showing how to set achievable goals, self-assess, and chart progress by using “I can” statements that facilitate this process.
Learners take ownership of their individual development as a musician. The document guides the facilitation of music learning toward
more functional, interactive, and culturally diverse processes. It provides examples of learning targets that can measure student growth
at a defined proficiency level and describes the standard and indicator in terms of individual lessons. This document provides a clearer
understanding of what learners need to know and be able to do to move from one level to the next. The ultimate goal of general music
is to provide foundational support for the development of each student as a musician and leads them to continued participation in choral
and/or instrumental music, as well as expanding their knowledge and interest as composers, theorists, and consumers of music.

Page 215

General Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
GM.CR
NL.1

Benchmark
GM.CR
NM.1

Benchmark
GM.CR
NH.1

Benchmark
GM.CR
IL.1

Benchmark
GM.CR
IM.1

Benchmark
GM.CR
IH.1

Benchmark
GM.CR
AL.1

Benchmark
GM.CR
AM.1

Benchmark
GM.CR
AH.1

I can imitate
a musical
statement by
sight and
sound.

I can answer
a musical
question.

I can
arrange a
musical idea.

I can
compose a
rhythmic and
melodic
phrase.

I can add
harmony to
compose or
arrange
phrases for a
given mood.

I can arrange,
compose, and
explain intent
using
melody,
rhythm, and
harmony.

I can
collaborate
with others to
compose or
arrange a
variety of
musical
styles.

I can compose
music within
expanded
forms.

Indicator
GM.CR
NL.1.1
I can match
sound and
pattern.

Indicator
GM.CR
NM.1.1
I can identify
simple
forms.

Indicator
GM.CR
NH.1.1
I can use
rhythm
patterns,
songs or
words to
create a
musical idea.

I can
combine
musical ideas
to create
phrases for
voice,
instruments,
or body
movement.
Indicator
GM.CR
IL.1.1
I can explain
the use of
ostinato to
arrange a
melodic idea.

Indicator
GM.CR
IM.1.1
I can
organize
rhythmic and
melodic
patterns into
a musical
phrase.

Indicator
GM.CR
IH.1.1
I can identify
key
signatures in
melodic
phrases.

Indicator
GM.CR
AL.1.1
I can use the
circle of
fifths to
explain
transposition
of a written
musical
work.

Indicator
GM.CR
AM.1.1
I can revise a
composition
based on the
feedback
from others
to improve
composed
works.

Indicator
GM.CR
AH.1.1
I can create an
original
composition
independently.

Page 216

Indicator
GM.CR
NL.1.2
I can imitate
a given music
rhythm or
sound using
symbols.

Indicator
GM.CR
NM.1.2
I can identify
same and
different
patterns.

Indicator
GM.CR
NH.1.2
I can create a
musical idea
given
specific
instructions.

Indicator
GM.CR
IL.1.2
I can
construct
arrangements of
simple pieces
for voices or
instruments.

Indicator
GM.CR
IM.1.2
I can create a
melodic
phrase over a
given
rhythmic
idea.

Indicator
GM.CR
IH.1.2
I can
construct a
rhythmic,
melodic and
harmonic
idea for a
given mood.

Indicator
GM.CR
AL.1.2
I can use and
explain
compositional
techniques
to compose
works in a
music form.

Indicator
GM.CR
AM.1.2
I can work
with others to
compose an
original
composition.

Indicator
GM.CR
AH.1.2
I can create a
new
arrangement
from a given
composition.

Benchmark
GM.CR
IM.2
I can
improvise a
rhythm
pattern to
embellish a
given a
harmonic
phrase.
Indicator
GM.CR
IM.2.1
I can
embellish a
bass line
with
improvised
rhythm from
an instrument
or music
software.

Benchmark
GM.CR
IH.2
I can
improvise a
simple
melodic
phrase given
a harmonic
phrase.

Benchmark
GM.CR
AL.2
I can perform
a brief
improvisatio
n given a
chord
progression
and meter.

Benchmark
GM.CRAM.2

Benchmark
GM.CR
AH.2
I can perform
and refine an
extended
spontaneous
improvisation
independently

Indicator
GM.CR
IH.2.1
I can identify
chord
changes.

Indicator
GM.CR
AL.2.1
I can
improvise
harmonizing
parts.

Anchor Standard 2: I can improvise music.
Benchmark
GM.CR
NL.2
I can imitate
simple
rhythm
patterns
within a
given meter.

Benchmark
GM.CRNM.2

Benchmark
GM.CRNH.2

I can imitate
simple tonal
patterns
within a
given key
and tonality.

I can
improvise
responses to
given
rhythmic
patterns.

Indicator
GM.CR
NL.2.1
I can identify
same and
different
rhythms
patterns.

Indicator
GM.CR
NM.2.1
I can identify
same and
different
melodic
patterns.

Indicator
GM.CR
NH.2.1
I can
improvise
simple
ostinati
patterns
within a
given meter.

Benchmark
GM.CR
IL.2
I can
improvise
short melodic
question and
answer
patterns.

Indicator
GM.CR
IL.2.1
I can
improvise
simple tonal
patterns
within a
given key.

I can perform
an
improvisatio
n given a
motive,
chord
progression,
and meter.
Indicator
GM.CR
AM.2.1
I can perform
an
improvisation on a
given
motive.

Indicator
GM.CR
AH.2.1
I can
improvise
responding to
aural cues.

Page 217

Indicator
GM.CR
NL.2.2
I can echo
simple
rhythm
patterns.

Indicator
GM.CR
NM.2.2
I can echo
simple tonal
patterns.

Indicator
GM.CR
NH.2.2
I can
improvise
rhythm
patterns,
songs or
chants to
create a
musical idea.

Indicator
GM.CR
IL.2.2
I can
improvise
rhythmic and
melodic
patterns to
create a
musical
phrase.

Indicator
GM.CR
IM.2.2
I can sing on
a neutral
syllable an
improvised
rhythm.

Indicator
GM.CR
IH.2.2
I can
embellish a
given
melodic
phrase that
corresponds
with simple
chord
changes.

Indicator
GM.CR
AL.2.2
I can
improvise
short
melodies
using
accurate and
consistent
style, meter,
and tonality.

Indicator
GM.CR
AM.2.2
I can
improvise
extended
passages
using
consistent
style, meter,
and tonality.

Indicator
GM.CR
AH.2.2
I can
demonstrate
and refine
musicality
during
improvisational solos.

Artistic Processes: Performing - I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Benchmark
GM.PNL.3
I can use my
voice in
many ways.

Benchmark
GM.P
NM.3
I can use my
singing
voice.

Benchmark
GM.P
NH.3
I can sing,
chant, and
move to
demonstrate
a steady beat.

Benchmark
GM.P
IL.3
I can sing a
variety of
simple part
songs.

Benchmark
GM.P
IM.3
I can sing
with
expression
and technical
accuracy.

Benchmark
GM.P
IH.3
I can sing a
variety of
songs with
expression
and technical
accuracy.

Benchmark
GM.P
AL.3
I can sing
expressively
and apply
technical and
stylistic
criteria in a
variety of
songs alone
and in
various
ensembles.

Benchmark
GM.P
AM.3
I can
collaborate
with others
make
technical and
stylistic
decisions.

Benchmark
GM.P
AH.3
I can make
technical and
stylistic
choices about
my
performance
as a singer
alone and in
various
ensembles.

Page 218

Indicator
GM.P
NL.3.1
I can sing
songs in my
range.

Indicator
GM.P
NM.3.1
I can match
pitch when I
sing.

Indicator
GM.P
NH.3.1
I can sing or
move using a
steady beat.

Indicator
GM.P
IL.3.1
I can sing 2part songs.

Indicator
GM.P
NL.3.2
I can use my
voice to
imitate other
sounds.

Indicator
GM.P
NM.3.2
I can sing
with my
head voice
and chest
voice.

Indicator
GM.P
NH.3.2
I can use
good posture
and breath
support when
I sing.

Indicator
GM.P
IL.3.2
I can sing
with
appropriate
diction and
articulation.

Indicator
GM.P
IM.3.1
I can sing
with proper
intonation
alone and in
different
ensembles.
Indicator
GM.P
IM.3.2
I can apply
dynamics
and
expression
when I sing.

Indicator
GM.P
IH.3.1
I can sing
phrasing
while
responding to
a director’s
cues.
Indicator
GM.P
IH.3.2
I can sight
read in
multiple
tonalities and
rhythms.

Indicator
GM.P
AL.3.1
I can sing in
a group with
balance.

Indicator
GM.P
AL.3.2
I can blend
with others in
an ensemble.

Indicator
GM.P
AM.3.1
I can sing in
ensembles.

Indicator
GM.P
AH.3.1
I can use a
variety of
technical and
stylistic
choices in my
performance.
Indicator
Indicator
GM.P
GM.P
AM.3.2
AH.3.2
I can rehearse I can apply a
with an
variety of
ensemble to
musical
improve my
choices for
work.
performance.

Anchor Standard 4: I can play instruments alone and with others.
Benchmark
GM.P
NL.4
I can make
sounds with
classroom
instruments
and other
sound
sources.

Benchmark
GM.P
NM.4
I can imitate
short
rhythmic
patterns.

Benchmark
GM.P
NH.4
I can play
and read
rhythmic,
melodic, and
chord
patterns.

Benchmark
GM.P
IL.4
I can play
accompaniments and
simple songs
on classroom
instruments.

Benchmark
GM.P
IM.4
I can play
and read
complimentary,
contrasting
instrumental
parts
accurately,
and
independently.

Benchmark
GM.P
IH.4
I can play
and read my
part with an
ensemble
using
accurate
technique
and posture.

Benchmark
GM.P
AL.4
I can play an
instrument
expressively
and apply
technical and
stylistic
techniques
in variety of
music alone
and in
various
ensembles.

Benchmark
GM.P
AM.4
I can
collaborate
with others to
apply
technical and
stylistic
techniques
in a variety
of music
alone and in
various
ensembles.

Benchmark
GM.P
AH.4
I can make
technical and
stylistic
choices about
my
performance
as an
instrument-a
list alone and
in various
ensembles.

Page 219

Indicator
GM.P
NL.4.1
I can use my
body to make
sounds.

Indicator
GM.P
NM.4.1
I can echo an
ostinato
rhythm
pattern.

Indicator
GM.P
NH.4.1
I can use
music
notation to
play
instruments.

Indicator
GM.P
IL.4.1
I can play
accompanyments and
songs in
major and
minor
tonalities.

Indicator
GM.P
IM.4.1
I can play my
part
independently in an
ensemble.

Indicator
GM.P
IH.4.1
I can rehearse
for
improvement
in an
ensemble.

Indicator
GM.P
AL.4.1
I can play in
various
musical
styles on
instruments.

Indicator
GM.P
AM.4.1
I can
collaborate
with others to
improve my
ensemble.

Indicator
GM.P
NL.4.2
I can play
pitched and
unpitched
instruments.

Indicator
GM.P
NM.4.2
I can play
melodic
patterns
using steps
and skips.

Indicator
GM.P
NH.4.2
I can play
pentatonic
scales on
instruments.

Indicator
GM.P
IL.4.2
I can play
using proper
technique
and posture.

Indicator
GM.P
IM.4.2
I can play my
instrument
with
technical
accuracy.

Indicator
GM.P
IH.4.2
I can use
proper
technique to
express
music.

Indicator
GM.P
AL.4.2
I can balance
my sound
with others in
an ensemble.

Indicator
GM.P
AM.4.2
I can control
pitch and
tone quality
with proper
dynamics.

Indicator
GM.P
NL.4.3
I can follow
the teacher
when I use
classroom
instruments.

Indicator
GM.P
NM.4.3
I can ask and
answer
musical
questions
using
instruments.

Indicator
GM.P
NH.4.3
I can identify
rhythmic
notation.

Indicator
GM.P
IL.4.3
I can play in
treble and
bass clefs.

Indicator
GM.P
IM.4.3
I can read
from
notation,
songs I play.

Indicator
GM.P
IH.4.3
I can play my
part
independentl
y while
others play.

Indicator
GM.P
AL.4.3
I can sight
read a
musical part.

Indicator
GM.P
AH.4.1
I can control
my instrument
across
expanded
dynamic
ranges using
stylistic
nuances and
expressive
inflections.
Indicator
GM.P
AH.4.2
I can adjust
my
intonation
relative to
chord tones.

Page 220

Anchor Standard 5: I can read and notate music.
Benchmark
GM.P
NL.5
I can read
rhythm
patterns.

Benchmark
GM.P
NM.5
I can read
simple
rhythmic and
melodic
notation.

Benchmark
GM.P
NH.5
I can read,
write simple
rhythmic and
melodic
standard
notation.

Benchmark
GM.P
IL.5
I can explain
note names
and basic
rhythms.

Indicator
GM.P
NL.5.1
I can read
rhythm
patterns with
my voice,
body, and
instruments.

Indicator
GM.P
NM.5.1
I can name
notes in
treble clef.

Indicator
GM.P
NH.5.1
I can read
standard
notation.

Indicator
GM.P
IL.5.1
I can read all
notes in
treble and
bass clefs

Indicator
GM.P
NL.5.2
I can read
basic
rhythms.

Indicator
GM.P
NM.5.2
I can read
simple
quarter,
eighth, half,
whole notes
and rests.

Indicator
GM.P
NH.5.2
I can read
meter in
4/4, 3/4, and
2/4.

Indicator
GM.P
IL.5.2
I can read
basic
rhythms
including
dotted
rhythms.

Benchmark
GM.P
IM.5
I can
interpret
musical
symbols
within
multiple
meters, clefs,
and
expressive
symbols.
Indicator
GM.P
IM.5.1
I can read
alto/tenor
clef.

Indicator
GM.P
IM.5.2
I can identify
compound,
complex, and
syncopated
rhythms.

Benchmark
GM.P
IH.5
I can read
and notate
short musical
works in a
variety of
clefs and
meters.

Indicator
GM.P
IH.5.1
I can read
and use key
signatures.

Benchmark
GM.P
AL.5
I can sight
read a variety
of music at
Grade 2 with
technical
accuracy.

Indicator
GM.P
AL.5.1
I can sight
read musical
works in
simple
meters and
tonalities
with
technical
accuracy.
Indicator
Indicator
GM.P
GM.P
IH.5.2
AL.5.2
I can read
I can respond
and use meter to a director
signatures.
while sightreading.

Benchmark
GM.P
AM.5
I can sight
read a variety
of music at
Grade 3 with
technical
accuracy.

Benchmark
GM.P
AH.5
I can sight
read a variety
of music at
Grade 4 with
technical
accuracy.

Indicator
GM.P
AM.5.1
I can sight
read musical
works in a
variety of
keys and
clefs.

Indicator
GM.P
AH.5.1
I can sight
read musical
works in a
variety of
keys, clefs,
meters.

Indicator
GM.P
AM.5.2
I can apply
tempo and
dynamic
markings to
my sightreading.

Indicator
GM.P
AH.5.2
I can apply
expressive
music
markings to
my sightreading.

Page 221

Artistic Processes: Responding- I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Benchmark
GM.R
NL.6
I can identify
contrasts in
music.

Benchmark
GM.R
NM.6
I can identify
the elements
of music.

Benchmark
GM.R
NH.6
I can describe
how the
music
elements are
used.

Benchmark
GM.R
IL.6
I can explain
how the
elements of
music are
used in a
variety of
genres,
cultures, and
time periods.

Benchmark
GM.R
IM.6
I can
examine how
the elements
of music are
used in a
variety of
genres,
cultures, and
time periods.

Benchmark
GM.R
IH.6
I can find
evidence of
how music is
informed by
the structure,
elements in a
variety of
genres,
cultures, and
time periods.

Indicator
GM.R
NL.6.1
I can identify
dynamics and
steady beat.

Indicator
GM.R
NM.6.1
I can identify
changes in
dynamics,
tempo and
rhythm.

Indicator
GM.R
NH.6.1
I can use
appropriate
vocabulary to
describe
pitch,
tempo, and
dynamics.

Indicator
GM.R
IL.6.1
I can listen,
identify and
explain
pitch,
tempo, and
dynamics to
describe
music of
different
styles.

Indicator
GM.R
IM.6.1
I can
examine the
elements of
pitch,
tempo, and
dynamics in a
variety of
musical
styles
presented
aurally and
visually.

Indicator
GM.R
IH.6.1
I can listen,
identify, and
explain the
elements of
pitch,
tempo,
dynamics,
and style
used in
different
cultures and
time periods.

Benchmark
GM.R
AL.6
I can
examine the
use of
compositional
techniques
within
multiple
musical
works.

Benchmark
GM.R
AM.6
I can
collaborate
with others to
justify the
use of
compositional
techniques
within
musical
works.
Indicator
Indicator
GM.R
GM.R
AL.6.1
AM.6.1
I can describe I can
and use
collaborate
meter,
with others to
tonality,
determine
intervals,
intent of
chords, and
changes in
harmonic
meter,
progressions tonality and
when
harmony
analyzing
that
written and
contribute to
aural
musical style.
compositions
.

Benchmark
GM.R
AH.6
I can analyze
compositional
techniques, to
explain a
composer’s
intent.

Indicator
GM.R
AH.6.1
I can
determine the
intent of
changes in
meter,
tonality and
harmony in a
variety of
musical
compositions
to create
tension and
emotional
response.

Page 222

Indicator
GM.R
NL.6.2
I can identify
same and
different
sound
sources.

Indicator
GM.R
NM.6.2
I can name
voice types
and
instrument
families.

Indicator
GM.R
NH.6.2
I can identify
by sight and
sound voice
types and
classroom
instruments.

Indicator
GM.R
IL.6.2
I can listen to
and identify
orchestral,
band, and
electronic
instruments
by sight and
sound.

Indicator
GM.R
IM.6.2
I can
examine the
contribution
of timbre in
a variety of
musical
instruments/v
oices to
musical style
and mood.

Indicator
GM.R
IH.6.2
I can
examine the
use of timbre
and texture in
music from a
variety of
different
genres.

Indicator
GM.R
AL.6.2
I can
examine and
discuss
culturally
authentic
practices
found in
musical
works.

Indicator
GM.R
NL.6.3
I can name
same and
different
sections.

Indicator
GM.R
NM.6.3
I can identify
examples of
some basic
musical
forms.

Indicator
GM.R
NH.6.3
I can identify
examples of
complex
musical
forms.

Indicator
GM.R
IL.6.3
I can
examine
musical
forms to
describe a
musical style.

Indicator
GM.R
IM.6.3
I can identify
musical
forms
presented
aurally and
visually.

Indicator
GM.R
IH.6.3
I can
examine the
use of
musical
forms
presented in a
varied
repertoire of
music.

Indicator
GM.R
AL.6.3
I can analyze
and describe
how the use
of expressive
devices and
form are
used in
culturally and
historically
diverse
genres.

Indicator
GM.R
AM.6.2
I can identify
compositional
techniques
used to
achieve
unity,
variety,
tension and
release in
music to
evoke an
emotional
response
from the
listener.
Indicator
GM.R
AM.6.3
I can
examine the
use of
musical form
and
expressive
devices in a
variety of
20th & 21st
Century
compositions.

Indicator
GM.R
AH.6.2
I can examine
timbre and
the use of
voices,
instruments,
and other
sound sources
in a variety of
musical
styles,
cultures, and
genres.

Indicator
GM.R
AH.6.3
I can examine
the use of
musical form
when
analyzing
aural
examples of a
varied
repertoire of
music and
inform my
personal
music
preferences.

Page 223

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
NL.7
I can use my
words to talk
about music.

Benchmark
GM.R
NM.7
I can
demonstrate
how to be an
audience
member in
different
musical
settings.

Benchmark
GM.R
NH.7
I can use
musical
vocabulary to
describe
personal
preference
choices.

Benchmark
GM.R
IL.7
I can use
musical
vocabulary to
critique a
performance.

Benchmark
GM.R
IM.7
I can evaluate
the quality of
musical
performances
and/ or
compositions
of others
using
assessment
tools.

Benchmark
GM.R
IH.7
I can evaluate
the quality of
personal
performances
and/or
compositions
using
assessment
tools.

Benchmark
GM.R
AL.7
I can
collaborate to
develop
strategies for
improvement
of group
performances.

Indicator
GM.R
NL.7.1
I can listen
and respond
to music.

Indicator
GM.R
NM.7.1
I can model
and describe
audience
behavior in
different
settings.

Indicator
GM.R
NH.7.1
I can talk and
write about
music using
musical
vocabulary.

Indicator
GM.R
IL.7.1
I can describe
the quality of
a musical
performance.

Indicator
GM.R
IM.7.1
I can apply
assessment
tools to
evaluate tone
quality,
intonation,
articulation,
rhythmic
accuracy,
musicality,
posture, and
stage
presence to a
live or
recorded
performance.

Indicator
GM.R
IH.7.1
I can apply
assessment
tools to
evaluate tone
quality,
intonation,
articulation,
rhythmic
accuracy,
musicality,
posture, and
stage
presence to
my personal
performance.

Indicator
GM.R
AL.7.1
I can
compare a
group
performance
to a
benchmark to
refine the
performance.

Benchmark
GM.R
AM.7
I can make
critical
evaluations
of
performance,
compositions,
arrangements, and
improvisations.
Indicator
GM.R
AM.7.1
I can listen or
view a
variety of
performances
and offer
suggestions
for improvement.

Benchmark
GM.R
AH.7
I can justify
personal
performance
decisions.

Indicator
GM.R
AH.7.1
I can use
multiple
media sources
to critique my
personal
performances.

Page 224

Artistic Processes: Connecting- I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Benchmark
GM.C
NL.8
I can
recognize
and perform
musical
selections
from my own
culture and
some time
periods.

Benchmark
GM.C
NM.8
I can identify
and perform
musical
selections
from a
culture other
than mine
and a
historical
time period.

Benchmark
GM.C
NH.8
I can identify
and perform
musical
selections
from multiple
cultures
and/or
historical
time periods.

Benchmark
GM.C
IL.8
I can
examine
relationships
among
musical
selections
from multiple
cultures
and/or
historical
time periods.

Benchmark
GM.C
IM.8
I can research
the role of
music within
a specific
culture or
historical
time period
and present
what I
discovered.

Benchmark
GM.C
IH.8
I can perform
and modify a
musical work
using
characteristic
s from a
culture or
time period.

Benchmark
GM.C
AL.8
I can analyze
a diverse
repertoire of
music from a
cultural or
historical
time period.

Benchmark
GM.C
AM.8
I can
examine
contemporary
musical
works to
determine the
influence of
historical and
cultural
traditions.

Benchmark
GM.C
AH.8
I can examine,
create, and
perform music
based on
historical and
cultural
contributions.

Indicator
GM.C
NL.8.1
I can
recognize
that all
cultures and
time periods
use music.

Indicator
GM.C
NM.8.1
I can find
similar
elements of
music within
a culture/time
period.

Indicator
GM.C
NH.8.1
I can find
similar
elements of
music in
different
cultures/time
periods.

Indicator
GM.C
IL.8.1
I can identify
similarities
and
differences in
music from
multiple
cultures and
time periods.

Indicator
GM.C
IM.8.1
I can use
music
vocabulary
terms such as
form, tempo,
dynamics,
etc. to
describe
musical
works from
similar
cultures and
time periods.

Indicator
GM.C
IH.8.1
I can change
a musical
work using
the elements
of music
from a
culture or
time period.

Indicator
GM.C
AL.8.1
I can explain
specific
cultural and
historical
traditions and
infuse these
ideas into my
music.

Indicator
GM.C
AM.8.1
I can select
musical
elements in
contemporary
music that
reflect
cultural and
historical
influences.

Indicator
GM.C
AH.8.1
I can use
historical and
cultural
contributions
to justify my
musical
choices.

Page 225

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Benchmark
GM.C
NL.9
I can explore
general
music
concepts
among arts
disciplines
other content
areas and
related
careers in
familiar
settings.

Benchmark
GM.C
NM.9
I can
recognize
general
music
concepts
among arts
disciplines,
other content
areas and
related
careers.

Benchmark
GM.C
NH.9
I can apply
general
music
concepts to
arts
disciplines,
other content
areas and
related
careers
including
South
Carolina.

Benchmark
GM.C
IL.9
I can explore
a range of
skills shared
among arts
disciplines,
other content
areas and
how they can
be applied to
a career in
music.

Benchmark
GM.C
IM.9
I can
recognize
specific skills
shared
among arts
disciplines,
other content
areas and
how they can
be applied to
a career in
music.

Benchmark
GM.C
IH.9
I can analyze
the tools,
concepts, and
materials
used among
arts
disciplines,
other content
areas and
how they are
used in music
careers.

Indicator
GM.C
NL.9.1
I can identify
the
relationship
between
music and
another
subject in my
school.

Indicator
GM.C
NM.9.1
I can make
connections
between
music and
another
subject in my
school.

Indicator
GM.C
NH.9.1
I can
demonstrate
and describe
the
relationship
between
music and a
concept from
another
subject in my
school.

Indicator
GM.C
IL.9.1
I can apply
music
concepts to
other arts
disciplines
and content
areas.

Indicator
GM.C
IM.9.1
I can
examine the
relationship
between
music and
specific
content from
another arts
discipline
and content
area.

Indicator
GM.C
IH.9.1
I can apply
concepts
from other
arts
disciplines
and content
areas to my
music.

Benchmark
GM.C
AL.9
I can apply
concepts
among arts
disciplines
and other
content areas
to general
music and
analyze how
my interests
and skills
will prepare
me for a
career.
Indicator
GM.C
AL.9.1
I can explain
ideas from
other arts
disciplines
and content
areas through
music.

Benchmark
GM.C
AM.9
I can explain
how
economic
conditions,
cultural
values and
location
influence
music and the
need for
music related
careers.

Benchmark
GM.C
AH.9
I can research
societal
political and
cultural issues
as they relate
to other arts
and content
areas and
apply to my
role as a
musician.

Indicator
GM.C
AM.9.1
I can explain
how my
artistic
choices are
influenced by
cultural and
social values.

Indicator
GM.C
AH.9.1
I can analyze
complex
ideals that
influence my
artistic
perspective
and creative
work.

Page 226

Indicator
GM.C
NL.9.2
I can identify
topics in
music that
interest me.

Indicator
GM.C
NM.9.2
I can identify
life skills
necessary for
a music
career.

Indicator
GM.C
NH.9.2
I can identify
specific
careers in
music.

Indicator
GM.C
IL.9.2
I can
demonstrate
and describe
the skills
needed for
careers in
music.

Indicator
GM.C
IM.9.2
I can
examine the
educational
requirements
needed for a
variety of
careers in
music.

Indicator
GM.C
IH.9.2
I can
compare
similarities
and
differences in
a variety of
music careers
and roles of
musicians in
those careers.

Indicator
GM.C
AL.9.2
I can identify
and describe
traditional
and emerging
careers in
music.

Indicator
GM.C
AM.9.2
I can discuss
the impact of
economic
issues as they
affect the
impact on
music
careers.

Indicator
GM.C
AH.9.2
I can analyze
my personal
career choices
in the arts or
non-arts
disciplines.

Page 227

Novice General Music Standards
Artistic Processes: Creating-I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Novice
Low
Benchmark
GM.CR
NL.1
I can imitate a musical statement by sight and
sound.
Indicator
GM.CR
NL.1.1

Novice
Mid
Benchmark
GM.CR
NM.1
I can answer a musical question.

I can match sound and pattern.

I can identify simple forms.

Sample Learning Targets

Sample Learning Targets



I can clap a repeated rhythm.



I can imitate sounds using my voice.



I can use found sounds to create a
composition.



I can…

Novice
High
Benchmark
GM.CRNH.1
I can arrange a musical idea.

Indicator
GM.CR
NM.1.1



I can identify call and response as a
musical form.



I can identify AB/ABA form in simple
songs.



I can...

Indicator
GM.CR
NH.1.1
I can use rhythm patterns, songs or words to
create a musical idea.
Sample Learning Targets


I can arrange a sound poem.



I can arrange a melodic idea on a
barred instrument.



I can arrange rhythm patterns with
flashcards.



I can use movement to show melodic
contour.



I can…

Page 228

Indicator
GM.CR
NL.1.2
I can imitate a given music rhythm or sound
using symbols.

Indicator
GM.CR
NM.1.2
I can identify same and different patterns.

Indicator
GM.CR
NH.1.2
I can create a musical idea given specific
instructions.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can use symbols to show a rhythm
pattern.



I can give a rhythmic response to a
rhythm question.



I can create rhythmic compositions
using standard notation.



I can compose by drawing icons to
represent music beats.



I can identify same/different melodic
and rhythmic patterns.



I can use notation technology to
arrange musical patterns.



I can...



I can identify patterns of
same/different in simple songs and
patterns.



I can use movement to share a musical
idea.





I can...

I can use technology to create musical
ideas.



I can...

Anchor Standard 2: I can improvise music.
Novice
Low
Benchmark
GM.CR
NL.2
I can imitate simple rhythm patterns within a
given meter.

Novice
Mid
Benchmark
GM.CR
NM.2
I can imitate simple tonal patterns within a
given key and tonality.

Novice
High
Benchmark
GM.CR
NH.2
I can improvise responses to given rhythmic
patterns.

Page 229

Indicator
GM.CR
NL.2.1
I can identify same and different rhythms
patterns.

Indicator
GM.CR
NM.2.1
I can identify same and different melodic
patterns.

Indicator
GM.CR
NH.2.1
I can improvise simple ostinati patterns
within a given meter.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can use a counting system to decide
same and different.



I can listen to two melodic samples
and label same and different.



I can improvise an ostinato rhythm
pattern on an instrument.



I can listen to two rhythm samples and
label same and different.





I can use a counting system to
improvise a rhythm pattern.



I can sing a melodic pattern that is the
same and one that is different from the
one I hear.

I can...



I can...



I can...

Indicator
GM.CR
NL.2.2
I can echo simple rhythm patterns.

Indicator
GM.CR
NM.2.2
I can echo simple tonal patterns.

Indicator
GM.CR
NH.2.2
I can improvise rhythm patterns, songs or
chants to create a musical idea.

Sample Learning Targets
 I can use body percussion to echo a
rhythm pattern.

Sample Learning Targets
 I can echo simple tonal patterns using
tonal solfege or pitch name.

Sample Learning Targets
 I can write new words for a familiar
tune.



I can echo simple rhythm patterns on
an instrument.



I can echo simple tonal patterns on a
neutral syllable.



I can improvise a chant for a given
rhythmic pattern.



I can use chants to echo a rhythm
pattern.



I can echo simple tonal patterns on an
instrument.



I can...



I can...



I can...

Page 230

Artistic Processes: Performing-I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Novice
Low
Benchmark
GM.P
NL.3
I can use my voice in many ways.

Novice
Mid
Benchmark
GM.P
NM.3
I can use my singing voice.

Novice
High
Benchmark
GM.P
NH.3
I can sing, chant, and move to demonstrate a
steady beat.

Indicator
GM.P
NL.3.1
I can sing songs in my range.

Indicator
GM.P
NM.3.1
I can match pitch when I sing.

Indicator
GM.P
NH.3.1
I can sing or move using a steady beat.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can echo, speak, chant, sing and
move to music.



I can match pitch with piano or my
teacher.



I can sing songs from memory.



I can sing songs in unison.



I can...



I can...



I can identify beat/no beat.



I can use rhythm sticks to demonstrate
steady beat.



I can move to a steady beat following
a conductor.



I can...

Page 231

Indicator
GM.P
NL.3.2
I can use my voice to imitate other sounds.

Indicator
GM.P
NM.3.2
I can sing with my head voice and chest voice.

Indicator
GM.P
NH.3.2
I can use good posture and breath support
when I sing.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can make sound effects with my
voice.



I can sing with a group.



I can explore different sounds with
voice changing software.



I can sing patriotic songs.



I can sing folk songs.



I can echo short melodic patterns.



I can...



I can demonstrate good singing
posture.



I can use basic dynamics (piano and
forte) when I sing.



I can...

Anchor Standard 4: I can play instruments alone and with others.
Novice
Low
Benchmark
GM.P
NL.4
I can make sounds with classroom instruments
and other sound sources.
Indicator
GM.P
NL.4.1
I can use my body to make sounds.

Novice
Mid
Benchmark
GM.P
NM.4
I can imitate short rhythmic patterns.
Indicator
GM.P
NM.4.1
I can echo an ostinato rhythm pattern.

Novice
High
Benchmark
GM.P
NH.4
I can play and read rhythmic, melodic, and
chord patterns.
Indicator
GM.P
NH.4.1
I can use music notation to play instruments.

Sample Learning Targets
 I can echo your pattern by clapping.

Sample Learning Targets
 I can play classroom instruments to
follow a rhythm pattern.

Sample Learning Targets
 I can play guitar following a chord
sheet.



I can patch, clap, and stomp a pattern.



I can...



I can play an ostinato on a classroom
instrument.



I can play recorder from notation B,
A, & G.



I can play djembes in a Drum circle.



I can...
Page 232

Indicator
GM.P
NL.4.2
I can play pitched and unpitched instruments.

Indicator
GM.P
NM.4.2
I can play melodic patterns using steps and
skips.

Indicator
GM.P
NH.4.2
I can play pentatonic scales on instruments.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can echo your patterns with my
rhythm sticks.



I can play xylophones and other
pitched instruments.



I can use instruments to make sound
stories.



I can...









I can select the correct notes to make a
pentatonic scale on a barred
instrument.

I can use classroom instruments or
sound software to create sound stories.



I can play leaps/skips/steps on barred
instruments.

I can identify a skip and step on a
barred instrument.



I can...

I can...

Indicator
GM.P
NL.4.3
I can follow the teacher when I use classroom
instruments.

Indicator
GM.P
NM.4.3
I can ask and answer musical questions
using instruments.

Indicator
GM.P
NH.4.3
I can identify rhythmic notation.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can follow my teacher’s pattern on
my rhythm instrument.



I can respond to a musical question on
a classroom instrument.



I can identify quarter, eighth, half, and
whole notes and their rests.



I can start and stop with the conductor.







I can write and perform a 4-beat
answer to a 4-beat question.

I can play a rhythm pattern from
notation.

I can...



I can...



I can...

Page 233

Anchor Standard 5: I can read and notate music.
Novice
Low
Benchmark
GM.P
NL.5
I can read rhythm patterns.
Indicator
GM.P
NL.5.1
I can read rhythm patterns with my voice,
body, and instruments.
Sample Learning Targets


I can read quarter notes/rests.



I can identify beat/no beat.





I can notate one and two sounds to a
beat.

I can...

Novice
Mid
Benchmark
GM.P
NM.5
I can read simple rhythmic and melodic
notation.
Indicator
GM.P
NM.5.1
I can name notes in treble clef.

Novice
High
Benchmark
GM.P
NH.5
I can read, write simple rhythmic and melodic
standard notation.
Indicator
GM.P
NH.5.1
I can read standard notation.

Sample Learning Targets

Sample Learning Targets






I can read notes from the staff in treble
clef.



I can write simple melodic and
rhythmic notation.

I can use technology to practice
reading and writing simple notation.



I can write music notation using
music software.

I can identify a second, third, fifth, and
octave.



I can write the notes for “Twinkle,
Twinkle Little Star” on staff paper.



I can...

I can...

Page 234

Indicator
GM.P
NL.5.2
I can read basic rhythms.

Indicator
GM.P
NM.5.2
I can read simple quarter, eighth, half, whole
notes, and rests.

Indicator
GM.P
NH.5.2
I can read meter in 4/4, 3/4, and 2/4.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can clap a rhythm pattern from
traditional/non-traditional notation.



I can play a rhythm pattern from
notation on a non-pitched instrument.



I can…



I can complete measures using the
correct rhythmic notation for a given
meter signature.



I can add missing bar lines for given
meters.



I can...



I can write simple melodic and
rhythmic notation.



I can conduct music in 4/4.



I can write the counting for simple
rhythms to include sixteenth notes.



I can write music notation using
music software.



I can...

Artistic Processes: Responding-I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Novice
Low
Benchmark
GM.R
NL.6
I can identify contrasts in music.

Novice
Mid
Benchmark
GM.R
NM.6
I can identify the elements of music.

Novice
High
Benchmark
GM.R
NH.6
I can describe how the music elements are
used.

Page 235

Indicator
GM.R
NL.6.1
I can identify dynamics and steady beat.

Indicator
GM.R
NM.6.1
I can identify changes in dynamics, tempo and
rhythm.

Indicator
GM.R
NH.6.1
I can use appropriate vocabulary to describe
pitch, tempo, and dynamics.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can clap or march to a steady beat.







I can use movement to express
dynamics in music.

I can identify dynamic contrasts by
sight and sound.

I can show loud and soft with body
movements.



I can identify same and different
rhythm patterns.





Given two pitches, I can name the
second as higher, lower, or the same as
the first.

I can move to changes of tempo from
a recording or sound source.



I can...



I can …



I can...

Indicator
GM.R
NL.6.2
I can identify same and different sound
sources.

Indicator
GM.R
NM.6.2
I can name voice types and instrument
families.

Indicator
GM.R
NH.6.2
I can identify by sight and sound voice types
and classroom instruments.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can name different sounds from my
environment to create a sound poem.



I can identify a child, adult woman,
and adult male voice.



I can identify whisper, speaking,
shouting, or singing voice.



I can name classroom instruments by
sight and sound.

I can select a class instrument to
imitate the sound of a clock, horse’s
feet, rain, etc.







I can...



I can classify classroom instruments
into families by sight and sound
(woods, metals, shakers, etc.).



I can identify soprano, alto, and tenor,
bass.



I can...

I can…
Page 236

Indicator
GM.R
NL.6.3
I can name same and different sections.

Indicator
GM.R
NM.6.3
I can identify examples of some basic musical
forms.

Indicator
GM.R
NH.6.3
I can identify examples of complex musical
forms.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can show movements to music to
demonstrate same and different.



I can use shapes to identify sections of
music.



I can …



I can identify ABA form.





I can identify verse and refrain from
the music and by hearing it.

I can identify repeated sections to label
simple forms.



I can identify motif, canon, rondo,
AABA, and theme and variations as a
musical form.



I can...



I can sing the response to a call and
response song.



I can...

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
NL.7
I can use my words to talk about music.
Indicator
GM.R
NL.7.1
I can listen and respond to music.

Sample Learning Targets

Benchmark
GM.R
NM.7
I can demonstrate how to be an audience
member in different musical settings.
Indicator
GM.R
NM.7.1
I can model and describe audience behavior in
different settings.

Benchmark
GM.R
NH.7
I can use musical vocabulary to describe
personal preference choices.
Indicator
GM.R
NH.7.1
I can talk and write about music using musical
vocabulary.

Sample Learning Targets

Sample Learning Targets



I can talk about music I listen to at
home.



I can applaud at appropriate times
during a concert.



I can use music vocabulary to describe
what I like in a song.



I can use my words to tell you the
music I like. I can…



I can sit quietly during a performance.



I can talk about a performance using
musical vocabulary. I can…
Page 237

Artistic Processes: Connecting-I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Novice
Low
Benchmark
GM.C
NL.8
I can recognize and perform musical selections
from my own culture and some time periods.

Novice
Mid
Benchmark
GM.C
NM.8
I can identify and perform musical selections
from a culture other than mine and a historical
time period.

Novice
High
Benchmark
GM.C
NH.8
I can identify and perform musical selections
from multiple cultures and/or historical time
periods.

Indicator
GM.C
NL.8.1
I can recognize that all cultures and time
periods use music.

Indicator
GM.C
NM.8.1
I can find similar elements of music within a
culture/time period.

Indicator
GM.C
NH.8.1
I can find similar elements of music in
different cultures/time periods.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can share music ideas with friends.



I can sing and talk about folk music.





I can name musical titles used for at
least one cultural event.



I can name the historical periods of
music.

I can name traditions from a culture
other than mine that uses music.



I can name composers from the
Classical Period (Mozart, Beethoven).



I can identify instruments from
multiple cultures.

I can identify patriotic music and its
purpose.



I can...



I can define a variety of cultures and
their use of music.

I can describe characteristics of the
Classical Period (balance, order,
following rules).



I can...






I can...

Page 238

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Novice
Low
Benchmark
GM.C
NL.9
I can explore general music concepts among
arts disciplines other content areas and related
careers in familiar settings.

Novice
Mid
Benchmark
GM.C
NM.9
I can recognize general music concepts among
arts disciplines, other content areas and related
careers.

Novice
High
Benchmark
GM.C
NH.9
I can apply general music concepts to arts
disciplines, other content areas and related
careers.

Indicator
GM.C
NL.9.1
I can identify the relationship between music
and another subject in my school.

Indicator
GM.C
NM.9.1
I can make connections between music and
another subject in my school.

Indicator
GM.C
NH.9.1
I can demonstrate and describe the relationship
between music and a concept from another
subject in my school.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets





I can identify songs that will help me
in another subject (ex. alphabet song).



I can write a parody to help me learn
the continents.

I can tell a story with found sounds or
computer generated sound.



I can sing the Alphabet song to
alphabetize words.



I can...

I can...



I can talk about music’s relationship to
the science of sound.



I can use music as a tool to learn about
fractions.



I can name some South Carolina
musicians.



I can...

Page 239

Indicator
GM.C
NL.9.2
I can identify topics in music that interest me.

Indicator
GM.C
NM.9.2
I can identify life skills necessary for a music
career.

Indicator
GM.C
NH.9.2
I can identify specific careers in music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can give examples of careers and
events that use music.



I can describe how my musical
performance improved.



I can name arts businesses and
organizations that hire musicians.



I can describe special types of music
for special events or times of year and
the people involved.



I can describe proper performer and
audience behavior for a concert.



I can describe music careers of
community members.



I can work with others to improve my
performance.



I can make a list of music careers.





I can...

I can...



I can...

Page 240

Intermediate General Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Intermediate
Low
Benchmark
GM.CR
IL.1

Intermediate
Mid
Benchmark
GM.CR
IM.1

Intermediate
High
Benchmark
GM.CR
IH.1

I can combine musical ideas to create phrases
for voice, instruments, or body movement.

I can compose a rhythmic and melodic phrase.

I can add harmony to compose or arrange
phrases for a given mood.

Indicator
GM.CR
IL.1.1
I can explain the use of ostinato to arrange a
melodic idea.

Indicator
GM.CR
IM.1.1
I can organize rhythmic and melodic patterns
into a musical phrase.

Indicator
GM.CR
IH.1.1
I can identify key signatures in melodic
phrases.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can use notation technology to
arrange musical patterns.



I can write a musical phrase for an
instrument.



I can create an ostinato pattern to a
simple song.



I can combine patterns to make a
musical phrase.

I can use technology to create musical
ideas.






I can...



I can add harmony to a familiar tune
to demonstrate knowledge of proper
key structure.



I can research appropriate keys for
specific instruments.



I can...

I can…

Page 241

Indicator
GM.CR
IL.1.2
I can construct arrangements of simple pieces
for voices or instruments.

Indicator
GM.CR
IM.1.2
I can create a melodic phrase over a given
rhythmic idea.

Indicator
GM.CR
IH.1.2
I can construct a rhythmic, melodic and
harmonic idea for a given mood.

Sample Learning Targets
 I can arrange a familiar song for an
instrument.

Sample Learning Targets
 I can create a melody using the
pentatonic scale.

Sample Learning Targets
 I can add harmony to a familiar tune
to change the mood.



I can use standard notation to
compose an arrangement of a tune.



I can use sol, mi, do over an ostinato
rhythm pattern to make a melody.



I can...



I can…



I can write a short chorale.



I can use music writing software to
compose or arrange my work.



I can...

Anchor Standard 2: I can improvise music.
Intermediate
Low
Benchmark
GM.CR
IL.2
I can improvise short melodic question and
answer patterns.

Intermediate
Mid
Benchmark
GM.CR
IM.2
I can improvise a rhythm pattern to embellish
a given a harmonic phrase.

Intermediate
High
Benchmark
GM.CR
IH.2
I can improvise a simple melodic phrase given
a harmonic phrase.

Page 242

Indicator
GM.CR
IL.2.1
I can improvise simple tonal patterns within a
given key.

Indicator
GM.CR
IM.2.1
I can embellish a bass line with improvised
rhythm from an instrument or music software.

Indicator
GM.CR
IH.2.1
I can identify chord changes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can improvise a scat tune.







I can play an improvised bass line on
a barred instrument.

I can identify chord changes to
improvise a short melody.

I can improvise my own simple tonal
patterns on my instrument.



I can improvise a rhythmic bass line
for a folk song on instruments or using
a music app.



I can identify different chord patterns
on a staff line.



I can write a I, IV, V chord
progression using notation.



I can...




I can improvise a new ending for a
familiar tune.



I can...

I can...

Indicator
GM.CR
IL.2.2
I can improvise rhythmic and melodic patterns
to create a musical phrase.

Indicator
GM.CR
IM.2.2
I can sing on a neutral syllable an improvised
rhythm.

Indicator
GM.CR
IH.2.2
I can embellish a given melodic phrase that
corresponds with simple chord changes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can scat to a jazz tune.





I can sing a familiar song with new
rhythmic patterns.

I can improvise my own simple tonal
patterns on my instrument.



I can improvise a new rhythm to end a
song.



I can...



I can improvise a new ending for a
familiar tune.



I can...



I can improvise a melody over a
recorded 12 bar blues progression.

I can improvise a scat tune.
 I can...

Page 243

Artistic Processes: Performing-I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Intermediate
Low
Benchmark
GM.P
IL.3
I can sing a variety of simple part songs.

Intermediate
Mid
Benchmark
GM.P
IM.3
I can sing with expression and technical
accuracy.

Intermediate
High
Benchmark
GM.P
IH.3
I can sing a variety of songs with expression
and technical accuracy.

Indicator
GM.P
IL.3.1
I can sing 2-part songs.

Indicator
GM.P
IM.3.1
I can sing with proper intonation alone and in
different ensembles.

Indicator
GM.P
IH.3.1
I can sing phrasing while responding to a
director’s cues.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can sing partner songs.



I can sing my part in an ensemble.



I can sing a round or canon.



I can sing intervals.



I can...



I can match pitch with a teacher or
pitch matching app.



I can sight read simple songs.



I can...



I can follow a conductor’s beat and
dynamic direction.



I can recognize from a score and sing
at various dynamic levels - p, mp, mf,
f.



I can...

Page 244

Indicator
GM.P
IL.3.2
I can sing with appropriate diction and
articulation.

Indicator
GM.P
IM.3.2
I can apply dynamics and expression when I
sing.

Indicator
GM.P
IH.3.2
I can sight read in multiple tonalities and
rhythms.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can imitate my teacher’s
pronunciation.



I can respond to director cues to use
dynamics and expression.



I can sing legato and staccato styles.





I can recognize and respond to
expressive markings in the music.

I can...





I can sight read my part in tune.



I can sing in 3-part harmony.



I can sight read music selected for
performance and pinpoint areas of
needed improvement.



I can...

I can...

Anchor Standard 4: I can play instruments alone and with others.
Intermediate
Low
Benchmark
GM.P
IL.4
I can play accompaniments and simple songs
on classroom instruments.

Intermediate
Mid
Benchmark
GM.P
IM.4
I can play and read complimentary and
contrasting instrumental parts accurately and
independently.

Intermediate
High
Benchmark
GM.P
IH.4
I can play and read my part with an ensemble
using accurate technique and posture.

Page 245

Indicator
GM.P
IL.4.1
I can play accompaniments and songs in
major and minor tonalities.

Indicator
GM.P
IM.4.1
I can play my part independently in an
ensemble.

Indicator
GM.P
IH.4.1
I can rehearse for improvement in an
ensemble.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can play parts using notation.







I can read my part with correct rhythm
and tempo.

I can practice in an ensemble using
rhythmic and melodic notation.

I can identify by sound - major/minor
tonalities.



I can explain how to practice my part
for improvement.



I can practice with a recording and
improve my ensemble part.



I can…



I can…



I can...

Indicator
GM.P
IL.4.2
I can play using proper technique and posture.

Indicator
GM.P
IM.4.2
I can play my instrument with technical
accuracy.

Indicator
GM.P
IH.4.2
I can use proper technique to express music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can play barred instruments with
correct mallet technique.



I can look at myself in the mirror to
play with proper posture.



I can...



I can complete a rubric that includes
posture and intonation about my
playing.



I can use proper technique for a
chosen instrument to perform in an
ensemble.



I can...



I can apply correct articulation to my
music. (legato, staccato, etc.)



I can...

Page 246

Indicator
GM.P
IL.4.3
I can play in treble and bass clefs.

Indicator
GM.P
IM.4.3
I can read from notation, songs I play.

Sample Learning Targets

Sample Learning Targets

Indicator
GM.P
IH.4.3
I can play my part independently while others
play.
Sample Learning Targets



I can identify treble and bass clef
notation.



I can read my part with correct rhythm
and tempo.



I can read rhythmic and melodic
notation.



I can identify the correct clef to play a
selected instrument.



I can play syncopated patterns from
notation.



I can practice in an ensemble.







I can...

I can...

I can...

Anchor Standard 5: I can read and notate music.
Intermediate
Low
Benchmark
GM.P
IL.5
I can explain note names and basic rhythms.

Intermediate
Mid
Benchmark
GM.P
IM.5
I can interpret musical symbols within multiple
meters, clefs, and expressive symbols.

Intermediate
High
Benchmark
GM.P
IH.5
I can read and notate short musical works in a
variety of clefs and meters.

Indicator
GM.P
IL.5.1
I can read all notes in treble and bass clefs.

Indicator
GM.P
IM.5.1
I can read alto/tenor clef.

Indicator
GM.P
IH.5.1
I can read and use key signatures.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can read treble clef notes.



I can explain duple and triple meter.




I can count 6/8 meter.
I can...



I can identify the notes in all
traditional clefs.



I can rewrite a treble clef passage in
another clef.



I can...



I can write the circle of fifths. I can
identify appropriate accidentals for a
given key.
I can identify enharmonic pitches.



I can...
Page 247

Indicator
GM.P
IL.5.2
I can read basic rhythms including dotted
rhythms.

Indicator
GM.P
IM.5.2
I can identify compound, complex, and
syncopated rhythms.

Indicator
GM.P
IH.5.2
I can read and use meter signatures.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can read treble clef notes.



I can notate a syncopated phrase.





I can explain duple and triple meter.



I can read songs in 5/8, 7/8, 9/8, and
2/2 meters.



I can apply a rhythmic counting
system to compound rhythms.



I can count 6/8 meter.

I can demonstrate basic conducting
patterns.



I can...





I can...

I can...

Artistic Processes: Responding-I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Intermediate
Low
Benchmark
GM.R
IL.6
I can explain how the elements of music are
used in a variety of genres, cultures, and time
periods.

Intermediate
Mid
Benchmark
GM.R
IM.6
I can examine how the elements of music are
used in a variety of genres, cultures, and time
periods.

Intermediate
High
Benchmark
GM.R
IH.6
I can find evidence of how music is informed
by the structure, elements in a variety of
genres, cultures, and time periods.

Page 248

Indicator
Indicator
GM.R
GM.R
IL.6.1
IM.6.1
I can listen, identify, and explain pitch, tempo, I can examine the elements of pitch, tempo,
and dynamics to describe music of different
and dynamics in a variety of musical styles
styles.
presented aurally and visually.

Indicator
GM.R
IH.6.1
I can listen, identify, and explain the elements
of pitch, tempo, dynamics, and style used in
different cultures and time periods.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify and explain music
characteristics from Native American
and Hispanic cultures.



While listening, I can name
characteristics of two different musical
styles.



I can identify and explain music from
the Baroque and Romantic Period.



I can compare use of rhythm between
at least 2 different cultures.



I can write the common tempi terms in
order.





I can compare and contrast how
elements of music are used in rap and
jazz.

I can...





I can name the characteristics of the
music from the Baroque Period.



I can explain the use of polyrhythms in
African music examples.



I can identify major and minor
tonality from aural and written
examples.



I can…

I can...

Page 249

Indicator
GM.R
IL.6.2

Indicator
GM.R
IM.6.2

Indicator
GM.R
IH.6.2

I can listen to and identify orchestral, band,
and electronic instruments by sight and sound.

I can examine the contribution of timbre in a
variety of musical instruments/voices to
musical style and mood.

I can examine the use of timbre and texture in
music from a variety of different genres.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify electronic instruments by
sight and sound.



I can choose an instrument to best fit a
musical style and mood.



I can compare and contrast the timbre
and texture of folk and opera.



I can identify the common orchestra
and band instruments by sight and
sound.



I can identify instruments that
contribute to a variety of non-classical
styles such as dobro, mandolin, fiddle.



I can identify the differences between
the timbre and texture of a string
versus full orchestra.



I can name instruments in each
musical family.





I can...



I can describe different vocal
techniques that support mood in rap,
country, pop, jazz.

I can...



I can...

Indicator
GM.R
IL.6.3
I can examine musical forms to describe a
musical style.

Indicator
GM.R
IM.6.3
I can identify musical forms presented aurally
and visually.

Indicator
GM.R
IH.6.3
I can examine the use of musical forms
presented in a varied repertoire of music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify verse and refrain in a
folk song.



I can use a labeling system to identify
a musical form.



I can use music vocabulary to justify
my choice of music time period.



I can determine the form of the “Star
Spangled Banner”.



I can identify the musical form from a
musical score.



I can study a score to cite examples of
compositional techniques.



I can...



I can name the musical form of
familiar music.



I can...
Page 250

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
IL.7
I can use musical vocabulary to critique a
performance.

Benchmark
GM.R
IM.7
I can evaluate the quality of musical
performances and/ or compositions of others
using assessment tools.

Benchmark
GM.R
IH.7
I can evaluate the quality of personal
performances and/or compositions using
assessment tools.

Indicator
GM.R
IL.7.1
I can describe the quality of a musical
performance.

Indicator
GM.R
IM.7.1
I can apply assessment tools to evaluate tone
quality, intonation, articulation, rhythmic
accuracy, musicality, posture, and stage
presence to a live or recorded performance.

Indicator
GM.R
IH.7.1
I can apply assessment tools to evaluate tone
quality, intonation, articulation, rhythmic
accuracy, musicality, posture, and stage
presence to my personal performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can write a response to a music
performance using music vocabulary.



I can apply a rubric/checklist to
evaluate a performance.



I can respond appropriately to outside
criticism of my performance.



I can name a strength and a weakness
from a performance.



I can name and define criteria from a
rubric.



I can discuss areas of needed practice
to improve my performance.



I can...



I can view or listen to recordings to
complete a rubric and suggest ideas for
improvement.



I can...



I can...

Page 251

Artistic Processes: Connecting- I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Intermediate
Low
Benchmark
GM.C
IL.8
I can examine relationships among musical
selections from multiple cultures and/or
historical time periods.
Indicator
GM.C
IL.8.1
I can identify similarities and differences in
music from multiple cultures and time periods.

Sample Learning Targets


I can describe the difference between
African and Native American music.






Intermediate
Mid
Benchmark
GM.C
IM.8
I can research the role of music within a
specific culture or historical time period and
present what I discovered.
Indicator
GM.C
IM.8.1
I can use music vocabulary terms such as
form, tempo, dynamics, etc. to describe
musical works from similar cultures and time
periods.
Sample Learning Targets


I can research an historical period and
report it to the class including musical
examples.

I can use music vocabulary to describe
a culture (ex. African - polyrhythms,
characteristic instruments).



I can describe the music of historical
composers using the elements of
music in my descriptions.

I can compare and contrast the time
period of Baroque and Classical.



I can...

Intermediate
High
Benchmark
GM.C
IH.8
I can perform and modify a musical work
using characteristics from a culture or time
period.
Indicator
GM.C
IH.8.1
I can change a musical work using the
elements of music from a culture or time
period.
Sample Learning Targets


I can use technology to arrange “Jingle
Bells” to reflect a different culture.



I can apply changes to a Classical
piece of music to reflect a different
time period.



I can...

I can...

Page 252

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Intermediate
Low
Benchmark
GM.C
IL.9
I can explore a range of skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Intermediate
Mid
Benchmark
GM.C
IM.9
I can recognize specific skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Intermediate
High
Benchmark
GM.C
IH.9
I can analyze the tools, concepts, and materials
used among arts disciplines, other content
areas and how they are used in music careers.

Indicator
GM.C
IL.9.1
I can apply music concepts to other arts
disciplines and content areas.

Indicator
GM.C
IM.9.1
I can examine the relationship between music
and specific content from another arts
discipline and content area.

Indicator
GM.C
IH.9.1
I can apply concepts from other arts disciplines
and content areas to my music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

I can make musical connections to the art
works of Kandinsky.
 I can define words with multiple
meanings in music and other subjects.




I can use tone color, pattern, texture,
etc.to talk about music and other arts
disciplines.
I can...



I can compare and contrast mood in
music, art, dance, and drama.



I can use music to complete a STEM
project.



I can relate music of the ‘50s to its
historical context.





I can trace the connection of the
American Revolution to army bands
and their music.

I can examine the relationship between
an element of music and other
disciplines including other arts
disciplines.



I can...



I can...

Page 253

Indicator
GM.C
IL.9.2
I can demonstrate and describe the skills
needed for careers in music.

Indicator
GM.C
IM.9.2
I can examine the educational requirements
needed for a variety of careers in music.

Indicator
GM.C
IH.9.2
I can compare similarities and differences in a
variety of music careers and roles of musicians
in those careers.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets





I can research skills needed for a
variety of music careers.



I can investigate and report about
music careers in SC.
I can...



I can name careers in music and
research the requirements for the
chosen career.
I can examine the requirements of a
music producer.
I can identify college degree programs
for music therapy.



I can name and describe skills
necessary for college study of music
leading to a job or profession.



I can…



I can use technology to compare
required skills for music careers.



I can identify common music skills
needed in different music careers.



I can...

Page 254

Advanced General Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Advanced
Low
Benchmark
GM.CR
AL.1
I can arrange, compose, and explain intent
using melody, rhythm, and harmony.

Advanced
Mid
Benchmark
GM.CR
AM.1
I can collaborate with others to compose or
arrange a variety of musical styles.

Advanced
High
Benchmark
GM.CR
AH.1
I can compose music within expanded forms.

Indicator
GM.CR
AL.1.1
I can use the circle of fifths to explain
transposition of a written musical work.

Indicator
GM.CR
AM.1.1
I can revise a composition based on the
feedback from others to improve composed
works.

Indicator
GM.CR
AH.1.1
I can create an original composition
independently.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can use the circle of fifths to
compose a work in complementary
keys.



I can use a technology system to
notate and transpose my work.



I can transpose a single part for two
different instruments.



I can record my composition to revise
based on feedback.



I can compose a concerto for my
instrument.



I can use anchor compositions from
known composers to compare stylistic
techniques.



I can write a song.



I can use technology storage systems
to organize my compositions.



I can...



I can...

Page 255

Indicator
GM.CR
AL.1.2
I can use and explain compositional techniques
to compose works in a musical form.

Indicator
GM.CR
AM.1.2
I can work with others to compose an original
composition.

Indicator
GM.CR
AH.1.2
I can create a new arrangement from a given
composition.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets






I can explain common music forms,
(for example: verse-refrain, AB,
ABA).
I can compose a short work in a given
music form.



I can use theme and variations to
compose a work with others.



I can use technology to collaborate
with team members while composing.



I can...

I can...



I can arrange a work for an ensemble.



I can use technology to store and
organize my compositions.



I can arrange a composition for an
alternative instrument.



I can...

Anchor Standard 2: I can improvise music.
Advanced
Low
Benchmark
GM.CR
AL.2
I can perform a brief improvisation given a
chord progression and meter.

Advanced
Mid
Benchmark
GM.CR
AM.2
I can perform an improvisation given a
motive, chord progression, and meter.

Advanced
High
Benchmark
GM.CR
AH.2
I can perform and refine an extended
spontaneous improvisation independently.

Page 256

Indicator
GM.CR
AL.2.1
I can improvise harmonizing parts.

Indicator
GM.CR
AM.2.1
I can perform an improvisation on a given
motive.

Indicator
GM.CR
AH.2.1
I can improvise responding to aural cues.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify chord changes in an
unfamiliar piece to improvise on an
instrument.



I can vocally improvise harmonic
embellishments over a given melody.



I can...



I can improvise stylistically
appropriate harmonizing parts.



I can perform on an instrument, music
apps, or sing an improvised part in an
ensemble.


Indicator
GM.CRAL.2.2

I can...



I can improvise rhythmic and melodic
variations on given melodies in
pentatonic, major, and minor keys.



I can improvise with freedom and
expression within a given key,
tonality, meter, and style.



I can...

I can improvise short melodies using accurate
and consistent style, meter, and tonality.

Indicator
GM.CR
AM.2.2
I can improvise extended passages using
consistent style, meter, and tonality.

Indicator
GM.CRAH.2.2
I can demonstrate and refine musicality during
improvisational solos.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can improvise a short passage using
only a chord progression or lead sheet.



I can follow a lead sheet to perform an
extended improvisation pattern.



I can show freedom of expression in
my improvisation.



I can create a lead sheet for a jazz
tune.





I can critique and provide feedback for
improvisation work.



I can perform on an instrument, music
apps, or sing an improvised part in an
ensemble.

I can...





I can...

I can...

Page 257

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Advanced
Low
Benchmark
GM.P
AL.3
I can sing expressively and apply technical and
stylistic criteria in a variety of songs alone and
in various ensembles.

Advanced
Mid
Benchmark
GM.P
AM.3
I can collaborate with others make technical
and stylistic decisions.

Advanced
High
Benchmark
GM.P
AH.3
I can make technical and stylistic choices
about my performance as a singer alone and in
various ensembles.

Indicator
GM.P
AL.3.1
I can sing in a group with balance.

Indicator
GM.P
AM.3.1
I can sing in ensembles.

Indicator
GM.P
AH.3.1
I can use a variety of technical and stylistic
choices in my performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets






I can identify the melodic line to
balance my part with ensemble
members.
I can respond to a conductor’s cues to
adjust balance during a performance.



I can sing solos, duets, trios, etc.





I can choose appropriate music for
myself.

I can use feedback of my performance
to improve my skills.



I can accept and use criticism of my
personal choices to improve my work.



I can...



I can...

I can...

Page 258

Indicator
GM.P
AL.3.2
I can blend with others in an ensemble.

Indicator
GM.P
AM.3.2
I can rehearse with an ensemble to improve my
work.

Indicator
GM.P
AH.3.2
I can apply a variety of musical choices for
performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can sing at various dynamic levels p, mp, mf, f. to blend with the group.



I can use song form to memorize
lyrics.



I can select music appropriate for my
voice and the venue of performance.



I can blend harmonic lines above the
bass line to improve intonation.





I can apply style decisions to my
personal performance.



I can demonstrate appropriate
ensemble behaviors to improve my
group.

I can...



I can...



I can rehearse and polish my part with
a recording in sectionals.



I can...

Anchor Standard 4: I can play instruments alone and with others.
Advanced
Low
Benchmark
GM.P
AL.4
I can play an instrument expressively and
apply technical and stylistic techniques in
variety of music alone and in various
ensembles.

Advanced
Mid
Benchmark
GM.P
AM.4
I can collaborate with others to apply technical
and stylistic techniques in a variety of music
alone and in various ensembles.

Advanced
High
Benchmark
GM.P
AH.4
I can make technical and stylistic choices
about my performance as an instrumentalist
alone and in various ensembles.

Page 259

Indicator
GM.P
AL.4.1
I can play in various musical styles on
instruments.

Indicator
GM.P
AM.4.1
I can collaborate with others to improve my
ensemble.

Indicator
GM.P
AH.4.1
I can control my instrument across expanded
dynamic ranges using stylistic nuances and
expressive inflections.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify intonation tendencies
for my instrument.



I can respond to musical score
markings in my part to improve
stylistic intentions in the ensemble.



I can...



I can listen to a recording of my
ensemble and suggest areas for
additional practice.



I can compare a benchmark
performance of my ensemble selection
to my group’s performance.





I can decide how to improve my
performance.



I can incorporate vibrato in a
stylistically appropriate manner.



I can...

I can...

Indicator
GM.P
AL.4.2
I can balance my sound with others in an
ensemble.

Indicator
GM.P
AM.4.2
I can control pitch and tone quality with
proper dynamics.

Indicator
GM.P
AH.4.2
I can adjust my intonation relative to chord
tones.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can perform the first four SCBDA
Senior Scales (major and relative
minor) for my instrument, in tune.



I can perform the SCBDA Clinic
chromatic range for my instrument, in
tune.



I can...



I can play repertoire with a vibrant
tone on my instrument.



I can accurately tune my instrument to
other instruments.



I can play my part with an
accompaniment app.



I can identify the tonic and dominant
to adjust my pitch.



I can identify and apply intonation
tendencies for my instrument.



I can use a pitch matching app to
improve my practice and intonation.



I can...



I can…

Page 260

Indicator
GM.P
AL.4.3
I can sight read a musical part.
Sample Learning Targets


I can demonstrate accurate fingering,
bow technique, or the instrument
fingering system for my instrument.



I can perform the SCBDA Clinic
chromatic range for my instrument, in
tune.



I can sight read with accuracy two
levels below my playing level.



I can...

Anchor Standard 5: I can read and notate music.
Advanced
Low
Benchmark
GM.P
AL.5
I can sight read a variety of music at Grade 2
with technical accuracy.

Advanced
Mid
Benchmark
GM.P
AM.5
I can sight read a variety of music at Grade 3
with technical accuracy.

Advanced
High
Benchmark
GM.P
AH.5
I can sight read a variety of music at Grade 4
with technical accuracy.

Page 261

Indicator
GM.P
AL.5.1

Indicator
GM.P
AM.5.1

Indicator
GM.P
AH.5.1

I can sight read musical works in simple
meters and tonalities with technical accuracy.

I can sight read musical works in a variety of
keys and clefs.

I can sight read musical works in a variety of
keys, clefs, meters.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can sight read a musical work in
duple meter.



I can sight read a musical work with 2
or more key changes with accuracy.



I can sight read a musical work with 2
or more meter changes with accuracy.



I can sight read a musical work in
major tonality.



I can apply tempo markings when I
sight read.



I can apply dynamic markings when I
sight read.



I can...



I can identify challenging rhythms in
my part during sight-reading.





I can use interactive sight-reading
music software to increase my
technical accuracy.

I can...



I can...

Indicator
GM.P
AL.5.2
I can respond to a director while sight-reading.

Indicator
GM.P
AM.5.2
I can apply tempo and dynamic markings to
my sight-reading.

Indicator
GM.P
AH.5.2
I can apply expressive music markings to my
sight-reading.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can respond to dynamic cues from a
conductor/director while sight-reading.



I can apply tempo markings when I
sight read.



I can interpret from the page,
expressive cues while I sight read.



I can respond to tempo cues from a
conductor/director while sight-reading.



I can identify challenging rhythms in
my part during sight-reading.



I can apply dynamic markings when I
sight read.



I can...



I can accurately select a tempo for
sight-reading based on cues in the
score.
I can…



I can use interactive sight-reading
music software to increase my
technical accuracy.
I can…
Page 262





Artistic Processes: Responding-I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Advanced
Low
Benchmark
GM.R
AL.6
I can examine the use of compositional
techniques within multiple musical works.

Advanced
Mid
Benchmark
GM.R
AM.6
I can collaborate with others to justify the use
of compositional techniques within musical
works.

Advanced
High
Benchmark
GM.R
AH.6
I can analyze compositional techniques, to
explain a composer’s intent.

Indicator
GM.R
AL.6.1
I can describe and use meter, tonality,
intervals, chords, and harmonic progressions
when analyzing written and aural
compositions.

Indicator
GM.R
AM.6.1
I can collaborate with others to determine
intent of changes in meter, tonality and
harmony that contribute to musical style.

Indicator
GM.R
AH.6.1
I can determine the intent of changes in meter,
tonality and harmony in a variety of musical
compositions to create tension and emotional
response.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify and write a 12 bar blues
progression.



I can listen and respond to musical
texture (thick/thin/ simple/complex).



I can justify the aim or purpose of a
composer’s musical work.



I can identify intervals from an aural
example.







I can define and recognize
monophonic, polyphonic, and
homophonic textures.

I can research the background of a
piece to determine possible
composer’s intent.

I can...



I can collaborate with others to
identify elements that explain the
musical style of a work.



I can...



I can...

Page 263

Indicator
GM.R
AL.6.2

Indicator
GM.R
AM.6.2

Indicator
GM.R
AH.6.2

I can examine and discuss culturally authentic
practices found in musical works.

I can identify compositional techniques used
to achieve unity, variety, tension and release in
music to evoke an emotional response from the
listener.

I can examine timbre and the use of voices,
instruments, and other sound sources in a
variety of musical styles, cultures, and genres.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can compare the impact of cultural
influences on musical works.







I can discuss a composer’s intent and
suggest a purpose for the musical
work.

I can research authentic examples of
cultural music and compare it to
current works.



I can cite examples of musical tension
within a musical score or performance.

I can...



I can...



I can write a critical analysis of a
musical work or performance.



I can analyze the instrument choice of
a calypso band.



I can...

Indicator
GM.R
AL.6.3
I can analyze and describe how the use of
expressive devices and form are used in
culturally and historically diverse genres.

Indicator
GM.R
AM.6.3
I can examine the use of musical form and
expressive devices in a variety of 20th & 21st
Century compositions.

Indicator
GM.R
AH.6.3
I can examine the use of musical form when
analyzing aural examples of a varied repertoire
of music and inform my personal music
preferences.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify sonata, concerto, and
sonata allegro forms from various
time periods.



I can discuss the impact of musical
form on the overall intent and
expression of a musical work.



I can compare multiple works of one
composer as to design, form, and
justify my personal preferences.



I can write an analysis of a
composition about the influence of
form on the overall work.



I can compare music forms from the
20th and 21st Centuries to earlier
musical examples.





I can…



I can…



I can compare and contrast a musical
work from two different composers of
the same time period as to musical
form.
I can…
Page 264

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
AL.7
I can collaborate to develop strategies for
improvement of group performances.

Benchmark
GM.R
AM.7
I can make critical evaluations of performance,
compositions, arrangements, and
improvisations.

Indicator
GM.R
AL.7.1

Benchmark
GM.R
AH.7
I can justify personal performance decisions.

Indicator
GM.R
AM.7.1

Indicator
GM.R
AH.7.1

I can compare a group performance to a
benchmark to refine the performance.

I can listen or view a variety of performances
and offer suggestions for improvement.

I can use multiple media sources to critique my
personal performances.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can compare the group to a
benchmark offering strategies for
improvement.



I can critique the work of others in my
group through collaboration.





I can watch and respond to a digital
recording of my performance.



I can analyze a score to make
performance decisions.



I can offer positive feedback and
suggest improvements of musical
performances.

I can...




I can research multiple examples of
one musical work as points of
comparison to my recorded
performance.



I can write a critical analysis of a
work.



I can...

I can...

Page 265

Artistic Processes: Connecting- I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Advanced
Low
Benchmark
GM.C
AL.8
I can analyze a diverse repertoire of music
from a cultural or historical time period.

Advanced
Mid
Benchmark
GM.C
AM.8
I can examine contemporary musical works to
determine the influence of historical and
cultural traditions.

Advanced
High
Benchmark
GM.C
AH.8
I can examine, create, and perform music
based on historical and cultural contributions.

Indicator
GM.C
AL.8.1
I can explain specific cultural and historical
traditions and infuse these ideas into my
music.

Indicator
GM.C
AM.8.1
I can select musical elements in contemporary
music that reflect cultural and historical
influences.

Indicator
GM.C
AH.8.1
I can use historical and cultural contributions
to justify my musical choices.

Sample Learning Targets
 I can compare music of the same time
periods from North America and
Europe.

Sample Learning Targets
 I can identify and research a musician
from the last decade and discuss
his/her impact in the music world.

Sample Learning Targets
 I can trace influences from musical
history to contemporary music styles
as they relate to American music.



I can trace connections of history to
musical style.



I can create a timeline of music history
to other historical events.





I can name modern composers and
describe their style influences from a
historical context.



I can...



I can create a set of program notes that
reflect my performance for a current
program using historical and cultural
contributions as points of interest.



I can...

I can...

Page 266

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Advanced
Low
Benchmark
GM.C
AL.9
I can apply concepts among arts disciplines
and other content areas to general music and
analyze how my interests and skills will
prepare me for a career.

Advanced
Mid
Benchmark
GM.C
AM.9
I can explain how economic conditions,
cultural values and location influence music
and the need for music related careers.

Advanced
High
Benchmark
GM.C
AH.9
I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a musician.

Indicator
GM.C
AL.9.1
I can explain ideas from other arts disciplines
and content areas through music.

Indicator
GM.C
AM.9.1
I can explain how my artistic choices are
influenced by cultural and social values.

Indicator
GM.C
AH.9.1
I can analyze complex ideals that influence my
artistic perspective and creative work.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can explain how the elements of the
arts have similarities and differences.



I can select music for a patriotic
celebration.



I can use a musical parody to explain
the Pythagorean theorem.







I can analyze the lyrics of music that I
purchase to determine the political and
societal issues of today.

I can describe the influence of social
values found in selected musical
works.



I can research and describe political
and cultural issues influencing
contemporary musical selections.

I can...



I can...

I can...


Page 267

Indicator
GM.C
AL.9.2
I can identify and describe traditional and
emerging careers in music.

Indicator
GM.C
AM.9.2
I can discuss the impact of economic issues as
they affect the impact on music careers.

Indicator
GM.C
AH.9.2
I can analyze my personal career choices in the
arts or non-arts disciplines.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I compare and contrast current
performing artists and discuss their
influences on our society.



I can prepare a feasibility study on the
influence of arts investment in my
community.



I can investigate a famous musician
and compare his/her career life to a
possible career for myself.



I can investigate the cost of live
performances and professional
musicians in a community.



I can explore careers in sound
engineering, producing, or music
video production using technology.



I can research products designed to be
competitive in the music market.



I can...





I can use research to predict possible
new careers for music.



I can evaluate my personal career
choices in the arts or non-arts
discipline.



I can...

I can...

Page 268

General Music Glossary
AABA A design sometimes called rounded binary form it was sometimes originally used for short pieces such as dances.Also called
song form, AABA is a variation of ABA in which the first section, A, is played twice before the middle section, B, and a third
time to conclude the piece. During the late eighteenth century, the rounded binary form developed into the sonata form.
AB Binary form A basic musical form consisting of two sections, A and B; usually they are repeated, creating the form AABB.
ABATernary form A basic musical form consisting of three sections (A, B, and A), the third section being virtually identical to the
first. If it is identical, the third section often is not written out, the performer simply being directed to repeat the first section
(usually marked da capo or D.C.), as in the da capo aria and minuet or scherzo with trio.
Accompaniment A musical part that supports or partners a solo instrument, voice, or group.
Arrangement/Arrange Composition based on existing music (e.g., scoring for voices not used in the original piece, adding a
percussion part to the original).
Articulation The manner or style in which the notes in a piece of music are sung. Attacking and releasing. Beginning and ending a
sound clearly and distinctly.
Aural/Aurally By ear; without reference to or memorization of written music.
Blend To merge voices to form a unified resonant sound in which no individual timbre dominates.
Body Percussion The sounds created by using body parts as percussion instruments (for example, clapping hands, stamping feet).
Call and Response The alteration of musical phrases between groups of musicians. Three terms have been used to distinguish between
different forms of call and response: adjacent (the response follows immediately after the call section); overlapping (the
response begins before the call section has concluded; and interlocking (there is a continuous response with a counter solo
passage over it, so that the call and response are “locked” together).
Canon Two or more voices in which one voice enters after another in exact imitation of the first. (See rounds.)

Page 269

Characteristic Tone Quality The particular sound that is characteristic of a specific instrument at all dynamic and pitch levels and
with all articulations.
Chord Three or more pitches sounded simultaneously or functioning as if sounded simultaneously.color (See timbre, definition 1.)
Compositional Techniques Formal melodic, rhythmic, and harmonic techniques used by composers to create music.Combining music
elements to form a whole including style, form, balance, complexity, and continuity.
Contour The shape of a melody or melodic line, contour can be seen by viewing a written piece, as well as heard as to the direction up,
down, or staying the same pitch.
Cut Time Also called alla breve. Used for quick duple time in which the half note, or occasionally the whole note, is given one beat
instead of two. Descants. Harmonizing voice parts added above the melody.
Developmentally Appropriate Taking into account the fact that developmental change is qualitative, sequential, directional,
cumulative, multifactorial, and individual, diatonic A musical scale (major or minor) comprising intervals of five whole steps
and two half steps.
Diction The choice and use of words and phrases in speech or writing.
Dotted Rhythms Rhythm patterns that contain dotted notes (a dot after the note indicates that the note should be extended by half as
much again as the note’s principal time value).
Dynamics Changes in volume; varying degrees of loudness and softness. Adjective form, dynamic.
Elements of Music Seven basic building blocks of music as follows:
1.Rhythm: (beat, meter, tempo, syncopation)
2. Dynamics: (forte, piano, [etc.], crescendo, decrescendo)
3. Melody: (pitch, theme, conjunct, disjunct)
4. Harmony: (chord, progression, consonance, dissonance, key, tonality, atonality)
5. Tone color: (register, range, instrumentation)
Page 270

6. Texture: (monophonic, homophonic, polyphonic, imitation, counterpoint)
7. Form: (binary, ternary, strophic, through-composed)
Embellishments A group of notes or a single note added to a basic melody as ornamentation. In Orff-Schulwerk, embellishment is also
a color part.
Ensemble Skills The abilities that allow a group of musicians to perform together with a refined degree of unanimity of phrasing,
dynamics, and style.
Enunciation The clarity with which words are spoken or sung.
Form The structure or organization of a musical phrase or composition. AB, or binary, form (in which two contrasting sections are
present) is the most basic. ABA, or ternary, form is derived from binary form and results from the repetition of the first section.
Larger musical forms include rondo, theme and variation, sonata, and symphony.
Genre A type or style of music; an established form of musical composition such as ballad, concerto, folk music, lullaby, march,
spiritual.
Good Posture The position of the body for singing. The chin should be parallel to the floor. The shoulders should be held back and
down with the chest held high but not in a strained position. The abdomen should be flat and firm and held in an expandable
position. The hands should be relaxed and still at the sides. Knees should be flexibly loose and never locked. The feet should be
flat on the floor and held shoulder width apart. The weight of the body should be balanced on both feet and the body should be
held slightly forward.
Harmony/Harmonic -(1) The pattern of intervals and chords in a composition. (2) The ways in which chords and intervals are related
to one another and the ways in which one interval or chord can be connected to another. Adjective form, harmonic.
Head Voice A clear, open tone that resonates in the head and not in the throat or chest.
Improvisation/Improvise The creation of music in the course of performance. Verb form, improvise.
Intervals (1) Pairs of notes sounded at the same time. (2) The distances between two pitches. (Skip, steps, leaps).
Intonation The proper production of a musical tone so that it is played or sung in tune with characteristic tone for voice or instrument.
Page 271

Improvise/Improvisation Create and perform (music, drama, or verse) spontaneously or without preparation.
Key Signatures The sharp, flat, or natural signs placed at the beginning of a staff indicating the tonality of the composition.
Major and Minor Tonalities Keys based upon seven-tone diatonic scales and derived from ancient Greek modes and modalities. A
pentatonic tonality is based upon a five-note scale usually made up of the pitches do, re, mi, so, and la.
Match the Pitch To sing (or play) the same pitch given by another instrument or person.
Meter The way beats of music are grouped, often in sets of two or three.
Motive A short tune or musical figure that characterizes and unifies a composition. It can be of any length but is usually only a few
notes long. A motive can be a melodic, harmonic, or rhythmic pattern that is easily recognizable throughout the composition.
Notation/Notate A system used for writing down music showing aspects of music tones such as the tones to be sounded (pitch), the
time each tone should be held in relation to the others (duration), and the degree of loudness (dynamics) at which the tone
should be played. Verb form, notate.
Ostinati Short music patterns that are repeated persistently throughout a performance, composition, or a section of one. (Singular form,
ostinato.)
Partner Songs Two or more different songs that are performed at the same time to create harmony.
Pentatonic A scale made up of five tones (usually do, re, mi, so and la) as opposed to the seven-tone diatonic scale and the twelve-tone
chromatic scale. The pentatonic scale is found in the music of many Asian and African peoples, as well as in some European
folk music. See tonality.
Pitch(1) The property of a musical tone that is determine by the frequency of the sound waves creating it. (2) The highness or lowness
of a tone.
Pitched Adjective describing instruments that produce various tones; includes the families of brass, woodwinds, strings, and
keyboards.
Question-and-Answer Adjective describing a pattern or phrase in which a pair of musical statements complement one another in
rhythmic symmetry and harmonic balance.
Page 272

Rhythm Syllables Musical training involving both ear training and sight singing. Whether the teacher chooses Kodály, Orff, Suzuki, or
another method, it must be used appropriately, sequentially, and consistently.
Rondo The musical form in which the first section, A, recurs after each of several contrasting sections: ABACA.
Rounds Songs or instrumental pieces that begin with a single voice or instrument on the melody, followed at intervals by the other
voices or instruments that enter individually and perform exactly the same melody, thus forming a polyphonic harmony out of a
simple melody. (See canon.)
Solfège A music exercise involving both ear training and sight singing. Whether the teacher uses Kodály’s methodology, John
Feierabend’s Conversational Solfege series, or the Alexander Technique, it must be used appropriately, sequentially, and
consistently.
Style/Stylistic The composer’s manner of treating the various elements that make up a composition—the overall form, melody,
rhythm, harmony, instrumentation, and so forth—as well as for the performer’s manner of presenting the composition.
Adjective form, stylistic. Adverb form, stylistically. syncopation Stress on a normally unstressed beat.
Tempo(1) A steady succession of units of rhythm; the beat. (2) The speed at which a piece of music is performed or is written to be
performed. Texture. The number and relationship of musical lines in a composition.
Theme and Variation A musical form consisting of a main idea followed by changed versions of that idea.
Timbre(1) The blend of overtones (harmonics) that distinguish a note played on a flute, for example, from the same note played on the
violin. (2) The distinctive tone quality of a particular musical instrument.
Tonality The use of a central note, called the tonic, around which the other tonal material of a composition (notes, intervals, chords) is
built and to which the music returns for a sense of rest and finality. The term tonality refers particularly to harmony and to
chords and their relationships.
Triplets Three notes of equal length that are performed in the duration of two notes of equal length.
Two-and Three-part Songs written for two voices or three voices (for example, soprano and alto; soprano, alto, and baritone).
Unpitched Adjective describing instruments that do not produce various tones; includes such percussion instruments as claves,
maracas, and wood blocks.
Page 273

Verse and Refrain The verse section of the song is the section in which different sets of words are sung to the same repeated melody
and contrasts with a refrain, where the words and melody are both repeated.

Page 274

References
Ammer, C. (1987). The Harper dictionary of music. (2nd ed.). New York: Harper and Row.
Apel, W., & Daniel, R. T. (1961). The Harvard brief dictionary of music. New York: Washington Square Press.
Catterall, J. S. (2009). Doing well and doing good by doing art: A 12-year study of education in the visual and performing arts. Los
Angeles: I-Group Books.
Cole, R. & Schwartz, E. (2009). Virginia Tech multimedia music dictionary. Retrieved from http://www.music.vt.edu/musicdictionary/
Correct Singing Posture. University of Kansas: Department of Music and Dance. Retrieved from
http://web.ku.edu/~cmed/gummposture/posture.html
Kennedy, M. (1985). The Oxford dictionary of music. Oxford: Oxford University Press.
MacDonald, R. A., Kreutz, G., & Mitchell, L. (2012). Music, health, and well-being. New York: Oxford University Press.
Merlot Classics. (2007). Music dictionary. Dolmetsch Organization. http://www.dolmetsch.com/musictheorydefs.htm
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
Randel, D. M. (Ed.). (1986). The new Harvard dictionary of music. Cambridge, MA: Belknap Press.
Sadie, S. (Ed.). (1988). The Grove concise dictionary of music. London: Macmillan.
South Carolina Department of Education. (2010). South Carolina academic standards for general music. Retrieved from
http://ed.sc.gov/scdoe/assets/file/agency/ccr/Standards-Learning/documents/AcademicStandards2010DraftGeneralMusic.pdf

Page 275

South Carolina
College- and Career-Ready Standards for
Design Proficiency

South Carolina Department of Education
Columbia, South Carolina
2017
Page 92

Design
Introduction
Design is all around us and permeates every aspect of our lives. From waking up and deciding what to wear to making choices about
our environment, purchases, and recreation, we interact with the work of designers. The design fields include, but are not limited to,
Communication Design, Environmental Design, Experiential Design, and Object Design.
Functionality and aesthetics are two concepts that determine how we use a design and what we see in a design. For example, the
science of a bridge (function) must also be aesthetically pleasing for its environment. The design process guides students to experience
this interface by proceeding through a sequence of steps to find solutions for a design challenge. These steps include the following:
defining the design challenge, conducting research, brainstorming solutions, constructing a prototype, presenting a design solution to a
sample target group, receiving feedback, reflecting on the feedback, and making improvements on the prototype.
Students are guided through the design process to make creative and considerate decisions concerning the interaction of function and
aesthetics toward constructing a well-crafted prototype. The process requires that students present their design solution/prototype,
explain their thought processes, and receive feedback from stakeholders. This feedback allows students to analyze and reflect upon
their work in order to make thoughtful revisions toward improvement.
The design standards are organized in steps that parallel the design process. Students move through the standards, as shaped by the
design process, by working independently and collaboratively with others in order to reach an aesthetically-effective and functional
outcome.
Students are immersed cognitively when involved in the design process. The use of skills such as communication, creativity, critical
thinking, and problem solving are truly embodied in their work. Teaching through design reaches diverse learners who are able to
approach design thinking from their own personal perspectives and abilities.
These design standards are written to be applicable across all content areas. Traditionally considered under visual arts, problem solving
through design thinking may be applied to their artistic work but, just as importantly, it also may be used for project work in other
disciplines. Effective practices will be employed in all student work as a result of studying the South Carolina College and Career
Ready Standards for Design Proficiency.
Page 93

Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
De.CR
NL.1
I can
recognize
design
questions.

Benchmark
De.CR
NM.1
I can
recognize
how design
questions are
used to solve
problems.

Benchmark
De.CR
NH.1
I can answer
design
challenge
questions.

Benchmark
De.CR
IL.1
I can work
with a team
to answer
design
challenge
questions.

Benchmark
De.CR
IM.1
I can work
with a team
from a given
list to
identify and
describe a
design
challenge to
develop.

Benchmark
De.CR
IH.1
I can work
with a team
from a given
list of design
challenges
and select
one to
describe.

Benchmark
De.CR
AL.1
I can work
with a team
to conceive
many design
challenge
possibilities
relating to a
certain topic.

Benchmark
De.CR
AM.1
I can work
with a team
to conceive
many design
challenge
possibilities.

Benchmark
De.CR
AH.1
I can work on
my own to
conceive
many design
challenge
possibilities.

Page 94

Indicator
De.CR
NL.1.1
I can answer
the design
challenge
questions
who, what,
and where, in
order to
define the
design
challenge.

Indicator
De.CR
NM.1.1
I can answer
the design
challenge
questions
who, what,
when, and
where in
order to
define the
design
challenge.

Indicator
De.CR
NH.1.1
I can answer
the design
challenge
questions
who, what,
when, where,
why, and
how in order
to define the
design
challenge.

Indicator
De.CR
IL.1.1
I can work
with a team
to answer the
design
challenge
questions
who, what,
when, where,
why, and
how to
define the
design
challenge.

Indicator
De.CR
IM.1.1
I can work
with a team
to select a
design
challenge
from a given
list using
criteria to
answer the
design
challenge
questions
and define
the challenge.

Indicator
De.CR
IH.1.1
I can work in
a team to
discuss
design
challenges
from a given
list and select
one to define
from answers
to the design
challenge
questions.

Indicator
De.CR
AL.1.1
I can work
with a team
using design
thinking
strategies to
list several
design
challenge
options about
a topic and
select one to
define.

Indicator
De.CR
AM.1.1
I can work
with a team
using design
thinking
strategies to
list many
design
challenge
possibilities
and prioritize
to select one
to define.

Indicator
De.CR
AH.1.1
I can use
design
thinking
strategies to
list many
design
challenge
possibilities
and prioritize
to select one
to define.

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Benchmark
De.CR
NL.2
I can
recognize
research
methods.

Benchmark
De.CR
NM.2
I can
recognize
how
research is
used to solve
a design
problem.

Benchmark
De.CR
NH.2
I can apply
research
methods.

Benchmark
De.CR
IL.2
I can work
with a team
to research
aspects of
the design
challenge.

Benchmark
De.CR
IM.2
I can work
with a team
to research
and describe
aspects of
the design
challenge.

Benchmark
De.CR
IH.2
I can work
with a team
to explain
why
researched
aspects of
the design
challenge are
needed.

Benchmark
De.CR
AL.2
I can work
with a team
to analyze the
aspects of
the design
challenge.

Benchmark
De.CR
AM.2
I can work
independentl
y or with a
team to
evaluate the
parts of the
design
challenge.

Benchmark
De.CR
AH.2
I can lead a
discussion to
evaluate the
parts of the
design
challenge.

Page 95

Indicator
De.CR
NL.2.1
I can use a
research
method to
investigate
the design
challenge.

Indicator
De.CR
NM.2.1
I can use
research
methods to
investigate
the design
challenge.

Indicator
De.CR
NH.2.1
I can use a
variety of
methods to
investigate
the design
challenge.

Indicator
De.CR
IL.2.1
I can work
with a team
to identify
necessary
information
for the
design
challenge.

Indicator
De.CR
IM.2.1
I can
communicate
my research
to the team.

Indicator
De.CR
IH.2.1
I can work
with a team
to prioritize
research
from the
individual
team
members.

Indicator
De.CR
AL.2.1
I can
examine my
research and
report the
connections
of that
information
with the
team.

Indicator
De.CR
AM.2.1
I can work
with a team
to determine
the
importance
of the
research
from the
team
members.

Indicator
De.CR
AH.2.1
I can guide
my team in
determining
the
importance
of the
research
from the
team
members.

Benchmark
De.CR
AM.3
I can work
independentl
y or with a
team to
evaluate the
usable design
solutions to
the challenge.

Benchmark
De.CR
AH.3
I can lead a
discussion to
evaluate the
usable design
solutions to
the challenge.

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Benchmark
De.CR
NL.3
I can
recognize
design
thinking.

Benchmark
De.CR
NM.3
I can
recognize
how design
thinking is
used to solve
a design
problem.

Benchmark
De.CR
NH.3
I can apply
design
thinking
strategies.

Benchmark
De.CR
IL.3
I can work
with a team
using design
thinking
strategies to
generate
ideas for
design
solutions to
the challenge.

Benchmark
De.CR
IM.3
I can work
with a team
using design
thinking
strategies to
generate
some usable
design
solutions to
the challenge.

Benchmark
De.CR
IH.3
I can work
with a team
using design
thinking
strategies to
generate
many usable
design
solutions to
the challenge.

Benchmark
De.CR
AL.3
I can work
with a team
to analyze
usable design
solutions to
the challenge.

Page 96

Indicator
De.CR
NL.3.1
I can use a
design
thinking
strategy to
list possible
design
solutions to
the challenge.

Indicator
De.CR
NM.3.1
I can use
more than
one design
thinking
strategy to
list possible
design
solutions to
the challenge.

Indicator
De.CR
NH.3.1
I can use a
variety of
design
thinking
strategies to
list possible
design
solutions to
the challenge.

Indicator
De.CR
IL.3.1
I can work
with a team
using a
variety of
design
thinking
strategies to
list possible
design
solutions
without
judgement.

Indicator
De.CR
IM.3.1
I can work
with a team
to turn ideas
into possible
design
solution
concepts.

Indicator
De.CR
IH.3.1
I can work
with a team
to determine
which design
solutions
effectively
meet the
challenge
criteria.

Indicator
De.CR
AL.3.1
I can
examine,
discuss, and
select
possible
design
solutions to
best address
the challenge.

Indicator
De.CR
AM.3.1
I can work
with a team
to develop
criteria to
determine the
value of the
usable design
solutions to
the challenge.

Indicator
De.CR
AH.3.1
I can guide
my team in
determining
the value of
the usable
design
solutions to
the challenge.

Benchmark
De.CR
IH.4
I can work
with a team
to create a
prototype to
solve a
design
challenge.

Benchmark
De.CR
AL.4
I can work
with a team
to create a
prototype
that solves
multiple
aspects of a
design
challenge.

Benchmark
De.CR
AM.4
I can work
with a team
to create a
prototype
that solves all
aspects of a
design
challenge
functionally
and
aesthetically.

Benchmark
De.CR
AH.4
I can use
sophisticated
materials,
techniques,
and processes
to create the
most viable
prototype.

Anchor Standard 4: I can create an original prototype.
Benchmark
De.CR
NL.4
I can
recognize a
prototype.

Benchmark
De.CR
NM.4
I can
recognize
how a
prototype is
used to solve
a design
challenge.

Benchmark
De.CR
NH.4
I can explore
materials,
techniques
and processes
to create a
prototype.

Benchmark
De.CR
IL.4
I can work
with a team
to make a
prototype
that
represents a
solution to a
design
challenge.

Benchmark
De.CR
IM.4
I can work
with a team
to make
multiple
prototypes
that represent
various
solutions to a
design
challenge.

Page 97

Indicator
De.CR
NL.4.1
I can explore
using
physical
models,
space
models,
interactions,
and
storytelling
as
prototypes.

Indicator
De.CR
NM.4.1
I can use
strategies to
create a twodimensional
drawing or a
threedimensional
model of a
design
solution.

Indicator
De.CR
NH.4.1
I can use
basic
materials and
techniques
to develop a
model of my
design ideas.

Indicator
De.CR
IL.4.1
I can work
with a team
to make a
prototype to
experience
the design
challenge
criteria.

Indicator
De.CR
IM.4.1
I can work
with a team
to make
prototypes
to experience
the design
challenge
criteria.

Indicator
De.CR
IH.4.1
I can work
with a team
to make a
prototype
that
addresses
functional
aspects and
aesthetics.

Indicator
De.CR
AL.4.1
I can work
with a team
to select
materials,
techniques,
and processes
to create a
prototype.

Indicator
De.CR
AM.4.1
I can work
with a team
to select and
apply the best
materials,
techniques,
and processes
to create a
prototype.

Indicator
De.CR
AH.4.1
I can select
and apply
professional
materials,
techniques,
and processes
to create a
prototype.

Benchmark
De.P
AL.5
I can work
with a team
to prepare
and deliver a
presentation
to a sample
target group.

Benchmark
De.P
AM.5
I can work
with a team
to develop a
wellprepared,
aesthetically
pleasing
presentation
for a sample
target group
that includes
community
business
leaders or
professionals
in the field.

Benchmark
De.P
AH.5
I can develop
a wellprepared,
aesthetically
pleasing
presentation
for a sample
target group
that includes
professionals
and business
leaders in my
community.

Artistic Processes: Presenting-I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Benchmark
De.P
NL.5
I can share
my design
with a small
group.

Benchmark
De.P
NM.5
I can identify
how a design
presentation
is used to
solve a
design
challenge.

Benchmark
De.P
NH.5
I can present
my design
solution to a
design
challenge.

Benchmark
De.P
IL.5
I can work
with a team
to present our
design
solution to a
challenge.

Benchmark
De.P
IM.5
I can work
with a team
to select an
approach to
present our
design
solution to a
challenge.

Benchmark
De.P
IH.5
I can work
with a team
to prepare
and deliver a
presentation
that has
defined
criteria.

Page 98

Indicator
De.P
NL.5.1
I can share
my
prototype
and answer
simple
questions
about the
design
solution.

Indicator
De.P
NM.5.1
I can explain
the design
challenge
and my
design
solution.

Indicator
De.P
NH.5.1
I can present
my design
solution to
the challenge
using a
visual.

Indicator
De.P
IL.5.1
I can work
with a team
to present our
design
solution to
the challenge
using one or
more visuals.

Indicator
De.P
IM.5.1
I can work
with a team
to select an
approach
using
technology
for the
design
solution
presentation.

Indicator
De.P
IH.5.1
I can work
with a team
to create a
presentation
that includes
specific
criteria and
delivers
required
information
concerning
the design
challenge
and design
solution.

Indicator
De.P
AL.5.1
I can work in
a team to
present our
design
solution to a
group of
possible
users/consum
ers for
feedback.

Indicator
De.P
AM.5.1
I can work in
a team to
present our
design
solution to a
sample target
group that
includes
community
business
leaders and
professionals
in a related
field for
feedback.

Indicator
De.P
AH.5.1
I can present
our design
solution to a
sample target
audience that
includes
professionals
and business
leaders in a
related field
for feedback.

Page 99

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Benchmark
De.R
NL.6

Benchmark
De.R
NM.6

Benchmark
De.R
NH.6

Benchmark
De.R
IL.6

I can
recognize
how
reflection is
necessary in
the design
process.

I can
recognize
that revision
is necessary
in the design
process.

I can
encourage
feedback to
my design
and the
designs of
others by
asking and
answering
questions.

I can reflect
on and
provide
feedback to a
design
solution.

Indicator
De.R
NL.6.1
I can identify
the strengths
of my design
and designs
of others.

Indicator
De.R
NM.6.1
I can identify
areas of my
design and
the designs
of others that
need
improvement
.

Indicator
De.R
NH.6.1
I can prepare
some
questions for
feedback to
help me
revise my
design.

Indicator
De.R
IL.6.1
I can work
with a team
to record
feedback and
summarize
design
solution
recommendat
ions.

Benchmark
De.R
IM.6

Benchmark
De.R
IH.6

Benchmark
De.R
AL.6

Benchmark
De.R
AM.6

Benchmark
De.R
AH.6

I can
interpret
feedback
from my
peers to
revise our
design
solution.

I can work
with a team
to analyze
and explain
the steps of
the design
solution
revision.

I can work
with a team
to retest our
revised
design
solution and
analyze the
results.

I can
facilitate the
repetition of
the design
process to
revise and
retest the
design
solution.

Indicator
De.R
IM.6.1
I can work
with a team
to list and
prioritize
feedback to
improve our
design
solution.

Indicator
De.R
IH.6.1
I can work
with a team
to plan and
develop the
steps to
improve our
design
solution.

Indicator
De.R
AL.6.1
I can work
with a team
to improve
the
functionality
of our design
solution and
record the
results of the
modifications

I can work
with a team
to explain
future
improvement
s and repeat
the design
process to
revise and
retest the
design
solution.
Indicator
De.R
AM.6.1
I can work
with a team
to repeat the
design
process as
necessary to
improve the
design
solution.

Indicator
De.R
AH.6.1
I can guide
and frame
questions to
facilitate the
design
process to
improve a
design
solution.

Page 100

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Benchmark
De.C
NL.7
I can
recognize
some
examples of
design found
in my home
and
community.

Benchmark
De.C
NM.7
I can
recognize
differences in
designs
found in my
home and
community.

Benchmark
De.C
NH.7
I can describe
differences in
designs from
various
cultures
throughout
history.

Benchmark
De.C
IL.7
I can identify
improvements
or changes in
designs found
in various
cultures and
time periods.

Indicator
De.C
NL.7.1
I can find and
name some
designs
(object
environment
al,
communicati
on, or
experiential)
around me.

Indicator
De.C
NM.7.1
I can name
some
different
design
materials and
methods of
construction.

Indicator
De.C
NH.7.1
I can
compare how
designs are
different in
various
cultures
throughout
history.

Indicator
De.C
IL.7.1
I can compare
design
similarities
and
differences
among
different
cultures and
time periods.

Benchmark
De.C
IM.7
I can describe
why
improvements or
changes were
made in
designs
found in
various
cultures and
time periods.
Indicator
De.C
IM.7.1
I can explain
the possible
reasons
improvement
s and/or
changes were
made in a
design
through
different
cultures and
time periods.

Benchmark
De.C
IH.7
I can
analyze a
variety of
design
works from
different
cultures and
time
periods.

Benchmark
De.C
AL.7
I can
examine past
design works
to determine
their
influence on
present
designs.

Benchmark
De.C
AM.7
I can work
with a team
to analyze the
influence of
past design
works on
present
design
challenges.

Benchmark
De.C
AH.7
I can evaluate
my design
solution to
determine the
effective use
of past
design
works.

Indicator
De.C
IH.7.1
I can
recognize
patterns in
design
choices and
make
connections
to the
developmen
t of design
through
different
cultures and
time
periods.

Indicator
De.C
AL.7.1
I can find and
compare how
choices from
a current
design reflect
influences of
past design
solutions.

Indicator
De.C
AM.7.1
I can work
with a team
to explain
how the
designer's
choices on
the current
design
challenge
reflect
influences of
design
solutions
from the past.

Indicator
De.C
AH.7.1
I can assess
my design
choices and
relate them to
past design
influences.

Page 101

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Benchmark
De.C
NL.8
I can explore
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
NM.8
I can
recognize
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
NH.8
I can apply
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
IL.8
I can explore
a range of
skills shared
among arts
disciplines,
other content
areas and
how they can
be applied in
a design
career.

Benchmark
De.C
IM.8
I can
recognize
specific skills
shared
among arts
disciplines,
other content
areas and
how they can
be applied in
a design
career.

Indicator
De.C
NL.8.1
I can connect
design with
objects in my
home and
school.

Indicator
De.C
NM.8.1
I can
recognize
that design
exists in all
arts
disciplines
and other
content areas.

Indicator
De.C
NH.8.1
I can use
design
concepts in
other subjects
in my school.

Indicator
De.C
IL.8.1
I can
investigate a
range of
skills used in
various
design
careers, arts
disciplines,
and content
areas.

Indicator
De.C
IM.8.1
I can name
design skills
used in
various arts
disciplines
and content
areas and
relate these
skills to a
career in
design.

Benchmark
De.C
IH.8
I can analyze
the tools,
concepts, and
materials
used among
arts
disciplines,
other content
areas and
how they are
used in a
design
career.
Indicator
De.C
IH.8.1
I can
investigate
tools,
concepts and
materials
used in other
arts
disciplines
and content
areas.

Benchmark
De.C
AL.8
I can apply
concepts
among arts
disciplines
and other
content areas
to design and
analyze how
my interests
and skills
will prepare
me for a
career.
Indicator
De.C
AL.8.1
I can use
concepts
found in
various arts
disciplines
and other
content areas
in a design
work.

Benchmark
De.C
AM.8
I can explain
how
economic
conditions,
cultural
values, and
location
influence
design and
the need for
design
related
careers.
Indicator
De.C
AM.8.1
I can describe
how
economic
conditions,
cultural
values, and
geographic
locations
affect design
and design
careers.

Benchmark
De.C
AH.8
I can research
societal,
political, and
cultural
issues as they
relate to other
arts and
content areas
and apply to
my role as a
designer.

Indicator
De.C
AH.8.1
I can
examine the
importance
of the work
of a designer
in issues that
relate to a
global
society.

Page 102

Indicator
De.C
NL.8.2
I can
recognize
that people
have careers
in design.

Indicator
De.C
NM.8.2
I can identify
design
businesses
and careers in
my
community.

Indicator
De.C
NH.8.2
I can identify
ways design
thinking is
used in other
careers or
vocations.

Page 103

Novice Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Novice
Low
Benchmark
De.CR
NL.1
I can recognize design questions.

Novice
High
Benchmark
De.CR
NH.1
I can answer design challenge questions.

Indicator
De.CR
NL.1.1
I can answer the design challenge questions
who, what, and where, in order to define the
design challenge.

Novice
Mid
Benchmark
De.CR
NM.1
I can recognize how design questions are
used to solve problems.
Indicator
De.CR
NM.1.1
I can answer the design challenge questions
who, what, when, and where in order to define
the design challenge.

Sample Learning Targets
 I can answer “who” the design
challenge impacts.

Sample Learning Targets
 I can answer “when” the design
challenge will occur.

Sample Learning Targets
 I can answer “why” the design
challenge is needed.



I can answer “what” the design
challenge is for.



I can answer “where” the design
challenge will be impacted.





I can use design questions to
recognize how to define a design
challenge.



I can…

Indicator
De.CR
NH.1.1
I can answer the design challenge questions
who, what, when, where, why, and how in
order to define the design challenge.



I can answer “how” the design
challenge will be implemented.



I can…

I can…

Page 104

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Novice
Low
Benchmark
De.CR
NL.2
I can recognize research methods.

Novice
High
Benchmark
De.CR
NH.2
I can apply research methods.

Indicator
De.CR
NL.2.1
I can use a research method to investigate the
design challenge.

Novice
Mid
Benchmark
De.CR
NM.2
I can recognize how research is used to solve
a design problem.
Indicator
De.CR
NM.2.1
I can use research methods to investigate the
design challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can observe (using the five senses)
the existing designs.



I can observe an object in use.




Indicator
De.CR
NH.2.1
I can use a variety of methods to investigate
the design challenge.



I can use more than one of the
following: observation, printed
materials, technology, and/or
interviewing.



I can use a variety of the following
research methods: observation,
printed materials, technology, and/or
interviewing.

I can see and feel the parts of a
design object.



I can use printed materials to learn
about an object.



I can…



I can interview others for research
information.

I can observe, sketch, or record
(photography, video) an object to
show what I’ve learned about the
design object.





I can…

I can interview individuals with
experience with an object to
determine possible aspects to
redesign.



I can…

Page 105

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Novice
Low
Benchmark
De.CR
NM.3
NL.3
I can recognize design thinking.

Novice
Mid
Benchmark
De.CR
I can recognize how design thinking is used
to solve a design problem.

Novice
High
Benchmark
De.CR
NH.3
I can apply design thinking strategies.

Indicator
De.CR
NL.3.1
I can use a design thinking strategy to list
possible design solutions to the challenge.

Indicator
De.CR
NM.3.1
I can use more than one design thinking
strategy to list possible design solutions to
the challenge.

Indicator
De.CR
NH.3.1
I can use a variety of design thinking
strategies to list possible design solutions to
the challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can name possible solutions.



I can stay on topic to randomly call
out ideas for possible design
solutions.



I can…



I can use more than one of the
following: list aloud, popcorn
brainstorming, passing
brainstorming to provide possible
solutions.



I can determine which solutions can
be used in the design challenge.



I can provide a visual or drawing to
explain my idea.



I can create questions rather than
ideas to inspire further thinking.



I can organize my ideas using mind
maps.



I can listen to others and participate in
one conversation at a time to provide
possible design solutions.



I can…



I can…

Page 106

Anchor Standard 4: I can create an original prototype.
Novice
Low
Benchmark
De.CR
NL.4
I can recognize a prototype.

Novice
Mid
Benchmark
De.CR
NM.4
I can recognize how a prototype is used to
solve a design challenge.

Novice
High
Benchmark
De.CR
NH.4
I can explore materials, techniques and
processes to create a prototype.

Indicator
De.CR
NL.4.1
I can explore using physical models, space
models, interactions, and storytelling as
prototypes.

Indicator
De.CR
NM.4.1
I can use strategies to create a twodimensional drawing or a three-dimensional
model of a design solution.

Indicator
De.CR
NH.4.1
I can use basic materials and techniques to
develop a model of my design ideas.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can role play to act out solutions to
a design challenge.



I can use clay or other materials to
create a model of a new cup design.



I can explore space models with
geometric forms in a given area.



I can draw a new logo design.



I can…



I can use my words to tell about my
design idea.



I can…



I can work with a team to explore and
select the most appropriate materials
to build/compose the prototype.



I can work with a team to explore and
select the most appropriate
techniques and processes to
build/compose the prototype.



I can…

Page 107

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Novice
Low
Benchmark
De.P
NL.5
I can share my design with a small group.

Novice
Mid
Benchmark
De.P
NM.5
I can identify how a design presentation is
used to solve a design challenge.

Novice
High
Benchmark
De.P
NH.5
I can present my design solution to a design
challenge.

Indicator
De.P
NL.5.1
I can share my prototype and answer simple
questions about the design solution.

Indicator
De.P
NM.5.1
I can explain the design challenge and my
design solution.

Indicator
De.P
NH.5.1
I can present my design solution to the
challenge using a visual.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can show my prototype to my
peers.



I can explain the “who, what, where”
of the design challenge.



I can draw my ideas to present my
design challenge solution.



I can answer questions about the
“what” of the design solution.



I can explain the “when, and how” of
the design challenge.





I can ...



I can create a presentation board to
help explain my design challenge
solution.

I can ...



I can...

Page 108

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Novice
Low
Benchmark
De.R
NL.6

Novice
Mid
Benchmark
De.R
NM.6

Novice
High
Benchmark
De.R
NH.6

I can recognize how reflection is necessary in
the design process.

I can recognize that revision is necessary in
the design process.

I can encourage feedback to my design and
the designs of others by asking and answering
questions.

Indicator
De.R
NL.6.1
I can identify the strengths of my design and
designs of others.

Indicator
De.R
NM.6.1
I can identify areas of my design and the
designs of others that need improvement.

Indicator
De.R
NH.6.1
I can prepare some questions for
feedback to help me revise my design.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can listen and respond to the
opinions of others.



I can work with others to list possible
improvements to our solution.



I can ask what new materials could be
used in a design solution.



I can list the positive comments about
my design.



I can list changes I would make to my
design solution.



I can ask simple questions about a
design solution.



I can...



I can...



I can ask questions about who needs
the design.



I can...

Page 109

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Novice
Low
Benchmark
De.C
NL.7
I can recognize some examples of design
found in my home and community.

Novice
Mid
Benchmark
De.C
NM.7
I can recognize differences in designs found
in my home and community.

Novice
High
Benchmark
De.C
NH.7
I can describe differences in designs from
various cultures throughout history.

Indicator
De.C
NL.7.1
I can find and name some designs (object
environmental, communication, or
experiential) around me.
Sample Learning Targets

Indicator
De.C
NM.7.1
I can name some different design materials
and methods of construction.

Indicator
De.C
NH.7.1
I can compare how designs are different in
various cultures throughout history.

Sample Learning Targets

Sample Learning Targets



I can find and name some design
objects that I use every day.



I can identify the methods used for
communication design.



I can group designs that have similar
styles, subject, or media.



I can find and name some
environmental designs in my school
and community.



I can discuss the materials used in an
environmental design.







I can identify common characteristics
within a design from different styles,
periods, and cultures.

I can recognize the use of
communication design in
newspapers, billboards, and
commercials.

I can describe how a design was
made.



I can…



I can recognize the use of
experiential design in play grounds,
video games and amusement parks.



I can…



I can…

Page 110

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Novice
Low
Benchmark
De.C
NL.8
I can explore design thinking in arts
disciplines, other content areas, and related
careers.

Novice
Mid
Benchmark
De.C
NM.8
I can recognize design thinking in arts
disciplines, other content areas, and related
careers.

Novice
High
Benchmark
De.C
NH.8
I can apply design thinking in arts disciplines,
other content areas, and related careers.

Indicator
De.C
NL.8.1
I can explore how design exists in all arts
disciplines and other content areas.
Sample Learning Targets

Indicator
De.C
NM.8.1
I can recognize that design exists in all arts
disciplines and other content areas.
Sample Learning Targets

Indicator
De.C
NH.8.1
I can use design concepts in other subjects in
my school.
Sample Learning Targets



I can name designed objects in my
home and classroom.



I can identify ways design is used in
my community.



I can use the design process to solve
problems in other subjects.



I can talk about design choices found
in my home and classroom.



I can draw designs used in my
community.





I can draw examples of everyday
designs.



I can use design thinking to
brainstorm multiple solutions in
other subjects.

I can…



I can…



I can…

Page 111

Indicator
De.C
NL.8.2
I can recognize that people have careers in
design.

Indicator
De.C
NM.8.2
I can identify design businesses and careers in
my community.

Indicator
De.C
NH.8.2
I can identify ways design thinking is used in
other careers or vocations.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can recognize that musicians are
designers.



I can identify businesses in my
community that hire designers.



I can identify how design thinking is
used in business and industry.



I can recognize that buildings are
designed by architects.



I can identify where and how
designers impact my community.





I can recognize that choreographers
are designers.



I can locate design companies in my
community.

I can identify design thinking skills
that are used in education and service
organizations.



I can…

I can…





I can…

Page 112

Intermediate Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Intermediate
Low
Benchmark
De.CR
IL.1
I can work with a team to answer design
challenge questions.

Intermediate
High
Benchmark
De.CR
IH.1
I can work with a team from a given list of
design challenges and select one to describe.

Indicator
De.CR
IL.1.1
I can work with a team to answer the design
challenge questions who, what, when, where,
why, and how in order to define the design
challenge

Intermediate
Mid
Benchmark
De.CR
IM.1
I can work with a team from a given list to
identify and describe a design challenge to
develop.
Indicator
De.CR
IM.1.1
I can work with a team to select a design
challenge from a given list using certain
criteria and answer the design challenge
questions to define the challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can communicate and listen to others
when answering the design challenge
questions.



I can work with a team to choose a
design challenge based on its
importance to me and my community.



I can record information from the
group’s discussion.



I can work with a team to choose a
design challenge based on the need
for improvement to how it looks and
how it works.



I can...


I can…

Indicator
De.CR
IH.1.1
I can work in a team to discuss design
challenges from a given list and select one to
define from answers to the design challenge
questions.



I can work with a team to compare
and contrast the design challenge
options and select one based on their
importance to me and my community.



I can work with a team to compare
and contrast the design challenge
options and select one based on their
need for improvement to how it looks
and how it works.
I can…



Page 113

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Intermediate
Low
Benchmark
De.CR
IL.2
I can work with a team to research aspects of
the design challenge.

Intermediate
Mid
Benchmark
De.CR
IM.2
I can work with a team to research and
describe aspects of the design challenge.

Intermediate
High
Benchmark
De.CR
IH.2
I can work with a team to explain why
researched aspects of the design challenge
are needed.

Indicator
De.CR
IL.2.1
I can work with a team to identify necessary
information for the design challenge.

Indicator
De.CR
IM.2.1
I can communicate my research to the team.

Indicator
De.CR
IH.2.1
I can work with a team to prioritize research
from the individual team members.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify with a team what
information is necessary about the
existing design.



I can use visuals, technology,
demonstrations, and/or descriptions,
to report the research.



I can work with others to select the
best research methods to gather
necessary information.



I can discuss the research with others.



I can use printed materials to present
necessary information.



I can demonstrate the existing
function of a design.



I can work with others to create a
survey and/or use technology to learn
about a design.



I can list the research from the team
members.



I can work with others to identify the
most significant research.

Page 114

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Intermediate
Low
Benchmark
De.CR
IL.3
I can work with a team using design thinking
strategies to generate ideas for design
solutions to the challenge.
Indicator
De.CR
IL.3.1
I can work with a team using a variety of
design thinking strategies to list possible
design solutions without judgement.

Intermediate
Mid
Benchmark
De.CR
IM.3
I can work with a team using design
thinking strategies to generate some usable
design solutions to the challenge.
Indicator
De.CR
IM.3.1
I can work with a team to turn ideas into
possible design solution concepts.

Intermediate
High
Benchmark
De.CR
IH.3
I can work with a team using design
thinking strategies to generate many usable
design solutions to the challenge.
Indicator
De.CR
IH.3.1
I can work with a team to determine which
design solutions effectively meet the
challenge criteria.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can work with a team using a
variety of the following: list aloud,
popcorn brainstorming, passing
brainstorming, questioning
brainstorming, webbing, mind
mapping to provide possible
solutions.



I can build on the ideas of others in
creating possible solutions.



I can work with a team to determine
which solutions can be used in the
design challenge.



I can…



I can contribute my ideas concerning
usable solutions.



I can respond to others’ ideas
concerning usable solutions.



I can…



I can work with others to prioritize
choices concerning effective
solutions.



I can work with others to select
possible solutions.



I can…

Page 115

Anchor Standard 4: I can create an original prototype.
Intermediate
Low
Benchmark
De.CR
IL.4
I can work with a team to make a prototype
that represents a solution to a design
challenge.
Indicator
De.CR
IL.4.1
I can work with a team to make a prototype
to experience the design challenge criteria.

Intermediate
Mid
Benchmark
De.CR
IM.4
I can work with a team to make multiple
prototypes that represent various solutions to
a design challenge.
Indicator
De.CR
IM.4.1
I can work with a team to make prototypes
to experience the design challenge criteria.

Sample Learning Targets

Sample Learning Targets



I can work with others to create a
prototype that allows a concept to be
experienced.



I can create a simple prototype that is
made quickly and inexpensively to
experience feedback early and often.



I can…







I can work with others to create
multiple prototypes concerning one
design challenge that allow a concept
to be experienced.
I can work with a team to create
multiple simple prototypes that are
made quickly and inexpensively.

Intermediate
High
Benchmark
De.CR
IH.4
I can work with a team to create a prototype
to solve a design challenge.
Indicator
De.CR
IH.4.1
I can work with a team to make a prototype
that addresses functional aspects and
aesthetics.
Sample Learning Targets


I can work with a team to determine
the functionality of the prototype.



I can work with a team to improve
the functionality of the prototype to
address many aspects.



I can make a prototype that uses the
elements and/or principles of the arts
disciplines.



I can…

I can…

Page 116

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Intermediate
Low
Benchmark
De.P
IL.5
I can work with a team to present our design
solution to a challenge.

Intermediate
Mid
Benchmark
De.P
IM.5
I can work with a team to select an approach
to present our design solution to a challenge.

Intermediate
High
Benchmark
De.P
IH.5
I can work with a team to prepare and deliver
a presentation that has defined criteria.

Indicator
De.P
IL.5.1
I can work with a team to present our design
solution to the challenge using one or more
visuals.

Indicator
De.P
IM.5.1
I can work with a team to select an approach
using technology for the design solution
presentation.

Sample Learning Targets

Sample Learning Targets

Indicator
De.P
IH.5.1
I can work with a team to create a
presentation that includes specific criteria
and delivers required information concerning
the design challenge and design solution.
Sample Learning Targets



I can work with a team to prepare one
or more visuals such as photographs,
drawings, diagrams, charts, and 3D
examples to present our design
solution.



I can work with others to create a
slideshow presentation.



I can work with others to create a
webpage to present a design solution.



I can work with a team to explain the
“who, what, when, where, why, and
how” of the design challenge.



I can work with others to combine
still photos and videos to present a
design solution.



I can work with a team to explain the
“who, what, when, where, why, and
how” of the design solution.



I can ...





I can work in a team to prepare a
presentation that includes specific
criteria such as a title, infographics,
text, graphics, and/or media.



I can work with a team to prepare a
presentation that includes required
information such as the goal,
identified population, challenge
statement, key aspects, data, and
design solution.



I can…

I can…
Page 117

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Intermediate
Low
Benchmark
De.R
IL.6

Intermediate
Mid
Benchmark
De.R
IM.6

Intermediate
High
Benchmark
De.R
IH.6

I can reflect on and provide feedback to a
design solution.

I can interpret feedback from my peers to
revise our design solution

I can work with a team to analyze and explain
the steps of the design solution revision.

Indicator
De.R
IL.6.1
I can work with a team to record feedback
and summarize design solution
recommendations.

Indicator
De.R
IM.6.1
I can work with a team to list and prioritize
feedback to improve our design solution.

Indicator
De.R
IH.6.1
I can work with a team to plan and develop
the steps to improve our design solution.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can record feedback about our
design in my journal.



I can explain some of the solutions
presented as feedback to the group.





I can work with others to make a list
of the most important improvements
that need to be made to the design
solution.



I can...



I can work with others to review
feedback to determine next steps in
the revision process.



I can work with others to make
changes to our prototype that
improves our solution.



I can record my improvement ideas
for a design solution.



I can...

I can...

Page 118

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Intermediate
Low
Benchmark
De.C
IL.7
I can identify improvements or changes in
designs found in various cultures and time
periods.
Indicator
De.C
IL.7.1
I can compare design similarities and
differences among different cultures and time
periods.
Sample Learning Targets

Intermediate
Mid
Benchmark
De.C
IM.7
I can describe why improvements or changes
were made in designs found in various
cultures and time periods.
Indicator
De.C
IM.7.1
I can explain the possible reasons
improvements and/or changes were made in a
design through different cultures and time
periods.
Sample Learning Targets

Intermediate
High
Benchmark
De.C
IH.7
I can analyze a variety of design works from
different cultures and time periods.
Indicator
De.C
IH.7.1
I can recognize patterns in design choices and
make connections to the development of
design through different cultures and time
periods.
Sample Learning Targets



I can write a description about
characteristics of a specific design
style, period, or culture.



I can explain the possible reasons a
chair design evolved through cultures
and time periods.



I can make connections between
design choices on chairs from
different cultures and time periods.



I can compare changes in the designs
of furniture from other cultures over
time.



I can explain the possible reasons a
simple tool changed through cultures
and time periods.



I can make connections between
design choices on furniture from
different cultures and time periods.



I can…



I can…



I can…

Page 119

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Intermediate
Low
Benchmark
De.C
IL.8
I can explore a range of skills shared among
arts disciplines, other content areas and how
they can be applied in a design career.

Intermediate
Mid
Benchmark
De.C
IM.8
I can recognize specific skills shared among
arts disciplines, other content areas and how
they can be applied in a design career.

Indicator
De.C
IL.8.1
I can investigate a range of skills used in
various design careers, arts disciplines, and
content areas.
Sample Learning Targets

Indicator
De.C
IM.8.1
I can name design skills used in various arts
disciplines and content areas and relate these
skills to a career in design.
Sample Learning Targets



I can recognize skills that are specific
to a career in design.



I can research design careers.



I can pick and write about my favorite
design career.



I can list things that are designed by
people with a specific career in
design.



I can match a design product to a
design career.



I can list specific skills needed for a
design career.



I can…



I can…

Intermediate
High
Benchmark
De.C
IH.8
I can analyze the tools, concepts, and
materials used among arts disciplines, other
content areas and how they are used in a
design career.
Indicator
De.C
IH.8.1
I can investigate tools, concepts and materials
used in other arts disciplines and content
areas.
Sample Learning Targets


I can recognize skills that are specific
to design careers that are attained in
other arts disciplines and content
areas.



I can discuss costs of using different
materials to create the same design.



I can…

Page 120

Advanced Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Advanced
Low
Benchmark
De.CR
AL.1
I can work with a team to conceive many
design challenge possibilities relating to a
certain topic.
Indicator
De.CR
AL.1.1
I can work with a team using design thinking
strategies to list several design challenge
possibilities about a topic and select one to
define.
Sample Learning Targets

Advanced
Mid
Benchmark
De.CR
AM.1
I can work with a team to conceive many
design challenge possibilities.

Advanced
High
Benchmark
De.CR
AH.1
I can work on my own to conceive many
design challenge possibilities.

Indicator
De.CR
AM.1.1
I can work with a team using design thinking
strategies to list many design challenge
possibilities and prioritize to select one to
define.
Sample Learning Targets

Indicator
De.CR
AH.1.1
I can use design thinking strategies to list
many design challenge possibilities and
prioritize to select one to define.



I can work with a team to
brainstorm by randomly calling out
ideas.



I can work with a team to
brainstorm by creating questions
rather than ideas to inspire further
thinking.



I can work with a team to use visual
diagrams to organize information and
ideas. I can…

I can work with a team to compare
and contrast the design challenge
options and select one to define.



I can…





I can work with a team using a
variety of the following: list aloud,
popcorn brainstorming,
questioning brainstorming,
webbing, mind mapping to provide
many possible design challenges.

Sample Learning Targets


I can use a variety of the following:
list aloud, popcorn brainstorming,
questioning brainstorming,
webbing, mind mapping to provide
many possible design challenges.



I can compare and contrast the design
challenge options and select one to
define.



I can…
Page 121

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Advanced
Low
Benchmark
De.CR
AL.2
I can work with a team to analyze the aspects
of the design challenge.
Indicator
De.CR
AL.2.1
I can examine my research and report the
connections of that information with the
team.
Sample Learning Targets


I can review with a team the
research from multiple sources.



I can report the connections among
the data to my team.



Advanced
Mid
Benchmark
De.CR
AM.2
I can work independently or with a team to
evaluate the parts of the design challenge.
Indicator
De.CR
AM.2.1
I can work with a team to determine the
importance of the research from the team
members.
Sample Learning Targets


I can work with others to determine
the importance of the production and
cost improvement needed.



I can work with others to determine
the importance of the aesthetic
improvement needed.

I can…


I can work with others to determine
the importance functional
improvement needed.



I can…

Advanced
High
Benchmark
De.CR
AH.2
I can lead a discussion to evaluate the parts of
the design challenge.
Indicator
De.CR
AH.2.1
I can guide my team in determining the
importance of the research from the team
members.
Sample Learning Targets


I can present findings from research
that supports the need for aesthetic,
production, and/or functional
improvements.



I can justify the need for a new design
or redesign concept.



I can…

Page 122

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Advanced
Low
Benchmark
De.CR
AL.3
I can work with a team to analyze usable
design solutions to the challenge.
Indicator
De.CR
AL.3.1
I can examine, discuss, and select possible
design solutions to best address the
challenge.
Sample Learning Targets


I can review and discuss connections
among the possible solutions.



I can work with others to combine
parts of design solution ideas to
solve the design challenge.



I can…

Advanced
Mid
Benchmark
De.CR
AM.3
I can work independently or with a team to
evaluate the usable design solutions to the
challenge.
Indicator
De.CR
AM.3.1
I can work with a team to develop criteria to
determine the value of the usable design
solutions to the challenge.
Sample Learning Targets


I can work with others to list criteria
such as time, cost, functionality,
aesthetics, etc.



I can work with others to prioritize
design solutions based on chosen
criteria.





I can work with a team to reach a
consensus concerning the most viable
solutions to the design challenge.

Advanced
High
Benchmark
De.CR
AH.3
I can lead a discussion to evaluate the usable
design solutions to the challenge.
Indicator
De.CR
AH.3.1
I can guide my team in determining the value
of the usable design solutions to the
challenge.
Sample Learning Targets


I can lead a discussion to determine
the criteria.



I can lead a discussion that reaches a
consensus concerning the most viable
solutions to the design challenge.



I can justify how the solutions
effectively address the identified
needs.



I can…

I can…

Page 123

Anchor Standard 4: I can create an original prototype.
Advanced
Low
Benchmark
De.CR
AL.4
I can work with a team to create a prototype
that solves multiple aspects of a design
challenge.
Indicator
De.CR
AL.4.1
I can work with a team to select materials,
techniques, and processes to create a
prototype.
Sample Learning Targets






I can work with a team to select the
most appropriate materials to
build/compose the prototype from
those explored.
I can work with a team to select the
most appropriate techniques and
processes to build/compose the
prototype from those explored.

Advanced
Mid
Benchmark
De.CR
AM.4
I can work with a team to create a prototype
that solves all aspects of a design challenge
functionally and aesthetically.
Indicator
De.CR
AM.4.1
I can work with a team to select and apply the
best materials, techniques, and processes to
create a prototype.
Sample Learning Targets


I can work with a team to apply the
best materials to build/compose the
prototype from those explored.



I can work with a team to apply the
best techniques and processes to
build/compose the prototype from
those explored.



Advanced
High
Benchmark
De.CR
AH.4
I can use sophisticated materials, techniques,
and processes to create the most viable
prototype.
Indicator
De.CR
AH.4.1
I can select and apply professional materials,
techniques, and processes to create a
prototype.
Sample Learning Targets


I can apply professional materials to
build/compose the prototype.



I can apply professional techniques
and processes to build/compose the
prototype.



I can…

I can…

I can…

Page 124

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Advanced
Low
Benchmark
De.P
AL.5
I can work with a team to prepare and deliver
a presentation to a sample target group.

Indicator
De.P
AL.5.1
I can work in a team to present our design
solution to a group of possible
users/consumers for feedback.
Sample Learning Targets


I can work in a team and ask
questions of the target group so I can
effectively get the feedback.



I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, with a team to attain
feedback from the sample group.



Advanced
Mid
Benchmark
De.P
AM.5
I can work with a team to develop a wellprepared, aesthetically pleasing presentation
for a sample target group that includes
community business leaders or professionals
in the field.
Indicator
De.P
AM.5.1
I can work in a team to present our design
solution to a sample target group that includes
community business leaders and professionals
in a related field for feedback.
Sample Learning Targets




I can…


I can work in a team and ask
questions of the target group with
professionals so I can effectively get
the feedback.
I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, with a team to attain
feedback from the sample group with
business leaders in my community.

Advanced
High
Benchmark
De.P
AH.5
I can develop a well-prepared, aesthetically
pleasing presentation for a sample target
group that includes professionals and business
leaders in my community.
Indicator
De.P
AH.5.1
I can present our design solution to a sample
target audience that includes professionals and
business leaders in a related field for
feedback.
Sample Learning Targets


I can ask questions of the target group
with professionals so I can effectively
get the feedback.



I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, to attain feedback from the
sample group with professionals.



I can…

I can…

Page 125

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Advanced
Low
Benchmark
De.R
AL.6

Advanced
Mid
Benchmark
De.R
AM.6

Advanced
High
Benchmark
De.R
AH.6

I can work with a team to retest our revised
design solution and analyze the results.

I can work with a team to explain future
improvements and repeat the design process
to revise and retest the design solution.

I can facilitate the repetition of the design
process to revise and retest the design
solution.

Indicator
De.R
AL.6.1
I can work with a team to improve the
functionality of our design solution and
record the results of the modifications.

Indicator
De.R
AM.6.1
I can work with a team to repeat the design
process as necessary to improve the design
solution.

Indicator
De.R
AH.6.1
I can guide and frame questions to facilitate
the design process to improve a design
solution.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets




I can work with others to make
improvements to the prototype’s
functionality.
I can chart the progress of our
revisions to help my team improve
the functionality of the design.



I can....




I can retest my solution and revise as
many times as necessary to achieve
the most effective solution.
I can...



I can lead a class discussion on how
to revise a design challenge.



I can form questions to lead the
reflection process.



I can...

Page 126

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Advanced
Low
Benchmark
De.C
AL.7
I can examine past design works to determine
their influence on present designs.

Advanced
Mid
Benchmark
De.C
AM.7
I can work with a team to analyze the
influence of past design works on present
design challenges.

Advanced
High
Benchmark
De.C
AH.7
I can evaluate my design solution to
determine the effective use of past design
works.

Indicator
De.C
AL.7.1
I can find and compare how choices from a
current design reflect influences of past
design solutions.

Indicator
De.C
AM.7.1
I can work with a team to explain how the
designer's choices on the current design
challenge reflect influences of design
solutions from the past.

Indicator
De.C
AH.7.1
I can assess my design choices and relate
them to past design influences.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets



I can identify influences from
previous designs in a current design
solution.



I can work with others to identify
influences from previous designs in a
current design solution.



I can defend my interpretations of
how different styles, periods, and
cultures have influenced my designs.



I can explain how specific past
designs are reflected in a current
design.



I can work with others to explain how
specific past designs are reflected in a
current design.



I can debate my choices made in my
designs that are influenced by
different styles, periods, and cultures.



I can…



I can…



I can…

Page 127

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Advanced
Low
Benchmark
De.C
AL.8
I can apply concepts among arts disciplines
and other content areas to design and analyze
how my interests and skills will prepare me
for a career.

Advanced
Mid
Benchmark
De.C
AM.8
I can explain how economic conditions,
cultural values, and location influence design
and the need for design related careers.

Advanced
High
Benchmark
De.C
AH.8
I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a designer.

Indicator
De.C
AL.8.1
I can use concepts found in various arts
disciplines and other content areas in a design
work.
Sample Learning Targets

Indicator
De.C
AM.8.1
I can describe how economic conditions,
cultural values, and geographic locations
affect design and design careers.
Sample Learning Targets

Indicator
De.C
AH.8.1
I can examine the importance of the work of a
designer in issues that relate to a global
society.
Sample Learning Targets



I can use the elements and/or
principles of arts disciplines in a
current design work.



I can discuss the relationships
between the designer and other
careers.



I can use concepts found in dance in a
current design work.





I can research processes of other
careers to determine how design
affects it.

I can…



I can justify community investment in
design.



I can…



I can defend the impact of design
careers within a society.



I can promote the intrinsic value of
design to individuals and society



I can find an important design
problem in another country and create
a design solution to help.



I can…

Page 128

Design Glossary
Aesthetics Concerned with appearance or the appreciation of beauty.
Artistic Processes The way the brain and the body make art and define the link between art making and the learner.
Aspects A particular part or parts of the design challenge.
Assess To estimate or evaluate the value of information researched.
Beta Testing Using a prototype to receive feedback from a sample target group.
Brainstorm A step in the problem solving process to producing an idea or several ideas. Ex. Popcorn Brainstorming, Passing
Brainstorming, Questioning Brainstorming.
Communication Design Design directed towards making connections between people. Ex. Graphic design, packaging design, web
design, etc.
Craftsmanship A degree or level of skill involved in creating a craft or work of art.
Define (a design challenge) Answering the design challenge questions to provide a clear description of what the design challenge is.
Design An outline, sketch, plan, model, or prototype of a solution to be developed or constructed that considers aesthetic decisions.
See definitions of Object Design, Environmental Design, Communication Design, and Experiential Design.
Design Challenge A design problem or a design issue defined as having the need to be altered, changed, or created in a particular way
to solve.
Design Challenge Questions Basic questions used to gather information concerning a design problem: Who, What, Where, When,
Why, and How. The answers to these questions define the design challenge.
Design Problem A specific design aspect or issue regarded as needing to be dealt with, overcome, or changed.
Design Process A process designed to identify a specific design problem, research the problem, create a solution to the problem, and
present the solution to the problem.
Page 129

Design Solution A means of solving a design problem.
Design Thinking To use one's mind to apply the process of design.
Design Thinking Strategies Methods or procedures used to brainstorm ideas or reason the process of design. Ex: Mind Maps, Concept
Maps, Webbings, Electronic Brainstorms, etc.
Environmental Design Design of surroundings or conditions in which a person, place, or thing interacts. Ex. Interior design,
playground design, community planning, etc.
Experiential Design Design based on personal interactivity or experiences; sometimes referred to as interactive design. Ex. Design
parades, design festivals, design theme parks, etc.
Feedback A reaction or response to a particular design problem or design solution.
Functional Referring to a design having a special activity, purpose, or task.
Interactions A person or a group of persons interacting with a prototype.Investigate To examine, research, or inquire the design
problem or aspects of the design problem in order to create a solution.
Mind Mapping A visual diagram used to organize information and ideas. It starts with a single idea, written or drawn in the center of a
blank page, to which associated words or ideas are added, continuing to associate the words and ideas.
Object Design Design of a material thing often related to industrial and product design. Ex. Design of tools, toys, cars, etc.
Passing Brainstorming A brainstorming technique in which individuals convey ideas one after another building upon the ideas of
others in a group.
Physical Model A three dimensional replication or copy of a prototype
Popcorn Brainstorming A brainstorming technique in which individuals freely state ideas in a group.
Presentation An activity in which an individual or a team shows, describes, or explains a design solution to a group of people.
Prototype A two-dimensional product or three-dimensional model of a design solution. See definitions of physical models, space
models, interactions, and storytelling.
Page 130

Questioning Brainstorming A brainstorming technique in which individuals generate questions in a group that may later be explored.
Research Investigating the design challenge determining the who, what, where, when, why, and how using a variety of research
methods; surveys, observations, interviews, experiments, internet, encyclopedias, newspapers, magazines, etc.
Sample Learning Target A broad lesson learning scenario.
Space Model a 2D or 3D replication or copy within which all things move
Standard Principle that is used as a basis for judgment.
Storytelling The use of words to describe the function or purpose of a prototype
Team A group organized to meet specific goals.
Techniques The use of tools and materials in unique ways that are specific to the designer and the medium.
Webbing Is a brainstorming technique that provides a visual structure or framework for idea development and can assist with
organizing and prioritizing information.

Page 131

References
Alfonso N. (2009, December 2). ABC Nightline - IDEO Shopping Cart [Online Video]. Retrieved from
https://www.youtube.com/watch?v=M66ZU2PCIcM
Lerman, L. &Borstel, J., (2003). Liz Lerman’s critical response process, a method of for getting useful feedback on anything you make,
from dance to dessert. Liz Lerman Dance Exchange.
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
Thayer School of Engineering at Dartmouth. (2012, March 28). IDEO workshop part four: prototyping [Online Video]. Retrieved from
https://www.youtube.com/watch?v=Rbjej4A6oRk
Thayer School of Engineering at Dartmouth. (2012, March 28). IDEO workshop part three: brainstorm [Online Video]. Retrieved from
https://www.youtube.com/watch?v=Ocb1bonXWc8
VandeZande, R. (2016). Art and Design Education [PowerPoint slides]. Retrieved from
https://drive.google.com/a/kent.edu/file/d/0B7Etk0esSRy2NEVMQl9sUVM0bFE/view?usp=sharing
VandeZande, R. (2011). Design education supports social responsibility and the economy. Arts Education Policy Review, 112(1), 2634.

Page 132

South Carolina
College- and Career-Ready Standards for
Media Arts Proficiency

South Carolina Department of Education
Columbia, South Carolina
2017
Page 375

Media Arts
Introduction
Education systems in the United States have long recognized the need for national standards to provide the basis for a common
curriculum and academic programs throughout the country. The South Carolina academic standards for Media Arts are emerging to
new levels in South Carolina’s academic standards for the Visual and Performing Arts. Studies in Media Arts utilize many of the
elements and principles from other arts disciplines while creating a body of work that reflects its own list of developing elements and
principles. Original media artworks reflect the aesthetics that are embedded in the visual and performing arts.
Our students are increasingly using media as a source of communication and networking. It is imperative that our educational system
and its constituents remain current with the trends and technologies that accompany the use of media. This includes appropriate use of
media and the ability to interpret Media Arts productions both socially and professionally to nurture the 21st century learner. The new
21st century skills movement (www.21stcenturyskills.org) specifically references media literacy as one of the skills all students need to
be attractive to employers in this new century. This also aligns with the skills for the South Carolina profile of the high school graduate.
Teachers should understand that these standards need to be reinforced throughout these proficiency levels as the students use more
advanced tools and media applications as well as more complex terminology and concepts.
Studies in Media Arts function as a components of an overall school curriculum that addresses the role of diversity role in the
classroom. Therefore, a school’s Media Arts curriculum should include sequential Media Arts courses as well as specialized courses.
Media Arts can include courses in animation; film studies; graphic design; sound design and recording; digital photography; digital
painting/illustration; and social media/web presence. Programs of study are designed to expose the student to a variety of future career
opportunities while making them globally aware of the importance that Media Arts plays in the classroom and beyond.

Page 376

Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can use past, current and emerging technology tools, procedures, and processes to
create a variety of media artworks in a safe and responsible manner.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.CR
NL.1

Benchmark
MA.CR
NM.1

Benchmark
MA.CR
NH.1

Benchmark
MA.CR
IL.1

Benchmark
MA.CR
IM.1

Benchmark
MA.CR
IH.1

Benchmark
MA.CR
AL.1

Benchmark
MA.CR
AM.1

Benchmark
MA.CR
AH.1

I can
recognize
technology
tools,
procedures
and processes
and use them
in a safe and
responsible
manner to
make media
artworks.

I can identify
a technology
tool,
procedure,
and process to
make still
pictures,
moving
pictures, or
digital audio.

I can identify
multiple
technology
processes to
make still
pictures,
moving
pictures, or
digital audio.

I can explain
and use a
technology
tool,
procedure and
process to
convey
meaning in
media
artwork.

I can explain
and use
multiple
technology
tools,
procedures
and processes
to convey
meaning in
media
artwork.

I can apply the
most
appropriate
technology
tool, procedure
and process to
convey a
message to
make a media
artwork.

I can apply
some
effective
technology
tools,
procedures
and processes
to convey a
message to
make a
variety of
media
artworks.

I can
manipulate
multiple
technology
tools,
procedures
and processes
to convey
messages to
make a
variety of
media
artworks in
the most
effective way.

I can use and
justify the
most effective
technology
tools,
procedures
and processes
to make a
variety of
still, moving,
and/or digital
audio images
to convey
meaning
using
personal
voice.

Page 377

Indicator
MA.CR
NL.1.1
I can safely
and
responsibly
show the
parts of a
technology
tool used to
make media
arts.

Indicator
MA.CR
NM.1.1
I can safely
and
responsibly
identify and
use parts of
some
technology
tools used to
make media
arts.

Indicator
MA.CR
NH.1.1

Indicator
MA.CR
IL.1.1

Indicator
MA.CR
IM.1.1

Indicator
MA.CR
IH.1.1

Indicator
MA.CR
AL.1.1

Indicator
MA.CR
AM.1.1

Indicator
MA.CR
AH.1.1

I can safely
and
responsibly
identify and
use multiple
technology
tools to make
media arts.

I can explain
and safely use
a technology
tool to
convey
meaning in
media arts.

I can explain
and safely use
multiple
technology
tools to
convey
meaning in
media arts.

I can choose
some
appropriate
technology
tools to convey
a message
while making a
media artwork
in a safe and
responsible
manner.

I can choose
multiple
effective
technology
tools to
convey a
message
while making
a variety of
media
artworks in a
safe and
responsible
manner.

I can
manipulate
multiple
technology
tools to
convey
messages to
make a
variety of
media
artworks in
the most
effective way
in a safe and
responsible
manner.

I can justify
the most
effective
technology
tools to make
a variety of
media
artworks that
convey
meaning
using
personal
voice in a safe
and
responsible
manner.

Page 378

Indicator
MA.CR
NL.1.2

Indicator
MA.CR
NM.1.2

Indicator
MA.CR
NH.1.2

Indicator
MA.CR
IL.1.2

Indicator
MA.CR
IM.1.2

Indicator
MA.CR
IH.1.2

Indicator
MA.CR
AL.1.2

Indicator
MA.CR
AM.1.2

Indicator
MA.CR
AH.1.2

I can follow
the steps of
some
technology
procedures
and processes
to make
media
artworks.

I can identify
the steps of a
technology
procedure and
process to
make media
artworks.

I can identify
the steps of
multiple
technology
procedures
and processes
to make
media
artworks.

I can explain
the steps of a
technology
procedure and
process to
convey
meaning in
media arts.

I can explain
the steps of
multiple
technology
procedures
and processes
to convey
meaning in
media arts.

I can choose the
appropriate
technology
procedure to
convey a
message while
making a media
artwork.

I can choose
some
effective
technology
procedures
and processes
to convey a
message
while making
a variety of
media
artworks.

I can
manipulate
multiple
technology
procedures
and processes
to convey
messages to
make a
variety of
media
artworks in
the most
effective way.

I can justify
the most
effective
technology
procedures
and processes
to make a
variety of
media
artworks that
convey
meaning
using
personal
voice.

Page 379

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles.
Benchmark
MA.CR
NL.2

Benchmark
MA.CR
NM.2

Benchmark
MA.CR
NH.2

Benchmark
MA.CR
IL.2

Benchmark
MA.CR
IM.2

Benchmark
MA.CR
IH.2

Benchmark
MA.CR
AL.2

Benchmark
MA.CR
AM.2

Benchmark
MA.CR
AH.2

I can
recognize and
explore some
elements and
principles in
media arts.

I can combine
elements and
principles of
media arts to
make media
artwork.

I can
communicate
meaning in
my work by
selecting and
arranging
elements and
principles of
media arts.

I can apply
elements and
principles of
media arts to
revise my
work.

I can analyze
and apply the
elements and
principles of
media arts as
a response to
an artistic
problem.

I can analyze
my media
artwork
through a
critique and
refine my
work based on
given criteria.

I can create,
refine, and
communicate
ideas based on
the elements
and principles
of media arts
to complete a
variety of
media
artworks.

I can
document and
justify the
planning and
development
of a media
artwork from
the inception
of the idea to
completion.

I can create a
body of work
in a variety of
media art
forms that
explore
personal
themes, ideas,
or concepts.

Indicator
MA.CR
NL.2.1

Indicator
MA.CR
NM.2.1

Indicator
MA.CR
NH.2.1

Indicator
MA.CR
IL.2.1

Indicator
MA.CR
IM.2.1

Indicator
MA.CR
IH.2.1

Indicator
MA.CR
AL.2.1

Indicator
MA.CR
AM.2.1

Indicator
MA.CR
AH.2.1

I can
recognize
some
elements or
principles of
media arts to
communicate
an idea.

I can combine
elements and
principles of
media arts
using multiple
media
techniques.

I can change
the meaning
of a media
artwork using
different
elements or
principles.

I can identify
improvement
s needed in
my media
artwork and
explore
strategies to
strengthen the
intended
meaning.

I can explain
how multiple
elements or
principles of
media arts are
used to
convey
meaning in
media
artworks.

I can
participate in a
formal critique
to revise my
artwork.

I can apply
organizational
strategies that
communicate a
personal
meaning,
theme, idea, or
concept.

I can create a
process folio
to document
the planning
of a media
artwork.

I can explain
and defend
the choices I
made to
communicate
my artistic
ideas across
multiples
media
artworks.
Page 380

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content, ideas,
skills, and media works for display.
Benchmark
MA.P
NL.3

Benchmark
MA.P
NM.3

Benchmark
MA.P
NH.3

Benchmark
MA.P
IL.3

Benchmark
MA.P
IM.3

Benchmark
MA.P
IH.3

Benchmark
MA.P
AL.3

Benchmark
MA.P
AM.3

Benchmark
MA.P
AH.3

I can identify
media
artworks as
communication.

I can explain
ways media
artworks are
presented.

I can consider
audience
response
when
discussing
how media
artworks are
presented.

I can identify
a target
audience for
presentation
of my media
artwork.

I can identify
and choose
multiple
formats used
in presenting
media
artworks for a
target
audience.

I can compare
presentation
formats for
different
media
artworks and
its intended
audience.

I can present
media
artworks
considering
combinations
of formats and
target
audience.

I can analyze
and interpret
the
effectiveness
of a media
arts
presentation
for an
intended
audience.

I can promote
and present
media
artworks for
intentional
impacts
through a
variety of
contexts such
as markets
and venues.

Indicator
MA.P
IH.3.1

Indicator
MA.P
AL.3.1

Indicator
MA.P
AM.3.1

Indicator
MA.P
AH.3.1

I can select my
intended
audience and
choose
multiple media
formats to get
the most
views.

I can evaluate
the
effectiveness
of virtual and
physical
presentations
of a media
artwork.

I can create a
media plan
(funding,
distribution,
and viewing)
to promote
my media
artwork.

Indicator
MA.P
NL.3.1

Indicator
MA.P
NM.3.1

Indicator
MA.P
NH.3.1

Indicator
MA.P
IL.3.1

Indicator
MA.P
IM.3.1

I can present
a media
artwork to an
audience.

I can identify
venues
appropriate
for still and
moving
images in
media
artworks.

I can examine
how audience
response
varies
depending on
how media
artwork is
presented.

I can identify
multiple ways
to share my
work through
different
media outlets.

I can choose
proper format
for my media
artwork.

I can choose
the most
effective
media format
for a select
audience.

Page 381

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Benchmark
MA.R
NL.4

Benchmark
MA.R
NM.4

Benchmark
MA.R
NH.4

Benchmark
MA.R
IL.4

Benchmark
MA.R
IM.4

Benchmark
MA.R
IH.4

Benchmark
MA.R
AL.4

Benchmark
MA.R
AM.4

Benchmark
MA.R
AH.4

I can explore
message and
purpose in
my media
artworks and
the work of
others.

I can identify
some
messages and
purposes in
media
artworks.

I can identify
the messages
and purposes,
in my media
artworks and
the work of
others.

I can explain
the messages
and purposes
in media
artworks.

I can analyze
and describe
reactions and
interpretation
s of messages
and purposes
in a variety of
media
artworks.

I can
investigate
personal and
group
intentions
about
messages and
purposes in
media
artworks.

I can discuss
and analyze
the message
and purpose in
a variety of
media
artworks.

I can analyze
the message
and intent of
a variety of
media
artworks.

I can justify
the message,
intent, and
impact of
diverse media
artworks,
considering
complex
factors of
context and
bias.

Page 382

Indicator
MA.R
NL.4.1

Indicator
MA.R
NM.4.1

Indicator
MA.R
NH.4.1

Indicator
MA.R
IL.4.1

Indicator
MA.R
IM.4.1

Indicator
MA.R
IH.4.1

Indicator
MA.R
AL.4.1

Indicator
MA.R
AM.4.1

Indicator
MA.R
AH.4.1

I can describe
parts of a
media
artwork.

I can identify
how media
artworks are
put together.

I can identify
the subject,
composition,
and media
arts elements
and
principles
for a variety
of media
artworks.

I can explain
how to use
the elements
and
principles of
media art to
compose a
media
artwork.

I can show
the
similarities
and
differences in
how media
artworks are
organized by
the elements
and
principles.

I can rephrase
ways in which
a variety of
media
artworks
organize
criteria.

I can analyze
the
organization of
the elements
and principles
of media
artworks.

I can critique
how the
composition
characteristics
in multiple
media
artworks work
together.

I can justify
the
organizational
choices made
by media
artists.

Indicator
MA.R
NL.4.2

Indicator
MA.R
NM.4.2

Indicator
MA.R
NH.4.2

Indicator
MA.R
IL.4.2

Indicator
MA.R
IM.4.2

Indicator
MA.R
IH.4.2

Indicator
MA.R
AL.4.2

Indicator
MA.R
AM.4.2

Indicator
MA.R
AH.4.2

I can describe
my thoughts
about
messages in
media
artworks.

I can identify
ideas, issues,
and/or
experiences
presented in
the messages
of media
artworks.

I can explain
the
techniques
used in
different
media
artworks that
reflect
varying
messages and
points of
view.

I can explore
the language,
tone, and
point of view
used in media
texts to
influence
meaning and
interpretation
of messages.

I can analyze
the
effectiveness
of a
presentation
and treatment
of messages in
media artwork.

I can interpret
the qualities
of and
relationships
between the
components,
style, and
message
communicate
d by media
artworks and
artists.

I can justify
my
interpretation
of language,
tone, and
point of view
of the
message in a
media
artwork.

I can name a
message in
media
artworks.

I can
investigate
increasingly
complex
messages in
media
artworks.

Page 383

Indicator
MA.R
NL.4.3
I can name a
purpose of
some media
artworks.

Indicator
MA.R
NL.4.4
I can make a
statement
about my
media
artwork.

Indicator
MA.R
NM.4.3

Indicator
MA.R
NH.4.3

Indicator
MA.R
IL.4.3

Indicator
MA.R
IM.4.3

Indicator
MA.R
IH.4.3

Indicator
MA.R
AL.4.3

Indicator
MA.R
AM.4.3

Indicator
MA.R
AH.4.3

I can identify
the purpose of
a media
artwork.

I can identify
the purpose
and audience
of a media
artwork.

I can explain
that different
media can
produce
artworks that
have the same
purpose.

I can
investigate
increasingly
complex
techniques
that artists use
to convey
purpose in
media
artwork.

I can find and
interpret data
to explore
multiple
differences in
the purpose of
media artwork.

I can analyze
formal and
informal
situations, the
effectiveness
of
presentation,
and treatment
of media to
convey the
purpose.

I can analyze
and interpret
the qualities
of
relationships
between the
components,
style,
message, and
how they
relate to the
purpose.

I can justify
my
interpretation
and
explanation of
the purpose of
multiple
media
artwork.

Indicator
MA.R
NM.4.4

Indicator
MA.R
NH.4.4

Indicator
MA.R
IL.4.4

Indicator
MA.R
IM.4.4

Indicator
MA.R
IH.4.4

Indicator
MA.R
AL.4.4

Indicator
MA.R
AM.4.4

Indicator
MA.R
AH.4.4

I can describe
multiple
elements and
principles of
media art in
my work.

I can identify
elements and
principals of
media arts in
artist’s
statements.

I can develop
an artist’s
statement that
describes
media arts
criteria and
intent of my
work.

I can develop
an artist’s
statement that
merges
personal
influences
with intent
and media arts
criteria for my
work.

I can develop
an artist’s
statement that
identifies
common
themes in
personal
influences,
intent and
media arts
criteria for
work.

I can justify
my choices of
criteria,
cultural
influences,
personal
experiences,
to create my
own voice in
my artist
statement.

I can describe I can
my media
recognize an
artwork.
element
and/or
principle of
media art in
my work.

Page 384

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Benchmark
MA.C
NL.5

Benchmark
MA.C
NM.5

Benchmark
MA.C
NH.5

Benchmark
MA.C
IL.5

Benchmark
MA.C
IM.5

Benchmark
MA.C
IH.5

Benchmark
MA.C
AL.5

Benchmark
MA.C
AM.5

Benchmark
MA.C
AH.5

I can explore
ideas that
connect
media arts to
different
cultures and
time periods.

I can
recognize
some
examples of
media arts
found in
different
cultures and
time periods.

I can identify
media arts
used for
different
purposes in
various
cultures and
time periods.

I can analyze
a variety of
media
artworks from
different
cultures and
time periods.

I can describe
why different
media
artworks are
used for
different
purposes in
various
cultures and
time periods.

I can analyze
similarities
and
differences in
media
artworks
among
different
cultures and
time periods.

I can examine
past media arts
works to
determine their
influence on
media today.

I can explain
the influence
of past media
arts works
throughout
different time
periods and
how that
reflects on
media today.

I can evaluate
media arts
works from
the past and
apply the
most effective
ones to my
work and the
work of
others.

Page 385

Indicator
MA.C
NL.5.1

Indicator
MA.C
NM.5.1

Indicator
MA.C
NH.5.1

Indicator
MA.C
IL.5.1

Indicator
MA.C
IM.5.1

Indicator
MA.C
IH.5.1

Indicator
MA.C
AL.5.1

Indicator
MA.C
AM.5.1

I can
recognize
ideas that
connect
media arts to
history,
cultures, and
the world.

I can relate to
ideas that
connect
media arts to
history,
cultures, and
the world.

I can show
how ideas
connect
media arts to
history,
cultures, and
the world.

I can explain
how ideas
connect
media arts to
history,
cultures, and
the world.

I can
compare and
contrast how
to connect
media arts
ideas to
history,
cultures, and
the world.

I can interpret
how media
arts ideas
connect to
history,
cultures, and
the world.

I can
participate in
formal and
informal
situations
relating to how
media art
connects to
history,
cultures, and
the world.

I can examine
the
relationship
between
media arts,
history,
cultures, and
the world.

Indicator
MA.C
AH.5.1
I can justify
the
relationship
between
media arts,
history,
cultures, and
the world.

Page 386

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Benchmark
MA.C
NL.6

Benchmark
MA.C
NM.6

Benchmark
MA.C
NH.6

Benchmark
MA.C
IL.6

Benchmark
MA.C
IM.6

Benchmark
MA.C
IH.6

Benchmark
MA.C
AL.6

Benchmark
MA.C
AM.6

Benchmark
MA.C
AH.6

I can explore
media arts
concepts
among other
arts
disciplines,
content areas,
and related
careers.

I can
recognize a
media arts
concept
among other
arts
disciplines,
content areas,
and related
careers.

I can apply
media arts
concepts
among other
arts
disciplines,
content areas,
and related
careers.

I can explore
a range of
media arts
skills shared
among other
arts
disciplines,
content areas,
and careers.

I can
recognize
specific
media arts
skills shared
among other
arts
disciplines,
content areas,
and careers.

I can analyze
the media arts
tools,
concepts, and
materials used
among other
arts
disciplines,
content areas,
and careers.

I can apply
media arts
concepts to
other arts
disciplines and
content areas
to prepare me
for a career.

I can research
aspects of
media arts
careers to
influence my
career path.

I can analyze
complex ideas
from other
arts
disciplines
and content
areas to
inspire my
creative work
and evaluate
its impact on
my artistic
perspective.

Page 387

Indicator
MA.C
NL.6.1

Indicator
MA.C
NM.6.1

Indicator
MA.C
NH.6.1

I can identify
a relationship
between
media arts
and another
subject in my
school.

I can
demonstrate a
relationship
between
media arts
and another
subject in my
school.

I can
demonstrate
and describe
the
relationship
between
media arts
and a concept
from another
subject in my
school.

Indicator
MA.C
NL.6.2

Indicator
MA.C
NM.6.2

Indicator
MA.C
NH.6.2

I can identify
different
types of
media arts
that interest
me.

I can identify
and
demonstrate
the skills used
to make
media
artwork that
interests me.

I can describe
specific
careers in
media arts.

Indicator
MA.C
IL.6.1
I can apply
media arts
concepts to
other arts
disciplines
and content
areas.

Indicator
MA.C
IL.6.2
I can
demonstrate
and describe
the skills
needed for
careers in
media arts.

Indicator
MA.C
IM.6.1

Indicator
MA.C
IH.6.1

Indicator
MA.C
AL.6.1

Indicator
MA.C
AM.6.1

I can examine
the
relationship
between
media arts
and specific
content from
another arts
discipline and
content area.

I can apply
concepts from
other arts
disciplines and
content areas
to my media
artwork.

I can explain
ideas from
other arts
disciplines and
content areas
through media
arts.

Indicator
MA.C
IM.6.2

Indicator
MA.C
IH.6.2

Indicator
MA.C
AL.6.2

Indicator
MA.C
AM.6.2

Indicator
MA.C
AH.6.2

I can identify
specific skills
required for
various
careers in
media arts.

I can research
topics about
careers in
media arts that
interest me.

I can identify
the skills,
training, and
education
necessary to
pursue a career
in media arts
that interests
me.

I can pursue
opportunities
that will lead
me to a career
in media arts.

I can
demonstrate
skills
necessary for
a career in
media arts.

I can explain
how
economic
conditions,
cultural
values, and
location
influence
media arts
and the need
for related
careers.

Indicator
MA.C
AH.6.1
I can research
societal,
political, and
cultural issues
as they relate
to other arts
and content
areas and
apply to my
role as a
media artist.

Page 388

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Benchmark
MA.C
NL.7

Benchmark
MA.C
NM.7

I can imitate
digital
citizenship
when I am
online.

I can identify
digital
citizenship
when I am
online.

Benchmark
MA.C
NH.7
I can show
digital
citizenship
when I am
online.

Benchmark
MA.C
IL.7
I can model
and explain
aspects of
digital
citizenship
when I am
online.

Benchmark
MA.C
IM.7

Benchmark
MA.C
IH.7

Benchmark
MA.C
AL.7

I can describe
different
aspects of
digital
citizenship
when I am
online.

I can interpret
different
aspects of
digital
citizenship
when I am
online.

I can
participate in
formal and
informal
situations to
discuss and
demonstrate
digital
citizenship
when I am
online.

Benchmark
MA.C
AM.7

Benchmark
MA.C
AH.7

I can analyze
and identify
the
appropriate
digital
citizenship
strategy to use
when I am
online.

I can justify
my choice of
digital
citizenship
strategy to
use when I
am online.

Page 389

Indicator
MA.C
NL.7.1
Internet
Safety

Indicator
MA.C
NM.7.1
Internet
Safety

Indicator
MA.C
NH.7.1
Internet
Safety

Indicator
MA.C
IL.7.1
Internet
Safety

I can explore
the internet
safely and
responsibly
when logging
on to my
device.

I can identify
several safe
ways to
search topics
on the
internet.

I can share
with others
how to safely
search for
information
on the
internet.

I can explain
and
demonstrate
several safe
and reliable
ways to
search for
accurate
information
on the
internet.

Indicator
MA.C
IM.7.1
Internet
Safety
I can
collaborate
with other
students in
various safe
and reliable
ways to
search for
information
on the
internet.

Indicator
MA.C
IH.7.1
Internet
Safety

Indicator
MA.C
AL.7.1
Internet Safety

Indicator
MA.C
AM.7.1
Internet
Safety

Indicator
MA.C
AH.7.1
Internet
Safety

I can identify
predictable
situation when
using the
internet.

I can
participate in
formal and
informal
situations
when
collaborating
with others
and can model
appropriate
and positive
etiquette.

I can analyze
various ways
to use digital
citizenship to
collaborate
with the
world in an
appropriate
and positive
way.

I can compile
a selection of
information
that is found
on the
internet and
how helps me
justify my
own voice as
an artist.

Page 390

Indicator
MA.C
NL.7.2
Digital
Footprint
Privacy

Indicator
MA.C
NM.7.2
Digital
Footprint
Privacy

Indicator
MA.C
NH.7.2
Digital
Footprint
Privacy

Indicator
MA.C
IL.7.2
Digital
Footprint
Privacy

Indicator
MA.C
IM.7.2
Digital
Footprint
Privacy

Indicator
MA.C
IH.7.2
Digital
Footprint
Privacy

Indicator
MA.C
AL.7.2
Digital
Footprint
Privacy

Indicator
MA.C
AM.7.2
Digital
Footprint
Privacy

Indicator
MA.C
AH.7.2
Digital
Footprint
Privacy

I can explore
how to post
safely on the
internet.

I can identify
several safe
online
platforms to
post on the
internet.

I can share
various ways
to post safely
on the
internet.

I can explain
and model
how to post
safely on the
internet.

I can analyze
various ways
to post safely
on the
internet.

I can
investigate
several ways
that
information on
the internet is
not safe or
responsible
and ways to
respond to
these
problems.

I can
participate in
formal and
informal
situations
when
collaborating
with others to
post safely on
the internet.

I can
investigate
various ways
to post safely
on the internet
and know the
difference
between a
positive and a
negative post.

I can justify
my choice of
what I post on
the internet to
interact with
the world in
an appropriate
and positive
way.

Page 391

Indicator
MA.C
NL.7.3
Copyright

Indicator
MA.C
NM.7.3
Copyright

Indicator
MA.C
NH.7.3
Copyright

Indicator
MA.C
IL.7.3
Copyright

Indicator
MA.C
IM.7.3
Copyright

Indicator
MA.C
IH.7.3
Copyright

I can identify
that a media
artwork has
an owner.

I can find the
owner of a
media
artwork on
the internet.

I can credit
the owner of
media
artwork on
the internet
when I intend
to use it.

I can explain
and model the
use of media
artwork that
is owned by
another artist,
and can
demonstrate
my
responsibilities and rights
when using
the work for
educational
purposes.

I can identify
media
artwork that
is owned by
another artist,
and can
demonstrate
my
responsibilities and rights
when using
the work for
educational or
personal
purposes.

I can handle
unexpected
situations with
copyright and
fair use rules
as it applies to
my artwork,
performance,
or
presentation.

Indicator
MA.C
AL.7.3
Copyright

I can
participate in
formal and
informal
situations
when
collaborating
with others to
discuss
copyright laws
that apply to a
media artwork.

Indicator
MA.C
AM.7.3
Copyright

Indicator
MA.C
AH.7.3
Copyright

I can analyze
and
synthesize
various ways
that copyright
laws apply to
my work and
the work of
others.

I can justify
my choice of
how I use
copyright
law to protect
my work and
the work of
others.

Page 392

Novice Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new artistic ideas and work.
Anchor Standard 1: I can use past, current and emerging technology tools, procedures, and processes to
create a variety of media artworks in a safe and responsible manner.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.CR
NL.1

Benchmark
MA.CR
NM.1

Benchmark
MA.CR
NH.1

I can recognize technology tools, procedures
and processes and use them in a safe and
responsible manner to make media artworks.

I can identify a technology tool, procedure,
and process to make still pictures, moving
pictures, or digital audio.

I can identify multiple technology processes to
make still pictures, moving pictures, or digital
audio.

Page 393

Indicator
MA.CR
NL.1.1

Indicator
MA.CR
NM.1.1

Indicator
MA.CR
NH.1.1

I can safely and responsibly show the parts of
a technology tool used to make media arts.

I can safely and responsibly identify and use
parts of some technology tools used to make
media arts.

I can safely and responsibly identify and use
multiple technology tools to make media arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can name some parts of a camera.

• I can label the parts of a camera.

• I can name the differences between a
computer, tablet, and a smartphone.

• I can follow the safety procedures when
using a media tool.

• I can pick from a list of tools which tool
would be the best to take a photograph that
tells a story about what makes me happy.

• I can...

• I can secure an iPad into a stand safely.

• I can identify where external components
are entered into a device.

• I can...

• I can...

Page 394

Indicator
MA.CR
NL.1.2

Indicator
MA.CR
NM.1.2

Indicator
MA.CR
NH.1.2

I can follow the steps of some technology
procedures and processes to make media
artworks.

I can identify the steps of a technology
procedure and process to make media
artworks.

I can identify the steps of multiple technology
procedures and processes to make media
artworks.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can take a photograph.

• I can take a photograph that is in focus.

• I can record my voice.

• I can take picture to tell a story.

• I can make an instructional video on how to
take a photograph. (The video will include
still and moving pictures.)

• I can record a video.

• I can video someone teaching a lesson.

• I can...

• I can...

• I can integrate still and moving images into
an iMovie trailer.
• I can...

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles of media arts.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.CR
NL.2

Benchmark
MA.CR
NM.2

Benchmark
MA.CR
NH.2

I can recognize and explore some elements
and principles in media arts.

I can combine elements and principles of
media arts to make media artwork.

I can communicate meaning in my work by
selecting and arranging elements and
principles of media arts.

Page 395

Indicator
MA.CR
NL.2.1

Indicator
MA.CR
NM.2.1

Indicator
MA.CR
NH.2.1

I can recognize some elements or principles
of media arts to communicate an idea.

I can combine elements and principles of
media arts using multiple media techniques.

I can change the meaning of a media artwork
using different elements or principles.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify when the music changes mood • I can use sound and moving images to tell a
in a film.
story.

• I can change the font and color on a
magazine cover to create emphasis.

• I can explore different music choices for a
video.

• I can change the speed and camera angle of
an animation.

• I can use lighting and contrast in a photo to
convey mood.

• I can explore how different fonts are used on • I can...
magazine covers.

• I can...

• I can ...

Page 396

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content, ideas,
skills, and media works for display.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.P
NL.3

Benchmark
MA.P
NM.3

Benchmark
MA.P
NH.3

I can identify media artworks as
communication.

I can explain ways media artworks are
presented.

I can consider audience response when
discussing how media arts works are
presented.

Page 397

Indicator
MA.P
NL.3.1

Indicator
MA.P
NM.3.1

Indicator
MA.P
NH.3.1

I can present a media artwork to an audience.

I can identify venues appropriate for still and
moving images in media artworks.

I can examine how audience response varies
depending on how media artwork is presented.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can present photography as a media
artwork.

• I can name places to show my work.

• I can identify how children respond
differently to a cereal commercial than
adults.

• I can present film as moving pictures to
create meaning.
• I can present posters and brochures as
advertisements.
• I can...

• I can identify where a still image is more
appropriate to use than a moving image.
• I can identify two places to post a video.
• I can…

• I can identify how a movie trailer and a
movie poster target audience for different
purposes.
• I can examine how different ages of people
chose different formats to view media.
• I can...

Page 398

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.R
NL.4

Benchmark
MA.R
NM.4

Benchmark
MA.R
NH.4

I can explore message and purpose in my
media artworks and the work of others.

I can identify some messages and purposes in
media artworks.

I can identify the messages and purposes, in
my media artworks and the work of others.

Page 399

Indicator
MA.R
NL.4.1

Indicator
MA.R
NM.4.1

Indicator
MA.R
NH.4.1

I can describe parts of a media artwork.

I can identify how media artworks are put
together.

I can identify the subject, composition, and
media arts elements and principles for a
variety of media artworks.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify a type of media artwork
(video, podcast, animation, etc.)

• I can name color as an element of a media
artwork.

• I can describe how different camera angles
are used in a film.

• I can name the subject in a photograph.

• I can tell that a photograph only shows
value, as an element of art, because it is
black and white.

• I can describe the rule of thirds and how it is
used in more than one media art form.

• I can identify color, size, font, and space
choices in a media artwork.
• I can...

• I can define an element and a principle of
media art in a short film or advertisement.
• I can…

• I can recognize how lighting is used to
change the mood or intent of the film.
• I can recognize how costume choices are
used to convey meaning.
• I can...

Page 400

Indicator
MA.R
NL.4.2

Indicator
MA.R
NM.4.2

Indicator
MA.R
NH.4.2

I can name a message in media artworks.

I can describe my thoughts about messages in
media artworks.

I can identify ideas, issues, and/or experiences
presented in the messages of media artworks.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify a message in a commercial.

• I can tell my thoughts about the message in a • I can explain my thoughts about the ideas,
movie, video, etc.
issues and experiences shown on a TV
episode and movie.

• I can identify the message in a print
advertisement.
• I can identify the plot in a movie.
• I can…

• I can explain for whom a media artwork
message was created.
• I can...

• I can explain my thoughts about ideas,
issues and experiences shown in an
advertisement and photograph.
• I can explain my thoughts about the
effectiveness of an advertisement or film.
• I can…

Page 401

Indicator
MA.R
NL.4.3

Indicator
MA.R
NM.4.3

Indicator
MA.R
NH.4.3

I can name a purpose of some media artworks. I can identify the purpose of a media artwork.

I can identify the purpose and audience of a
media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can predict the message of a media artwork • I can describe the preferences of an artist
based on an image.
that makes a video blog.
•
• I can describe the preferences of a radio
• I can identify the choices made by a
broadcaster.
choreographer in music video.
• I can explain the choices made by a
filmmaker when making a movie.
• I can...

• I can identify how a filmmaker uses light,
sound, costume, and setting to convey a
purpose.

• I can explain why different age groups may
respond differently to an internet meme.
• I can explain how different age groups may
respond differently to a movie or TV show.
• I can explain how people from different
backgrounds would react to a video game.
• I can...

• I can…

Page 402

Indicator
MA.R
NL.4.4

Indicator
MA.R
NM.4.4

Indicator
MA.R
NH.4.4

I can make a statement about my media
artwork.

I can describe my media artwork.

I can recognize an element and/or principle
of media art in my work.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can name the subject of my media
artwork.

• I can explain what inspired me to make my
artwork.

• I can explain some compositional elements
in my media artwork.

• I can name the setting of my media artwork.

• I can explain how I made my media
artwork.

• I can write a title for my work.

• I can...

• I can...

• I can...

• I can explain how setting, color, lighting,
etc., are used in my work.

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.C
NL.5

Benchmark
MA.C
NM.5

Benchmark
MA.C
NH.5

I can explore ideas that connect media arts to
different cultures and time periods.

I can recognize some examples of media arts
found in different cultures and time periods.

I can identify media arts used for different
purposes in various cultures and time periods.

Page 403

Indicator
MA.C
NL.5.1

Indicator
MA.C
NM.5.1

Indicator
MA.C
NH.5.1

I can recognize ideas that connect media arts
to history, cultures, and the world.

I can relate to ideas that connect media arts to
history, cultures, and the world.

I can show how ideas connect media arts to
history, cultures, and the world.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can watch a commercial and recognize that • I discuss ideas that connect to different
cultures are different based on clothing,
cultures other than my own by watching a
language or environment.
video, newsbyte, or commercial.
• I can choose a book that shows differences
in cultures and/or time periods.

• I can discuss ideas that connect to my
classmates from different cultures after
viewing current news reports.

• I can write about the connections to another
culture when looking at a media artwork.
• I can present a film that connects to my
family history.
• I can...

• I can...
• I can make an infographic about different
cultures and historical figures.
• I can...

Page 404

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.C
NL.6

Benchmark
MA.C
NM.6

Benchmark
MA.C
NH.6

I can explore media arts concepts among other I can recognize a media arts concept among
arts disciplines, content areas, and related
other arts disciplines, content areas, and
careers.
related careers.

I can apply media arts concepts among other
arts disciplines, content areas, and related
careers.

Page 405

Indicator
MA.C
NL.6.1

Indicator
MA.C
NM.6.1

Indicator
MA.C
NH.6.1

I can identify a relationship between media
arts and another subject in my school.

I can demonstrate a relationship between
media arts and another subject in my school.

I can demonstrate and describe the relationship
between media arts and a concept from
another subject in my school.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can gather an example and show how a
media art and a fine art can be the same, or
similar.

• I can show and describe the relationship
between a media art and fine art in a media
presentation.

• I can show how ideas connect media arts
and fine arts by creating a media
presentation. (Portraits)

• I can find ways that line can be shown in
visual art, dance, and media art.

• I can look at three commercials and tell what • I can show how ideas connect line in visual
other areas are connected within the
art, dance, and media art.
commercial such as E*TRADE’s baby,
Doritos and Clorox.
• I can make a picture, advertisement or short
video that uses another discipline.
• I can…
• I can...

• I can name another discipline used in a
video or picture conveying a message to an
audience.
• I can...

Page 406

Indicator
MA.C
NL.6.2

Indicator
MA.C
NM.6.2

Indicator
MA.C
NH.6.2

I can identify different types of media arts that I can identify and demonstrate the skills used
interest me.
to make a media artwork that interests me.

I can describe specific careers in media arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify the media I use at home or
school.

• I can name the skills used to make a
magazine layout.

• I can document the purpose of a director on
a film.

• I can identify the media I use for different
purposes.

• I can name the skills used to make a music
video.

• I can research roles in video game and music
video production.

• I can...

• I can name the skill used to make a
commercial or video game.

• I can research the role of a sound engineer.

• I can name the skills used to create a
podcast.
• I can...

• I can review movie credits to see all the
careers needed to make a movie.
• I can...

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Novice
Low

Novice
Mid

Novice
High

Benchmark
MA.C
NL.7

Benchmark
MA.C
NM.7

Benchmark
MA.C
NH.7

I can imitate digital citizenship when I am
online.

I can identify digital citizenship when I am
online.

I can show digital citizenship when I am
online.

Page 407

Indicator
MA.C
NL.7.1
Internet Safety

Indicator
MA.C
NM.7.1
Internet Safety

Indicator
MA.C
NH.7.1
Internet Safety

I can explore the internet safely and
responsibly when logging on to my device.

I can identify several safe ways to search
topics on the internet.

I can share with others how to safely search
for information on the internet.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can know where to find my password.

• I can remember my password, log onto a
computer and use a computer application
with my student account.

• I can share with others how to safely log in
to my computer and protect my login in and
personal information.

• I can log in to my electronic device.
• I can follow acceptable use policies at my
school, home, or in public.

• I can create a bookmark for a website on my • I can share a document safely and
browser.
responsibly on the internet within a group of
my peers.

• I can...

• I can download an approved application.

• I can...

• I can...

Page 408

Indicator
MA.C
NL.7.2
Digital Footprint
Privacy

Indicator
MA.C
NM.7.2
Digital Footprint
Privacy

Indicator
MA.C
NH.7.2
Digital Footprint
Privacy

I can explore how to post safely on the
internet.

I can identify several safe online platforms to
post on the internet.

I can share various ways to post safely on the
internet

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can give examples of positive ways to post • I can explore what information is
thoughts and ideas on the internet.
appropriate to post online.

• I can share visual examples of good ways to
post on the internet.

• I can post images while protecting my
identity and the identity of others.

• I can follow acceptable use policies for
posting online.

• I can...

• I can...

• I can...

Page 409

Indicator
MA.C
NL.7.3
Copyright

Indicator
MA.C
NM.7.3
Copyright

Indicator
MA.C
NH.7.3
Copyright

I can identify that a media artwork has an
owner.

I can find the owner of a media artwork on the
internet.

I can credit the owner of media artwork on the
internet when I intend to use it.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can recognize a watermark.

• I can locate the watermark on a photograph. • I can tag a photo with the owner.

• I can recognize the credits on a film.

I can use correct spelling and vocabulary it
search topics.

• I can create credits when making an iMovie
trailer.

• I can identify safe search engines and
databases.

• I can help a peer safely find a video on the
internet.

• I can locate the credits for a video on a
website.

• I can work with other to search for
information on a group project.

• I can find headers and footers to check facts
on a website.

• I can...

• I can safely search for soundbites to use in
my media artwork.
• I can safely search for photographs taken by
a famous photographer on the internet.
• I can...

• I can...

Page 410

Intermediate Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new artistic ideas and work.
Anchor Standard 1: I can use past, current, and emerging technology tools, procedures and processes to
create a variety of media artworks in a safe and responsible manner.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.CR
IL.1

Benchmark
MA.CR
IM.1

Benchmark
MA.CR
IH.1

I can explain and use a technology tool,
procedure and process to convey meaning in
media artwork.

I can explain and use multiple technology
tools, procedures and processes to convey
meaning in media artwork.

I can apply the most appropriate technology
tool, procedure and process to convey a
message to make a media artwork.

Page 411

Indicator
MA.CR
IL.1.1

Indicator
MA.CR
IM.1.1

Indicator
MA.CR
IH.1.1

I can explain and safely use a technology tool
to convey meaning in media arts

I can explain and safely use multiple
technology tools to convey meaning in media
arts.

I can choose some appropriate technology
tools to convey a message while making a
media artwork in a safe and responsible
manner.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can make a media artwork about my family • I can make a media artwork about my family • I can edit a photograph to illustrate a stance
history.
history with interviews incorporated from
on a political issue.
family members.
• I can document a day in my life.
• I can select Gifs that animate my positions
• I can document a day in my life and
on politics.
focusing on a specific theme that tells a
• I can...
story.
• I can...
• I can...

Page 412

Indicator
MA.CR
IL.1.2

Indicator
MA.CR
IM.1.2

Indicator
MA.CR
IH.1.2

I can explain the steps of a technology
procedure and process to convey meaning in
media arts.

I can explain the steps of multiple technology
procedures and processes to convey meaning
in media arts.

I can choose the appropriate technology
procedure to convey a message while making
a media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can create a vlog about a social issue that
demonstrates that I know how to shoot a
video with sound.

• I can take a self-portrait photograph to
convey personal meaning.

• I can create a PSA choosing the best tools
and process to help my school.

• I can record my voice to make a vlog and
tell a story about a personal experience.

• I can make a voice over to use for daily
announcements.

• I can...

• I can combine music and sound to add under
still pictures.

• I can make a short interview about
admirable character traits to demonstrate
POV and sound.
• I can...

• I can...

Page 413

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.CR
IL.2

Benchmark
MA.CR
IM.2

Benchmark
MA.CR
IH.2

I can apply elements and principles of media
arts to revise my work.

Indicator
MA.CR
IL.2.1

I can analyze and apply the elements and
principles of media arts as a response to an
artistic problem.

Indicator
MA.CR
IM.2.1

I can analyze my media artwork through a
critique and refine my work based on given
criteria.

Indicator
MA.CR
IH.2.1

I can identify improvements needed in my
media artwork and explore strategies to
strengthen the intended meaning.

I can explain how multiple elements or
principles of media arts are used to convey
meaning in media artworks.

I can participate in a formal critique to revise
my artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how lighting can change a
photograph’s mood.

• I can explain how background music and the • I can make and post a video blog on my
speaker's tone of voice can affect meaning
process of making my film for others.
in a podcast.
• I can explain why point of view is important
• I can describe and make suggestions about a
when making a film.
• I can explain how editing and pacing can
media artwork free of personal judgment
change the rhythm of a commercial.
based on the elements and principles of
media arts.
• I can...
• I can...
• I can…
Page 414

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content, ideas,
skills, and media works for display.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.P
IL.3

Benchmark
MA.P
IM.3

Benchmark
MA.P
IH.3

I can identify a target audience for
presentation of my media artwork.

I can identify and choose multiple formats
used in presenting media artworks for a target
audience.

Indicator
MA.P
IL.3.1

I can compare presentation formats for
different media artworks and its intended
audience.

Indicator
MA.P
IM.3.1

Indicator
MA.P
IH.3.1

I can identify multiple ways to share my work
through different media outlets.

I can choose proper format for my media
artwork.

I can choose the most effective media format
for a select audience.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can post a photograph on social media.

• I can reduce the file size of a video for better • I can choose whether to upload my film to
streaming.
YouTube or Vimeo for impact.
•
• I can change the resolution of my
• I can decide whether I want to post my filers
photograph for better printing.
digitally or printed for distribution.

• I can upload my story as a podcast.
• I can upload a video to YouTube.
• I can...

• I can change the resolution of a film to be
projected for a large screen.

• I can...

• I can...
Page 415

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.R
IL.4

Benchmark
MA.R
IM.4

Benchmark
MA.R
IH.4

I can explain the messages and purposes in
media artworks.

I can analyze and describe reactions and
interpretations of messages and purposes in a
variety of media artworks.

Indicator
MA.R
IL.4.1

I can investigate personal and group intentions
about messages and purposes in media
artworks.

Indicator
MA.R
IM.4.1

Indicator
MA.R
IH.4.1

I can explain how to use the elements and
principles of media art to compose a media
artwork.

I can show the similarities and differences in
how media artworks are organized by the
elements and principles.

I can rephrase ways in which varieties of
media artworks organize criteria.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how separate things such as
framing and angles can change the film.

• I can see how film and video can be similar
in terms of image style but very different
when it comes to file size.

• I can look at a propaganda poster and remix
that using a new subject.

• I can...

• I can…

• I can…

Page 416

Indicator
MA.R
IL.4.2

Indicator
MA.R
IM.4.2

Indicator
MA.R
IH.4.2

I can explain the techniques used in different
media artworks that reflect varying messages
and points of view.

I can investigate increasingly complex
messages in media artworks.

I can explore the language, tone, and point of
view used in media texts to influence meaning
and interpretation of messages.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can relate how camera angles are
connected to the perception of the message
in a film.

• I can share what a director’s point of view
and message is in a film.

• I can analyze how point of view can
influence the audience of a news story.

• I can explain the difference in target
audience of a viral video and a full length
feature film.

• I can analyze how a director's personal
beliefs can influence their final product in a
documentary.

• I can explain the different target audience of
a meme and an ad campaign.

• I can explain how personal views can
influence an audience member’s reaction to
a commercial.

• I can identify various artistic techniques that
are used in advertising to convey a specific
message for a specific group of people.
• I can describe the main target audience of a
movie, or television show, based on the
message.
• I can…

• I can…

• I can...

Page 417

Indicator
MA.R
IL.4.3

Indicator
MA.R
IM.4.3

Indicator
MA.R
IH.4.3

I can explain that different media can produce
artworks that have the same purpose.

I can investigate increasingly complex
I can find and interpret data to explore
techniques that artists use to convey purpose in multiple differences in the purpose of media
media artwork.
artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify the way color is used in print
media.

• I can identify various lighting techniques in
a work of media art and how those
techniques influence the audience reaction.

• I can examine how an artist’s choice of
music in a short film. can influence the
audience.

• I can identify various sound techniques in a
work of media art and explain why they
were used.

• I can examine how the use of a particular
color on a meme can influence an audience.

• I can identify how text size and placement
on magazine covers can sway my opinion.
• I can...

• I can see advertisements are changed
depending on the target audience's location.

• I can...

• I can…

Page 418

Indicator
MA.R
IL.4.4

Indicator
MA.R
IM.4.4

Indicator
MA.R
IH.4.4

I can describe multiple elements and
principles of media art in my work.

I can identify elements and principles of
media arts in artist statements.

I can develop an artist’s statement that
describes media arts criteria and intent of my
work.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can write an artist statement that describes
and interprets and element or principle of
art.

• I can create a blog that describes, interprets
and analyzes my artwork.

• I can journal daily about my process and
purpose of creating artwork in class.

• I can write an artist statement that describes
how color is used in my media artwork.

• I can talk about an artist statement that
describes how quadrants are used to create
emotions in my media artwork.

• I can create a blog that describes, interprets
and analyzes my artwork.

• I can write an artist statement that describes
how line creates movement in my media
artwork.

• I can talk about an artist statement that
describes how angles are used in my media
artwork.

• I can..

• I can...

• I can...

Page 419

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.C
IL.5

Benchmark
MA.C
IM.5

Benchmark
MA.C
IH.5

I can analyze a variety of media artworks from I can describe why different media artworks
different cultures and time periods.
are used for different purposes in various
cultures and time periods.

I can analyze similarities and differences in
media artworks among different cultures and
time periods.

Page 420

Indicator
MA.C
IL.5.1

Indicator
MA.C
IM.5.1

Indicator
MA.C
IH.5.1

I can explain how ideas connect media arts to
history, cultures and the world.

I can compare and contrast how to connect
media arts ideas to history, cultures and the
world.

I can interpret how media arts ideas connect to
history, cultures, and the world.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can watch a commercial and talk about
how advertisements from different parts of
the world look.

• I can discuss how different advertisements
from different parts of the world look.

• I can watch 3 different Coca Cola
commercials and make connections to other
cultures and global advertising.

• I can watch a music video and discuss how
dancing styles change for different cultures.
• I can...

• I can compare and contrast the clothing in
music videos from around the world.
• I can...

• I can look at a video of or go to Disney
World and talk about the impact the ride
“It’s a Small World” has as a means of
teaching me more about culture.
• I can...

Page 421

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.C
IL.6

Benchmark
MA.C
IM.6

Benchmark
MA.C
IH.6

I can explore a range of media arts skills
shared among other arts disciplines, content
areas, and careers.

I can recognize specific media arts skills
shared among other arts disciplines, content
areas, and careers.

I can analyze media arts tools, concepts, and
materials used among other arts disciplines
content areas, and careers.

Page 422

Indicator
MA.C
IL.6.1

Indicator
MA.C
IM.6.1

Indicator
MA.C
IH.6.1

I can apply media arts concepts to other arts
disciplines and content areas.

I can examine the relationship between media
arts and specific content from another arts
discipline and content area.

I can apply concepts from other arts
disciplines and content areas to my media
artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how ideas connect media arts
and fine arts by creating a media
presentation.

• I can compare and contrast a media art with • I can interpret how the idea of line is used in
a fine art to discover the similarities and
painting and photography.
differences between the two.
• I can interpret the use of line and movement
• I can take a painting and create a media
in dance and photography to find similarities
artwork that represents the idea conveyed in
to create a short film.
the painting. i.e. “Off the and onto the
stage.”
• I can make connections to mathematics and
science using video games to enhance the
• I can look through magazine and cut out
playability of the game.
print ads that contain elements of math and
English.
• I can...

• I can explain how line connects media arts
to visual art and dance.
• I can talk about symbolism used in English
that is seen in print ads or websites to get a
message across to a target audience.
• I can...

• I can...

Page 423

Indicator
MA.C
IL.6.2

Indicator
MA.C
IM.6.2

Indicator
MA.C
IH.6.2

I can demonstrate and describe the skills
needed for careers in media arts.

I can identify specific skills required for
various careers in media arts.

I can research topics about careers in media
arts that interest me.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can define careers needed when making
my media artwork.

• I can discuss and assign a variety of jobs and • I can research and write what my job
responsibilities needed when making a
responsibilities and title is in making an
comic book (ie. illustrator, colorists, inker,
infomercial and explain my contributions to
etc.).
the work.

• I can describe the skills needed to be a
cinematographer.
• I can...

• I can identify the differences in skills needed • I can research and write what skills I would
for broadcast journalism and
need to work as a radio announcer.
photojournalism.
• I can...
• I can...

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Intermediate
Low

Intermediate
Mid

Intermediate
High

Benchmark
MA.C
IL.7

Benchmark
MA.C
IM.7

Benchmark
MA.C
IH.7

I can model and explain aspects of digital
citizenship when I am online.

I can describe different aspects of digital
citizenship when I am online.

I can interpret different aspects of digital
citizenship when I am online.

Page 424

Indicator
MA.C
IL.7.1
Internet Safety

Indicator
MA.C
IM.7.1
Internet Safety

Indicator
MA.C
IH.7.1
Internet
Safety

I can explain and demonstrate several safe and I can collaborate with other students in various I can identify predictable situation when using
reliable ways to search for accurate
safe and reliable ways to search for
the internet.
information on the internet.
information on the internet.

Sample Learning Targets

Sample Learning Targets

• I can create a presentation that explains how • I can safely and responsibly work with
to keep my password information secure.
others online to create list of rules and steps
on how to protect my personal information.
• I can use my personal secure information to
create an account on an educational website. • I can identify ways to manage my online
information, safety, and collaborate with
other students on the internet in a safe and
• I can...
responsible way.

Sample Learning Targets
• . I can identify spam e-mail and delete it
from my account.
• I can recognize and report cyber bullying in
an online chatroom.
• I can...

• I can identify predictable situations that
might arise when I am searching for
information on the internet.
• I can...

Page 425

Indicator
MA.C
IL.7.2
Digital Footprint
Privacy

Indicator
MA.C
IM.7.2
Digital Footprint
Privacy

Indicator
MA.C
IH.7.2
Digital Footprint
Privacy

I can explain and model how to post safely on
the internet.

I can analyze various ways to post safely on
the internet.

I can investigate several ways that information
on the internet is not safe or responsible and
ways to respond to these problems.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can create a presentation that shows how to • I can compare and contrast different ways
post safely on the internet.
for students to post on the internet and how
to post in a constructive way.
• I can describe procedures to protect my
identity and the identity of others.
• I can describe multiple ways I can have an
online presence.
• I can...
• I can...

• I can show examples of why it is important
to post positive and constructive comments
on social media.
• I can...

Page 426

Indicator
MA.C
IL.7.3
Copyright

Indicator
MA.C
IM.7.3
Copyright

Indicator
MA.C
IH.7.3
Copyright

I can explain and model the use of media
artwork that is owned by another artist, and
can demonstrate my responsibilities and rights
when using the work for educational purposes.

I can identify media artwork that is owned by I can handle unexpected situations with
another artist, and can demonstrate my
copyright and fair use rules as it applies to
responsibilities and rights when using the work my artwork, performance, or presentation.
for educational or personal purposes.

Sample Learning Targets

Sample Learning Targets

• I can create a poster about the copyright
laws as they apply to photography.

• I can report how to attribute copyright to an • I can explain that as a creator of an original
artist in a media presentation.
piece of art I can: make copies of my work,
distribute copies of my work, or
perform/display my work publicly, or make
• I can compare and contrast primary,
derivative works.
secondary and tertiary sources of
information to decide which one best fits the
needs of my media project.
• I can make a PSA presentation that
demonstrates what to do when a problem
arises when searching for information on the
• I can...
internet.

• I can explain and demonstrate several ways
to search for a particular media form on the
internet.
• I can...

Sample Learning Targets

• I can...

Page 427

Advanced Media Arts Standards
Artistic Processes: Creating- I can conceive and develop new artistic ideas and work.
Anchor Standard 1: I can use past, current and emerging technology tools, procedures, and processes to
create a variety of media artworks in a safe and responsible manner.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.CR
AL.1

Benchmark
MA.CR
AM.1

Benchmark
MA.CR
AH.1

I can apply some effective technology tools,
I can manipulate multiple technology tools,
procedures and processes to convey a message procedures and processes to convey messages
to make a variety of media artworks.
to make a variety of media artworks in the
most effective way.

I can use and justify the most effective
technology tools, procedures and processes to
make a variety of still, moving, and/or digital
audio images to convey meaning using
personal voice.

Page 428

Indicator
MA.CR
AL.1.1

Indicator
MA.CR
AM.1.1

Indicator
MA.CR
AH.1.1

I can choose multiple effective technology
tools to convey a message while making a
variety of media artworks in a safe and
responsible manner.

I can manipulate multiple technology tools to
convey messages to make a variety of media
artworks in the most effective way in a safe
and responsible manner.

I can justify the most effective technology
tools to make a variety of media artworks that
convey meaning using personal voice in a safe
and responsible manner.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can make a media artwork about my
family history.

• I can make a media artwork about my
family history with interviews
incorporated from family members.

• I can edit a photograph to illustrate a
stance on a political issue.

• I can document a day in my life.
• I can...

• I can document a day in my life and
focusing on a specific theme that tells a
story.

• I can create a moving film collage to
demonstrate a timeline.
• I can...

• I can...

Page 429

Indicator
MA.CR
AL.1.2

Indicator
MA.CR
AM.1.2

Indicator
MA.CR
AH.1.2

I can choose some effective technology
procedures and processes to convey a
message while making a variety of media
artworks.

I can manipulate multiple technology
procedures and processes to convey
messages to make a variety of media
artworks in the most effective way.

I can justify the most effective technology
procedures and processes to make a
variety of media artworks that convey
meaning using personal voice.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can create a vlog about a social issue
that demonstrates that I know how to
shoot a video with sound.

• I can take a self-portrait photograph to
convey personal meaning.

• I can create a PSA choosing the best
tools and process to help my school.

• I can record my voice to make a vlog
and tell a story about a personal
experience.

• I can make a voice over to use for daily
announcements.

• I can make a short interview about
admirable character traits to demonstrate
POV and sound.
• I can...

• I can...

• I can combine music and sound to add
under still pictures.
• I can...

Page 430

Anchor Standard 2: I can organize, improve, and complete artistic work using media arts elements and
principles.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.CR
AL.2

Benchmark
MA.CR
AM.2

Benchmark
MA.CR
AH.2

I can document and justify the planning
and development of a media artwork from
the inception of the idea to completion.

I can create a body of work in a variety of
media art forms that explore personal
themes, ideas, or concepts.

I can create, refine, and communicate
ideas based on the elements and
principles of media arts to complete a
variety of media artworks.

Page 431

Indicator
MA.CR
AL.2.1

Indicator
MA.CR
AM.2.1

Indicator
MA.CR
AH.2.1

I can apply organizational strategies that
communicate a personal meaning, theme,
idea, or concept.

I can create a process folio to document
the planning of a media artwork.

I can explain and defend the choices I
made to communicate my artistic ideas
across multiple media artworks. .

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how lighting can change a
photograph’s mood.

• I can explain how background music and • I can make and post a video blog on my
the speaker's tone of voice can affect
process of making my film for others.
meaning in a podcast.
• I can describe and make suggestions
• I can explain how editing and pacing can
about a media artwork free of personal
change the rhythm of a commercial.
judgment based on the elements and
principles of media arts.
• I can...
• I can…

• I can explain why point of view is
important when making a film.
• I can...

Page 432

Artistic Processes: Presenting- I can share artistic ideas and work.
Anchor Standard 3: I can develop, refine, and select work for presentation that reflects specific content,
ideas, skills, and media works for display.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.P
AL.3

Benchmark
MA.P
AM.3

Benchmark
MA.P
AH.3

I can present media artworks considering
combinations of formats and target
audience.

I can analyze and interpret the
effectiveness of a media arts presentation
for an intended audience.

I can promote and present media artworks
for intentional impacts through a variety of
contexts such as markets and venues.

Page 433

Indicator
MA.P
AL.3.1

Indicator
MA.P
AM.3.1

Indicator
MA.P
AH.3.1

I can select my intended audience and
choose multiple media formats to get the
most views.

I can evaluate the effectiveness of virtual
and physical presentations of a media
artwork.

I can create a media plan (funding,
distribution, and viewing) to promote my
media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can post a photograph on social media.

• I can reduce the file size of a video for
better streaming.

• I can choose whether to upload my film
to YouTube or Vimeo for impact.

• I can change the resolution of my
photograph for better printing.

• I can decide whether I want to post my
filers digitally or printed for distribution.

• I can change the resolution of a film to
be projected for a large screen.

• I can...

• I can upload my story as a podcast.
• I can upload a video to YouTube.
• I can...

• I can...

Page 434

Artistic Processes: Responding- I can interpret (read) and evaluate how media is represented and conveys
meaning.
Anchor Standard 4: I can describe, analyze, and evaluate the meaning of my work and the work of others.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.R
AL.4

Benchmark
MA.R
AM.4

Benchmark
MA.R
AH.4

I can discuss and analyze the message and
purpose in a variety of media artworks.

I can analyze the message and intent of a
variety of media artworks.

I can justify the message, intent and
impacts of diverse media artworks,
considering complex factors of context
and bias.

Page 435

Indicator
MA.R
AL.4.1

Indicator
MA.R
AM.4.1

Indicator
MA.R
AH.4.1

I can analyze the organization of the
elements and principals of media
artworks.

I can critique how the composition
characteristics in multiple media artworks
work together.

I can justify the organizational choices
made by media artist.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can explain how separate things such as • I can see how film and video can be
• I can look at a propaganda poster and
framing and angles can change the film.
similar in terms of image style but very
remix that using a new subject.
different when it comes to file size.
• I can explain how color theory themes
• I can create a series of podcast that
can change the emotion in a film.
• I can see how film and video can be
follow a similar format and style.
similar in terms of image style but very
different when it comes to point of view. • I can…
• I can...
• I can compare a infomercial’s use of
color to the color in a printed advertising
image.
• I can…

Page 436

Indicator
MA.R
AL.4.2

Indicator
MA.R
AM.4.2

Indicator
MA.R
AH.4.2

I can analyze the effectiveness of a
presentation and treatment of messages in
media artwork.

I can interpret the qualities of and
relationships between the components,
style, and message communicated by
media artworks and artists.

I can justify my interpretation of language,
tone, and point of view of the message in a
media artwork.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can relate how camera angles are
connected to the perception of the
message in a film.

• I can share what a director’s point of
view and message is in a film.

• I can analyze how point of view can
influence the audience of a news story.

• I can explain the difference in target
audience of a viral video and a full
length feature film.

• I can analyze how a director's personal
beliefs can influence their final product
in a documentary.

• I can identify various artistic techniques
that are used in advertising to convey a
specific message for a specific group of
people.
• I can describe the main target audience
of a movie, or television show, based on
the message.

• I can explain the different target
• I can explain how personal views can
audience of a meme and an ad campaign.
influence an audience member’s reaction
to a commercial.
• I can…
• I can...

• I can…

Page 437

Indicator
MA.R
AL.4.3

Indicator
MA.R
AM.4.3

Indicator
MA.R
AH.4.3

I can analyze formal and informal
situations, the effectiveness of
presentation, and treatment of media to
convey the purpose.

I can analyze and interpret the qualities of I can justify my interpretation and
relationships between the components,
explanation of the purpose of multiple
style, message, and how they relate to the media artworks.
purpose.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can identify the way color is used in
print media.

• I can identify various lighting techniques • I can examine how an artist’s choice of
in a work of media art and how those
music in a short film can influence the
techniques influence the audience
audience.
reaction.
• I can identify how text size and
placement on magazine covers can sway
• I can examine how the use of a particular
my opinion.
• I can identify various sound techniques
color on a meme can influence an
in a work of media art and explain why
audience.
they were used.
• I can...
• I can...
• I can see advertisements are changed
depending on the target audience's
location.
• I can…

Page 438

Indicator
MA.R
AL.4.4

Indicator
MA.R
AM.4.4

Indicator
MA.R
AH.4.4

I can develop an artist’s statement that merges
personal influences with intent and media arts
criteria for my work.

I can develop an artist’s statement that
identifies common themes in personal
influences, intent and media arts criteria for
work.

I can justify my choices of criteria, cultural
influences, personal experiences, to create my
own voice in my artist statement.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can write an artist statement that
• I can create a blog that describes,
describes and interprets personal choices
common themes in a series of films on
when using of lighting angles and
school culture.
shadows in a video game.
• I can write an artist statement about
• I can write an artist statement that
specific intent used in a radio
describes and interprets the use emphasis
announcement on religious views.
in costumes designs to express intent for
a dance video.
• I can…
• I can…

• I can validate my choices through an
artist statement on expressing my
personal voice on making a film about
persons’ with disabilities.
• I can defend my cultural influences in an
artist statement for a contest for
International Day.
• I can...

Page 439

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 5: I can examine the role of media arts through history and cultures.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.C
AL.5

Benchmark
MA.C
AM.5

Benchmark
MA.C
AH.5

I can examine past media arts works to
determine their influence on media today.

I can explain the influence of past media
arts works throughout different time
periods and how that reflects on media
today.

I can evaluate media arts works from the
past and apply the most effective ones to
my work and the work of others.

Page 440

Indicator
MA.C
AL.5.1

Indicator
MA.C
AM.5.1

Indicator
MA.C
AH.5.1

I can participate in formal and informal
I can examine the relationship between
I can justify the relationship between
situations relating to how a media art
media arts, history, cultures, and the world. media arts, history, cultures, and the
connects to history, cultures and the world.
world.

Sample Learning Targets

Sample Learning Targets

• I can make a short documentary on an
African Drumming group that relates
native music to our country.

• I can make digital print advertisements
• I can make a short film project based on
based on a series of provided samples
a cultural theme identified by the
from the past, and use to define how
student. Students create three
different cultures will respond to the ads.
advertisements, then present these three
“ads” to the class and describe and
explain their connections.
• I can create an animation short reflecting
cultures from another country through
environment and action.
• I can make a presentation that connects
similar and different international
policies during different presidencies.
• I can...

• I can make a propaganda poster that
focuses on human rights and its changes
throughout history.
• I can...

Sample Learning Targets

• I can...

Page 441

Anchor Standard 6: I can relate media arts to personal meaning, other arts disciplines, content areas, and
careers.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.C
AL.6

Benchmark
MA.C
AM.6

Benchmark
MA.C
AH.6

I can apply media arts concepts to other arts
I can research aspects of media arts careers to
disciplines and content areas to prepare me for influence my career path.
a career.

Indicator
MA.C
AL.6.1

Indicator
MA.C
AM.6.1

I can analyze complex ideas from other arts
disciplines and content areas to inspire my
creative work and evaluate its impact on my
artistic perspective.
Indicator
MA.C
AH.6.1

I can explain ideas from other arts disciplines
and content areas through media arts.

I can explain how economic conditions,
cultural values, and location influence media
arts and the need for related careers.

I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a media artist.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can make a video project using dance
themes from painter Jonathan Green.

• I can make a video of a music (band,
orchestra, choral) concert using more
than one camera and editing to produce
one work to be shared with the school
and community.

• I can make an interdisciplinary project
that to present to an organization in the
community or within the school.

• I can make a video project that
demonstrates how lines are similar in
media arts, visual arts, and dance.
• I can...

• I can make a video of dance using the
elements of earth, air, fire and water.

• I can make a short film that reflects
similarities and differences between
media arts and other disciplines with
regard to fundamental concepts. I can…
Page 442

Indicator
MA.C
AL.6.2

Indicator
MA.C
AM.6.2

Indicator
MA.C
AH.6.2

I can identify the skills, training, and
education necessary to pursue a career in
media arts that interests me.

I can pursue opportunities that will lead
me to a career in media arts.

I can demonstrate skills necessary for a
career in media arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can serve in a variety of roles such as
• I can research classes and workshops needed • I can take on an internship in the
to develop specific media arts techniques
director, camera operator, editor, sound
community in a media arts career
and
skills.
engineer, teleprompter and gaffer when
capacity.
making a short film to explore various
• I can participate in media arts opportunities
roles and skills related to filmmaking.
in my community.
• I can get an entry-level part time job
working in different media fields, such
• I can use my portfolio of work to
as a color flatter for the comic industry.
• I can...
identify skills that I am interested in
pursuing as career.
• I can...
• I can…

Page 443

Anchor Standard 7: I can practice digital citizenship in researching and creating art.
Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
MA.C
AL.7

Benchmark
MA.C
AM.7

Benchmark
MA.C
AH.7

I can participate in formal and informal
situations to discuss and demonstrate
digital citizenship when I am online.

I can analyze and identify the appropriate
digital citizenship strategy to use when I
am online.

Indicator
MA.C
AL.7.1
Internet Safety

I can justify my choice of digital
citizenship strategy to use when I am
online.

Indicator
MA.C
AM.7.1
Internet Safety

Indicator
MA.C
AH.7.1
Internet Safety

I can participate in formal and informal
situations when collaborating with others
and can model appropriate and positive
netiquette.

I can analyze various ways to use digital
citizenship to collaborate with the world
in an appropriate and positive way.

I can compile a selection of information
that is found on the internet and how
helps me justify my own voice as an
artist.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can discuss and critique internet safety
and model how to use it in a safe and
responsible manner online.

• I can devise an internet safety plan for
other students to follow when they are
online in school.

• I can interact with my peers in an online
critique of an artwork, and justify my
constructive criticism.

• I can...

• I can...

• I can...

Page 444

Indicator
MA.C
AL.7.2
Digital Footprint
Privacy

Indicator
MA.C
AM.7.2
Digital Footprint
Privacy

Indicator
MA.C
AH.7.2
Digital Footprint
Privacy

I can participate in formal and informal
situations when collaborating with others
to post safely on the internet.

I can investigate various ways to post
safely on the internet and know the
difference between a positive and a
negative post.

I can justify my choice of what I post on
the internet to interact with the world in an
appropriate and positive way.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can participate on an online critique of
other student artwork in a positive and
constructive manner.

• I can create a constructive and positive
response to a blog post of another
student.

• I can communicate online in an
appropriate and positive on my webpage.

• I can...

• I can...

• I can create a blog and communicate
about global issues in a positive and
constructive way.
• I can...

Page 445

Indicator
MA.C
AL.7.3
Copyright

Indicator
MA.C
AM.7.3
Copyright

Indicator
MA.C
AH.7.3
Copyright

I can participate in formal and informal
situations when collaborating with others
to discuss copyright laws that apply to a
media artwork.

I can analyze and synthesize various ways
that copyright laws apply to my work and
the work of others.

I can justify my choice of how I use
copyright law to protect my work and the
work of others.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

• I can take a stance in a debate about why • I can create a presentation about the
we should have copyright law.
similarities and differences between
photography and video copyright law.
• I can look at three websites with
information about the same story and
• I can create a blog that discusses how to
decide which information is valid,
use, understand and synthesize
reliable, and unbiased.
information found on different websites.
• I can...

• I can...

• I can demonstrate how I followed
copyright law in the creating of ideas
and media artwork.
• I can appraise information found on the
internet and find appropriate information
that validates and justifies my choice of
imagery, text, and sound in my artwork.
• I can...

Page 446

Media Arts Glossary
Artist Statement An artist statement lets you convey the reasoning behind your work-- why you chose a particular subject matter, why
you work in a certain medium, etc. And further, a well-written statement shows the relationship of you to your artwork, and
helps creates a connection with the viewer that will make your work (and your name) more memorable.
Attention Principle of directing perception through sensory and conceptual impact.
Balance Principle of the equitable and/or dynamic distribution of items in a media arts composition or structure for aesthetic meaning,
as in a visual frame, or within game architecture.
Codes and Conventions Codes are systems of signs put together (usually in a sequence) to create meaning. These systems may be
verbal, visual, non-verbal, or aural (e.g., sound effects, music). Visual media may use a number of technical codes such as
camera angles, framing, composition, and lighting to convey a particular attitude to a subject. Conventions are the commonly
accepted or generally known methods of doing something. Codes and conventions are used together in the study and
examination of a specific media genre. The camera angles used in a film, for example, should be studied in terms of the way
camera angles are conventionally used in the particular type of film.
Components The discrete portions and aspects of media artworks, including: elements, principles, processes, parts, assemblies, etc.,
such as: light, sound, space, time, shot, clip, scene, sequence, movie, narrative, lighting, cinematography, interactivity, etc.
Composition Principle of arrangement and balancing of components of a work for meaning and message.
Constraints Limitations on what is possible, both real and perceived.
Continuity The maintenance of uninterrupted flow, continuous action or self-consistent detail. across the various scenes or components
of a media artwork, i.e. game components, branding, movie timeline, series, etc.
Context The situation surrounding the creation or experience of media artworks that influences the work, artist or audience. This can
include how, where, and when media experiences take place, as well as additional internal and external factors (personal,
societal, cultural, historical, physical, virtual, economic, systemic, etc.)
Contrast Principle of using the difference between items, such as elements, qualities and components, to mutually complement them.
Page 447

Convention An established, common, or predictable rule, method, or practice within media arts production, such as the notion of a
‘hero’ in storytelling.
Copyright The exclusive right to make copies, license, and otherwise exploit a produced work.
Criteria The elements and principles students use to design their work
Digital Citizenship A standard of behavior with regard to the appropriate use of technology. A set of ethical and social norms that
oppose the misuse and abuse of technology.
Digital Identity How one is presented, perceived and recorded online, including personal and collective information and sites, ecommunications, commercial tracking, etc.
Divergent Thinking Unique, original, uncommon, idiosyncratic ideas; thinking “outside of the box.”
Design Thinking A cognitive methodology that promotes innovative problem solving through the prototyping and testing process
commonly used in design.
Elements of Media Arts Include but not limited to, light, sound, time, POV, performance, framing, narrative, and editing.
Emphasis Principle of giving greater compositional strength to a particular element or component in a media artwork.
Ethics Moral guidelines and philosophical principles for determining appropriate behavior within media arts environments.
Exaggeration Principle of pushing a media arts element or component into an extreme for provocation, attention, contrast, as seen in
character, voice, mood, message, etc.
Experiential Design Area of media arts wherein interactive, immersive spaces and activities are created for the user; associated with
entertainment design.
Fairness Complying with appropriate, ethical and equitable rules and guidelines.
Fair Use Permits limited use of copyrighted material without acquiring permission from the rights holders, including commentary,
search engines, criticism, etc.
Force Principle of energy or amplitude within an element, such as the speed and impact of a character’s motion
Page 448

Generative Methods Various inventive techniques for creating new ideas and models, such as brainstorming, play, open exploration,
experimentation, inverting assumptions, rule bending, etc.
Heterogeneity How an artwork can be made up of many distinct experiences and parts that are independent however when placed
together bring deeper meaning. Ex. installation that includes recorded, sounds, images, and performances.
Hybridization Principle of combining two existing media forms to create new and original forms, such as merging theatre and
multimedia.
Information Literacy Skills The abilities necessary to access, utilize, and critically evaluate the products of the mass media, including
an informed understanding of the nature of the media and the methods they employ.
Intent Purpose behind making a media art work whether personal or analyzed through the work made by others.
Interactivity A diverse range of articulating capabilities between media arts components, such as user, audience, sensory elements,
etc., that allow for inputs and outputs of responsive connectivity via sensors, triggers, interfaces, etc., and may be used to obtain
data, commands, or information and may relay immediate feedback, or other communications; contains unique sets of aesthetic
principles.
Judgement The ability to make informed and cognizant decisions regarding a media artwork, especially in the critique process.
Juxtaposition Placing greatly contrasting items together for effect.
Legal The legislated parameters and protocols of media arts systems, including user agreements, publicity releases, copyright, etc.
Manage Audience Experience The act of designing and forming user sensory episodes through multi-sensory captivation, such as
using sequences of moving image and sound to maintain and carry the viewer’s attention, or constructing thematic spaces in
virtual or experiential design
Markets The various commercial and informational channels and forums for media artworks, such as T.V., radio, internet, fine arts,
non-profit, communications, etc.
Meaning The formulation of significance and purposefulness in media artworks.

Page 449

Media Arts Contexts The diverse locations and circumstances of media arts, including its markets, networks, technologies and
vocations.
Media Environments Spaces, contexts and situations where media artworks are produced and experienced, such as in theaters,
production studios and online.
Media Literacy A series of communication competencies, including the ability to access, analyze, evaluate, and communicate
information in a variety of forms, including print and nonprint messages.
Media Messages The various artistic, emotional, expressive, prosaic, commercial, utilitarian and informational communications of
media artworks.
Media Texts Aural, print, graphic, and electronic communications with a public audience. Such texts often involve numerous people in
their construction and are usually shaped by the technology used in their production. Media texts include papers and magazines,
television, video and film, radio, computer software, and the Internet.
Medium Material used to create the art piece and determine the nature of the final work.Ex.film, digital imaging, web design.
Message Media messages contain “texts” and “subtexts.” The text is the actual words, pictures and/or sounds in a media message. The
subtext is the hidden and underlying meaning of the message. Media messages reflect the values and viewpoints of media
makers.
Modeling or Concept Modeling Creating a digital or physical representation or sketch of an idea, usually for testing; prototyping.
Movement Principle of motion of diverse items within media artworks.
Multimodal Perception The coordinated and synchronized integration of multiple sensory systems (vision, touch, auditory, etc.) in
media artworks.
Multimedia Theatre The combination of live theatre elements and digital media (sound, projections, video, etc.) into a unified
production for a live audience.
Narrative Structure The framework for a story, usually consisting of an arc of beginning, conflict, and resolution
Netiquette The correct or acceptable way of communicating on the Internet.
Page 450

Personal Aesthetic An individually formed, idiosyncratic style or manner of expressing oneself; an artist’s “voice.”
Perspective Principle pertaining to the method of three-dimensional rendering, point-of-view, and angle of composition.
Point of View The position from which something or someone is observed; the position of the narrator in relation to the story, as
indicated by the narrator's outlook from which the events are depicted and by the attitude toward the characters.
Positioning The principle of placement or arrangement.
Principles Media Arts Principles that include but not limited to, interactivity, heterogeneity, hybridization, medium, and temporality.
Production Processes The diverse processes, procedures, or steps used to carry out the construction of a media artwork, such as
prototyping, playtesting, and architecture construction in game design.
Prototyping Creating a testable version, sketch or model of a media artwork, such as a game, character, website, application, etc.
Representation Media representations are the ways in which the media portrays particular groups, communities, experiences, ideas, or
topics from a particular ideological or value perspective.
Resisting Closure Delaying completion of an idea, process, or production, or persistently extending the process of refinement, towards
greater creative solutions or technical perfection
Responsive Use of Failure Incorporating errors towards persistent improvement of an idea, technique, process or product
Rules The laws, or guidelines for appropriate behavior; protocols.
Safety Maintaining proper behavior for the welfare of self and others in handling equipment and interacting with media arts
environments and groups.
Soft Skills Diverse organizational and management skills, useful to employment, such as collaboration, planning, adaptability,
communication, etc.
Stylistic Convention A common, familiar, or even “formulaic” presentation form, style, technique, or construct, such as the use of
tension building techniques in a suspense film, for example.

Page 451

Systemic Communications Socially or technologically organized and higher-order media arts communications such as networked
multimedia; television formats and broadcasts; “viral” videos; social multimedia (e.g. “vine” videos); remixes; transmedia, etc.
System(s) The complex and diverse technological structures and contexts for media arts production, funding, distribution, viewing, and
archiving.
Technological The mechanical aspects and contexts of media arts production, including hardware, software, networks, code, etc.
Temporality How the passage of time can change one’s interpretation of an artwork or one’s ability to witness the artwork.
Tone Principle of “color,” “texture,” or “feel,” of a media arts element or component, as for sound, lighting, mood, sequence, etc.
Transdisciplinary Production Accessing multiple disciplines during the conception and production processes of media creation, and
using new connections or ideas that emerge to inform the work.
Transmedia Production Communicating a narrative and/or theme over multiple media platforms, while adapting the style and
structure of each story component to the unique qualities of the platforms.
Virtual Channels Network based presentation platforms such as: Youtube, Vimeo, Deviantart, etc.
Virtual Worlds Online, digital, or synthetic environments (e.g. Minecraft, Second Life).
Vocational The workforce aspects and contexts of media arts.

Page 452

References
Artist Statement | Art School Resources - ArtStudy.org. N.p., n.d. Web. 05 Feb. 2017.
Baker, F. (n.d.). Media Literacy Clearinghouse. Retrieved from http://frankwbaker.com/
Baker, F. W. (2010). Introduction to Media Arts (SCDE Visual & Performing Arts: 2010). Retrieved from
http://www.frankwbaker.com/media_literacy_supportdoc.htm
Boles, D. (1994). The language of media literacy: A glossary of terms. Mediacy, 16, (3). Retrieved from
http://www.mediaawareness.ca/english/resources/educational/teaching_backgrounders/media_
literacy/glossary_media_literacy.cfm
CI5472 Teaching Film, Television, and Media." CI5472 Teaching Film, Television, and Media. N.p., n.d. Web. 05 Feb. 2017.
Common Sense Media. (2016). Common Sense Education: Digital Citizenship. Retrieved from
https://www.commonsensemedia.org/educators/digital-citizenship
Elements and Principles of Media Art. St. Rosemary Educational Institution. (2016, July 18). Retrieved from
http://schoolworkhelper.net/elements-and-principles-of-media-art/
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
"Reading Between the Lines: Media Literacy." Wellness, Alcohol and Violence Education (WAVE). N.p., n.d. Web. 05 Feb. 2017.
South Carolina Department of Education. (2010). South Carolina Academic Standards for Media Arts. Retrieved from
http://ed.sc.gov/scdoe/assets/file/agency/ccr/Standards-Learning/documents/AcademicStandardsforMediaArts.pdf

Page 453


