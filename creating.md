
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas.
Anchor Standard 1: I can arrange and compose music.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
CM.CR NL.1 I can notate simple rhythmic patterns using a defined selection of note values.

Benchmark
CM.CR NM.1 I can notate simple melodic patterns using a defined selection of pitches.

Benchmark
CM.CR NH.1 I can notate musical ideas using musical symbols to represent pitch and rhythm.

Benchmark
CM.CR IL.1 I can arrange a short song for my voice.
Benchmark
CM.CR IM.1 I can arrange a short song for two voices, using harmony.

Benchmark
CM.CR IH.1 I can arrange a short song for an ensemble, demonstrati ng an understandi ng of voicing and texture.

Benchmark
CM.CR AL.1 I can describe how I use melody, rhythm, and harmony to compose or arrange a work for a specific purpose.

Benchmark
CM.CR AM.1 I can collaborate with others to compose or arrange a musical work for a specific purpose.

Benchmark
CM.CR AH.1 I can compose short, original musical ideas and works using all the elements of music for a specific purpose.

Page 135

Indicator
CM.CR NL.1.1 I can recognize long and short sounds and identify simple rhythms from notation.

Indicator
CM.CR NM.1.1 I can create and recognize high and low sounds to represent some pitches.

Indicator
CM.CR NH.1.1 I can sing a variety of pitches and rhythms and label a music staff with clef and metric symbols.

Indicator
CM.CR IL.1.1 I can create a simple tune (monophonic melody) without accompanim ent, within specified guidelines.

Indicator
CM.CR IM.1.1 I can develop a simple tune with accompanyi ng parts (homophonic work).

Indicator
CM.CR IH.1.1 I can combine different voices to create various tone colors in my arrangement.

Indicator
CM.CR AL.1.1 I can arrange melodic themes for specific purposes, using arrangement and compositional techniques.
Indicator
CM.CR AM.1.1 I can sing in ensembles, working with others to develop ideas as we compose or arrange a composition.

Indicator
CM.CR AH.1.1 I can create musical ideas and works using chord progressions and modulations.

Indicator
CM.CR NL.1.2 I can write music rhythms or sounds, using symbols.

Indicator
CM.CR NM.1.2 I can write high and low notes on a music staff to represent pitches.

Indicator
CM.CR NH.1.2 I can write note and rest values on a music staff.

Indicator
CM.CR IL.1.2 I can create a melody using rhythms that are appropriate for the time signature.

Indicator
CM.CR IM.1.2 I can develop an original arrangement of a traditional canon or round.

Indicator
CM.CR IH.1.2 I can experiment with changes in tone color, creating variety and contrast through a combination of different voices.

Indicator
CM.CR AL.1.2 I can use compositional techniques to compose works in a given musical form.

Indicator
CM.CR AM.1.2 I can work with others to analyze arrangements and original compositions for improvements.

Indicator
CM.CR AH.1.2 I can use characteristic forms of music to create a choral composition for a specific purpose.

Page 136

Indicator
CM.CR NL.1.3 I can identify same and different rhythm patterns.

Indicator
CM.CR NM.1.3 I can identify same and different melodic patterns.

Indicator
CM.CR NH.1.3 I can write beats and rhythms within measures.

TODO

Indicator
CM.CR.1 IL.3
Indicator
CM.CR IM.1.3
Indicator
CM.CR IH.1.3
Indicator
CM.CR AL.1.3
Indicator
CM.CR AM.1.3
Indicator
CM.CR.1 AH.3
I can develop a melody using pitches that are appropriate for the tonality.

I can develop my song using I, IV, and V chord progressions.

I can experiment with nonchord tones and chord progressions.

I can compose short compositions in major and minor keys.

I can compose an original composition in Four-Part Chorale Style.

I can compose a choral composition with a variety of expressive devices.

Benchmark
CM.CR IM.2 I can improvise simple tonal patterns within a given tonality.

Benchmark
CM.CR IH.2 I can improvise simple melodic phrases.

Benchmark
CM.CR AL.2 I can perform a brief improvisatio n given a chord progression and meter.

Benchmark
CM.CR AM.2 I can perform an improvisatio n given a motive, chord progression and meter.

Benchmark
CM.CR AH.2 I can perform an extended improvisation with freedom and expression featuring motivic development within a given tonality, meter, and style.

# Anchor Standard 2: I can improvise music.

Benchmark
CM.CR NL.2 I can imitate simple rhythm patterns within a given meter.

Benchmark
CM.CR NM.2 I can imitate simple tonal patterns within a given tonality.

Benchmark
CM.CR NH.2 I can imitate simple melodic phrases given simple chord changes.

Benchmark
CM.CR IL.2 I can improvise simple rhythmic patterns within a given meter.

Page 137

Indicator
CM.CR NL.2.1 I can imitate rhythm using a neutral syllable (shhh, ba, etc.)

Indicator
CM.CR NM.2.1 I can produce one-phrase responses using two to three pitches on a neutral syllable (such as loo or la).

Indicator
CM.CR NH.2.1 I can imitate simple melodic phrases given simple chord progressions.

Indicator
CM.CR IL.2.1 I can improvise my own simple rhythmic pattern using a neutral syllable.

TODO

Indicator
CM.CR NL.2.2
Indicator
CM.CR NM.2.2
Indicator
CM.CR NH.2.2
Indicator
CM.CR IL.2.2
I can imitate rhythm using a taka-di-mi or a counting system.

I can echo simple tonal patterns using solfege.

I can embellish a given melodic phrase that corresponds with a simple chord progression.

I can improvise my own simple rhythm patterns using ta-ka-di-mi or a counting system.

Indicator
CM.CR IM.2.1 I can improvise my own simple tonal patterns on a neutral syllable.

Indicator
CM.CR IM.2.2 I can improvise my own simple tonal patterns using solfege.

Indicator
CM.CR IH.2.1 I can identify chord changes to improvise a short melody.

Indicator
CM.CR AL.2.1 I can improvise a short passage using only a chord progression.

Indicator
CM.CR AM.2.1 I can perform an improvesation on a given motive.

TODO

Indicator
CM.CR IH.2.2
Indicator
CM.CR AL.2.2
Indicator
CM.CR AM.2.2
I can improvise simple melodic phrases that correspond with chord progression s in an unfamiliar song.

I can improvise a short passage in an established meter.

I can improvise an extended passage using only a chord progression.

Indicator
CM.CR AM.2.1 I can improvise an extended unaccompani ed solo within a given tonality, meter, and style.

Indicator
CM.CR AH.2.2 I can improvise freely within a given tonality, meter, and style, responding to aural cues from other members of an ensemble.

Page 138

TODO

HERE

Novice Choral Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new ideas.
Anchor Standard 1: I can arrange and compose music.
Novice
Low
Benchmark
CM.CR NL.1 I can notate simple rhythmic patterns using
a defined selection of note values
Indicator
CM.CR NL.1.1 I can recognize long and short sounds and
identify simple rhythms from notation.

Novice
Mid
Benchmark
CM.CR NM.1 I can notate simple melodic patterns using a
defined selection of pitches.
Indicator
CM.CR NM.1.1 I can create and recognize high and low
sounds to represent some pitches.

Sample Learning Targets

Sample Learning Targets






I can echo rhythmic patterns on a
neutral syllable.
I can echo rhythmic syllables on taka-di-mi or other sight-reading
system.
I can...



I can echo tonal patterns on a neutral
syllable.



I can echo tonal patterns on a
Kodaly or solfege syllable.



I can...

Novice
High
Benchmark
CM.CR NH.1 I can notate musical ideas using musical
symbols to represent pitch and rhythm.
Indicator
CM.CR NH.1.1 I can sing a variety of pitches and rhythms
and label a music staff with clef and metric
symbols.
Sample Learning Targets


I can identify a treble and bass clef.



I can identify simple time signatures
(2/4, 3/4, 4/4)



I can identify compound time
signatures (6/8, 9/8,12/8)



I can…

Page 152

Indicator
CM.CR NL.1.2 I can write music rhythms or sounds, using
symbols.
Sample Learning Targets


Indicator
CM.CR NM.1.2 I can write high and low notes on a music
staff to represent pitches.
Sample Learning Targets

I can notate quarter notes/rests and
eighth notes/rests using nontraditional notation.



I can notate line and space notes.



I can notate using ledger lines.



I can notate quarter notes/rests and
eighth notes/rests using traditional
notation.



I can...



I can...
Indicator
CM.CR NL.1.3
I can identify same and different rhythm
patterns.
Sample Learning Targets


I can use symbols to notate four beat
rhythm patterns.



I can recognize the difference
between long and short sounds.



I can…

Indicator
CM.CR NH.1.2 I can write note and rest values on a music
staff.
Sample Learning Targets


I can notate quarter and half, notes
on a music staff with stems going the
correct direction.



I can notate eighth notes on a music
staff with stems and flags going the
correct direction.



I can...

Indicator
CM.CR NM.1.3 I can identify same and different melodic
patterns
Sample Learning Targets

Indicator
CM.CR NH.1.3 I can write beats and rhythms within
measures.
Sample Learning Targets



I can identify skips and leaps.





I can notate a four-measure rhythm
within a simple time signature.

I can identify repeated patterns and
pitches.



I can create a four-measure
composition with music notation
software.



I can….



I can trace a melodic line.



I can...

Page 153

Anchor Standard 2: I can improvise music.
Novice
Low
Benchmark
CM.CR NL.2 I can imitate simple rhythm patterns within
a given meter.
Indicator
CM.CR NL.2.1 I can imitate rhythm using neutral syllables
(shhh, ba, etc.).
Sample Learning Targets


I can echo four-beat rhythm patterns
on a neutral syllable while patsching
a steady beat.



I can clap a one-phrase response
using quarter notes and eighth notes.



I can...

Novice
Mid
Benchmark
CM.CR NM.2 I can imitate simple tonal patterns within a
given tonality.
Indicator
CM.CR NM.2.1 I can produce one-phrase responses using two
to three pitches on a neutral syllable (such as
loo or la).
Sample Learning Targets






I can sing a one-phrase response
using two-three pitches with major
tonality.
I can sing a one-phrase response
using two to three pitches with a
minor tonality.

Novice
High
Benchmark
CM.CR NH.2 I can imitate simple melodic phrases given
simple chord changes.
Indicator
CM.CR NH.2.1 I can imitate simple melodic phrases given
simple chord progressions.
Sample Learning Targets


I can identify by ear the tonic major
triad in a familiar song.



I can identify by ear the tonic minor
triad in a familiar song.



I can...

I can...

Page 154

Indicator
CM.CR NL.2.2 I can imitate rhythm using a ta-ka-di-mi or
a counting system.

Indicator
CM.CR NM.2.2 I can echo simple tonal patterns using
solfege.

Sample Learning Targets

Sample Learning Targets



I can chant four-beat rhythm
patterns on ta-ka-di-mi or counting
system while patsching a steady beat.



I can clap a four-beat rhythm pattern.



I can...



I can echo sing a tonic triad major
tonal pattern on solfege.



I can echo sing a tonic triad minor
tonal pattern on solfege.



I can echo sing tonic-dominant
major tonal patterns on solfege.



I can echo sing pentatonic tonal
patterns on solfege.



I can…

Indicator
CM.CR NH.2.2 I can embellish a given melodic phrase that
corresponds with a simple chord
progression.
Sample Learning Targets


I can use rhythmic syllables to
embellish a familiar melody in simple
meter.



I can use passing tones to embellish a
familiar melody over a simple chord
progression.



I can use a music loop app to
improvise a basic rhythmic pattern
over a generated pattern.



I can…

Page 155
