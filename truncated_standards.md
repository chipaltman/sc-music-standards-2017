
# Design
## Introduction

Design is all around us and permeates every aspect of our lives. From waking up and deciding what to wear to making choices about
our environment, purchases, and recreation, we interact with the work of designers. The design fields include, but are not limited to,
Communication Design, Environmental Design, Experiential Design, and Object Design.
Functionality and aesthetics are two concepts that determine how we use a design and what we see in a design. For example, the
science of a bridge (function) must also be aesthetically pleasing for its environment. The design process guides students to experience
this interface by proceeding through a sequence of steps to find solutions for a design challenge. These steps include the following:
defining the design challenge, conducting research, brainstorming solutions, constructing a prototype, presenting a design solution to a
sample target group, receiving feedback, reflecting on the feedback, and making improvements on the prototype.
Students are guided through the design process to make creative and considerate decisions concerning the interaction of function and
aesthetics toward constructing a well-crafted prototype. The process requires that students present their design solution/prototype,
explain their thought processes, and receive feedback from stakeholders. This feedback allows students to analyze and reflect upon
their work in order to make thoughtful revisions toward improvement.
The design standards are organized in steps that parallel the design process. Students move through the standards, as shaped by the
design process, by working independently and collaboratively with others in order to reach an aesthetically-effective and functional
outcome.
Students are immersed cognitively when involved in the design process. The use of skills such as communication, creativity, critical
thinking, and problem solving are truly embodied in their work. Teaching through design reaches diverse learners who are able to
approach design thinking from their own personal perspectives and abilities.
These design standards are written to be applicable across all content areas. Traditionally considered under visual arts, problem solving
through design thinking may be applied to their artistic work but, just as importantly, it also may be used for project work in other
disciplines. Effective practices will be employed in all student work as a result of studying the South Carolina College and Career
Ready Standards for Design Proficiency.
Page 93

Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
De.CR
NL.1
I can
recognize
design
questions.

Benchmark
De.CR
NM.1
I can
recognize
how design
questions are
used to solve
problems.

Benchmark
De.CR
NH.1
I can answer
design
challenge
questions.

Benchmark
De.CR
IL.1
I can work
with a team
to answer
design
challenge
questions.

Benchmark
De.CR
IM.1
I can work
with a team
from a given
list to
identify and
describe a
design
challenge to
develop.

Benchmark
De.CR
IH.1
I can work
with a team
from a given
list of design
challenges
and select
one to
describe.

Benchmark
De.CR
AL.1
I can work
with a team
to conceive
many design
challenge
possibilities
relating to a
certain topic.

Benchmark
De.CR
AM.1
I can work
with a team
to conceive
many design
challenge
possibilities.

Benchmark
De.CR
AH.1
I can work on
my own to
conceive
many design
challenge
possibilities.

Page 94

Indicator
De.CR
NL.1.1
I can answer
the design
challenge
questions
who, what,
and where, in
order to
define the
design
challenge.

Indicator
De.CR
NM.1.1
I can answer
the design
challenge
questions
who, what,
when, and
where in
order to
define the
design
challenge.

Indicator
De.CR
NH.1.1
I can answer
the design
challenge
questions
who, what,
when, where,
why, and
how in order
to define the
design
challenge.

Indicator
De.CR
IL.1.1
I can work
with a team
to answer the
design
challenge
questions
who, what,
when, where,
why, and
how to
define the
design
challenge.

Indicator
De.CR
IM.1.1
I can work
with a team
to select a
design
challenge
from a given
list using
criteria to
answer the
design
challenge
questions
and define
the challenge.

Indicator
De.CR
IH.1.1
I can work in
a team to
discuss
design
challenges
from a given
list and select
one to define
from answers
to the design
challenge
questions.

Indicator
De.CR
AL.1.1
I can work
with a team
using design
thinking
strategies to
list several
design
challenge
options about
a topic and
select one to
define.

Indicator
De.CR
AM.1.1
I can work
with a team
using design
thinking
strategies to
list many
design
challenge
possibilities
and prioritize
to select one
to define.

Indicator
De.CR
AH.1.1
I can use
design
thinking
strategies to
list many
design
challenge
possibilities
and prioritize
to select one
to define.

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Benchmark
De.CR
NL.2
I can
recognize
research
methods.

Benchmark
De.CR
NM.2
I can
recognize
how
research is
used to solve
a design
problem.

Benchmark
De.CR
NH.2
I can apply
research
methods.

Benchmark
De.CR
IL.2
I can work
with a team
to research
aspects of
the design
challenge.

Benchmark
De.CR
IM.2
I can work
with a team
to research
and describe
aspects of
the design
challenge.

Benchmark
De.CR
IH.2
I can work
with a team
to explain
why
researched
aspects of
the design
challenge are
needed.

Benchmark
De.CR
AL.2
I can work
with a team
to analyze the
aspects of
the design
challenge.

Benchmark
De.CR
AM.2
I can work
independentl
y or with a
team to
evaluate the
parts of the
design
challenge.

Benchmark
De.CR
AH.2
I can lead a
discussion to
evaluate the
parts of the
design
challenge.

Page 95

Indicator
De.CR
NL.2.1
I can use a
research
method to
investigate
the design
challenge.

Indicator
De.CR
NM.2.1
I can use
research
methods to
investigate
the design
challenge.

Indicator
De.CR
NH.2.1
I can use a
variety of
methods to
investigate
the design
challenge.

Indicator
De.CR
IL.2.1
I can work
with a team
to identify
necessary
information
for the
design
challenge.

Indicator
De.CR
IM.2.1
I can
communicate
my research
to the team.

Indicator
De.CR
IH.2.1
I can work
with a team
to prioritize
research
from the
individual
team
members.

Indicator
De.CR
AL.2.1
I can
examine my
research and
report the
connections
of that
information
with the
team.

Indicator
De.CR
AM.2.1
I can work
with a team
to determine
the
importance
of the
research
from the
team
members.

Indicator
De.CR
AH.2.1
I can guide
my team in
determining
the
importance
of the
research
from the
team
members.

Benchmark
De.CR
AM.3
I can work
independentl
y or with a
team to
evaluate the
usable design
solutions to
the challenge.

Benchmark
De.CR
AH.3
I can lead a
discussion to
evaluate the
usable design
solutions to
the challenge.

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Benchmark
De.CR
NL.3
I can
recognize
design
thinking.

Benchmark
De.CR
NM.3
I can
recognize
how design
thinking is
used to solve
a design
problem.

Benchmark
De.CR
NH.3
I can apply
design
thinking
strategies.

Benchmark
De.CR
IL.3
I can work
with a team
using design
thinking
strategies to
generate
ideas for
design
solutions to
the challenge.

Benchmark
De.CR
IM.3
I can work
with a team
using design
thinking
strategies to
generate
some usable
design
solutions to
the challenge.

Benchmark
De.CR
IH.3
I can work
with a team
using design
thinking
strategies to
generate
many usable
design
solutions to
the challenge.

Benchmark
De.CR
AL.3
I can work
with a team
to analyze
usable design
solutions to
the challenge.

Page 96

Indicator
De.CR
NL.3.1
I can use a
design
thinking
strategy to
list possible
design
solutions to
the challenge.

Indicator
De.CR
NM.3.1
I can use
more than
one design
thinking
strategy to
list possible
design
solutions to
the challenge.

Indicator
De.CR
NH.3.1
I can use a
variety of
design
thinking
strategies to
list possible
design
solutions to
the challenge.

Indicator
De.CR
IL.3.1
I can work
with a team
using a
variety of
design
thinking
strategies to
list possible
design
solutions
without
judgement.

Indicator
De.CR
IM.3.1
I can work
with a team
to turn ideas
into possible
design
solution
concepts.

Indicator
De.CR
IH.3.1
I can work
with a team
to determine
which design
solutions
effectively
meet the
challenge
criteria.

Indicator
De.CR
AL.3.1
I can
examine,
discuss, and
select
possible
design
solutions to
best address
the challenge.

Indicator
De.CR
AM.3.1
I can work
with a team
to develop
criteria to
determine the
value of the
usable design
solutions to
the challenge.

Indicator
De.CR
AH.3.1
I can guide
my team in
determining
the value of
the usable
design
solutions to
the challenge.

Benchmark
De.CR
IH.4
I can work
with a team
to create a
prototype to
solve a
design
challenge.

Benchmark
De.CR
AL.4
I can work
with a team
to create a
prototype
that solves
multiple
aspects of a
design
challenge.

Benchmark
De.CR
AM.4
I can work
with a team
to create a
prototype
that solves all
aspects of a
design
challenge
functionally
and
aesthetically.

Benchmark
De.CR
AH.4
I can use
sophisticated
materials,
techniques,
and processes
to create the
most viable
prototype.

Anchor Standard 4: I can create an original prototype.
Benchmark
De.CR
NL.4
I can
recognize a
prototype.

Benchmark
De.CR
NM.4
I can
recognize
how a
prototype is
used to solve
a design
challenge.

Benchmark
De.CR
NH.4
I can explore
materials,
techniques
and processes
to create a
prototype.

Benchmark
De.CR
IL.4
I can work
with a team
to make a
prototype
that
represents a
solution to a
design
challenge.

Benchmark
De.CR
IM.4
I can work
with a team
to make
multiple
prototypes
that represent
various
solutions to a
design
challenge.

Page 97

Indicator
De.CR
NL.4.1
I can explore
using
physical
models,
space
models,
interactions,
and
storytelling
as
prototypes.

Indicator
De.CR
NM.4.1
I can use
strategies to
create a twodimensional
drawing or a
threedimensional
model of a
design
solution.

Indicator
De.CR
NH.4.1
I can use
basic
materials and
techniques
to develop a
model of my
design ideas.

Indicator
De.CR
IL.4.1
I can work
with a team
to make a
prototype to
experience
the design
challenge
criteria.

Indicator
De.CR
IM.4.1
I can work
with a team
to make
prototypes
to experience
the design
challenge
criteria.

Indicator
De.CR
IH.4.1
I can work
with a team
to make a
prototype
that
addresses
functional
aspects and
aesthetics.

Indicator
De.CR
AL.4.1
I can work
with a team
to select
materials,
techniques,
and processes
to create a
prototype.

Indicator
De.CR
AM.4.1
I can work
with a team
to select and
apply the best
materials,
techniques,
and processes
to create a
prototype.

Indicator
De.CR
AH.4.1
I can select
and apply
professional
materials,
techniques,
and processes
to create a
prototype.

Benchmark
De.P
AL.5
I can work
with a team
to prepare
and deliver a
presentation
to a sample
target group.

Benchmark
De.P
AM.5
I can work
with a team
to develop a
wellprepared,
aesthetically
pleasing
presentation
for a sample
target group
that includes
community
business
leaders or
professionals
in the field.

Benchmark
De.P
AH.5
I can develop
a wellprepared,
aesthetically
pleasing
presentation
for a sample
target group
that includes
professionals
and business
leaders in my
community.

Artistic Processes: Presenting-I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Benchmark
De.P
NL.5
I can share
my design
with a small
group.

Benchmark
De.P
NM.5
I can identify
how a design
presentation
is used to
solve a
design
challenge.

Benchmark
De.P
NH.5
I can present
my design
solution to a
design
challenge.

Benchmark
De.P
IL.5
I can work
with a team
to present our
design
solution to a
challenge.

Benchmark
De.P
IM.5
I can work
with a team
to select an
approach to
present our
design
solution to a
challenge.

Benchmark
De.P
IH.5
I can work
with a team
to prepare
and deliver a
presentation
that has
defined
criteria.

Page 98

Indicator
De.P
NL.5.1
I can share
my
prototype
and answer
simple
questions
about the
design
solution.

Indicator
De.P
NM.5.1
I can explain
the design
challenge
and my
design
solution.

Indicator
De.P
NH.5.1
I can present
my design
solution to
the challenge
using a
visual.

Indicator
De.P
IL.5.1
I can work
with a team
to present our
design
solution to
the challenge
using one or
more visuals.

Indicator
De.P
IM.5.1
I can work
with a team
to select an
approach
using
technology
for the
design
solution
presentation.

Indicator
De.P
IH.5.1
I can work
with a team
to create a
presentation
that includes
specific
criteria and
delivers
required
information
concerning
the design
challenge
and design
solution.

Indicator
De.P
AL.5.1
I can work in
a team to
present our
design
solution to a
group of
possible
users/consum
ers for
feedback.

Indicator
De.P
AM.5.1
I can work in
a team to
present our
design
solution to a
sample target
group that
includes
community
business
leaders and
professionals
in a related
field for
feedback.

Indicator
De.P
AH.5.1
I can present
our design
solution to a
sample target
audience that
includes
professionals
and business
leaders in a
related field
for feedback.

Page 99

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Benchmark
De.R
NL.6

Benchmark
De.R
NM.6

Benchmark
De.R
NH.6

Benchmark
De.R
IL.6

I can
recognize
how
reflection is
necessary in
the design
process.

I can
recognize
that revision
is necessary
in the design
process.

I can
encourage
feedback to
my design
and the
designs of
others by
asking and
answering
questions.

I can reflect
on and
provide
feedback to a
design
solution.

Indicator
De.R
NL.6.1
I can identify
the strengths
of my design
and designs
of others.

Indicator
De.R
NM.6.1
I can identify
areas of my
design and
the designs
of others that
need
improvement
.

Indicator
De.R
NH.6.1
I can prepare
some
questions for
feedback to
help me
revise my
design.

Indicator
De.R
IL.6.1
I can work
with a team
to record
feedback and
summarize
design
solution
recommendat
ions.

Benchmark
De.R
IM.6

Benchmark
De.R
IH.6

Benchmark
De.R
AL.6

Benchmark
De.R
AM.6

Benchmark
De.R
AH.6

I can
interpret
feedback
from my
peers to
revise our
design
solution.

I can work
with a team
to analyze
and explain
the steps of
the design
solution
revision.

I can work
with a team
to retest our
revised
design
solution and
analyze the
results.

I can
facilitate the
repetition of
the design
process to
revise and
retest the
design
solution.

Indicator
De.R
IM.6.1
I can work
with a team
to list and
prioritize
feedback to
improve our
design
solution.

Indicator
De.R
IH.6.1
I can work
with a team
to plan and
develop the
steps to
improve our
design
solution.

Indicator
De.R
AL.6.1
I can work
with a team
to improve
the
functionality
of our design
solution and
record the
results of the
modifications

I can work
with a team
to explain
future
improvement
s and repeat
the design
process to
revise and
retest the
design
solution.
Indicator
De.R
AM.6.1
I can work
with a team
to repeat the
design
process as
necessary to
improve the
design
solution.

Indicator
De.R
AH.6.1
I can guide
and frame
questions to
facilitate the
design
process to
improve a
design
solution.

Page 100

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Benchmark
De.C
NL.7
I can
recognize
some
examples of
design found
in my home
and
community.

Benchmark
De.C
NM.7
I can
recognize
differences in
designs
found in my
home and
community.

Benchmark
De.C
NH.7
I can describe
differences in
designs from
various
cultures
throughout
history.

Benchmark
De.C
IL.7
I can identify
improvements
or changes in
designs found
in various
cultures and
time periods.

Indicator
De.C
NL.7.1
I can find and
name some
designs
(object
environment
al,
communicati
on, or
experiential)
around me.

Indicator
De.C
NM.7.1
I can name
some
different
design
materials and
methods of
construction.

Indicator
De.C
NH.7.1
I can
compare how
designs are
different in
various
cultures
throughout
history.

Indicator
De.C
IL.7.1
I can compare
design
similarities
and
differences
among
different
cultures and
time periods.

Benchmark
De.C
IM.7
I can describe
why
improvements or
changes were
made in
designs
found in
various
cultures and
time periods.
Indicator
De.C
IM.7.1
I can explain
the possible
reasons
improvement
s and/or
changes were
made in a
design
through
different
cultures and
time periods.

Benchmark
De.C
IH.7
I can
analyze a
variety of
design
works from
different
cultures and
time
periods.

Benchmark
De.C
AL.7
I can
examine past
design works
to determine
their
influence on
present
designs.

Benchmark
De.C
AM.7
I can work
with a team
to analyze the
influence of
past design
works on
present
design
challenges.

Benchmark
De.C
AH.7
I can evaluate
my design
solution to
determine the
effective use
of past
design
works.

Indicator
De.C
IH.7.1
I can
recognize
patterns in
design
choices and
make
connections
to the
developmen
t of design
through
different
cultures and
time
periods.

Indicator
De.C
AL.7.1
I can find and
compare how
choices from
a current
design reflect
influences of
past design
solutions.

Indicator
De.C
AM.7.1
I can work
with a team
to explain
how the
designer's
choices on
the current
design
challenge
reflect
influences of
design
solutions
from the past.

Indicator
De.C
AH.7.1
I can assess
my design
choices and
relate them to
past design
influences.

Page 101

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Benchmark
De.C
NL.8
I can explore
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
NM.8
I can
recognize
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
NH.8
I can apply
design
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
De.C
IL.8
I can explore
a range of
skills shared
among arts
disciplines,
other content
areas and
how they can
be applied in
a design
career.

Benchmark
De.C
IM.8
I can
recognize
specific skills
shared
among arts
disciplines,
other content
areas and
how they can
be applied in
a design
career.

Indicator
De.C
NL.8.1
I can connect
design with
objects in my
home and
school.

Indicator
De.C
NM.8.1
I can
recognize
that design
exists in all
arts
disciplines
and other
content areas.

Indicator
De.C
NH.8.1
I can use
design
concepts in
other subjects
in my school.

Indicator
De.C
IL.8.1
I can
investigate a
range of
skills used in
various
design
careers, arts
disciplines,
and content
areas.

Indicator
De.C
IM.8.1
I can name
design skills
used in
various arts
disciplines
and content
areas and
relate these
skills to a
career in
design.

Benchmark
De.C
IH.8
I can analyze
the tools,
concepts, and
materials
used among
arts
disciplines,
other content
areas and
how they are
used in a
design
career.
Indicator
De.C
IH.8.1
I can
investigate
tools,
concepts and
materials
used in other
arts
disciplines
and content
areas.

Benchmark
De.C
AL.8
I can apply
concepts
among arts
disciplines
and other
content areas
to design and
analyze how
my interests
and skills
will prepare
me for a
career.
Indicator
De.C
AL.8.1
I can use
concepts
found in
various arts
disciplines
and other
content areas
in a design
work.

Benchmark
De.C
AM.8
I can explain
how
economic
conditions,
cultural
values, and
location
influence
design and
the need for
design
related
careers.
Indicator
De.C
AM.8.1
I can describe
how
economic
conditions,
cultural
values, and
geographic
locations
affect design
and design
careers.

Benchmark
De.C
AH.8
I can research
societal,
political, and
cultural
issues as they
relate to other
arts and
content areas
and apply to
my role as a
designer.

Indicator
De.C
AH.8.1
I can
examine the
importance
of the work
of a designer
in issues that
relate to a
global
society.

Page 102

Indicator
De.C
NL.8.2
I can
recognize
that people
have careers
in design.

Indicator
De.C
NM.8.2
I can identify
design
businesses
and careers in
my
community.

Indicator
De.C
NH.8.2
I can identify
ways design
thinking is
used in other
careers or
vocations.

Page 103

Novice Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Novice
Low
Benchmark
De.CR
NL.1
I can recognize design questions.
Indicator
De.CR
NL.1.1
I can answer the design challenge questions
who, what, and where, in order to define the
design challenge.

Novice
Mid
Benchmark
De.CR
NM.1
I can recognize how design questions are
used to solve problems.
Indicator
De.CR
NM.1.1
I can answer the design challenge questions
who, what, when, and where in order to define
the design challenge.

Indicator
De.CR
NH.1.1
I can answer the design challenge questions
who, what, when, where, why, and how in
order to define the design challenge.

Sample Learning Targets
x , FDQ DQVZHU ³ZKR´ WKH design
challenge impacts.

Sample Learning Targets
x , FDQ DQVZHU ³ZKHQ´ WKH design
challenge will occur.

Sample Learning Targets
x , FDQ DQVZHU ³ZK\´ WKH design
challenge is needed.

x

, FDQ DQVZHU ³ZKDW´ WKH design
challenge is for.

x

, FDQ DQVZHU ³ZKHUH´ WKH design
challenge will be impacted.

x

x

I can use design questions to
recognize how to define a design
challenge.

x

, FDQ«

Novice
High
Benchmark
De.CR
NH.1
I can answer design challenge questions.

x

, FDQ DQVZHU ³KRZ´ WKH design
challenge will be implemented.

x

, FDQ«

, FDQ«

Page 104

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Novice
Low
Benchmark
De.CR
NL.2
I can recognize research methods.

Novice
High
Benchmark
De.CR
NH.2
I can apply research methods.

Indicator
De.CR
NL.2.1
I can use a research method to investigate the
design challenge.

Novice
Mid
Benchmark
De.CR
NM.2
I can recognize how research is used to solve
a design problem.
Indicator
De.CR
NM.2.1
I can use research methods to investigate the
design challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can observe (using the five senses)
the existing designs.

x

I can observe an object in use.

x

x

Indicator
De.CR
NH.2.1
I can use a variety of methods to investigate
the design challenge.

x

I can use more than one of the
following: observation, printed
materials, technology, and/or
interviewing.

x

I can use a variety of the following
research methods: observation,
printed materials, technology, and/or
interviewing.

I can see and feel the parts of a
design object.

x

I can use printed materials to learn
about an object.

x

, FDQ«

x

I can interview others for research
information.

I can observe, sketch, or record
(photography, video) an object to
VKRZ ZKDW ,¶YH OHDUQHG DERXW WKH
design object.

x
x

, FDQ«

I can interview individuals with
experience with an object to
determine possible aspects to
redesign.

x

, FDQ«

Page 105

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Novice
Low
Benchmark
De.CR
NL.3
I can recognize design thinking.

Novice
Mid
Benchmark
De.CR
I can recognize how design thinking is used
to solve a design problem.

Novice
High
Benchmark
De.CR
NH.3
I can apply design thinking strategies.

Indicator
De.CR
NL.3.1
I can use a design thinking strategy to list
possible design solutions to the challenge.

Indicator
De.CR
NM.3.1
I can use more than one design thinking
strategy to list possible design solutions to
the challenge.

Indicator
De.CR
NH.3.1
I can use a variety of design thinking
strategies to list possible design solutions to
the challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can name possible solutions.

x

I can stay on topic to randomly call
out ideas for possible design
solutions.

x

, FDQ«

x

I can use more than one of the
following: list aloud, popcorn
brainstorming, passing
brainstorming to provide possible
solutions.

x

I can determine which solutions can
be used in the design challenge.

x

I can provide a visual or drawing to
explain my idea.

x

I can create questions rather than
ideas to inspire further thinking.

x

I can organize my ideas using mind
maps.

x

I can listen to others and participate in
one conversation at a time to provide
possible design solutions.

x

, FDQ«

x

, FDQ«

Page 106

Anchor Standard 4: I can create an original prototype.
Novice
Low
Benchmark
De.CR
NL.4
I can recognize a prototype.

Novice
Mid
Benchmark
De.CR
NM.4
I can recognize how a prototype is used to
solve a design challenge.

Novice
High
Benchmark
De.CR
NH.4
I can explore materials, techniques and
processes to create a prototype.

Indicator
De.CR
NL.4.1
I can explore using physical models, space
models, interactions, and storytelling as
prototypes.

Indicator
De.CR
NM.4.1
I can use strategies to create a twodimensional drawing or a three-dimensional
model of a design solution.

Indicator
De.CR
NH.4.1
I can use basic materials and techniques to
develop a model of my design ideas.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can role play to act out solutions to
a design challenge.

x

I can use clay or other materials to
create a model of a new cup design.

x

I can explore space models with
geometric forms in a given area.

x

I can draw a new logo design.

x
x

I can use my words to tell about my
design idea.

x

, FDQ«

x

I can work with a team to explore and
select the most appropriate materials
to build/compose the prototype.

x

I can work with a team to explore and
select the most appropriate
techniques and processes to
build/compose the prototype.

x

, FDQ«

, FDQ«

Page 107

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Novice
Low
Benchmark
De.P
NL.5
I can share my design with a small group.

Novice
Mid
Benchmark
De.P
NM.5
I can identify how a design presentation is
used to solve a design challenge.

Novice
High
Benchmark
De.P
NH.5
I can present my design solution to a design
challenge.

Indicator
De.P
NL.5.1
I can share my prototype and answer simple
questions about the design solution.

Indicator
De.P
NM.5.1
I can explain the design challenge and my
design solution.

Indicator
De.P
NH.5.1
I can present my design solution to the
challenge using a visual.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can show my prototype to my
peers.

x

, FDQ H[SODLQ WKH ³ZKR ZKDW ZKHUH´
of the design challenge.

x

I can draw my ideas to present my
design challenge solution.

x

I can answer questions about the
³ZKDW´ RI WKH design solution.

x

, FDQ H[SODLQ WKH ³ZKHQ DQG KRZ´ RI
the design challenge.

x

I can create a presentation board to
help explain my design challenge
solution.

x

I can ...

x

I can ...
x

I can...

Page 108

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Novice
Low
Benchmark
De.R
NL.6

Novice
Mid
Benchmark
De.R
NM.6

Novice
High
Benchmark
De.R
NH.6

I can recognize how reflection is necessary in
the design process.

I can recognize that revision is necessary in
the design process.

I can encourage feedback to my design and
the designs of others by asking and answering
questions.

Indicator
De.R
NL.6.1
I can identify the strengths of my design and
designs of others.

Indicator
De.R
NM.6.1
I can identify areas of my design and the
designs of others that need improvement.

Indicator
De.R
NH.6.1
I can prepare some questions for
feedback to help me revise my design.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can listen and respond to the
opinions of others.

x

I can work with others to list possible
improvements to our solution.

x

I can ask what new materials could be
used in a design solution.

x

I can list the positive comments about
my design.

x

I can list changes I would make to my
design solution.

x

I can ask simple questions about a
design solution.

x

I can...

x

I can...

x

I can ask questions about who needs
the design.

x

I can...

Page 109

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Novice
Low
Benchmark
De.C
NL.7
I can recognize some examples of design
found in my home and community.

Novice
Mid
Benchmark
De.C
NM.7
I can recognize differences in designs found
in my home and community.

Novice
High
Benchmark
De.C
NH.7
I can describe differences in designs from
various cultures throughout history.

Indicator
De.C
NL.7.1
I can find and name some designs (object
environmental, communication, or
experiential) around me.
Sample Learning Targets

Indicator
De.C
NM.7.1
I can name some different design materials
and methods of construction.

Indicator
De.C
NH.7.1
I can compare how designs are different in
various cultures throughout history.

Sample Learning Targets

Sample Learning Targets

x

I can find and name some design
objects that I use every day.

x

I can identify the methods used for
communication design.

x

I can group designs that have similar
styles, subject, or media.

x

I can find and name some
environmental designs in my school
and community.

x

I can discuss the materials used in an
environmental design.

x

I can identify common characteristics
within a design from different styles,
periods, and cultures.

x
x

I can recognize the use of
communication design in
newspapers, billboards, and
commercials.

I can describe how a design was
made.

x

, FDQ«

x

I can recognize the use of
experiential design in play grounds,
video games and amusement parks.

x

, FDQ«

x

, FDQ«

Page 110

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Novice
Low
Benchmark
De.C
NL.8
I can explore design thinking in arts
disciplines, other content areas, and related
careers.

Novice
Mid
Benchmark
De.C
NM.8
I can recognize design thinking in arts
disciplines, other content areas, and related
careers.

Novice
High
Benchmark
De.C
NH.8
I can apply design thinking in arts disciplines,
other content areas, and related careers.

Indicator
De.C
NL.8.1
I can explore how design exists in all arts
disciplines and other content areas.
Sample Learning Targets

Indicator
De.C
NM.8.1
I can recognize that design exists in all arts
disciplines and other content areas.
Sample Learning Targets

Indicator
De.C
NH.8.1
I can use design concepts in other subjects in
my school.
Sample Learning Targets

x

I can name designed objects in my
home and classroom.

x

I can identify ways design is used in
my community.

x

I can use the design process to solve
problems in other subjects.

x

I can talk about design choices found
in my home and classroom.

x

I can draw designs used in my
community.

x

I can use design thinking to
brainstorm multiple solutions in
other subjects.

x

I can draw examples of everyday
designs.

x

, FDQ«
x

, FDQ«

x

, FDQ«

Page 111

Indicator
De.C
NL.8.2
I can recognize that people have careers in
design.

Indicator
De.C
NM.8.2
I can identify design businesses and careers in
my community.

Indicator
De.C
NH.8.2
I can identify ways design thinking is used in
other careers or vocations.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can recognize that musicians are
designers.

x

I can identify businesses in my
community that hire designers.

x

I can identify how design thinking is
used in business and industry.

x

I can recognize that buildings are
designed by architects.

x

I can identify where and how
designers impact my community.

x

x

I can recognize that choreographers
are designers.

x

I can locate design companies in my
community.

I can identify design thinking skills
that are used in education and service
organizations.

x

, FDQ«

, FDQ«

x

x

, FDQ«

Page 112

Intermediate Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Intermediate
Low
Benchmark
De.CR
IL.1
I can work with a team to answer design
challenge questions.

Intermediate
High
Benchmark
De.CR
IH.1
I can work with a team from a given list of
design challenges and select one to describe.

Indicator
De.CR
IL.1.1
I can work with a team to answer the design
challenge questions who, what, when, where,
why, and how in order to define the design
challenge

Intermediate
Mid
Benchmark
De.CR
IM.1
I can work with a team from a given list to
identify and describe a design challenge to
develop.
Indicator
De.CR
IM.1.1
I can work with a team to select a design
challenge from a given list using certain
criteria and answer the design challenge
questions to define the challenge.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can communicate and listen to others
when answering the design challenge
questions.

x

I can work with a team to choose a
design challenge based on its
importance to me and my community.

x

I can record information from the
JURXS¶V GLVFXVVLRQ

x

I can work with a team to choose a
design challenge based on the need
for improvement to how it looks and
how it works.

x

I can...
x

Indicator
De.CR
IH.1.1
I can work in a team to discuss design
challenges from a given list and select one to
define from answers to the design challenge
questions.

x

I can work with a team to compare
and contrast the design challenge
options and select one based on their
importance to me and my community.

x

I can work with a team to compare
and contrast the design challenge
options and select one based on their
need for improvement to how it looks
and how it works.
, FDQ«

, FDQ«
x

Page 113

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Intermediate
Low
Benchmark
De.CR
IL.2
I can work with a team to research aspects of
the design challenge.

Intermediate
Mid
Benchmark
De.CR
IM.2
I can work with a team to research and
describe aspects of the design challenge.

Intermediate
High
Benchmark
De.CR
IH.2
I can work with a team to explain why
researched aspects of the design challenge
are needed.

Indicator
De.CR
IL.2.1
I can work with a team to identify necessary
information for the design challenge.

Indicator
De.CR
IM.2.1
I can communicate my research to the team.

Indicator
De.CR
IH.2.1
I can work with a team to prioritize research
from the individual team members.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify with a team what
information is necessary about the
existing design.

x

I can use visuals, technology,
demonstrations, and/or descriptions,
to report the research.

x

I can work with others to select the
best research methods to gather
necessary information.

x

I can discuss the research with others.

x

I can use printed materials to present
necessary information.

x

I can demonstrate the existing
function of a design.

x

I can work with others to create a
survey and/or use technology to learn
about a design.

x

I can list the research from the team
members.

x

I can work with others to identify the
most significant research.

Page 114

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Intermediate
Low
Benchmark
De.CR
IL.3
I can work with a team using design thinking
strategies to generate ideas for design
solutions to the challenge.
Indicator
De.CR
IL.3.1
I can work with a team using a variety of
design thinking strategies to list possible
design solutions without judgement.

Intermediate
Mid
Benchmark
De.CR
IM.3
I can work with a team using design
thinking strategies to generate some usable
design solutions to the challenge.
Indicator
De.CR
IM.3.1
I can work with a team to turn ideas into
possible design solution concepts.

Intermediate
High
Benchmark
De.CR
IH.3
I can work with a team using design
thinking strategies to generate many usable
design solutions to the challenge.
Indicator
De.CR
IH.3.1
I can work with a team to determine which
design solutions effectively meet the
challenge criteria.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can work with a team using a
variety of the following: list aloud,
popcorn brainstorming, passing
brainstorming, questioning
brainstorming, webbing, mind
mapping to provide possible
solutions.

x

I can build on the ideas of others in
creating possible solutions.

x

I can work with a team to determine
which solutions can be used in the
design challenge.

x

, FDQ«

x

I can contribute my ideas concerning
usable solutions.

x

, FDQ UHVSRQG WR RWKHUV¶ LGHDV
concerning usable solutions.

x

x

I can work with others to prioritize
choices concerning effective
solutions.

x

I can work with others to select
possible solutions.

x

, FDQ«

, FDQ«

Page 115

Anchor Standard 4: I can create an original prototype.
Intermediate
Low
Benchmark
De.CR
IL.4
I can work with a team to make a prototype
that represents a solution to a design
challenge.
Indicator
De.CR
IL.4.1
I can work with a team to make a prototype
to experience the design challenge criteria.

Intermediate
Mid
Benchmark
De.CR
IM.4
I can work with a team to make multiple
prototypes that represent various solutions to
a design challenge.
Indicator
De.CR
IM.4.1
I can work with a team to make prototypes
to experience the design challenge criteria.

Sample Learning Targets

Sample Learning Targets

x

I can work with others to create a
prototype that allows a concept to be
experienced.

x

I can create a simple prototype that is
made quickly and inexpensively to
experience feedback early and often.

x

x

x

Intermediate
High
Benchmark
De.CR
IH.4
I can work with a team to create a prototype
to solve a design challenge.
Indicator
De.CR
IH.4.1
I can work with a team to make a prototype
that addresses functional aspects and
aesthetics.
Sample Learning Targets

I can work with others to create
multiple prototypes concerning one
design challenge that allow a concept
to be experienced.

x

I can work with a team to determine
the functionality of the prototype.

x

I can work with a team to create
multiple simple prototypes that are
made quickly and inexpensively.

I can work with a team to improve
the functionality of the prototype to
address many aspects.

x

I can make a prototype that uses the
elements and/or principles of the arts
disciplines.

x

, FDQ«

, FDQ«
x

, FDQ«

Page 116

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Intermediate
Low
Benchmark
De.P
IL.5
I can work with a team to present our design
solution to a challenge.

Intermediate
Mid
Benchmark
De.P
IM.5
I can work with a team to select an approach
to present our design solution to a challenge.

Intermediate
High
Benchmark
De.P
IH.5
I can work with a team to prepare and deliver
a presentation that has defined criteria.

Indicator
De.P
IL.5.1
I can work with a team to present our design
solution to the challenge using one or more
visuals.

Indicator
De.P
IM.5.1
I can work with a team to select an approach
using technology for the design solution
presentation.

Sample Learning Targets

Sample Learning Targets

Indicator
De.P
IH.5.1
I can work with a team to create a
presentation that includes specific criteria
and delivers required information concerning
the design challenge and design solution.
Sample Learning Targets

x

I can work with a team to prepare one
or more visuals such as photographs,
drawings, diagrams, charts, and 3D
examples to present our design
solution.

x

I can work with others to create a
slideshow presentation.

x

I can work with others to create a
webpage to present a design solution.

x

I can work with a team to explain the
³ZKR ZKDW ZKHQ ZKHUH ZK\ DQG
KRZ´ RI WKH design challenge.

x

I can work with others to combine
still photos and videos to present a
design solution.

x

I can work with a team to explain the
³ZKR ZKDW ZKHQ ZKHUH ZK\ DQG
KRZ´ RI WKH design solution.

x

I can ...

x

x

I can work in a team to prepare a
presentation that includes specific
criteria such as a title, infographics,
text, graphics, and/or media.

x

I can work with a team to prepare a
presentation that includes required
information such as the goal,
identified population, challenge
statement, key aspects, data, and
design solution.

x

, FDQ«

, FDQ«
Page 117

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Intermediate
Low
Benchmark
De.R
IL.6

Intermediate
Mid
Benchmark
De.R
IM.6

Intermediate
High
Benchmark
De.R
IH.6

I can reflect on and provide feedback to a
design solution.

I can interpret feedback from my peers to
revise our design solution

I can work with a team to analyze and explain
the steps of the design solution revision.

Indicator
De.R
IL.6.1
I can work with a team to record feedback
and summarize design solution
recommendations.

Indicator
De.R
IM.6.1
I can work with a team to list and prioritize
feedback to improve our design solution.

Indicator
De.R
IH.6.1
I can work with a team to plan and develop
the steps to improve our design solution.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can record feedback about our
design in my journal.

x

I can explain some of the solutions
presented as feedback to the group.

x

x

I can work with others to make a list
of the most important improvements
that need to be made to the design
solution.

x

I can...

x

I can work with others to review
feedback to determine next steps in
the revision process.

x

I can work with others to make
changes to our prototype that
improves our solution.

x

I can record my improvement ideas
for a design solution.

x

I can...

I can...

Page 118

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Intermediate
Low
Benchmark
De.C
IL.7
I can identify improvements or changes in
designs found in various cultures and time
periods.
Indicator
De.C
IL.7.1
I can compare design similarities and
differences among different cultures and time
periods.
Sample Learning Targets

Intermediate
Mid
Benchmark
De.C
IM.7
I can describe why improvements or changes
were made in designs found in various
cultures and time periods.
Indicator
De.C
IM.7.1
I can explain the possible reasons
improvements and/or changes were made in a
design through different cultures and time
periods.
Sample Learning Targets

Intermediate
High
Benchmark
De.C
IH.7
I can analyze a variety of design works from
different cultures and time periods.
Indicator
De.C
IH.7.1
I can recognize patterns in design choices and
make connections to the development of
design through different cultures and time
periods.
Sample Learning Targets

x

I can write a description about
characteristics of a specific design
style, period, or culture.

x

I can explain the possible reasons a
chair design evolved through cultures
and time periods.

x

I can make connections between
design choices on chairs from
different cultures and time periods.

x

I can compare changes in the designs
of furniture from other cultures over
time.

x

I can explain the possible reasons a
simple tool changed through cultures
and time periods.

x

I can make connections between
design choices on furniture from
different cultures and time periods.

x

, FDQ«

x

, FDQ«

x

, FDQ«

Page 119

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Intermediate
Low
Benchmark
De.C
IL.8
I can explore a range of skills shared among
arts disciplines, other content areas and how
they can be applied in a design career.

Intermediate
Mid
Benchmark
De.C
IM.8
I can recognize specific skills shared among
arts disciplines, other content areas and how
they can be applied in a design career.

Indicator
De.C
IL.8.1
I can investigate a range of skills used in
various design careers, arts disciplines, and
content areas.
Sample Learning Targets

Indicator
De.C
IM.8.1
I can name design skills used in various arts
disciplines and content areas and relate these
skills to a career in design.
Sample Learning Targets
x

I can research design careers.

x

I can list things that are designed by
people with a specific career in
design.

I can match a design product to a
design career.

x

I can list specific skills needed for a
design career.

, FDQ«

x

, FDQ«

x

I can recognize skills that are specific
to a career in design.

x

I can pick and write about my favorite
design career.

x

x

Intermediate
High
Benchmark
De.C
IH.8
I can analyze the tools, concepts, and
materials used among arts disciplines, other
content areas and how they are used in a
design career.
Indicator
De.C
IH.8.1
I can investigate tools, concepts and materials
used in other arts disciplines and content
areas.
Sample Learning Targets
x

I can recognize skills that are specific
to design careers that are attained in
other arts disciplines and content
areas.

x

I can discuss costs of using different
materials to create the same design.

x

, FDQ«

Page 120

Advanced Design Standards
Artistic Processes: Creating- I can conceive and develop new design ideas and work.
Anchor Standard 1: I can conceive and develop a design challenge.
Advanced
Low
Benchmark
De.CR
AL.1
I can work with a team to conceive many
design challenge possibilities relating to a
certain topic.
Indicator
De.CR
AL.1.1
I can work with a team using design thinking
strategies to list several design challenge
possibilities about a topic and select one to
define.
Sample Learning Targets

Advanced
High
Benchmark
De.CR
AH.1
I can work on my own to conceive many
design challenge possibilities.

Indicator
De.CR
AM.1.1
I can work with a team using design thinking
strategies to list many design challenge
possibilities and prioritize to select one to
define.
Sample Learning Targets

Indicator
De.CR
AH.1.1
I can use design thinking strategies to list
many design challenge possibilities and
prioritize to select one to define.

x

I can work with a team to
brainstorm by creating questions
rather than ideas to inspire further
thinking.

I can work with a team using a
variety of the following: list aloud,
popcorn brainstorming,
questioning brainstorming,
webbing, mind mapping to provide
many possible design challenges.

x

I can work with a team to use visual
diagrams to organize information and
ideas. , FDQ«

I can work with a team to compare
and contrast the design challenge
options and select one to define.

x

, FDQ«

x

I can work with a team to
brainstorm by randomly calling out
ideas.

x

x

Advanced
Mid
Benchmark
De.CR
AM.1
I can work with a team to conceive many
design challenge possibilities.

Sample Learning Targets
x

I can use a variety of the following:
list aloud, popcorn brainstorming,
questioning brainstorming,
webbing, mind mapping to provide
many possible design challenges.

x

I can compare and contrast the design
challenge options and select one to
define.

x

, FDQ«
Page 121

Anchor Standard 2: I can research to explore and identify aspects of the design challenge.
Advanced
Low
Benchmark
De.CR
AL.2
I can work with a team to analyze the aspects
of the design challenge.
Indicator
De.CR
AL.2.1
I can examine my research and report the
connections of that information with the
team.
Sample Learning Targets
x

I can review with a team the
research from multiple sources.

x

I can report the connections among
the data to my team.

x

Advanced
Mid
Benchmark
De.CR
AM.2
I can work independently or with a team to
evaluate the parts of the design challenge.
Indicator
De.CR
AM.2.1
I can work with a team to determine the
importance of the research from the team
members.
Sample Learning Targets
x

I can work with others to determine
the importance of the production and
cost improvement needed.

x

I can work with others to determine
the importance of the aesthetic
improvement needed.

, FDQ«
x

I can work with others to determine
the importance functional
improvement needed.

x

, FDQ«

Advanced
High
Benchmark
De.CR
AH.2
I can lead a discussion to evaluate the parts of
the design challenge.
Indicator
De.CR
AH.2.1
I can guide my team in determining the
importance of the research from the team
members.
Sample Learning Targets
x

I can present findings from research
that supports the need for aesthetic,
production, and/or functional
improvements.

x

I can justify the need for a new design
or redesign concept.

x

, FDQ«

Page 122

Anchor Standard 3: I can select and create possible solutions to the design challenge.
Advanced
Low
Benchmark
De.CR
AL.3
I can work with a team to analyze usable
design solutions to the challenge.
Indicator
De.CR
AL.3.1
I can examine, discuss, and select possible
design solutions to best address the
challenge.
Sample Learning Targets

Advanced
Mid
Benchmark
De.CR
AM.3
I can work independently or with a team to
evaluate the usable design solutions to the
challenge.
Indicator
De.CR
AM.3.1
I can work with a team to develop criteria to
determine the value of the usable design
solutions to the challenge.
Sample Learning Targets

x

I can review and discuss connections
among the possible solutions.

x

I can work with others to list criteria
such as time, cost, functionality,
aesthetics, etc.

x

I can work with others to combine
parts of design solution ideas to
solve the design challenge.

x

I can work with others to prioritize
design solutions based on chosen
criteria.

x

I can work with a team to reach a
consensus concerning the most viable
solutions to the design challenge.

x

, FDQ«

x

, FDQ«

Advanced
High
Benchmark
De.CR
AH.3
I can lead a discussion to evaluate the usable
design solutions to the challenge.
Indicator
De.CR
AH.3.1
I can guide my team in determining the value
of the usable design solutions to the
challenge.
Sample Learning Targets
x

I can lead a discussion to determine
the criteria.

x

I can lead a discussion that reaches a
consensus concerning the most viable
solutions to the design challenge.

x

I can justify how the solutions
effectively address the identified
needs.

x

, FDQ«

Page 123

Anchor Standard 4: I can create an original prototype.
Advanced
Low
Benchmark
De.CR
AL.4
I can work with a team to create a prototype
that solves multiple aspects of a design
challenge.
Indicator
De.CR
AL.4.1
I can work with a team to select materials,
techniques, and processes to create a
prototype.
Sample Learning Targets
x

x

I can work with a team to select the
most appropriate materials to
build/compose the prototype from
those explored.

Advanced
Mid
Benchmark
De.CR
AM.4
I can work with a team to create a prototype
that solves all aspects of a design challenge
functionally and aesthetically.
Indicator
De.CR
AM.4.1
I can work with a team to select and apply the
best materials, techniques, and processes to
create a prototype.
Sample Learning Targets
x

I can work with a team to apply the
best materials to build/compose the
prototype from those explored.

x

I can work with a team to apply the
best techniques and processes to
build/compose the prototype from
those explored.

I can work with a team to select the
most appropriate techniques and
processes to build/compose the
prototype from those explored.
x

x

Advanced
High
Benchmark
De.CR
AH.4
I can use sophisticated materials, techniques,
and processes to create the most viable
prototype.
Indicator
De.CR
AH.4.1
I can select and apply professional materials,
techniques, and processes to create a
prototype.
Sample Learning Targets
x

I can apply professional materials to
build/compose the prototype.

x

I can apply professional techniques
and processes to build/compose the
prototype.

x

, FDQ«

, FDQ«

, FDQ«

Page 124

Artistic Processes: Presenting- I can present new design ideas and work.
Anchor Standard 5: I can present my final design solution.
Advanced
Low
Benchmark
De.P
AL.5
I can work with a team to prepare and deliver
a presentation to a sample target group.

Indicator
De.P
AL.5.1
I can work in a team to present our design
solution to a group of possible
users/consumers for feedback.
Sample Learning Targets
x

I can work in a team and ask
questions of the target group so I can
effectively get the feedback.

x

I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, with a team to attain
feedback from the sample group.

x

Advanced
Mid
Benchmark
De.P
AM.5
I can work with a team to develop a wellprepared, aesthetically pleasing presentation
for a sample target group that includes
community business leaders or professionals
in the field.
Indicator
De.P
AM.5.1
I can work in a team to present our design
solution to a sample target group that includes
community business leaders and professionals
in a related field for feedback.
Sample Learning Targets
x

I can work in a team and ask
questions of the target group with
professionals so I can effectively get
the feedback.

x

I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, with a team to attain
feedback from the sample group with
business leaders in my community.

, FDQ«
x

Advanced
High
Benchmark
De.P
AH.5
I can develop a well-prepared, aesthetically
pleasing presentation for a sample target
group that includes professionals and business
leaders in my community.
Indicator
De.P
AH.5.1
I can present our design solution to a sample
target audience that includes professionals and
business leaders in a related field for
feedback.
Sample Learning Targets
x

I can ask questions of the target group
with professionals so I can effectively
get the feedback.

x

I can use methods such as surveys,
questionnaires, prompts, and/or beta
testing, to attain feedback from the
sample group with professionals.

x

, FDQ«

, FDQ«

Page 125

Artistic Processes: Responding- I can respond to feedback from others on new design ideas and work.
Anchor Standard 6: I can reflect and revise based on feedback and input.
Advanced
Low
Benchmark
De.R
AL.6

Advanced
Mid
Benchmark
De.R
AM.6

Advanced
High
Benchmark
De.R
AH.6

I can work with a team to retest our revised
design solution and analyze the results.

I can work with a team to explain future
improvements and repeat the design process
to revise and retest the design solution.

I can facilitate the repetition of the design
process to revise and retest the design
solution.

Indicator
De.R
AL.6.1
I can work with a team to improve the
functionality of our design solution and
record the results of the modifications.

Indicator
De.R
AM.6.1
I can work with a team to repeat the design
process as necessary to improve the design
solution.

Indicator
De.R
AH.6.1
I can guide and frame questions to facilitate
the design process to improve a design
solution.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

I can work with others to make
improvements to the prototype¶V
functionality.
I can chart the progress of our
revisions to help my team improve
the functionality of the design.

x

x

I can retest my solution and revise as
many times as necessary to achieve
the most effective solution.
I can...

x

I can lead a class discussion on how
to revise a design challenge.

x

I can form questions to lead the
reflection process.

x

I can...

I can....

Page 126

Artistic Processes: Connecting- I can relate artistic ideas and work with personal meaning and external
context.
Anchor Standard 7: I can identify and examine design through history and world culture.
Advanced
Low
Benchmark
De.C
AL.7
I can examine past design works to determine
their influence on present designs.

Advanced
Mid
Benchmark
De.C
AM.7
I can work with a team to analyze the
influence of past design works on present
design challenges.

Advanced
High
Benchmark
De.C
AH.7
I can evaluate my design solution to
determine the effective use of past design
works.

Indicator
De.C
AL.7.1
I can find and compare how choices from a
current design reflect influences of past
design solutions.

Indicator
De.C
AM.7.1
I can work with a team to explain how the
designer's choices on the current design
challenge reflect influences of design
solutions from the past.

Indicator
De.C
AH.7.1
I can assess my design choices and relate
them to past design influences.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify influences from
previous designs in a current design
solution.

x

I can work with others to identify
influences from previous designs in a
current design solution.

x

I can defend my interpretations of
how different styles, periods, and
cultures have influenced my designs.

x

I can explain how specific past
designs are reflected in a current
design.

x

I can work with others to explain how
specific past designs are reflected in a
current design.

x

I can debate my choices made in my
designs that are influenced by
different styles, periods, and cultures.

x

, FDQ«

x

, FDQ«

x

, FDQ«

Page 127

Anchor Standard 8: I can relate design ideas to other arts disciplines, content areas, and careers.
Advanced
Low
Benchmark
De.C
AL.8
I can apply concepts among arts disciplines
and other content areas to design and analyze
how my interests and skills will prepare me
for a career.

Advanced
Mid
Benchmark
De.C
AM.8
I can explain how economic conditions,
cultural values, and location influence design
and the need for design related careers.

Advanced
High
Benchmark
De.C
AH.8
I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a designer.

Indicator
De.C
AL.8.1
I can use concepts found in various arts
disciplines and other content areas in a design
work.
Sample Learning Targets

Indicator
De.C
AM.8.1
I can describe how economic conditions,
cultural values, and geographic locations
affect design and design careers.
Sample Learning Targets

Indicator
De.C
AH.8.1
I can examine the importance of the work of a
designer in issues that relate to a global
society.
Sample Learning Targets

x

I can use the elements and/or
principles of arts disciplines in a
current design work.

x

I can use concepts found in dance in a
current design work.

x

, FDQ«

x

x

I can discuss the relationships
between the designer and other
careers.
I can research processes of other
careers to determine how design
affects it.

x

I can justify community investment in
design.

x

, FDQ«

x

I can defend the impact of design
careers within a society.

x

I can promote the intrinsic value of
design to individuals and society

x

I can find an important design
problem in another country and create
a design solution to help.

x

, FDQ«

Page 128

Design Glossary
Aesthetics Concerned with appearance or the appreciation of beauty.
Artistic Processes The way the brain and the body make art and define the link between art making and the learner.
Aspects A particular part or parts of the design challenge.
Assess To estimate or evaluate the value of information researched.
Beta Testing Using a prototype to receive feedback from a sample target group.
Brainstorm A step in the problem solving process to producing an idea or several ideas. Ex. Popcorn Brainstorming, Passing
Brainstorming, Questioning Brainstorming.
Communication Design Design directed towards making connections between people. Ex. Graphic design, packaging design, web
design, etc.
Craftsmanship A degree or level of skill involved in creating a craft or work of art.
Define (a design challenge) Answering the design challenge questions to provide a clear description of what the design challenge is.
Design An outline, sketch, plan, model, or prototype of a solution to be developed or constructed that considers aesthetic decisions.
See definitions of Object Design, Environmental Design, Communication Design, and Experiential Design.
Design Challenge A design problem or a design issue defined as having the need to be altered, changed, or created in a particular way
to solve.
Design Challenge Questions Basic questions used to gather information concerning a design problem: Who, What, Where, When,
Why, and How. The answers to these questions define the design challenge.
Design Problem A specific design aspect or issue regarded as needing to be dealt with, overcome, or changed.
Design Process A process designed to identify a specific design problem, research the problem, create a solution to the problem, and
present the solution to the problem.
Page 129

Design Solution A means of solving a design problem.
Design Thinking To use one's mind to apply the process of design.
Design Thinking Strategies Methods or procedures used to brainstorm ideas or reason the process of design. Ex: Mind Maps, Concept
Maps, Webbings, Electronic Brainstorms, etc.
Environmental Design Design of surroundings or conditions in which a person, place, or thing interacts. Ex. Interior design,
playground design, community planning, etc.
Experiential Design Design based on personal interactivity or experiences; sometimes referred to as interactive design. Ex. Design
parades, design festivals, design theme parks, etc.
Feedback A reaction or response to a particular design problem or design solution.
Functional Referring to a design having a special activity, purpose, or task.
Interactions A person or a group of persons interacting with a prototype.Investigate To examine, research, or inquire the design
problem or aspects of the design problem in order to create a solution.
Mind Mapping A visual diagram used to organize information and ideas. It starts with a single idea, written or drawn in the center of a
blank page, to which associated words or ideas are added, continuing to associate the words and ideas.
Object Design Design of a material thing often related to industrial and product design. Ex. Design of tools, toys, cars, etc.
Passing Brainstorming A brainstorming technique in which individuals convey ideas one after another building upon the ideas of
others in a group.
Physical Model A three dimensional replication or copy of a prototype
Popcorn Brainstorming A brainstorming technique in which individuals freely state ideas in a group.
Presentation An activity in which an individual or a team shows, describes, or explains a design solution to a group of people.
Prototype A two-dimensional product or three-dimensional model of a design solution. See definitions of physical models, space
models, interactions, and storytelling.
Page 130

Questioning Brainstorming A brainstorming technique in which individuals generate questions in a group that may later be explored.
Research Investigating the design challenge determining the who, what, where, when, why, and how using a variety of research
methods; surveys, observations, interviews, experiments, internet, encyclopedias, newspapers, magazines, etc.
Sample Learning Target A broad lesson learning scenario.
Space Model a 2D or 3D replication or copy within which all things move
Standard Principle that is used as a basis for judgment.
Storytelling The use of words to describe the function or purpose of a prototype
Team A group organized to meet specific goals.
Techniques The use of tools and materials in unique ways that are specific to the designer and the medium.
Webbing Is a brainstorming technique that provides a visual structure or framework for idea development and can assist with
organizing and prioritizing information.

Page 131

References
Alfonso N. (2009, December 2). ABC Nightline - IDEO Shopping Cart [Online Video]. Retrieved from
https://www.youtube.com/watch?v=M66ZU2PCIcM
Lerman, L. &Borstel, J., (2003). /L] /HUPDQ¶V FULWLFDO UHVSRQVH SURFHVV a method of for getting useful feedback on anything you make,
from dance to dessert. Liz Lerman Dance Exchange.
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
Thayer School of Engineering at Dartmouth. (2012, March 28). IDEO workshop part four: prototyping [Online Video]. Retrieved from
https://www.youtube.com/watch?v=Rbjej4A6oRk
Thayer School of Engineering at Dartmouth. (2012, March 28). IDEO workshop part three: brainstorm [Online Video]. Retrieved from
https://www.youtube.com/watch?v=Ocb1bonXWc8
VandeZande, R. (2016). Art and Design Education [PowerPoint slides]. Retrieved from
https://drive.google.com/a/kent.edu/file/d/0B7Etk0esSRy2NEVMQl9sUVM0bFE/view?usp=sharing
VandeZande, R. (2011). Design education supports social responsibility and the economy. Arts Education Policy Review, 112(1), 2634.

Page 132

# Choral Music
## Introduction


In writing the 2017 South Carolina Choral Music Standards, our goal was to bridge the 2010 South Carolina Choral Music Standards
with the 2014 National Core Arts Standards for Music to create a simplified, relevant document for teachers and students to use in the
Choral Music classroom. 7KH SXUSRVH RI WKLV GRFXPHQW DQG WKH ³, FDQ´ ODQJXDJH LV WR HQDEOH WKH WHDFKHU WR EHFRPe the facilitator of
goals for the student using benchmarks to set achievable goals and to self-assess to take ownership of their learning.
Choral students come to us from a variety of musical backgrounds and experiences. A freshman high school choral classroom may
consist of students who perform at novice levels as well as students who perform at advanced levels. Moving from a grade-level based
model to a proficiency-based model allows teachers to meet students at their individual ability level to differentiate learning most
effectively. Many choral teachers are also teachers of general or instrumental music. For simplified planning, we have chosen to
streamline the wording of several standards, benchmarks, and indicators with the other music areas. The sample learning targets are
specific to Choral Music. Our hope is that the 2017 South Carolina Choral Music Standards will not only be a valuable resource for the
teacher as a facilitator, but also for the learner to be actively engaged in his or her educational goals.

Page 134

Choral Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas.
Anchor Standard 1: I can arrange and compose music.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
CM.CR
NL.1
I can notate
simple
rhythmic
patterns
using a
defined
selection of
note values.

Benchmark
CM.CR
NM.1
I can notate
simple
melodic
patterns
using a
defined
selection of
pitches.

Benchmark
CM.CR
NH.1
I can notate
musical ideas
using musical
symbols to
represent
pitch and
rhythm.

Benchmark
CM.CR
IL.1
I can
arrange a
short song
for my voice.

Benchmark
CM.CR
IM.1
I can
arrange a
short song
for two
voices,
using
harmony.

Benchmark
CM.CR
IH.1
I can
arrange a
short song
for an
ensemble,
demonstrati
ng an
understandi
ng of
voicing and
texture.

Benchmark
CM.CR
AL.1
I can describe
how I use
melody,
rhythm, and
harmony to
compose or
arrange a
work for a
specific
purpose.

Benchmark
CM.CR
AM.1
I can
collaborate
with others to
compose or
arrange a
musical work
for a specific
purpose.

Benchmark
CM.CR
AH.1
I can
compose
short, original
musical ideas
and works
using all the
elements of
music for a
specific
purpose.

Page 135

Indicator
CM.CR
NL.1.1
I can
recognize
long and
short sounds
and identify
simple
rhythms
from
notation.

Indicator
CM.CR
NM.1.1
I can create
and
recognize
high and low
sounds to
represent
some
pitches.

Indicator
CM.CR
NH.1.1
I can sing a
variety of
pitches and
rhythms and
label a music
staff with clef
and metric
symbols.

Indicator
CM.CR
IL.1.1
I can create a
simple tune
(monophonic melody)
without
accompanim
ent, within
specified
guidelines.

Indicator
CM.CR
IM.1.1
I can
develop a
simple tune
with
accompanyi
ng parts
(homophonic work).

Indicator
CM.CR
IH.1.1
I can
combine
different
voices to
create
various tone
colors in my
arrangement.

Indicator
CM.CR
AL.1.1
I can arrange
melodic
themes for
specific
purposes,
using
arrangement
and
compositional techniques.

Indicator
CM.CR
AM.1.1
I can sing in
ensembles,
working with
others to
develop ideas
as we
compose or
arrange a
composition.

Indicator
CM.CR
AH.1.1
I can create
musical ideas
and works
using chord
progressions
and
modulations.

Indicator
CM.CR
NL.1.2
I can write
music
rhythms or
sounds,
using
symbols.

Indicator
CM.CR
NM.1.2
I can write
high and low
notes on a
music staff to
represent
pitches.

Indicator
CM.CR
NH.1.2
I can write
note and rest
values on a
music staff.

Indicator
CM.CR
IL.1.2
I can create a
melody using
rhythms that
are
appropriate
for the time
signature.

Indicator
CM.CR
IM.1.2
I can
develop an
original
arrangement of a
traditional
canon or
round.

Indicator
CM.CR
IH.1.2
I can
experiment
with
changes in
tone color,
creating
variety and
contrast
through a
combination
of different
voices.

Indicator
CM.CR
AL.1.2
I can use
compositional
techniques
to compose
works in a
given
musical
form.

Indicator
CM.CR
AM.1.2
I can work
with others to
analyze
arrangements and
original
compositions
for improvements.

Indicator
CM.CR
AH.1.2
I can use
characteristic
forms of
music to
create a
choral
composition
for a specific
purpose.

Page 136

Indicator
CM.CR
NL.1.3
I can
identify
same and
different
rhythm
patterns.

Indicator
CM.CR
NM.1.3
I can identify
same and
different
melodic
patterns.

Indicator
CM.CR
NH.1.3
I can write
beats and
rhythms
within
measures.

Indicator
CM.CR.1
IL.3

Indicator
CM.CR
IM.1.3

Indicator
CM.CR
IH.1.3

Indicator
CM.CR
AL.1.3

Indicator
CM.CR
AM.1.3

Indicator
CM.CR.1
AH.3

I can develop
a melody
using pitches
that are
appropriate
for the
tonality.

I can
develop my
song using I,
IV, and V
chord progressions.

I can
experiment
with nonchord tones
and chord
progressions.

I can
compose
short
compositions
in major and
minor keys.

I can
compose an
original
composition
in Four-Part
Chorale
Style.

I can
compose a
choral
composition
with a variety
of expressive
devices.

Benchmark
CM.CR
IM.2
I can
improvise
simple tonal
patterns
within a
given
tonality.

Benchmark
CM.CR
IH.2
I can
improvise
simple
melodic
phrases.

Benchmark
CM.CR
AL.2
I can perform
a brief
improvisatio
n given a
chord
progression
and meter.

Benchmark
CM.CR
AM.2
I can perform
an
improvisatio
n given a
motive,
chord
progression
and meter.

Benchmark
CM.CR
AH.2
I can perform
an extended
improvisation
with freedom
and
expression
featuring
motivic
development
within a given
tonality,
meter, and
style.

Anchor Standard 2: I can improvise music.
Benchmark
CM.CR
NL.2
I can
imitate
simple
rhythm
patterns
within a
given
meter.

Benchmark
CM.CR
NM.2
I can imitate
simple tonal
patterns
within a
given
tonality.

Benchmark
CM.CR
NH.2
I can imitate
simple
melodic
phrases
given simple
chord
changes.

Benchmark
CM.CR
IL.2
I can
improvise
simple
rhythmic
patterns
within a given
meter.

Page 137

Indicator
CM.CR
NL.2.1
I can
imitate
rhythm
using a
neutral
syllable
(shhh, ba,
etc.)

Indicator
CM.CR
NM.2.1
I can produce
one-phrase
responses
using two to
three pitches
on a neutral
syllable (such
as loo or la).

Indicator
CM.CR
NH.2.1
I can imitate
simple
melodic
phrases
given simple
chord
progressions.

Indicator
CM.CR
IL.2.1
I can
improvise my
own simple
rhythmic
pattern using
a neutral
syllable.

Indicator
CM.CR
NL.2.2

Indicator
CM.CR
NM.2.2

Indicator
CM.CR
NH.2.2

Indicator
CM.CR
IL.2.2

I can
imitate
rhythm
using a taka-di-mi or
a counting
system.

I can echo
simple tonal
patterns
using solfege.

I can
embellish a
given
melodic
phrase that
corresponds
with a simple
chord
progression.

I can
improvise my
own simple
rhythm
patterns using
ta-ka-di-mi or
a counting
system.

Indicator
CM.CR
IM.2.1
I can
improvise
my own
simple tonal
patterns on
a neutral
syllable.

Indicator
CM.CR
IM.2.2
I can
improvise
my own
simple tonal
patterns
using
solfege.

Indicator
CM.CR
IH.2.1
I can
identify
chord
changes to
improvise a
short
melody.

Indicator
CM.CR
AL.2.1
I can
improvise a
short passage
using only a
chord
progression.

Indicator
CM.CR
AM.2.1
I can perform
an improvesation on a
given
motive.

Indicator
CM.CR
IH.2.2

Indicator
CM.CR
AL.2.2

Indicator
CM.CR
AM.2.2

I can
improvise
simple
melodic
phrases that
correspond
with chord
progression
s in an
unfamiliar
song.

I can
improvise a
short passage
in an
established
meter.

I can
improvise an
extended
passage
using only a
chord
progression.

Indicator
CM.CR
AM.2.1
I can
improvise an
extended
unaccompani
ed solo within
a given
tonality,
meter, and
style.
Indicator
CM.CR
AH.2.2
I can
improvise
freely within a
given
tonality,
meter, and
style,
responding to
aural cues
from other
members of an
ensemble.

Page 138

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.
Benchmark
CM.P
NL.3

Benchmark
CM.P
NM.3

Benchmark
CM.P
NH.3

Benchmark
CM.P
IL.3

Benchmark
CM.P
IM.3

Benchmark
CM.P
IH.3

Benchmark
CM.P
AL.3

Benchmark
CM.P
AM.3

Benchmark
CM.P
AH.3

I can
produce a
steady, free
tone on a
comfortable
pitch.

I can produce
a steady, free
tone within a
limited
range.

I can produce
a steady, free
tone while
singing in
tune.

I can produce
a centered
tone in a
comfortable
tessitura.

I can
produce a
centered
tone in some
tessituras
specific to
my vocal
range.

I can
produce a
centered
tone in most
tessituras
specific to
my vocal
range.

I can produce
a welldeveloped
tone in all
tessituras
specific to
my vocal
range.

Indicator
CM.P
NL.3.1
I can sing
some simple
patterns
alone and
with others.

Indicator
CM.P
NM.3.1
I can identify
and sing in
my head and
chest voices.

Indicator
CM.P NH.3.1

Indicator
CM.P
IL.3.1
I can sing with
a resonant,
centered, and
free tone in
harmony.

Indicator
CM.PIM.3.1

Indicator
CM.P
IM.3.1
I can sing
2-3 part
songs with
centered
tone
quality, in
tune, while
demonstrati
ng dynamic
changes.

Indicator
CM.PAL.3.1

I can
consistently
produce a
welldeveloped,
vibrant tone
across the
entire range
of my voice.
Indicator
CM.PAM.3.1

I can sing
with a welldeveloped
tone, some 34 part songs,
demonstratin
g balance
and
intonation,
by adjusting
my voice to
FRQGXFWRU¶V
cues.

I can sing
alone and
within a 3-4
part
ensemble,
singing with
welldeveloped
tone quality
while
maintaining
balance and
intonation.

I can adjust
tone
color/timbre
in response to
stylistic
demands and
the musical
needs of an
ensemble.
Indicator
CM.P
AH.3.1
I can
manipulate the
tone quality
of my voice to
reflect the
stylistic
demands of a
piece of
music.

I can blend
my voice
with others
singing in
tune in my
head voice.

I can sing
my assigned
part in tune
with
appropriate
tone
quality,
resonance
and vocal
timbre.

Page 139

Indicator
CM.P
NL.3.2
I can
demonstrate
correct
singing
posture.

Indicator
CM.P
NM.3.2
I can sing
songs based
on the
pentatonic
scale.

Indicator
CM.P
NH.3.2
I can sing in
tune with
breath
support.

Indicator
CM.P
IL.3.2
I can sing in
tune my
assigned part
with clear
tone quality,
using breath
control and
correct
posture.

Indicator
CM.P
IM.3.2
I can sing
with a
centered
tone and a
steady
tempo.

Indicator
CM.P
IH.3.2

Indicator
CM.P
AL.3.2

Indicator
CM.P
AM.3.2

Indicator
CM.P
AH.3.2

I can sing
2-3 part
songs with
centered
tone
quality, in
tune, while
demonstrat
ing
articulation
changes.

I can sing
with a welldeveloped
tone,
incorporating
all musical
symbols,
tempo and
expressive
indications.

I can sing
with welldeveloped
tone quality
and increased
vocal
technique.

I can sing in a
variety of
languages
with welldeveloped
tone quality,
making
needed
adjustments in
vocal
technique.

Benchmark
CM.P
AM.4
I can sing
with
increased
fluency and
expression in
small and
large
ensembles a
varied
repertoire/
genre of
choral music.

Benchmark
CM.P
AH.4
I can sing with
increased
fluency and
expression
from memory
varied
repertoire/
genres of
choral music.

Anchor Standard 4: I can perform with technical accuracy and expression.
Benchmark
CM.P
NL.4
I can speak,
chant, sing,
and move to
demonstrate
awareness
of beat.

Benchmark
CM.P
NM.4
I can speak,
chant, sing
and move to
demonstrate
awareness of
beat, tempo,
dynamics,
and melodic
direction.

Benchmark
CM.P
NH.4
I can sing
expressively,
alone or in
groups,
matching
dynamic
levels and
responding to
the cues of a
conductor.

Benchmark
CM.P
IL.4
I can sing
expressively
with
appropriate
dynamics and
phrasing.

Benchmark
CM.P
IM.4
I can sing
expressively
with
appropriate
dynamics,
phrasing,
and
interpretation.

Benchmark
CM.P
IH.4
I can sing
while
interpreting
my
FRQGXFWRU¶V
cues in
order to
perform
with
expression
and
technical
accuracy.

Benchmark
CM.P
AL.4
I can sing
with
increased
fluency and
expression a
varied
repertoire/
genre of
choral music.

Page 140

Indicator
CM.P
NL.4.1
I can speak,
chant to the
beat.

Indicator
CM.P
NM.4.1
I can
demonstrate
different
tempo
markings
when singing
and moving
to the beat.

Indicator
CM.P
NH.4.1
I can
demonstrate
dynamic
levels in
response to a
conductor.

Indicator
CM.P
IL.4.1
I can sing,
observing a
variety of
dynamic
markings in
songs.

Indicator
CM.P
IM.4.1
I can
interpret a
FRQGXFWRU¶V
dynamic
and
phrasing
cues when
singing.

Indicator
CM.P
IH.4.1
I can
interpret a
FRQGXFWRU¶V
gesture with
rhythmic
and melodic
precision.

Indicator
CM.P
AL.4.1
I can sing
with
rhythmic
and melodic
precision
music from
diverse
genres.

Indicator
CM.P
AM.4.1
I can
interpret a
FRQGXFWRU¶V
gesture in a
varied
repertoire of
music.

Indicator
CM.P
AH.4.1
I can enhance
the expressive
quality of my
performance
through
singing from
memory.

Indicator
CM.P
NL.4.2
I can sing
and move to
the beat.

Indicator
CM.P
NM.4.2
I can
demonstrate
dynamic
levels when
singing and
moving to the
beat.

Indicator
CM.P
NH.4.2
I can respond
to a
FRQGXFWRU¶V
gradual
dynamic
cues when
singing.

Indicator
CM.P
IL.4.2
I can sing,
observing
phrasing
suggestions
and markings
in music.

Indicator
CM.P
IM.4.2
I can sing,
observing
phrasing
markings
and
breathing
appropriately alone and
in groups.

Indicator
CM.P
IH.4.2
I can
interpret a
FRQGXFWRU¶V
dynamic,
articulation
, and
phrasing
cues.

Indicator
CM.P
AL.4.2
I can sing
observing
dynamics,
articulation,
and
phrasing, in
the style of
the music.

Indicator
CM.P
AM.4.2
I can
interpret a
FRQGXFWRU¶V
gestures
appropriate
to the genre.

Indicator
CM.P
AH.4.2
I can sing a
cappella
vocal
selections
from memory.

Page 141

Anchor Standard 5: I can perform using music notation.
Benchmark
CM.P
NL.5
I can
identify
music
notation
symbols
representing
simple
familiar
tonal and
rhythm
patterns
and tunes.

Benchmark
CM.P
NM.5
I can read
and perform
tonal and
rhythmic
patterns
using music
notation.

Benchmark
CM.P
NH.5
I can read
and perform
simple
unfamiliar
and familiar
songs using
music
notation.

Benchmark
CM.P
IL.5
I can identify
music
notation,
symbols
representing
an expanded
set of tonal,
rhythmic,
technical, and
expressive
ideas.

Benchmark
CM.P
IM.5
I can
perform at
sight simple
unfamiliar
musical
works.

Benchmark
CM.P
IH.5
I can use a
system to
fluently
sight-read
moderately
complex
melodies in
treble and
bass clefs.

Benchmark
CM.P
AL.5
I can perform
at sight
complex
unfamiliar
musical
works with
accuracy.

Benchmark
CM.P
AM.5
I can perform
at sight
complex
unfamiliar
musical
works with
accuracy and
appropriate
expression/
interpretation.

Benchmark
CM.P
AH.5
I can perform
at sight
complex
unfamiliar
musical works
with accuracy,
appropriate
expression/
interpretation and
fluency.

Page 142

Indicator
CM.P
NL.5.1
I can use
nontraditional
notation to
identify
pitches in a
clef.

Indicator
CM.P
NM.5.1
I can sing
tonal
patterns
using a sightreading
system.

Indicator
CM.P
NH.5.1
I can perform
simple
unfamiliar
rhythm
patterns
using music
notation.

Indicator
CM.P
IL.5.1
I can identify
sharps, flats,
naturals, and
simple key
signatures.

Indicator
CM.P
IM.5.1
I can
perform at
sight simple
unfamiliar
musical
works with
accurate
pitches.

Indicator
CM.P
NL.5.2
I can
identify
note values.

Indicator
CM.P
NM.5.2
I can
identify basic
time
signatures.

Indicator
CM.P
NH.5.2
I can perform
simple
unfamiliar
tonal
patterns
using music
notation.

Indicator
CM.P
IL.5.2
I can sightread stepwise
tonic (do, re,
mi, fa, so)
patterns and
simple meter
based (2/4,
3/4, 4/4)
rhythmic
patterns.

Indicator
CM.P
IM.5.2
I can sight
read using
reading
systems
such as taka-di-mi,
Gordon,
count
singing, and
neutral
syllables to
unfamiliar
melodies
with tonic
triad skips.

Indicator
CM.P
IH.5.1
I can
perform at
sight
moderately
complex
unfamiliar
musical
works with
accurate
pitches.
Indicator
CM.P
IH.5.2
I can notate
intermediate
note values
and time
signatures.

Indicator
CM.P
AL.5.1
I can perform
at sight
complex
unfamiliar
musical
works with
accurate
pitches

Indicator
CM.P
AM.5.1
I can perform
at sight
complex
unfamiliar
musical
works with
correction
articulation.

Indicator
CM.P
AH.5.1
I can perform
at sight
complex
unfamiliar
works with
fluency.

Indicator
CM.P
AL.5.2
I can sight
read using
multiple
reading
systems (taka-di-mi and
Gordon,
count
singing,
neutral
syllables) in
my music.

Indicator
CM.P
AM.5.2
I can
identify
advanced
note values
and time
signatures
that represent
smaller beat
subdivisions
in my music.

Indicator
CM.P
AH.5.2
I can notate
advanced
values and
time
signatures
that represent
syncopation
and smaller
beat
subdivisions
in my music.

Page 143

Indicator
CM.P
NL.5.3
I can
identify
simple
familiar
rhythm
patterns
with
corresponding
notation.

Indicator
CM.P
NM.5.3
I can sing
using eighth,
quarter, half
and whole
notes and
rests.

Indicator
CM.P
NL.5.4

Indicator
CM.P
NM.5.4

I can
identify
simple
familiar
tonal
patterns
with
corresponding
notation.

I can sing a
variety of
tempos in
music.

Indicator
CM.PNH.5.3
I can sing in
unison and
simple 2part music.

Indicator
CM.P NH.5.4

I can sing
simple
patterns in
multiple
tonalities.

Indicator
CM.P
IL.5.3
I can identify
advanced note
values and
time
signatures
that represent
syncopation
and smaller
beat
subdivisions
in my music.

Indicator
CM.P
IM.5.3
I can apply
basic tempo
markings in
my music.

Indicator
CM.P
IH.5.3
I can apply
intermediate
tempo
markings in
my music.

Indicator
CM.P
IL.5.4

Indicator
CM.P
IM.5.4
I can apply
expressive
markings in
my music.

I can identify
expressive
markings in
my music.

Indicator
CM.P AL.5.3

Indicator
CM.PAM.5.3

Indicator
CM P
AH.5.3
I can justify
the use of
advanced
tempo
markings in
my music.

I can identify
the use of
advanced
tempo
markings in
my music.

I can analyze
the use of
advanced
tempo
markings in
my music.

Indicator
CM.P
IH.5.4

Indicator
CM.P
AL.5.4

Indicator
CM.P
AM.5.4

Indicator
CM.P
AH.5.4

I can apply
advanced
expressive
markings in
my music.

I can identify
technical,
expressive,
and formal
markings in
my music.

I can analyze
the technical,
expressive,
and formal
markings in
my music.

I can justify
the technical,
expressive,
and formal
markings in
my music.

Page 144

Artistic Processes: Responding- I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Benchmark
CM.R
NL.6
I can
identify the
elements of
music,
instrument
families,
and voice
types.

Benchmark
CM.R
NM.6
I can identify
and apply
basic music
symbols and
terminology.

Benchmark
CM.R
NH.6
I can identify
patterns in
music,
recognize
basic musical
forms, and
identify
criteria of a
musical
performance.

Benchmark
CM.R
IL.6
I can describe
how the
elements of
music are
used to
communicate
ideas and
evoke
emotional
responses in
myself and
others.

Benchmark
CM.R
IM.6
I can
identify and
explain how
the
elements of
music are
used in a
variety of
genres to
determine
my personal
preferences.

Benchmark
CM.R
IH.6
I can
evaluate a
performance
and offer
constructive
suggestions
for
improvemen
t using
provided
criteria.

Benchmark
CM.R
AL.6
I can analyze
a
composition
or
performance
and offer
constructive
suggestions
for
improvement
using
provided
criteria.

Benchmark
CM.R
AM.6
I can analyze
and critique
compositions
and
performances
from a
variety of
genres,
cultures and
time periods
using
personally
developed
criteria.

Benchmark
CM.R
AH.6
I can justify
my criteria for
evaluating
music works
and
performances
based on
personal and
collaborative
research.

Page 145

Indicator
CM.R
NL.6.1

I can
identify
rhythm,
dynamics,
pitch,
harmony,
tone color,
texture,
and form.

Indicator
CM.R
NL.6.2
I can
identify
instrument
families in
the
symphony
orchestra.

Indicator
CM.R
NM.6.1
I can
identify basic
music
symbols and
terms in
written
music.

Indicator
CM.R
NH.6.1
I can
recognize
patterns in
the music
that I hear.

Indicator
CM.R
IL.6.1

Indicator
CM.R
IM.6.1

Indicator
CM.R
IH.6.1

Indicator
CM.R
AL.6.1

I can explain
how music
elements are
used to
communicate
ideas.

I can
identify how
the melody,
harmony,
rhythm,
timbre,
texture,
form, and
expressive
elements
are different
in varying
genres of
music.
Indicator
CM.R
IM.6.2

I can
identify
advanced
musical
symbols,
key
signatures,
and
complex
meter.

I can identify
forms used in
varying
cultures and
historical
periods.

Indicator
CM.R
IH.6.2

Indicator
CM.R
AL.6.2

Indicator
CM.R
AM.6.2

Indicator
CM.R
AH.6.2

I can
describe
common
elements
found in
various
genres of
music.

I can
explain why
advanced
musical
symbols,
key
signatures,
and complex
meter are
used in
music.

I can describe
stylistic
qualities of
music from
different
cultures and
time periods.

I can identify
key
signature
changes and
modulations
in relation to
form.

I can justify
the
performance
decisions in a
variety of
musical
works.

Indicator
CM.R
NM.6.2

Indicator
CM.R
NH.6.2

Indicator
CM.R
IL.6.2

I can apply
my
knowledge of
musical
symbols and
terminology
to a
performance.

I can identify
common
forms such
as call and
response,
verse and
refrain,
ABA.

I can describe
how the
elements of
music affect
the mood of a
song.

Indicator
CM.R
AM.6.1
I can
describe
characteristics of a
variety of
musical
forms.

Indicator
CM.R
AH.6.1
I can justify
my
interpretation of a
musical work
based on the
elements of
music.

Page 146

Indicator
CM.R
NL.6.3

Indicator
CM.R
NM.6.3

Indicator
CM.R
NH.6.3

I can
identify
different
voice types.

I can identify
characteristic
s of a
performance
that I
like/dislike.

I can identify
criteria for a
music
performance.

Indicator
CM.R
IL.6.3
I can use the
elements of
music to
describe my
emotional
response to a
music
performance.

Indicator
CM.R
IM.6.3

Indicator
CM.R
IH.6.3

Indicator
CM.R
AL.6.3

Indicator
CM.R
AM.6.3

Indicator
CM.R
AH.6.3

I can use the
elements of
music to
describe
why I like
particular
genres.

I can use the
elements of
music to
offer
suggestions
for improvement.

I can describe
stylistic
qualities of
music from
different
historical
periods and
cultures and
offer
suggestions
for
improvement
of my
performance.

I can describe
stylistic
qualities of
music from
different
historical
periods and
how it
applies to my
instrument.

I can justify
my evaluation
of musical
works from
different
historical
periods and
cultures based
on my
personal and
collaborative
research.

Benchmark
CM.R
IM.7
I can
describe my
evaluation
of a
performance
to others.

Benchmark
CM.
IH.7
I can
describe the
quality of
my
performance
s and my
compositions.

Benchmark
CM.R
AL.7
I can analyze
performances
and
compositions
, offering
suggestions
for
improvement
using
provided
criteria.

Benchmark
CM.R
AM.7
I can analyze
and critique
compositions
and
performances
using
personallydeveloped
criteria.

Benchmark
CM.R
AH.7
I can justify
my criteria for
evaluating
musical works
and
performances
based on
personal and
collaborative
research.

Anchor Standard 7: I can evaluate music.
Benchmark
CM.R
NL.7
I can show
my personal
interest in
musical
performances of
others.

Benchmark
CM.R
NM.7
I can describe
my personal
interest in
music
performances
using music
terminology.

Benchmark
CM.R
NH.7
I can list
some criteria
to describe
my interest in
music
performances
using music
terminology.

Benchmark
CM.R
IL.7
I can describe
the quality of
music
performances
using provided
criteria.

Page 147

Indicator
CM.R
NL.7.1

Indicator
CM.R
NM.7.1

Indicator
CM.R
NH.7.1

Indicator
CM.R
IL.7.1

I can
recognize
some
elements of
music, such
as loud/soft,
fast/slow.

I can define
basic music
terminology
using my
own words.

I can describe I can define all
some of the
the elements
elements of
of music.
music.

Indicator
CM.P
NL.7.2
I can discuss
my
preference
for a piece
of music.

Indicator
CM.P
NM.7.2
I can identify
characteristic
s of a
performance
that I
like/dislike.

Indicator
CM.P
NH.7.2
I can identify
some criteria
for music
performance.

Indicator
CM.P
IL.7.2
I can describe
the
characteristics
of a quality
performance
using musical
terms.

Indicator
CM.R
IM.7.1

Indicator
CM.R
IH.7.1

Indicator
CM.R
AL.7.1

Indicator
CM.R
AM.7.1

Indicator
CM.R
AH.7.1

I can
identify
specific
criteria I use
when I
critique
RWKHUV¶
performances.
Indicator
CM.P
IL.7.2
I can use the
elements of
music to
evaluate a
composition.

I can
compare my
performance
to the
performance
of others.

I can
communicate
feedback for
personal
performances
and compositions.

I can analyze I can explain
personal
criteria used
compositions for evaluation.
and provide
criteria for
improvement.

Indicator
CM.P
IH.7.2
I can use the
elements of
music to
evaluate my
performance
or the
performance
of others.

Indicator
CM.P
AL.7.2
I can present
my
evaluation of
a formal or
informal
performance.

Indicator
CM.P
AM.7.2
I can analyze
performances
and provide
criteria for
improvement.

Indicator
CM.P
AH.7.2
I can justify
artistic
decisions used
in
compositions
and
performances.

Page 148

Artistic Processes: Connecting- I can connect musical ideas and works to personal experience, careers,
culture, history, and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Benchmark
CM.C
NL.8
I can
recognize
musical
selections
from some
cultures and
time
periods.

Benchmark
CM.C
NM.8
I can identify
musical
selections
from a
specific
culture and a
historical
time period.

Benchmark
CM.C
NH.8
I can identify
musical
selections
from multiple
cultures
and/or
historical
time periods.

Benchmark
CM.C
IL.8
I can examine
relationships
among
musical
selections
from multiple
cultures and/or
historical time
periods.

Indicator
CM.C
NL.8.1
I can
recognize
that all
cultures
perform
music.

Indicator
CM.C
NM.8.1
I can
recognize
similar
elements of
music in a
specific
culture.

Indicator
CM.P
NH.8.1
I can identify
similar
elements of
music in
different
cultures.

Indicator
CM.C
IL.8.1
I can examine
music from
multiple
cultures and
time periods.

Benchmark
CM.C
IM.8
I can
research the
role of
music
within a
specific
culture or
historical
time period
and present
what I
discovered.
Indicator
CM.C
IM.8.1
I can
research a
specific
culture/time
period and
perform a
song from
that
culture/time
period.

Benchmark
CM.C
IH.8
I can modify
a musical
work using
characteristi
cs from a
culture or
time period.

Benchmark
CM.C
AL.8
I can
examine
contemporary
musical
works to
determine the
influence of
historical and
cultural
traditions.

Benchmark
CM.C
AM.8
I can analyze
a diverse
repertoire of
music from a
cultural or
historical
time period.

Benchmark
CM.C
AH.8
I can examine
and perform
music based
on historical
and cultural
contributions.

Indicator
CM.C
IH.8.1
I can change
a musical
work using
the
elements of
music from
a culture or
time period.

Indicator
CM.C
AL.8.1
I can explain
specific
cultural and
historical
traditions and
infuse these
ideas into my
music.

Indicator
CM.C
AM.8.1
I can select
musical
elements in
contemporar
y music that
reflect
cultural and
historical
influences.

Indicator
CM.C
AH.8.1
I can use
historical and
cultural
contributions
to justify my
musical
choices.

Page 149

Anchor Standard 9: I can relate music to other arts disciplines, content areas and career path choices.
Benchmark
CM.C
NL.9
I can
explore
choral music
concepts
among arts
disciplines
other
content
areas and
related
careers.

Benchmark
CM.C
NM.9
I can
recognize
choral music
concepts
among arts
disciplines,
other content
areas, and
related
careers.

Benchmark
CM.C
NH.9
I can apply
choral music
concepts to
arts
disciplines,
other content
areas, and
related
careers.

Benchmark
CM.C
IL.9
I can explore a
range of skills
shared among
arts
disciplines,
other content
areas and how
they can be
applied to a
career in
music.

Benchmark
CM.C
IM.9
I can
recognize
specific
skills shared
among arts
disciplines,
other
content
areas and
how they
can be
applied to a
career in
music.

Indicator
CM.C
NL.9.1

Indicator
CM.C
NM.9.1

Indicator
CM.C
NH.9.1

Indicator
CM.C
IL.9.1

Indicator
CM.C
IM.9.1

I can
identify the
relationship
between
music and
another
subject in
my school.

I can
demonstrate
a relationship
between
music and
another
subject in my
school.

I can
demonstrate
and describe
the
relationship
between
music and a
concept from
another
subject in my
school.

I can apply
music
concepts to
other arts
disciplines and
content areas.

I can
examine the
relationship
between
music and
specific
content from
another arts
discipline
and content
area.

Benchmark
CM.C
IH.9
I can
analyze the
tools,
concepts,
and
materials
used among
arts
disciplines,
other
content
areas and
how they
are used in
music
careers.
Indicator
CM.C
IH.9.1

Benchmark
CM.C
AL.9
I can apply
concepts
among arts
disciplines
and other
content areas
to choral
music and
analyze how
my interests
and skills
will prepare
me for a
career.

Benchmark
CM.C
AM.9
I can explain
how
economic
conditions,
cultural
values and
location
influence
music and the
need for
music related
careers.

Benchmark
CM.C
AH.9
I can research
societal
political and
cultural issues
as they relate
to other arts
and content
areas and
apply to my
role as a
musician.

Indicator
CM.C
AL.9.1

Indicator
CM.C
AM.9.1

Indicator
CM.C
AH.9.1

I can apply
concepts
from other
arts
disciplines
and content
areas to my
music.

I can explain
ideas from
other arts
disciplines
and content
areas through
music.

I can explain
how my
artistic
choices are
influenced by
cultural and
social values.

I can analyze
complex ideas
that influence
my artistic
perspective
and creative
work.

Page 150

Indicator
CM.C
NL.9.2

Indicator
CM.C
NM.9.2

Indicator
CM.C
NH.9.2

I can
identify and
discuss
examples of
musicians in
my
community.

I can identify
life skills
necessary for
a music
career.

I can identify
specific
careers in
music.

Indicator
CM.C
IL.9.2
I can
demonstrate
and describe
the skills
needed for
careers in
music.

Indicator
CM.C
IM.9.2
I can
examine the
educational
requirement
s needed for
a variety of
careers in
music.

Indicator
CM.C
IH.9.2
I can
research
skills
needed for
various
music
careers.

Indicator
CM.C
AL.9.2

Indicator
CM.C
AM.9.2

Indicator
CM.C
AH.9.2

I can describe
traditional
and emerging
careers in
music.

I can pursue
opportunities
that will lead
me to a
career in
music.

I can research
my personal
career choices
in the arts.

Page 151

Novice Choral Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new ideas.
Anchor Standard 1: I can arrange and compose music.
Novice
Low
Benchmark
CM.CR
NL.1
I can notate simple rhythmic patterns using
a defined selection of note values
Indicator
CM.CR
NL.1.1
I can recognize long and short sounds and
identify simple rhythms from notation.

Novice
Mid
Benchmark
CM.CR
NM.1
I can notate simple melodic patterns using a
defined selection of pitches.
Indicator
CM.CR
NM.1.1
I can create and recognize high and low
sounds to represent some pitches.

Sample Learning Targets

Sample Learning Targets

x

I can echo rhythmic patterns on a
neutral syllable.

x

I can echo tonal patterns on a neutral
syllable.

x

I can echo rhythmic syllables on taka-di-mi or other sight-reading
system.

x

I can echo tonal patterns on a
Kodaly or solfege syllable.

x

I can...

x

Novice
High
Benchmark
CM.CR
NH.1
I can notate musical ideas using musical
symbols to represent pitch and rhythm.
Indicator
CM.CR
NH.1.1
I can sing a variety of pitches and rhythms
and label a music staff with clef and metric
symbols.
Sample Learning Targets
x

I can identify a treble and bass clef.

x

I can identify simple time signatures
(2/4, 3/4, 4/4)

x

I can identify compound time
signatures (6/8, 9/8,12/8)

x

I FDQ«

I can...

Page 152

Indicator
CM.CR
NL.1.2
I can write music rhythms or sounds, using
symbols.
Sample Learning Targets

Indicator
CM.CR
NM.1.2
I can write high and low notes on a music
staff to represent pitches.
Sample Learning Targets

I can notate quarter notes/rests and
eighth notes/rests using nontraditional notation.

x

I can notate line and space notes.

x

I can notate using ledger lines.

x

I can notate quarter notes/rests and
eighth notes/rests using traditional
notation.

x

I can...

x

I can...

x

Indicator
CM.CR
NL.1.3
I can identify same and different rhythm
patterns.
Sample Learning Targets
x

I can use symbols to notate four beat
rhythm patterns.

x

I can recognize the difference
between long and short sounds.

x

, FDQ«

Indicator
CM.CR
NH.1.2
I can write note and rest values on a music
staff.
Sample Learning Targets
x

I can notate quarter and half, notes
on a music staff with stems going the
correct direction.

x

I can notate eighth notes on a music
staff with stems and flags going the
correct direction.

x

I can...

Indicator
CM.CR
NM.1.3
I can identify same and different melodic
patterns
Sample Learning Targets

Indicator
CM.CR
NH.1.3
I can write beats and rhythms within
measures.
Sample Learning Targets

x

I can identify skips and leaps.

x

I can notate a four-measure rhythm
within a simple time signature.

x

I can identify repeated patterns and
pitches.

x

I can create a four-measure
composition with music notation
software.

x

I FDQ«

x

I can trace a melodic line.

x

I can...

Page 153

Anchor Standard 2: I can improvise music.
Novice
Low
Benchmark
CM.CR
NL.2
I can imitate simple rhythm patterns within
a given meter.
Indicator
CM.CR
NL.2.1
I can imitate rhythm using neutral syllables
(shhh, ba, etc.).
Sample Learning Targets

Novice
Mid
Benchmark
CM.CR
NM.2
I can imitate simple tonal patterns within a
given tonality.
Indicator
CM.CR
NM.2.1
I can produce one-phrase responses using two
to three pitches on a neutral syllable (such as
loo or la).
Sample Learning Targets

x

I can echo four-beat rhythm patterns
on a neutral syllable while patsching
a steady beat.

x

I can sing a one-phrase response
using two-three pitches with major
tonality.

x

I can clap a one-phrase response
using quarter notes and eighth notes.

x

I can sing a one-phrase response
using two to three pitches with a
minor tonality.

x

I can...
x

Novice
High
Benchmark
CM.CR
NH.2
I can imitate simple melodic phrases given
simple chord changes.
Indicator
CM.CR
NH.2.1
I can imitate simple melodic phrases given
simple chord progressions.
Sample Learning Targets
x

I can identify by ear the tonic major
triad in a familiar song.

x

I can identify by ear the tonic minor
triad in a familiar song.

x

I can...

I can...

Page 154

Indicator
CM.CR
NL.2.2
I can imitate rhythm using a ta-ka-di-mi or
a counting system.

Indicator
CM.CR
NM.2.2
I can echo simple tonal patterns using
solfege.

Sample Learning Targets

Sample Learning Targets

x

I can chant four-beat rhythm
patterns on ta-ka-di-mi or counting
system while patsching a steady beat.

x

I can clap a four-beat rhythm pattern.

x

I can...

x

I can echo sing a tonic triad major
tonal pattern on solfege.

x

I can echo sing a tonic triad minor
tonal pattern on solfege.

x

I can echo sing tonic-dominant
major tonal patterns on solfege.

x

I can echo sing pentatonic tonal
patterns on solfege.

x

, FDQ«

Indicator
CM.CR
NH.2.2
I can embellish a given melodic phrase that
corresponds with a simple chord
progression.
Sample Learning Targets
x

I can use rhythmic syllables to
embellish a familiar melody in simple
meter.

x

I can use passing tones to embellish a
familiar melody over a simple chord
progression.

x

I can use a music loop app to
improvise a basic rhythmic pattern
over a generated pattern.

x

, FDQ«

Page 155

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.
Novice
Low
Benchmark
CM.P
NL.3
I can produce a steady, free tone on a
comfortable pitch.
Indicator
CM.P
NL.3.1
I can sing some simple patterns alone and
with others.
Sample Learning Targets

Novice
Mid
Benchmark
CM.P
NM.3
I can produce a steady, free tone within a
limited range.
Indicator
CM.P
NM.3.1
I can identify and sing in my head and chest
voices.
Sample Learning Targets

x

I can echo sing some sol-mi patterns.

x

I can sing sirens, sighs, lip trills.

x

I can sing the response in a call and
response song.

x

I can sing a major scale.

x
x

I can match pitch when I sing a
simple song.

I can sing vocal warm-ups moving
by half-steps.

I can sing through a simple phrase without
taking a breath.
x I can sing on pitch high/low.
x

x

I can identify head and chest voice
by listening to performance examples.

x

I can...

Novice
High
Benchmark
CM.P
NH.3
I can produce a steady, free tone while singing
in tune.
Indicator
CM.P NH.3.1
I can blend my voice with others singing in
tune in my head voice.
Sample Learning Targets
x

I can produce a light, clear tone while
singing different dynamic levels. (Ex.
Not shouting when you sing forte).

x

I can sing with a lifted soft palate.

x

, FDQ«

, FDQ«

Page 156

Indicator
CM.P
NL.3.2
I can demonstrate correct singing posture.
Sample Learning Targets
x

x

x

I can stand with knees slightly bent,
feet shoulder width apart, my ribcage
lifted, relaxed shoulders, and chin
parallel with the floor.
I can sit with both feet on the floor,
on the edge of my chair, my ribcage
lifted, relaxed shoulders, and chin
parallel with the floor.

Indicator
CM.P
NM.3.2
I can sing songs based on the pentatonic
scale.
Sample Learning Targets
x

I can sing in unison a folk song based
on the pentatonic scale.

x

I can sing vocal warm-ups based on
the pentatonic scale.

x

I can...

Indicator
CM.P
NH.3.2
I can sing in tune with breath support.
Sample Learning Targets
x

I can sing with soft palate lifted.

x

I can take a low diaphragmatic
breath.

x

, FDQ«

, FDQ«

Anchor Standard 4: I can perform with technical accuracy and expression.
Novice
Low
Benchmark
CM.P
NL.4
I can speak, chant, sing and move to
demonstrate awareness of beat.

Novice
Mid
Benchmark
CM.P
NM.4
I can speak, chant, sing, and move to
demonstrate awareness of beat, tempo,
dynamics, and melodic direction.

Novice
High
Benchmark
CM.P
NH.4
I can sing expressively, alone or in groups,
matching dynamic levels and responding to
the cues of a conductor.

Page 157

Indicator
CM.P
NL.4.1
I can speak, chant to the beat.
Sample Learning Targets
x

I can play chanting games.

x

I can echo chant patterns.

x

I can patsch the beat while I chant.

x

I can...

Indicator
CM.P
NL.4.2
I can sing and move to the beat.
Sample Learning Targets
x

I can play traditional singing games.

x

I can skip, hop, and jump to the beat.

x

I can...

Indicator
CM.P
NM.4.1
I can demonstrate different tempo markings
when singing and moving to the beat.
Sample Learning Targets

Indicator
CM.P
NH.4.1
I can demonstrate dynamic levels in response
to a conductor.
Sample Learning Targets

x

I can demonstrate adagio in a chant
while walking to the beat.

x

I can sing forte in response to a
FRQGXFWRU¶V JHVWXUH

x

I can demonstrate allegro in singing
while walking to the beat.

x

I can sing piano in a large ensemble
LQ UHVSRQVH WR D FRQGXFWRU¶V JHVWXUH

x

I can...

x

I can describe dynamics by symbol
located in my music score.

x

I can...

Indicator
CM.P
NM.4.2
I can demonstrate dynamic levels when
singing and moving to the beat.
Sample Learning Targets

Indicator
CM.P
NH.4.2
, FDQ UHVSRQG WR D FRQGXFWRU¶V JUDGXDO
dynamic cues when singing.
Sample Learning Targets

x

I can sing forte while patsching the
beat.

x

, FDQ UHVSRQG WR P\ FRQGXFWRU¶V cues
by singing a crescendo.

x

I can sing a crescendo while
patsching the beat.

x

, FDQ UHVSRQG WR P\ FRQGXFWRU¶V FXUV
by singing a decrescendo.

x

, FDQ«

x

I can...

Page 158

Anchor Standard 5: I can perform using music notation.
Novice
Low
Benchmark
CM.P
NL.5
I can identify music notation symbols
representing simple familiar tonal and
rhythm patterns and tunes.
Indicator
CM.P
NL.5.1
I can use non-traditional notation to identify
pitches in a clef.
Sample Learning Targets

Novice
Mid
Benchmark
CM.P
NM.5
I can read and perform tonal and rhythmic
patterns using music notation.

Novice
High
Benchmark
CM.P
NH.5
I can read and perform simple unfamiliar and
familiar songs using music notation.

Indicator
CM.P
NM.5.1
I can sing tonal patterns using a sightreading system.
Sample Learning Targets

Indicator
CM.P
NH.5.1
I can perform simple unfamiliar rhythm
patterns using music notation.
Sample Learning Targets

x

I can use pictures/objects to identify
high and low pitches.

x

I can sing music examples using
solfège and Kodaly.

x

I can use my hands to identify so and
mi.

x

I can sing so, mi, and la in a music
example.

I can...

x

I can sing so, mi, and do in a music
example.

x

I can...

x

x

I can chant an unfamiliar four-beat
rhythm pattern presented in
notation, in a familiar meter.

x

I can clap an unfamiliar four-beat
rhythm pattern presented in notation,
in a familiar meter.

x

I can...

Page 159

Indicator
CM.P
NL.5.2
I can identify note values.

Indicator
CM.P
NM.5.2
I can identify basic time meters.

Sample Learning Targets

Sample Learning Targets

x

I can identify quarter, eighth, half,
and whole notes in a familiar song.

x

I can sing music examples in simple
meter(2/4, 3/4 and 4/4)

x

I can identify strong and weak beats
in music.

x

I can identify the meter signature of a
familiar song.

x
x

Indicator
CM.P
NH.5.2
I can perform simple unfamiliar tonal
patterns using music notation.
Sample Learning Targets
x

I can perform so, mi, and la tonal
patterns presented in notation, in a
familiar tonality.

x

I can perform so, mi, and do tonal
patterns presented in notation, in a
familiar tonality.

x

I can...

I can...

I can...

Indicator
CM.P
NL.5.3
I can identify simple familiar rhythm
patterns with corresponding notation.
Sample Learning Targets

Indicator
CM.P
NM.5.3
I can sing using eighth, quarter, half and
whole notes and rests.
Sample Learning Targets

x

I can match the correct notation with
a familiar four-beat rhythm pattern
presented to me aurally.

x

I can perform a familiar four-beat
rhythm pattern presented in
notation.

x

I can select the correct notation to
represent a familiar four-beat rhythm
pattern presented aurally.

x

I can perform a familiar four-beat
tonal pattern presented in notation.

x
x

I can match the correct notation with
a familiar rhythm presented to me
aurally.

x

I can...

Indicator
CM.P
NH.5.3
I can sing in unison and simple 2-part
music.
Sample Learning Targets
I can sing a simple folk song in unison with
and without accompaniment.
x I can sing in a round or canon.

x

I can...

I can...

Page 160

Indicator
CM.P
NL.5.4
I can identify simple familiar tonal patterns
with corresponding notation.
Sample Learning Targets
x

I can match the correct notation with
a familiar three-pitch tonal pattern
presented to me aurally.

x

I can select the correct notation to
represent a familiar three-pitch tonal
pattern presented to me aurally.

x

Indicator
CM.P
NM.5.4
I can sing a variety of tempos in music.
Sample Learning Targets
I can perform accelerando and ritardando in a
song.
I can perform sudden tempo changes in a
song.
x I can...

Indicator
CM.P NH.5.4
I can sing simple patterns in multiple
tonalities.
Sample Learning Targets
x

I can sing pitches in major and
minor.

x

I can sing pitches in pentatonic
patterns.

x

I can...

I can...

Artistic Processes: Responding - I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Novice
Low
Benchmark
CM.R
NL.6
I can identify the elements of music,
instrument families, and voice types.

Novice
Mid
Benchmark
CM.R
NM.6
I can identify and apply basic music symbols
and terminology.

Novice
High
Benchmark
CM.R
NH.6
I can identify patterns in music, recognize
basic musical forms, and identify criteria of a
musical performance.

Page 161

Indicator
CM.P
NL.6.1
I can identify rhythm, dynamics, pitch,
harmony, tone color, texture and form.
Sample Learning Targets

Indicator
CM.P
NM.6.1
I can identify basic music symbols and terms
in written music.
Sample Learning Targets

Indicator
CM.P
NH.6.1
I can recognize patterns in the music that I
hear.
Sample Learning Targets

x

I can identify high and low pitches.

x

I can match dynamic symbols to
dynamics terms.

x

I can identify moments of repetition
in music that I hear.

x

I can move to show fast and slow
tempos in music.

x

I can define basic tempo terms.

x

x

I can...

I can identify when a melodic theme
returns in a different section of a
piece of music.

x

I can...

x

I can demonstrate the steady pulse of
music that I hear using body
percussion.

x

I can...

Indicator
CM.P
NL.6.2
I can identify instrument families in the
symphony orchestra.
Sample Learning Targets

Indicator
CM.P
NM.6.2
I can apply my knowledge of musical symbols
and terminology to a performance.
Sample Learning Targets

Indicator
CM.P
NH.6.2
I can identify common forms such as call and
response, verse and refrain, and ABA.
Sample Learning Targets

x

I can identify which instrument
family is playing the melody.

x

I can sing a simple song in allegro,
andante, and adagio tempos.

x

I can describe musical differences
between the verse and refrain form.

x

I can classify instruments into
families.

x

I can move to show dynamic changes
in music.

x

I can recognize and label the A
section and B section of a piece of
music that I see.

x

I can...

x

I can...
x

I can recognize the difference
between the call and response
sections of a song.

x

I can...
Page 162

Indicator
CM.P
NL.6.3

Indicator
CM.P
NM.6.3

Indicator
CM.P
NH.6.3

I can identify different voice types.

I can identify characteristics of a performance
that I like/dislike.

I can identify criteria for a music
performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can use my speaking, calling,
whispering and singing voices.

x

I can recognize male and female
voices.

x

x

x

I can use my own words to describe
my favorite part of a musical
performance.
I can discuss how I feel when I hear
sudden dynamic and tempo changes
in music.

x

I can evaluate music performances
using music terminology.

x

I can list specific criteria when
evaluating music performances.

x

I can...

, FDQ«
x

I can...

Anchor Standard 7: I can evaluate music.
Novice
Low
Benchmark
CM.P
NL.7
I can show my personal interest in musical
performances of others.

Novice
Mid
Benchmark
CM.P
NM.7
I can describe my personal interest in music
performances using music terminology.

Novice
High
Benchmark
CM.P
NH.7
I can list some criteria to describe my interest
in music performances using music
terminology.

Page 163

Indicator
CM.P
NL.7.1
I can recognize some elements of music, such
as loud/soft, fast/slow.

Indicator
CM.P
NM.7.1
I can define basic music terminology using
my own words.

Indicator
CM.P
NH.7.1
I can describe some of the elements of music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can move to show changes in
tempo.

x

I can identify basic musical terms
such soft and loud sounds.

x

I can describe the difference between
melody and harmony in music.

x

I can point up high when I hear high
sounds, and point down low when I
hear low sounds.

x

I can describe my feelings about a
performance using my own words.

x

.

x
x

I can...

I can identify pitch and rhythm
changes.

x

I can...

x

x

I can use my own words to discuss
what I hear in a song.
I can...

Indicator
CM.P
NL.7.2
I can discuss my preference for a piece of
music.
Sample Learning Targets
x

I can talk about sounds I enjoy in a
piece of music.

x

I can draw a picture showing how a
piece of music makes me feel.

x

Indicator
CM.P
NM.7.2
I can identify characteristics of a performance
that I like/dislike.
Sample Learning Targets
x

I can use my own words to describe
my favorite part of a musical
performance.

x

I can discuss how I feel when I hear
sudden tempo and dynamic changes
in music.

I can...
x

Indicator
CM.P
NH.7.2
I can identify some criteria for music
performance.
Sample Learning Targets
x

I can describe the tone quality of a
VLQJHU¶V YRLFH

x

I can describe the choral blend and
balance within an ensemble.

x

I can...

I can....

Page 164

Artistic Processes: Connecting - I can connect musical ideas and works to personal experience, careers,
culture, history and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Novice
Low
Benchmark
CM.P
NL.8
I can recognize musical selections from some
cultures and time periods.

Novice
Mid
Benchmark
CM.P
NM.8
I can identify musical selections from a
specific culture and a historical time period.

Indicator
CM.P
NL.8.1
I can recognize that all cultures perform
music.
Sample Learning Targets

Indicator
CM.P
NM.8.1
I can recognize similar elements of music in
a specific culture.
Sample Learning Targets

I can sing a patriotic song.
x

x

I can recognize historical/ cultural
events in my community where choral
music is performed.

x

I can recognize that call and
response is a type of form in
African-American music.

x

I can recognize syncopated patterns in
Latin music.

I can...
x

Novice
High
Benchmark
CM.P
NH.8
I can identify musical selections from
multiple cultures and/or historical time
periods.
Indicator
CM.P
NH.8.1
I can identify similar elements of music in
different cultures.
Sample Learning Targets
x

I can sing a song in AB form from
$PHULFD VXFK DV ³<DQNHH 'RRGOH
and find a similar AB form in a song
from AXVWUDOLD VXFK DV ³:DOW]LQJ
MatLOGD ´

x

I can use technology (YouTube
recordings and choral websites) to
identify different choral genres.

x

I can...

I can...

Page 165

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Novice
Low
Benchmark
CM.P
NL.9
I can explore choral music concepts among
arts disciplines, other content areas, and
related careers.
Indicator
CM.C
NL.9.1
I can identify the relationship between music
and another subject in my school.

Novice
Mid
Benchmark
CM.P
NM.9
I can recognize choral music concepts among
arts disciplines, other content areas, and
related careers.
Indicator
CM.C
NM.9.1
I can demonstrate a relationship between
music and another subject in my school.

Sample Learning Targets

Sample Learning Targets

x

x

x

I can sing high and low pitches in
³6WDU /LJKW 6WDU %ULJKW ´ DQG , FDQ
point to high and low images in Van
*RJK¶V Starry Night.
I can identify rhyming words in a
song.

x

I can count in beats of four while
moving to folk songs in 4/4 meter.

x

I can describe the relationship
between sound waves and pitch.

x

, FDQ«

Novice
High
Benchmark
CM.P
NH.9
I can apply choral music concepts to arts
disciplines, other content areas, and related
careers.
Indicator
CM.C
NH.9.1
I can demonstrate and describe the
relationship between music and a concept
from another subject in my school.
Sample Learning Targets
x

I can research the history of the
Underground Railroad and discuss
how this history is reflected in the
African-$PHULFDQ VRQJ ³)ROORZ WKH
Drinking Gourd ´

x

I can find repeated rhythms
(ostinati) in a song, and find repeated
stanzas in a poem.

x

, FDQ«

I can...

Page 166

Indicator
CM.C
NL.9.2
I can identify and discuss examples of
musicians in my community.

Indicator
CM.C
NM.9.2
I can identify life skills necessary for a music
career.

Indicator
CM.C
NH.9.2
I can identify specific careers in music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

x

I can discuss the sounds/songs I hear
at a football game.

x

I can describe proper performer and
audience behavior for a concert.

I can list places where I have seen
musicians perform.

x

I can work with others to improve my
performance.

I can identify musicians in my
community (choir director, church
singer).

x
x

x

I can use an internet search engine to
locate arts businesses who employ
musicians, (such as theaters, music
stores, university arts departments,
churches).

x

I describe music careers of
community members.

x

, FDQ«

, FDQ«

I can...

Page 167

Intermediate Choral Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new ideas.
Anchor Standard 1:I can arrange and compose music.
Intermediate
Low
Benchmark
CM.CR
IL.1
I can arrange a short song for my voice.

Intermediate
Mid
Benchmark
CM.CR
IM.1
I can arrange a short song for two voices,
using harmony.

Indicator
CM.CR
IL.1.1
I can create a simple tune (monophonic
melody) without accompaniment, within
specified guidelines.
Sample Learning Targets

Indicator
CM.CR
IM.1.1
I can develop a simple tune with
accompanying parts (homophonic work).

x

x

I can create an eight measure melody
using pitches from the pentatonic
scale.
I can create an eight measure melody
using the pitches do, re, mi, fa, so.

Sample Learning Targets
x

I can create a melodic ostinato
pattern to accompany my melody.

x

I can create a rhythmic
accompaniment on a percussion
instrument to accompany my
melody.

I can...
x

Intermediate
High
Benchmark
CM.CR
IH.1
I can arrange a short song for an ensemble,
demonstrating an understanding of voicing
and texture.
Indicator
CM.CR
IH.1.1
I can combine different voices to create
various tone colors in my arrangement.
x

Sample Learning Targets

x

I can arrange a song for SA voices.

x

I can arrange a song for SAB voices.

x

I can...

I can...

Page 168

Indicator
CM.CR
IL.1.2
I can create a melody using rhythms that are
appropriate for the time signature.

Indicator
CM.CR
IM.1.2
I can develop an original arrangement of a
traditional canon or round.

Indicator
CM.CR
IH.1.2
I can experiment with changes in tone color,
creating variety and contrast through a
combination of different voices.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can embellish my short song using
pitches from tonic triad and quarter
and eighth notes/rests.

x

I can create an original arrangement
of a canon for a small ensemble of 2
primary voice parts.

x

I can compare and contrast a SSA and
SAB arrangements of the same
song.

x

I can create a melody in duple meter.

x

I can write an original round that ends
on a tonic triad.

x

x

I can create a melody in triple meter.

I can compare and contrast SATB and
TTBB arrangements of the same
song.

x

I can...

x

I can...

x

I can...

Indicator
CM.CR.1
IL.3
I can develop a melody using pitches that are
appropriate for the tonality.

Indicator
CM.CR
IM.1.3
I can develop my song using I, IV, and V
chord progressions.

Indicator
CM.CR
IH.1.3
I can experiment with non-chord tones and
chord progressions.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can write an eight-measure melody
using pitches from a major scale.

x

x

x

I can provide an eight-measure
accompaniment using I and V chord
progressions.

I can write an eight-measure melody
using pitches from a minor scale.

x

I can write a melody over a I, IV, V
chord progression.

I FDQ«

x

, FDQ«

x

I can manipulate tones to create a
plagal cadence and a perfect
authentic cadence.

x

I can use notation software to
arrange phrases and basic chord
progressions.

x

, FDQ«

Page 169

Anchor Standard 2: I can improvise music.
Intermediate
Low
Benchmark
CM.CR
IL.2
I can improvise simple rhythmic patterns
within a given meter.
Indicator
CM.CR
IL.2.1
I can improvise my own simple rhythmic
pattern using a neutral syllable.
Sample Learning Targets

Intermediate
Mid
Benchmark
CM.CR
IM.2
I can improvise simple tonal patterns within
a given tonality.
Indicator
CM.CR
IM.2.1
I can improvise my own simple tonal
patterns on a neutral syllable.
Sample Learning Targets

x

I can improvise a four-beat rhythm
pattern while patsching a steady
beat in 4/4 meter.

x

I can improvise a three-note tonic
triad pattern in a major key on a
neutral syllable.

x

I can improvise a six-beat rhythm
pattern on a neutral syllable while
patsching a steady beat in ¾ meter.

x

I can improvise a three-note
dominant triad pattern in a major
key on a neutral syllable.

I can...

x

I can improvise a three-note tonic
triad pattern in a minor key on a
neutral syllable.

x

I can...

x

Intermediate
High
Benchmark
CM.CR
IH.2
I can improvise simple melodic phrases.
Indicator
CM.CR
IH.2.1
I can identify chord changes to improvise a
short melody.
Sample Learning Targets
x

I can identify different chord
patterns on a staff line.

x

I can write a I, IV, V chord
progression using notation.

x

I can...

Page 170

Indicator
CM.CR
IL.2.2
I can improvise my own simple rhythm
patterns using ta-ka-di-mi or a counting
system.
Sample Learning Targets

Indicator
CM.CR
IM.2.2
I can improvise my own simple tonal
patterns using solfege.
Sample Learning Targets

Indicator
CM.CR
IH.2.2
I can improvise simple melodic phrases that
correspond with chord progressions in an
unfamiliar song.
Sample Learning Targets

x

I can improvise a four-beat rhythm
pattern using ta-ka-di-mi while
patsching a steady beat in 4/4 meter.

x

I can improvise a three-note tonic
triad pattern in a major key on
solfege syllables.

x

I can improvise a melodic phrase
over a given chord progression in a
major tonality.

x

I can improvise a six-beat rhythm
pattern using ta-ka-di-mi while
patsching a steady beat in ¾ meter.

x

I can improvise a three-note
dominant triad pattern in a major
key on solfege syllables.

x

I can improvise a melodic phrase
over a given chord progression in a
minor tonality.

x

I can...

x

I can improvise a three-note tonic
triad pattern in a minor key on
solfege syllables.

x

I can use electronic musical tools to
mix or arrange music within a given
chord progression.

x

I can...

x

I can...

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.
Intermediate
Low
Benchmark
CM.P
IL.3
I can produce a centered tone in a comfortable
tessitura.

Intermediate
Mid
Benchmark
CM.P
IM.3
I can produce a centered tone in some
tessituras specific to my vocal range.

Intermediate
High
Benchmark
CM.P
IH.3
I can produce a centered tone in most
tessituras specific to my vocal range.

Page 171

Indicator
CM.P
IL.3.1
I can sing with a resonant, centered, and free
tone in harmony.
x

I can match pitch when I sing a
simple round.

x

I can sing a 2-part song with a
centered tone.

x

x

Indicator
CM.PIM.3.1
I can sing my assigned part in tune with
appropriate tone quality, resonance and
vocal timbre.
x I can sing ostinati, partner songs,
rounds and two-part music while
maintaining proper vowel
formation, and head voice.
x

I can sing my assigned part in a
cappella music in rehearsal and
performance settings.

x

I can sing ostinati, partner songs,
rounds and two-part music with
clear tone quality.

I can sing a 2-part song with blended
vowel formation between the voice
parts.
, FDQ«

x

x

x

x

I can sing the alto part in a SSA
arrangement with clear tone quality,
breath control, and correct posture.
I can sing the tenor part in a TTB
arrangement with clear tone quality,
breath control, and correct posture.

I can sing two- and three-part songs with
centered tone quality, in tune, while
demonstrating dynamic changes.
x I can sing two- and three-part music,
maintaining correct intonation,
breath support, and vocal timbre.
x

I can demonstrate appropriate diction
by articulating clarity of consonants
and purity of vowels.

x

I can sing forte without shouting.

x

, FDQ«

I can«.

Indicator
CM.P
IL.3.2
I can sing in tune my assigned part with clear
tone quality, using breath control and
correct posture.
Sample Learning Targets

Indicator
CM.P IM.3.1

Indicator
CM.P
IM.3.2
I can sing with a centered tone and a steady
tempo.
Sample Learning Targets
x I can perform with a centered tone
while singing allegro.

Indicator
CM.P
IH.3.2
I can sing two- and three-part songs with
centered tone quality, in tune, while
demonstrating articulation changes.
Sample Learning Targets
x I can sing staccato in a two part song
with a centered tone quality.

x

I can perform with a centered tone
while singing largo.

x

I can perform an accent in a three-part
song with a centered tone quality.

x

, FDQ«

x

, FDQ«

I can«
Page 172

Anchor Standard 4: I can perform with technical accuracy and expression.
Intermediate
Low
Benchmark
CM.P
IL.4
I can sing expressively with appropriate
dynamics and phrasing.

Intermediate
Mid
Benchmark
CM.P
IM.4
I can sing expressively with appropriate
dynamics, phrasing, and interpretation.

Indicator
CM.P
IL.4.1
I can sing, observing a variety of dynamic
markings in songs.
x I can sing a crescendo and a
decrescendo notated in music.

Indicator
CM.P
IM.4.1
, FDQ LQWHUSUHW D FRQGXFWRU¶V dynamic and
phrasing cues when singing.
x I can observe my FRQGXFWRU¶V FXH to
lift and breathe in a phrase of music.

x

I can sing a sforzando.

x

I can...

x

x
Indicator
CM.P
IL.4.2
I can sing, observing phrasing suggestions
and markings in music.
Sample Learning Targets

, FDQ REVHUYH P\ FRQGXFWRU¶V FXH WR
observe a fermata.

Intermediate
High
Benchmark
CM.P
IH.4
, FDQ VLQJ ZKLOH LQWHUSUHWLQJ P\ FRQGXFWRU¶V
cues in order to perform with expression and
technical accuracy.
Indicator
CM.P
IH.4.1
, FDQ LQWHUSUHW D FRQGXFWRU¶V JHVWXUH ZLWK
rhythmic and melodic precision.
x I can interpret my FRQGXFWRU¶V FXHV
to sing with accurate rhythmic
division and subdivision of beat.
x

I can interpret P\ FRQGXFWRU¶V
gesture to balance other voice parts
in my ensemble.

x

I can.

I can...

Indicator
CM.P
IM.4.2
I can sing, observing phrasing markings and
breathing appropriately alone and in groups.
Sample Learning Targets

Indicator
CM.P
IH.4.2
I can interpret D FRQGXFWRU¶V dynamic,
articulation, and phrasing cues.
Sample Learning Targets

x

I can observe legato markings in
music.

x

I can observe a breath mark when
notated in my music.

x

I can observe my GLUHFWRU¶V cues to
sing with marcato.

x

I can observe a tenuto marking in
music.
I can...

x

I can stagger breathe in a long phrase
with other singers in my section.
, FDQ«

x

I can sing phrases of irregular length
by following my conductor's cues.

x

x

Page 173

Anchor Standard 5: I can perform using music notation.
Intermediate
Low
Benchmark
CM.P
IL.5
I can identify music notation symbols
representing an expanded set of tonal,
rhythmic, technical, and expressive ideas.

Intermediate
Mid
Benchmark
CM.P
IM.5
I can perform at sight simple unfamiliar
musical works.

Intermediate
High
Benchmark
CM.P
IH.5
I can use a system to fluently sight-read
moderately complex melodies in treble and
bass clefs.

Indicator
CM.P
IL.5.1
I can identify sharps, flats, naturals, and
simple key signatures.

Indicator
CM.P
IM.5.1
I can perform at sight simple unfamiliar
musical works with accurate pitches.

Indicator
CM.P
IH.5.1
I can perform at sight moderately complex
unfamiliar musical works with accurate
pitches.
x I can read a three-part music score.

x

I can identify basic key signatures
and locate do on the staff.

x

I can identify accidentals in my
score.

x

I can...

x

I can read a two-part music score.

x

I can use solfege to read two-part
songs including skips of tonic triad.

x

I can...

x

I can use solfege to read three-part
music including skips of tonic and
dominant triads.

x

I can...

Page 174

Indicator
CM.P
IL.5.2
I can sight-read stepwise tonic (do, re, mi, fa,
so) patterns and simple meter based (2/4/,
3/4/, 4/4) rhythmic patterns.
Sample Learning Targets
x

x

I can use numbers, rhythm
syllables, or count singing to sightread rhythms for my voice part.

Indicator
CM.P
IM.5.2
I can sight read using reading systems such
as ta-ka-di-mi, Gordon, count singing, and
neutral syllables to unfamiliar melodies with
tonic triad skips.
Sample Learning Targets
x

I can read rhythm patterns including
basic divided beat in my music using
reading systems (ta-ka-di-mi,
Gordon syllables etc.).

I can use numbers, tonal syllables,
or count singing to sight-read
pitches for my voice part.

x

I can read tonal patterns including
tonic triad skips using solfege.

I can...

x

I can...

Indicator
CM.P
IH.5.2
I can notate intermediate note values and
time signatures.

Sample Learning Targets
x

x

x
x

I can notate rhythm including
extension dots in music notation
software.
I can notate rhythm patterns using
compound meter in music notation
software.
I can...

Indicator
CM.P
IL.5.3
I can identify advanced note values and time
signatures that represent syncopation and
smaller beat subdivisions in my music.

Indicator
CM.P
IM.5.3
I can apply basic tempo markings in my
music.

Indicator
CM.P
IH.5.3
I can apply intermediate tempo markings in
my music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify and count syncopated
rhythm patterns.

x

I can read a rhythmic passage and
apply the term adagio.

x

I can perform a rhythmic passage
and apply the term prestissimo.

x

I can identify and count sixteenth
note patterns.

x

I can sing a melodic phrase and apply
the term moderato.

x

I can perform a melodic phrase and
apply the term con brio.

x

I can...

x

I can...

x

I can...
Page 175

Indicator
CM.P
IL.5.4
I can identify expressive markings in my
music.
Sample Learning Targets

Indicator
CM.P
IM.5.4
I can apply expressive markings in my
music.
Sample Learning Targets

Indicator
CM.P
IH.5.4
I can apply advanced expressive markings in
my music.
Sample Learning Targets

x

I can label dolce markings in my
music.

x

I can sing a melodic passage with
dolce expression.

x

I can sing a melodic passage with bel
canto expression.

x

I can label cantible markings in my
music.

x

I can sing cantible in a melodic
passage.

x

I can sing a melodic passage with
gracioso expression.

x

I can...

x

I can...

x

, FDQ«

Artistic Processes: Responding - I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Intermediate
Low
Benchmark
CM.R
IL.6
I can describe how the elements of music are
used to communicate ideas and evoke
emotional responses in others and myself.

Intermediate
Mid
Benchmark
CM.R
IM.6
I can identify and explain how the elements
of music are used in a variety of genres to
determine my personal preferences.

Intermediate
High
Benchmark
CM.R
IH.6
I can evaluate a performance and offer
constructive suggestions for improvement
using provided criteria.

Page 176

Indicator
CM.R
IL.6.1
I can explain how music elements are used to
communicate ideas.

Sample Learning Targets

Indicator
CM.R
IM.6.1
I can identify how the melody, harmony,
rhythm, timbre, texture, form, and
expressive elements are different in varying
genres of music.
Sample Learning Targets

x

I can explain why a composer chose
to use certain dynamic and tempo
markings in a piece of music.

x

I can describe how harmony is
different in jazz music as opposed to
hip-hop music.

x

I can explain why the Armed Forces
Medley is in 4/4 time.

x

I can describe how the use of the
elements of music can be used to
determine a specific genre.

x

x

I can explain why a sea chantey or
work song has a strong driving beat.

x

Indicator
CM.R
IH.6.1
I can identify advanced musical symbols, key
signatures, and complex meter.

Sample Learning Targets
x

I can identify articulation marks,
ornaments.

x

I can identify key signatures,
accidentals, as they appear in music.
I can...

x

I can...

, FDQ«
Indicator
CM.R
IL.6.2

Indicator
CM.R
IM.6.2

I can describe how the elements of music
affect the mood of a song.

I can describe common elements found in
various genres of music.

Sample Learning Targets

Sample Learning Targets

x

I can talk about the effect of a minor
melody on my emotional response to
a piece of music.

x

I can find a 12-bar blues progression
in a Rock and Roll song from the
1950s.

x

I can explain why a lullaby is sung
softly and slowly.

x

I can find examples of Improvisation
in bluegrass and rap music.

x

I can...

x

, FDQ«

Indicator
CM.R
IH.6.2
I can explain why advanced musical symbols,
key signatures, and complex meter are used
in music.
Sample Learning Targets
x

I can explain how tonality is used to
affect the mood of a song.

x

I can explain how meter and
modulation is used to affect the
mood of a song.

x

, FDQ«
Page 177

Indicator
CM.R
IL.6.3

Indicator
CM.R
IM.6.3

Indicator
CM.R
IH.6.3

I can use the elements of music to describe
my emotional response to a music
performance.

I can use the elements of music to describe
why I like particular genres.

I can use the elements of music to offer
suggestions for improvement.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

I can discuss vocal timbre and how it
affects the mood of a song.

x

I can critically listen to different
genres of music and use the elements
of music to explain which genres are
my favorite.

I can critically listen to music
performances and use the elements of
music to describe my opinion.
, FDQ«

x

I can compare and contrast two
genres of music.

x

, FDQ«

x

I can listen to a recording of an
ensemble and critique vocal
technique.

x

I can use a recording device to
complete a self-critique.
, FDQ«

Anchor Standard 7: I can evaluate music.
Intermediate
Low
Benchmark
CM.R
IL.7
I can describe the quality of music
performances using provided criteria.

Intermediate
Mid
Benchmark
CM.R
IM.7
I can describe my evaluation of a performance
to others.

Intermediate
High
Benchmark
CM.
IH.7
I can describe the quality of my performances
and my compositions.

Page 178

Indicator
CM.R
IL.7.1
I can define all the elements of music.
Sample Learning Targets
x

I can name all of the elements of
music.

x

I can provide specific details about
the different elements of music.

x

Indicator
CM.R
IM.7.1
I can identify specific criteria I use when I
FULWLTXH RWKHUV¶ SHUIRUPDQFHV
Sample Learning Targets
x

I can identify criteria to create a
rubric for my performance and the
performance of others.

x

I can discuss with others which
specific characteristics of their
performance needed improvement.

I can...
x

Indicator
CM.P
IL.7.2
I can describe the characteristics of a quality
performance using musical terms.
Sample Learning Targets
x

I can listen to music performances to
determine if an ensemble maintained
good intonation.

x

I can listen to music performances to
determine if an ensemble maintained
good balance and blend.

x

I can...

Indicator
CM.R
IH.7.1
I can compare my performance to the
performance of others.
Sample Learning Targets
x

I can compare my own performance
to the performance of others using
rubric-based feedback from an
adjudicator at a county, region, or
state festival.

x

I can use a recording provided by an
adjudicator at a county, region, or
state festival to compare my
performance to the performance of
others.

x

I can...

I can...

Indicator
CM.P
IL.7.2
I can use the elements of music to evaluate a
composition.
Sample Learning Targets

Indicator
CM.P
IH.7.2
I can use the elements of music to evaluate
my performance or the performance of others.
Sample Learning Targets

x

I can select criteria for a rubric to
assess a choral composition.

x

I can use a rubric to evaluate my own
performance.

x

I can use the elements of music to
describe the anticipated level of
difficulty of a choral composition.

x

I can use a rubric to evaluate the
performance of others and provide
feedback. I can...

x

, FDQ«

Page 179

Artistic Processes: Connecting - I can connect musical ideas and works to personal experience, careers,
culture, history, and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Intermediate
Low
Benchmark
CM.C
IL.8
I can examine relationships among musical
selections from multiple cultures and/or
historical time periods.
Indicator
CM.P
IL.8.1
I can identify similarities and differences in
music from multiple cultures and time
periods.
Sample Learning Targets
x

x

x

I can compare and contrast two songs
from different time periods, such as
WLJKW KDUPRQLHV LQ ³%RRJLH :RRJLH
%XJOH %R\ ´ E\ WKH $QGUHZV 6LVWHUV
in the 1940s, to single melodic line in
³%LJ <HOORZ 7D[L ´ E\ -RQL 0LWFKHOO
in the 1970s.
I can compare and contrast a classical
piece from an oratorio with a modern
octavo written by a contemporary
composer.
I can use music terms to compare the
stylistic differences in a classical
choir and modern choir.

Intermediate
Mid
Benchmark
CM.C
IM.8
I can research the role of music within a
specific culture or historical time period and
present what I discovered.
Indicator
CM.P
IM.8.1
I can research a specific culture/ time period
and perform a song from that culture/time
period.
Sample Learning Targets
x

I can research a composer in music
history and perform his/her work in
the style pertaining to that time
period.

x

, FDQ UHVHDUFK WKH UROH RI WKH ³6WDU
6SDQJOHG %DQQHU´ LQ YDULRXV VHWWLQJV
and time periods since it was adopted
as our National Anthem in 1931.

x

I can read an informational text and
perform a song from that time period.

x

, FDQ«

Intermediate
High
Benchmark
CM.C
IH.8
I can modify a musical work using
characteristics from a culture or time period.
Indicator
CM.P
IH.8.1
I can change a musical work using the
elements of music from a culture or time
period.
Sample Learning Targets
x

I can change the interpretation of a
Bach Chorale by adding elements of
jazz music.

x

I can change the interpretation of a
spiritual by adding elements of a
classical style.

x

I can...

Page 180

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Intermediate
Low
Benchmark
CM.C
IL.9
I can explore a range of skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Intermediate
Mid
Benchmark
CM.C
IM.9
I can recognize specific skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Indicator
CM.C
IL.9.1
I can apply music concepts to other arts
disciplines and content areas.
Sample Learning Targets
x

I can compare and contrast the
elements of music to the elements
and principles of art.

x

I can perform a choral piece based on
a piece of creative writing, such as
³,QVFULSWLRQ RI +RSH ´ DQG WKHQ
change the dynamics of the piece..

Indicator
CM.C
IM.9.1
I can examine the relationship between music
and specific content from another arts
discipline and content area.
Sample Learning Targets
x

I can sing two- and three-part
harmony, and create ways to display
layering or thicker texture in arts
classes (for example, making a quilt
square for a class project).

x

I can examine the role of music in
theatre.

x

I can examine the role of music in
dance.

x

I can...

Intermediate
High
Benchmark
CM.C
IH.9
I can analyze the tools, concepts, and
materials used among arts disciplines, other
content areas and how they are used in music
careers.
Indicator
CM.C
IH.9.1
I can apply concepts from other arts
disciplines and content areas to my music.
Sample Learning Targets
x

I can use the elements of dance to
choreograph a movement sequence
for a choral work.
x

x

I can...

I can use the elements of theatre to
add expressive qualities to my
performance of a Broadway song.

I can...

Page 181

Indicator
CM.C
IL.9.2
I can demonstrate and describe the skills
needed for careers in music.

Indicator
CM.C
IM.9.2
I can examine the educational requirements
needed for a variety of careers in music.

Indicator
CM.C
IH.9.2
I can research skills needed for various music
careers.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can research skills needed for a
variety of music careers.

x

I can investigate and report about
music careers in SC.

x

I can name careers in music and
research the requirements for the
chosen career.

x

I can interview a person in an arts
field to discover the music skills
needed for that job.

x

I can examine the requirements of a
music producer.

x

I can research topics about careers in
music that interest me.

x

I can identify college degree
programs for music therapy.

x

, FDQ«

x

, FDQ«

I can...

Page 182

Advanced Choral Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new ideas.
Anchor Standard 1: I can arrange and compose music.
Advanced
Low
Benchmark
CM.CR
AL.1
I can describe how I use melody, rhythm,
and harmony to compose or arrange a work
for a specific purpose.
Indicator
CM.CR
AL.1.1
I can arrange melodic themes for specific
purposes, using arrangement and
compositional techniques.
Sample Learning Targets
x

I can use diminution, retrograde,
fragmentation, augmentation,
permutation to arrange a
composition.

x

I can arrange a melody for a specific
holiday.

x

, FDQ«

Advanced
Mid
Benchmark
CM.CR
AM.1
I can collaborate with others to compose or
arrange a musical work for a specific
purpose.
Indicator
CM.CR
AM.1.1
I can sing in ensembles, working with others
to develop ideas as we compose or arrange a
composition.
Sample Learning Targets
x

I can work with others to create a
multi-movement work.

x

I can use technology to collaborate
with team members while
composing/arranging a composition

x

Advanced
High
Benchmark
CM.CR
AH.1
I can compose short, original musical ideas
and works using all the elements of music
for a specific purpose.
Indicator
CM.CR
AH.1.1
I can create musical ideas and works using
chord progressions and modulations.
Sample Learning Targets
x

I can compose a four-part choral
piece in the style of a Bach Chorale
utilizing appropriate cadences and
chord progressions.

x

I compose in sonata form
transforming a piece through
exposition, development and
recapitulation.

x

I can...

I can...

Page 183

Indicator
CM.CR
AL.1.2
I can use compositional techniques to
compose works in a given musical form.

Indicator
CM.CR
AM.1.2
I can work with others to analyze
arrangements and original compositions for
improvements.

Indicator
CM.CR
AH.1.2
I can use characteristic forms of music to
create a choral composition for a specific
purpose.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

I can explain common music forms,
(for example: verse-refrain, AB,
ABA,).
I can use the circle of fifths to
compose a work in complementary
keys.

x

x

I can...
x

Indicator
CM.CR
AL.1.3
I can compose short compositions in major
and minor keys.
Sample Learning Targets

I can use a recording to analyze the
arrangements and original
compositions of my peers for
improvement.
I can use multiple recordings of my
composition, in order to compare and
contrast qualities of the performance
areas that need improvement.

x

I can use verse and refrain form to
compose a traditional hymn.

x

I can use ABA form to compose a
popular-contemporary style song.

x

I can...

I can....

Indicator
CM.CR
AM.1.3
I can compose an original composition in
four-part chorale style.
Sample Learning Targets

Indicator
CM.CR.1
AH.3
I can compose a choral composition with a
variety of expressive devices.
Sample Learning Targets

x

I can use a poem to create a sixteenmeasure composition in a major key.

x

I can write an SATB arrangement of a
traditional folk song.

x

I can use tempo to enhance
expression in a composition.

x

I can write a sixteen-measure
composition in a minor key to
accompany a dance.

x

I can use traditional voice leading
composition techniques.

x

I can use dynamics to enhance
expression in a composition.

x

I can «

x

x

, FDQ«

I can use articulation to enhance
expression in a composition.

x

, FDQ«
Page 184

Anchor Standard 2: I can improvise music.
Advanced
Low
Benchmark
CM.CR
AL.2
I can perform a brief improvisation given a
chord progression and meter.

Advanced
Mid
Benchmark
CM.CR
AM.2
I can perform an improvisation given a
motive, chord progression and meter.

Advanced
High
Benchmark
CM.CR
AH.2
I can perform an extended improvisation
with freedom and expression featuring
motivic development within a given tonality,
meter, and style.

Indicator
CM.CR
AL.2.1
I can improvise a short passage using only a
chord progression.

Indicator
CM.CR
AM.2.1
I can perform an improvisation on a given
motive.

Indicator
CM.CR
AM.2.1
I can improvise an extended unaccompanied
solo within a given tonality, meter, and
style.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can use passing tones, and other
non-harmonic tones to improvise a
short passage over a given chord
progression in major tonality.

x

I can use rhythmic variation,
including augmentation and
diminution, to improvise on a given
motive.

x
x

I can improvise a cadenza
I can incorporate a composer¶V
melodic and rhythmic motives into
my performance.

x

I can use passing tones, and other
non-harmonic tones to improvise a
short passage over a given chord
progression in minor tonality.

x

I can use passing tones and use nonharmonic tones to improvise on a
given motive.

x

I can...

x

I can...

x

I can...

Page 185

Indicator
CM.CR
AL.2.2
I can improvise a short passage in an
established meter.

Indicator
CM.CR
AM.2.2
I can improvise an extended passage using
only a chord progression.

Indicator
CM.CR
AH.2.2
I can improvise freely within a given
tonality, meter, and style, responding to aural
cues from other members of an ensemble.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can use rhythmic variations to
improvise a solo in 6/8 meter.

x

I can use melodic variations to
improvise a solo in cut-time.

x

, FDQ«

x

I can use rhythmic variations,
passing tones, and other onharmonic tones to improvise an
extended passage given a chord
progression.

x

I can improvise an extended melody
over a repeated chord progression.

x

I can use rhythmic variations to
improvise a solo in duple (such as 2/4
or 4/4) and triple (such as 6/8 meter).

x

I can...

x

I can improvise a solo incorporating
and responding to musical
gestures/ideas as performed by
another member of my ensemble.

x

I can use electronic musical tools to
mix or arrange or improvise music
within a given chord progression.

x

I can...

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a well-developed tone quality.
Advanced
Low
Benchmark
CM.P
AL.3
I can produce a well-developed tone in all
tessituras specific to my vocal range.

Advanced
Mid
Benchmark
CM.P
AM.3
I can consistently produce a well-developed,
vibrant tone across the entire range of my
voice.

Advanced
High
Benchmark
CM.P
AH.3
I can adjust tone color/timbre in response to
stylistic demands and the musical needs of an
ensemble.
Page 186

Indicator
CM.P
AL.3.1
I can sing with a well-developed tone, some
three- and four-part songs, demonstrating
balance and intonation, by adjusting my
YRLFH WR FRQGXFWRU¶V FXHV
Sample Learning Targets
x

, FDQ VLQJ ³+DOOHOXMDK´ E\ Leonard
Cohen maintaining correct balance
and intonation, making adjustments
as needed.

Indicator
CM.P
AM.3.1
I can sing alone and within a three- and fourpart ensemble, singing with well-developed
tone quality while maintaining balance and
intonation.
Sample Learning Targets
x

Indicator
CM.P
AH.3.1
I can manipulate the tone quality of my voice
to reflect the stylistic demands of a piece of
music.
Sample Learning Targets

I can sing in an ensemble ³City
Called Heaven´ E\ Moses Hogan
giving expression while maintaining
sensitivity to the vocal solo line.
x

x

I can sing in a quartet while
maintaining balance and intonation
within the ensemble.

x

I can prepare a vocal solo for a
festival, scholarship audition or
competitive event.

x

I can...

x

I can...

x
Indicator
CM.P
AL.3.2
I can sing with a well-developed tone,
incorporating all musical symbols, tempo
and expressive indications.
Sample Learning Targets
x

x

I can sing with a well-developed tone,
ZKLOH VLQJLQJ ³3UDLVH +LV +RO\
1DPH´ E\ .HLWK +DPSWRQ
maintaining technique, dynamics,
and tempo, adjusting as needed.
, FDQ SHUIRUP 0R]DUW¶V Requiem with
an orchestra while observing the
expressive indications of the Classical
style. I can...

I can sing ³2 )RUWXQD´ IURP 2UII¶V
³&DUPLQD %XUDQD´ making
adjustments in tone quality as
needed.
, FDQ VLQJ ³6OHHS´ E\ (ULF :KLWDFUH
and demonstrate straight, pure tone
singing.
I can...

Indicator
CM.P
AM.3.2
I can sing with well-developed tone quality
and increased vocal technique.
Sample Learning Targets
x

I can sing in an ensemble ³(OLMDK
5RFN´ DUUDQJHG E\ (PHUVRQ ZLWK
well-developed tone, making
adjustments as needed.

x

I can adjust my tone quality to match
the stylistic demands of an aria or
jazz ballad.

x

Indicator
CM.P
AH.3.2
I can sing in a variety of languages with welldeveloped tone quality, making needed
adjustments in vocal technique.
Sample Learning Targets
x

, FDQ VLQJ ³Esto les Digo´ E\ Kinley
Lange using correct vocal technique
with and without accompaniment.

x

I can perform a German art song.

x

I can...

I can...
Page 187

Anchor Standard 4: I can perform with technical accuracy and expression.
Advanced
Low
Benchmark
CM.P
AL.4
I can sing with increased fluency and
expression a varied repertoire/genre of
choral music.
Indicator
CM.P
AL.4.1
I can sing with rhythmic and melodic
precision music from diverse genres.
Sample Learning Targets
x

I can apply swing style to a piece of
jazz music.

x

I can sing polyphonic entrances in
music with accuracy.

x

Advanced
Mid
Benchmark
CM.P
AM.4
I can sing with increased fluency and
expression in small and large ensembles a
varied repertoire/genre of choral music.
Indicator
CM.P
AM.4.1
I can LQWHUSUHW D FRQGXFWRU¶V gesture in a
varied repertoire of music.
Sample Learning Targets
x

I can interpret the phrasing gesture
of my conductor in a traditional
madrigal.

x

I can...
x

Advanced
High
Benchmark
CM.P
AH.4
I can sing with increased fluency and
expression from memory varied
repertoire/genres of choral music.
Indicator
CM.P
AH.4.1
I can enhance the expressive quality of my
performance through singing from memory.
Sample Learning Targets
x

I can interpret the gestures of my
conductor while performing a South
African Traditional Folk song from
memory as an ensemble singer.

I can interpret the dynamic gesture
of my conductor in sforzando at the
end of a spiritual.

x

I can perform a recitative from the
Classical period.

, FDQ«

x

I can...

Page 188

Indicator
CM.P
AL.4.2
I can sing observing dynamics, articulation,
phrasing, the style of the music.
Sample Learning Targets

Indicator
CM.P
AM.4.2
, FDQ LQWHUSUHW D FRQGXFWRU¶V gestures
appropriate to the genre.
Sample Learning Targets

Indicator
CM.P
AH.4.2
I can sing a cappella vocal selections from
memory.
Sample Learning Targets

x

I can perform a Classical piece of
music with appropriate dynamics,
articulations and phrases.

x

I can interpret the gestures of my
conductor that are specific to jazz
music as an ensemble singer.

x

I can memorize the technical demands
of an a Capella piece of music as an
ensemble singer.

x

I can perform a Baroque piece of
music with appropriate dynamics,
articulation and phrases.

x

I can interpret the gestures of my
conductor to balance my voice part
in a tone cluster of pitches.

x

I can use a recording device to assess
myself singing an a Capella selection
from memory.

x

I can...

x

, FDQ«

x

I can...

Anchor Standard 5: I can perform using music notation.
Advanced
Low
Benchmark
CM.P
AL.5
I can perform at sight complex unfamiliar
musical works with accuracy.

Advanced
Mid
Benchmark
CM.P
AM.5
I can perform at sight complex unfamiliar
musical works with accuracy and appropriate
expression/interpretation.

Advanced
High
Benchmark
CM.P
AH.5
I can perform at sight complex unfamiliar
musical works with accuracy, appropriate
expression/interpretation, and fluency.

Page 189

Indicator
CM.P
AL.5.1
I can perform at sight complex unfamiliar
musical works with accurate pitches.

Indicator
CM.P
AM.5.1
I can perform at sight complex unfamiliar
musical works with correction articulation.

Indicator
CM.P
AH.5.1
I can perform at sight complex unfamiliar
works with fluency.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

I can sight-sing an assigned part of
the Schubert Mass in G with accurate
pitches and rhythms.
I can sight-sing an assigned part of
the Rutter Gloria with accurate
pitches and rhythms.

x

x

I can perform my voice part in a
SSAATTBB piece of choral music.

x

I can independently perform my part
in the Faure Requiem with accuracy.

x

I can perform a complex line of vocal
percussion.

x

I can follow the direction of the stems
to determine my vocal line in a closed
score.

x

I can...
x

I can...

I can...

Indicator
CM.P
AL.5.2
I can sight read using multiple reading
systems (ta-ka-di-mi and Gordon, count
singing, neutral syllables) in music.
Sample Learning Targets
x

x

I can read rhythmic patterns
including subdivision of the beat in
music using reading systems
(takadimi, Gordon syllables etc.).

Indicator
CM.P
AM.5.2
I can identify advanced note values and time
signatures that represent smaller beat
subdivisions in my music.
Sample Learning Targets
x

I can perform with accuracy
rhythmic patterns including
compound meter,

x

I can perform with accuracy
subdivision of beat in music using
reading systems.

x

I can...

Indicator
CM.P
AH.5.2
I can notate advanced values and time
signatures that represent syncopation and
smaller beat subdivisions in my music.
Sample Learning Targets
x

x

I can read rhythmic patterns including
borrowed division of beat in music
using reading systems.

x

I can notate advanced rhythmic and
tonal patterns using music notation
software.
I can notate syncopation and smaller
beat subdivisions using music
notation software.
I can...

I can...

Page 190

Indicator
CM.P AL.5.3
I can identify the use of advanced tempo
markings in my music.
Sample Learning Targets

Indicator
CM.PAM.5.3
I can analyze the use of advanced tempo
markings in my music.
Sample Learning Targets

Indicator
CM P
AH.5.3
I can justify the use of advanced tempo
markings in my music.
Sample Learning Targets

x

I can identify a composer's selection
of a specific tempo marking in a
piece of music.

x

I can analyze composer's selection of
a specific tempo marking in a piece
of music.

x

I can justify composer's selection of a
specific tempo marking in a piece of
music.

x

I can define advanced tempo
markings in a choral work.

x

I can analyze advanced tempo
markings to apply to my performance
of a choral work.

x

I can justify the use of advanced
tempo markings in a choral work.

x

I can...

x

I can...

x

I can...

Indicator
CM.P
AL.5.4
I can identify technical, expressive, and
formal markings in my music.
Sample Learning Targets
x

, FDQ LGHQWLI\ ³2YHU WKH 5DLQERZ´ DV
AABA form and the effect on
performance expression.

Indicator
CM.P
AM.5.4
I can analyze the technical, expressive, and
formal markings in my music.
Sample Learning Targets
x

x
x

I can identify the need for
embellishments in the return of the A
section of a Da Capo aria.

x

I can...

x

Indicator
CM.P
AH.5.4
I can justify the technical, expressive, and
formal markings in my music.
Sample Learning Targets

I can explain the use of fugal form in
the Bach Magnificat and the effect on
performance expression
I can apply embellishments in the
return of the A section of a Da Capo
aria.

x

I can perform the Faure Requiem with
appropriate performance expression.

x

I can justify and perform
embellishments in the return of the A
section of a Da Capo aria.

, FDQ«

x

I can...

Page 191

Artistic Processes: Responding - I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Advanced
Low
Benchmark
CM.R
AL.6
I can analyze a composition or performance
and offer constructive suggestions for
improvement using provided criteria.
Indicator
CM.R
AL.6.1
I can identify forms used in varying cultures
and historical periods.
Sample Learning Targets

Advanced
Mid
Benchmark
CM.R
AM.6
I can analyze and critique compositions and
performances from a variety of genres,
cultures and time periods using personally
developed criteria.
Indicator
CM.R
AM.6.1

Advanced
High
Benchmark
CM.R
AH.6
I can justify my criteria for evaluating music
works and performances based on personal
and collaborative research.

I can describe characteristics of a variety of
musical forms.
Sample Learning Targets

I can justify my interpretation of a musical
work based on the elements of music.
Sample Learning Targets

x

I can explain the components of
sonata form.

x

I can explain the characteristics of
musical forms and how they are
similar and different.

x

I can describe how sonata form
differs in symphonic works versus
instrumental works.

x

I can describe how a particular form
appears in different genres of music.

x

I can compare and contrast
characteristics of a variety of musical
forms and describe the way these
forms appear in compositions from
varying genres and styles.

x

x

I can describe how various forms
have evolved over time.
I can...

x

Indicator
CM.R
AH.6.1

x

I can identify harmonic structure of
the music I hear and perform.

x

I can describe how harmonic
structure changes the mood of a piece
of music.

x

I can identify specific performance
decisions of different performers.

x

I can describe how performance
decisions highlight the form and
harmonic structure set forth by the
composer.

x

I can...

I can...
Page 192

Indicator
CM.R
AL.6.2
I can describe stylistic qualities of music from
different cultures and time periods.
Sample Learning Targets

Indicator
CM.R
AM.6.2
I can identify key signature changes and
modulations in relation to form.
Sample Learning Targets

x

I can listen to music from different
time periods and describe the
differences in their styles.

x

I can identify an instrumental,
expressive, and tonal quality that
makes music from a specific culture
unique.

x

I can explain how modulations and
tonality unify a musical work.

I can list qualities of music from
various historical periods.

x

I can...

x

x

I can explain how modulations affect
harmonic structure.

x

I can describe how a modulation
bridges between sections in sonata
form.

I can justify the performance decisions in a
variety of musical works.
Sample Learning Targets

x

x

I can justify my performance
decisions based on my analysis of the
elements of music and their use in
the appropriate historical period.
I can create a presentation based on
my analysis of the elements of music
and their use in a specific historical
period.

I can...

I can...

Indicator
CM.R
AL.6.3
I can describe stylistic qualities of music from
different historical periods and cultures and
offer suggestions for improvement of my
performance.
Sample Learning Targets
x

I can describe stylistic qualities of
music from the Medieval time period.

x

I can describe stylistic qualities of
music from the Irish culture.

x

x

Indicator
CM.R
AH.6.2

Indicator
CM.R
AM.6.3
I can describe stylistic qualities of music from
different historical periods and how it applies
to my instrument.
Sample Learning Targets

Indicator
CM.R
AH.6.3
I can justify my evaluation of musical works
from different historical periods and cultures
based on my personal and collaborative
research.
Sample Learning Targets

x

I can apply stylistic qualities of music
from the Medieval time period to my
performance

x

I can justify performance choices in a
Medieval chant based on my personal
and collaborative research.

x

I can apply stylistic qualities of Irish
music to my performance.

x

x

, FDQ«

x

I can justify my performance choices
in an Irish song based on my personal
and collaborative research.
I can...
Page 193

, FDQ«

Anchor Standard 7: I can evaluate music.
Advanced
Low
Benchmark
CM.R
AL.7
I can analyze performances and
compositions, offering suggestions for
improvement using provided criteria.
Indicator
CM.R
AL.7.1

Advanced
Mid
Benchmark
CM.R
AM.7
I can analyze and critique compositions and
performances using personally-developed
criteria.
Indicator
CM.R
AM.7.1

Advanced
High
Benchmark
CM.R
AH.7
I can justify my criteria for evaluating musical
works and performances based on personal
and collaborative research.
Indicator
CM.R
AH.7.1

I can communicate feedback for personal
performances and compositions.
Sample Learning Targets

I can analyze personal compositions and
provide criteria for improvement.
Sample Learning Targets

I can explain criteria used for evaluation.
Sample Learning Targets

x

I can listen to my recorded
performance using a technological
device.

x

I can provide criteria for improvement
based on composition rules by
developing a self-assessment rubric.

x

I can develop criteria for the
evaluation of music works and
performances.

x

I can complete a rubric and evaluate
my own composition.

x

x

I can explain how the criteria for the
evaluation of music works and
performances were developed.

x

I can formulate constructive feedback
for my own personal performances
using the elements of music as the
basis for criteria of evaluation.

I can participate in a Skype or
FaceTime with members of my choir,
interacting with another choir and
critiquing HDFK RWKHU¶V performance
of a similar song.

x

I can...

x

x

, FDQ«

I can...

Page 194

Indicator
CM.P
AL.7.2
I can present my evaluation of a formal or
informal performance.
Sample Learning Targets

Indicator
CM.P
AM.7.2
I can analyze performances and provide
criteria for improvement.
Sample Learning Targets

x

I can evaluate and present feedback
on the performance of others.

x

x

I can evaluate and present feedback
on the compositions of others.

x

x

I can formulate feedback rubric for
the performance of others using the
elements of music as the basis for
criteria of evaluation.

I can analyze personal performances
and those of others using a
technological device.
I can provide criteria for improvement
of my personal performances and
those of others by creating a rubric
based on the elements of music.

x

I can...

x

Indicator
CM.P
AH.7.2
I can justify artistic decisions used in
compositions and performances.
Sample Learning Targets
x

I can determine what artistic elements
are used in a music composition.

x

I can discuss why the artistic elements
are important to that particular piece
of music.

x

I can...

I can...

Artistic Processes: Connecting - I can connect musical ideas and works to personal experience, careers,
culture, history, and other disciplines.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Advanced
Low
Benchmark
CM.C
AL.8
I can examine contemporary musical works to
determine the influence of historical and
cultural traditions.

Advanced
Mid
Benchmark
CM.C
AM.8
I can analyze a diverse repertoire of music
from a cultural or historical time period.

Advanced
High
Benchmark
CM.C
AH.8
I can examine and perform music based on
historical and cultural contributions.

Page 195

Indicator
CM.C
AL.8.1
I can explain specific cultural and historical
traditions and infuse these ideas into my
music.
Sample Learning Targets

Indicator
CM.C
AM.8.1
I can select musical elements in
contemporary music that reflect cultural and
historical influences.
Sample Learning Targets

x

I can recognize historical and cultural
influences in George Frederic
+DQGHO¶V classical music.

x

I can research the Classical elements
of music IRXQG LQ -RKQ 5XWWHU¶V ³)RU
WKH %HDXW\ RI WKH (DUWK ´

x

I can scat several measures of a jazz
piece and discuss why I made specific
choices in my improvisation.

x

I can listen for specific music
elements in a choral work and decide
which composer and/or time period
the piece represents.

x

, FDQ«
x

, FDQ«

Indicator
CM.C
AH.8.1
I can use historical and cultural contributions
to justify my musical choices.
Sample Learning Targets
x

I can justify why a particular octavo
based on an American folk song
VXFK DV ³6KHQDQGRDK´ is often
chosen for All-State competition
(using musical terms such as rhythm,
melody, dynamics, form, tempo,
etc.)

x

I can justify the historical and cultural
contributions of the American
Spiritual.

x

I can...

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Advanced
Low
Benchmark
CM.C
AL.9
I can apply concepts among arts disciplines
and other content areas to choral music and
analyze how my interests and skills will
prepare me for a career.

Advanced
Mid
Benchmark
CM.C
AM.9
I can explain how economic conditions,
cultural values and location influence music
and the need for music related careers.

Advanced
High
Benchmark
CM.C
AH.9
I can research societal political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a musician.

Page 196

Indicator
CM.C
AL.9.1
I can explain ideas from other arts disciplines
and content areas through music.
Sample Learning Targets

Indicator
CM.C
AM.9.1
. I can explain how my artistic choices are
influenced by cultural and social values.
Sample Learning Targets

x

I can analyze the lyrics of a choral
work and find a painting or drawing
which reflects the lyrics.

x

I can explain the connections between
the Vietnam War, political/social
unrest and the music of the era.

x

I can research the Flamenco and the
use of castanets in the dance and
SHUIRUPDQFH RI ³(O 9LWR´ E\ -RGL
Jensen.

x

I can explain the connections between
the poetry of Langston Hughes, the
art of William H. Johnson and Jazz
music.

, FDQ«

x

x

Indicator
CM.C
AH.9.1
I can analyze complex ideas that influence my
artistic perspective and creative work.
Sample Learning Targets
x

I can analyze conflict and resolution
during different time periods of the
United States and the effect on
popular music.

x

I can analyze the effect of mass
genocide and the performance of the
SLHFH ³3UD\HU RI WKH &KLOGUHQ´ E\
Kurt Bestor.

x

I cDQ«

I can...

Page 197

Indicator
CM.C
AL.9.2

Indicator
CM.C
AM.9.2

Indicator
CM.C
AH.9.2

I can describe traditional and emerging
careers in music.

I can pursue opportunities that will lead me to
a career in music.

I can research my personal career choices in
the arts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify the skills, training, and
education necessary to pursue a career
in music education.

x

I can conduct a survey comparing the
number of arts careers available in
rural, suburban and urban areas.

x

I can identify the skills, training and
education necessary to pursue a career
in music therapy.

x

I can job shadow a professional in the
music business industry.

x

I can identify college programs in
choral music and compare them to
other music degrees.

x

, FDQ«

x

I can...

x

I can conduct a survey, (using my
digital device), comparing the number
of arts careers available in a particular
region of a country to arts careers
available in my community.

x

I can personally interview or
Skype/FaceTime local and national
leaders in the music industry.

x

, FDQ«

Page 198

Choral Music Glossary
A Cappella Choral or vocal music performed without instrumental accompaniment.
AB Binary Form The most basic musical form in which two contrasting sections are present.
ABA Ternary Form A basic musical form consisting of three sections (A, B, and A), the third section being virtually identical to the
first. If it is exactly identical, the third section often is not written out, the performer simply being directed to repeat the first
section (usually marked da capo or D.C.), as in the da capo aria and minuet or scherzo with trio.
Accelerando Gradual increase of speed.
Accidentals A note of a pitch (or pitch class) that is not a member of the scale or mode indicated by the most recently applied key
signature, including the sharp ( ), flat (

), and natural (I) symbols.

Accompaniment The additional but subordinate music used to support a melodic line.
Adagio Slow, leisurely, or solemn tempo.
Allegro Bright, cheerful or lively tempo.
Alto Second-highest vocal range.
Andante Moving along, flowing, at a walking pace.
Aria A long, accompanied song for a solo voice, typically one in an opera or oratorio.
Arrangement/Arrange Composition based on existing music (e.g., scoring for voices not used in the original piece, adding a
percussion part to the original.
Articulation The manner or style in which the notes in a piece of music are sung.
Augmentation A melody, theme, or motif is presented in longer note-values than were previously used.
Aural By ear; without reference to or memorization of written music.
Page 199

Baroque A genre of classical music of c. 1600-1750 which included composers such as Bach, Handel, and Vivaldi and has a heavy use
of counterpoint and polyphony and conveyed drama and elaborate ornamentation.
Bass Lowest vocal range.
Bass Clef A symbol located at the beginning of a staff to indicate the pitches of the notes places on the lines and spaces below middle
C.
Beat A main accent or rhythmic unit in music.
Bel Canto A lyrical style of operatic singing using a full rich broad tone and smooth phrasing.
Blend (Vowel Alignment) The combination of voices in group singing so that individual performers are indistinguishable.
Blues A kind of jazz that evolved from the music of African-Americans, especially work songs and spirituals, in the early 20th century.
Breath Support Efficient and appropriate use of the breath stream for phonation.
Cadence A sequence of notes or chords comprising the close of a musical phrase.
Cadenza A virtuoso solo passage inserted into a movement in a concerto or other work, typically near the end.
Call and Response The alternation of musical phrases between groups of musicians.
Canon/Round A composition for two or more voices in which one voice enters after another in exact imitation of the first.
Cantabile In a smooth, singing style.
Chant To recite musically.
Choir A group of singers who usually sing in parts with several voices on each part.
Chorale A musical composition (or part of one) consisting of or resembling a harmonized version of a simple, stately hymn tune.
Chord Three or more pitches sounded simultaneously or functioning as if sounded simultaneously.
Chord Progression (or harmonic progression)A series of musical chords.
Page 200

Circle of Fifths The relationship among the 12 tones of the chromatic scale, their corresponding key signatures, and the associated
major and minor keys.
Classical (1) Music written in the European tradition during a period lasting approximately from 1750 to 1830. (2) Homophonic
texture, or an obvious melody with accompaniment. Melodies that tend to be almost voice-like and singable, allowing
composers to actually replace singers as the focus of the music.
Con Brio Performed with vigor.
Closed Score A musical score in which two or more parts are put on the same staff.
Complementary Keys Keys sharing many common tones.
Compose To create a musical work or idea.
Composer A person who creates a musical work or idea.
Composition A musical work.
Compound Meter Beats are divided into three notes.
Concerto A musical composition for a solo instrument or instruments accompanied by an orchestra, especially one conceived on a
relatively large scale.
Contemporary Music that can be understood as belonging to the period that started in the mid-1970s to early 1990s, which includes
modernist, postmodern, neo-romantic, and pluralist music. However, the term may also be employed in a broader sense to refer
to all post-1945 musical forms.
Count-Singing A choral rehearsal technique that involves singing the correct pitches, but replacing the lyrics with each note's position
within a measure.
Crescendo A gradual increase in the loudness of a sound or section of music.
Cut Time A meter with two half-note beats per measure.
Decrescendo A gradual decrease in the loudness of a sound or section of music.
Page 201

Diaphragmatic Breath Abdominal breathing, belly breathing or deep breathing; breathing that is done by contracting the diaphragm, a
muscle located horizontally between the thoracic cavity and abdominal cavity. Air enters the lungs and the belly expands during
this type of breathing.
Diction Enunciation. The clarity with which words are spoken or sung.
Diminution A melody, theme, or motifs presented in shorter note-values than were previously used.
Dolce Played in a tender, adoring manner; to play sweetly with a light touch.
Duple Meter A primary division of 2 beats to the bar, usually indicated by 2 and multiples (simple) or 6 and multiples (compound) in
the upper figure of the time signature.
Dynamics The loudness or softness of music.
Eighth A note/rest having the time duration of one eighth of the time duration of a whole.
Elements of Music The fundamental characteristics that make up a piece of music: rhythm, dynamics, pitch, harmony, tone color,
texture, form.
Embellishments A group of notes or a single note added to a basic melody as ornamentation.
Ensemble A group of musicians that perform as a unit. 2-part-ensemble is divided into two parts, frequently SA.3-part-ensemble is
divided into three parts, frequently SSA or SAB4-part-ensemble is divided into four parts, frequently SATB
Expression The art of playing or singing music with emotional communication. The elements of music that comprise expression
include dynamic indications, such as forte or piano, phrasing, differing qualities of timbre and articulation, color, intensity,
energy and excitement.
Fermata A symbol of musical notation indicating that the note should be prolonged beyond its normal duration or note value would
indicate.
Folk Song A song originating among the people of a country or area, passed by oral tradition from one singer or generation to the next,
often existing in several versions, and marked generally by simple, modal melody and stanzaic, narrative verse.
Page 202

Four-part chorale style Music written for four voices (SATB).
Form The structure or organization of a musical phrase or composition.
Forte (f) Loud.
Fortissimo (ff) Very loud.
Fragmentation The use of fragments or the "division of a musical idea (gesture, motive, theme, etc.) into segments." It is used in tonal
and atonal music, and is a common method of localized development and closure.
Fugue (fugal form) A contrapuntal composition in which a short melody or phrase (the subject) is introduced by one part and
successively taken up by others and developed by interweaving the parts.
Genre A type or style of music; an established form of musical composition such as a ballad, concerto, folk music, lullaby, march and
spiritual.
Grazioso Played in a graceful, smooth, or elegant in style.
Half A note/rest that has half the duration of a whole.
Harmony/Harmonic (1) The pattern of intervals and chords in a composition. (2) The ways in which chords and intervals are related
to one another and the ways in which one interval or chord can be connected to another. Adjective form, harmonic.
Head Voice A clear, open tone that resonates in the head and not in the throat or chest.
Historical/Cultural Context Music containing characteristics popular of a particular time-period or geographical region.
Improvisation/Improvise The creation of music in the course of performance. Verb form, improvise.
Interpretation Decoding motivations behind musical structures and the ways in which listeners and performers understand musical
works and practices.
Interval The distance between two pitches.
Intonation The proper production of a musical tone so that it is played or sung in tune.
Page 203

Jazz A type of music of black American origin characterized by improvisation, syncopation, and usually a regular or forceful rhythm,
emerging at the beginning of the 20th century.
Key Signature The sharp, flat, or natural signs placed at the beginning of a staff indicating the tonality of the composition.
Kodaly +DQG VLJQDOV DUH XVHG WR VKRZ WRQDO UHODWLRQVKLSV 7KH PRYHDEOH ³GR´ LV SUDFWLFHG 7KH PXVLFDO PDWHULDO HPSKDVL]HG LV WKH
mother-tongue folksong. Uses rhythm syllable "Ta" for quarter notes and "Ti-Ti" for eighth notes.
Legato In a smooth, flowing manner, without breaks between notes.
Loop A repeating section of sound material. Short sections of material can be repeated to create ostinato patterns.
Major A mode based on a scale consisting of a series of whole steps except for half steps between the third & fourth and seventh &
eighth degrees.
Marcato Played with emphasis.
Mass A choral composition that sets the invariable portions of the Eucharistic liturgy to music.
Melodic A repetitive pattern that can be used with any scale.
Melodic Direction The quality of movement of a melody, including nearness or farness of successive pitches or notes in a melody.
Melody A succession of tones comprised of mode, rhythm, and pitches so arranged as to achieve musical shape, being perceived as a
unity by the mind.
Meter The way beats of music are grouped, often in sets of two or three.
Mezzo Forte (mf) Medium loud.
Mezzo Piano (mp) Medium soft.
Minor A scale having half steps between the second and third, fifth and sixth, and seventh and eighth degrees, with whole steps for the
other intervals.
Modulation Changing from one key (tonic, or tonal center) to another.
Page 204

Motive A short tune or musical figure that characterizes and unifies a composition. It can be of any length but is usually only a few
notes long. A motive can be a melodic, harmonic or rhythmic pattern that is easily recognizable throughout the composition.
Musical Idea Musical fragment or succession of notes.
Non-Traditional Notation A word, place, character, or object regarded as typifying or representing something.
Notation/Notate A system used for writing down music showing aspects of music tones such as the tones to be sounded (pitch), the
time in (dynamics) at which the tone should be played. Verb form, notate.
Note A symbol used to represent the duration of a sound and, when placed on a music staff, to also indicate the pitch of the sound.
Octavo A generic term for sheet music (typically in the form of a booklet) containing a short choral work.
OpenScore A musical choral or orchestral score in which each part has a staff to itself.
Oratorio A large-scale musical work for orchestra and voices, typically a narrative on a religious theme, performed without the use of
costumes, scenery, or action.
Ostinati- Short music patterns that are repeated persistently throughout a performance, composition, or a section of one. (Singular
form, ostinato.)
Partner Songs-Two or more different songs that are performed at the same time to create harmony.
Passage A musical idea that may or may not be complete or independent.
Patsch Patting either the left, right, or both thighs with the hands.
Pattern A repetitive sequence.
Pentatonic A scale made up of five tones (usually do, re, mi, so and la).
Percussion Family of instruments in which sound arises from the striking of materials with sticks, hammers, or the hands.
Permutation Any ordering of the elements of a set.
Phrase A division or section of a musical line, somewhat comparable to a clause or sentence in language.
Page 205

Phrasing (1) A short musical idea similar to a sentence in spoken language; also a style of performance that gives shape to the musical
phrases. (2) The grouping of consecutive melodic notes, both in their composition and performance.
Pianissimo (pp) Very soft.
Piano (p) Soft.
Pitch (1) The property of a musical tone that is determined by the frequency of the sound waves creating it. (2) The highness or
lowness of a tone.
Plagal Cadence A chord progression where the subdominant chord is followed by the tonic chord (IV-I).
Posture The position of the body for singing. Chin parallel to the floor, shoulders back and down with chest held high. Abdomen flat
and firm, held in an expandable position. Hands relaxed and still at the sides. Knees flexibly loose and never locked. Feet flat on
the floor and shoulder-width apart. Weight of the body should be balanced on both feet and body held slightly forward.
Prestissimo Very fast.
Quarter (1)A note/rest having the time duration of one fourth of the time duration of a whole. (2)An ensemble of four performers.
Range The scope of notes that a voice can produce.
Recapitulation A part of a movement (especially one in sonata form) in which themes from the exposition are restated.
Recitative Musical declamation of the kind usual in the narrative and dialogue parts of opera and oratorio, sung in the rhythm of
ordinary speech with many words on the same note.
Repeat Reiteration of a tone at the same pitch level.
Repertoire A selection of musical pieces that an ensemble or performer knows or is prepared to perform.
Rest A symbol standing for a measured break in the sound with a defined duration.
Retrograde Reverses the order of the motive's pitches: what was the first pitch becomes the last, and vice versa.
Rhythm The systematic arrangement of musical sounds, principally according to duration and periodic stress.
Page 206

Rhythmic A set of beats and rests that defines the tempo and pace of a musical piece.
Ritardando Gradual decrease of speed.
Scale A group of notes (or pitch-classes) arranged sequentially, rising or falling.
Score A written or printed representation of a musical work.
Section A complete, but not independent musical idea.
Sforzando With sudden emphasis.
Sixteenth A note having one sixteenth the time value of a whole note.
Skip/Leap Any interval larger than a whole tone or whole step.
Soft Palate The fleshy, flexible part toward the back of the roof of the mouth. Lifting the soft palate reduces nasality in singing and
produces a more open tone.
Solfege A system that uses distinct syllables to identify the various notes of a scale:do, re, mi, fa, so, la, ti, do.
Solo A single performer or a passage that is to be performed by a single performer.
Sonata A composition for an instrumental soloist, often with a piano accompaniment, typically in several movements with one or more
in sonata form.
Soprano Highest vocal range.
Spiritual A religious folk song of African-American origin.
Staccato A dot above the note indicating that the note thus marked should be shortened to half its written length, the second half
replaced with silence.
Step Dynamics Phrases or sections of music increase or decrease volume by steps in a piece of music (pp-p-mf-f).
Step An interval of a second.
Page 207

Style 7KH FRPSRVHU¶V PDQQHU RI WUHDWLQJ WKH YDULRXV HOHPHQWV WKDW PDNH XS D FRPSRVLWLRQ DV ZHOO DV WKH SHUIRUPHU¶V PDQQHU RI
presenting the composition.
Syllables/Sight-Reading System A method of musical training involving both ear training and sight singing.
Syncopation To put stress on a normally unstressed beat.
Takadimi The beat is always called ta. Insimple meters, the division and subdivision are always ta-di and ta-ka-di-mi. Any note value
can be the beat, depending on thetime signature. In compound meters (wherein the beat is generally notated withdotted notes),
the division and subdivision are always ta-ki-da and ta-va-ki-di-da-ma.
Tempo (1) A steady succession of units of rhythm; the beat. (2) The speed at which a piece of music is performed or is written to be
performed.
Tenor A singing voice between baritone and alto, the highest of the ordinary adult male range.
Tenuto A note or chord held for its full time value or slightly more
Tessitura The general range of a melody or voice part; specifically, the part of the register in which most of the tones of a melody or
voice part lie.
Texture The number and relationship of musical lines in a composition.
Time (Meter) Signature Notation to specify how many beats (pulses) are to be contained in each bar and which note value is to be
given one beat.
Tonality The use of a central note, call the tonic, around which the other tonal material of a composition is built and to which the music
returns for a sense of rest and finality.
Tone Cluster A musical chord comprising at least three adjacent tones in a scale.
Tone Color/Timbre/Quality(1) The blend of overtones that distinguish a note played on a flute, for example, from the same note
played on a violin. (2) The distinctive tone quality of a particular musical instrument or voice. (3) the character of musical tones
with reference to their richness or perfection.
Page 208

Tonic Triad A chord of three notes, the lowest note being the tonic of the key, the middle note being the third tone of the key, and the
top note being the fifth tone of the key.
Traditional Notation Music written on one or more staves, using traditional note symbols and clefs to indicate pitch locations and
durations.
Transpose To reproduce in a different key, by raising or lowering in pitch.
Treble Clef A symbol located at the beginning of a staff to indicate the pitches of the notes placed on the lines and spaces above
middle C.
Trio An ensemble of three performers.
Triplets Three notes of equal length that are performed in the duration of two notes of equal length.
Triple Meter A primary division of 3 beats to the bar, usually indicated by 3 (simple) or 9 (compound) in the upper figure of the time
signature.
Unison Two or more musical parts sounding the same pitch or at an octave interval.
Variations Formal technique where material is repeated in an altered form. The changes may involve harmony, melody, counterpoint,
rhythm, timbre, orchestration, or any combination of these.
Verse and Refrain The verse section of the song is the section in which different sets of words are sung to the same repeated melody
and contrasts with a refrain, where the words and melody are both repeated.
Vocal Inflection Alteration in pitch or tone of the voice.
Vocalist A singer, typically one who regularly performs with a jazz or pop group.
Vocalize A vocal exercise that is sung without words, typically using different vowel sounds.
Vocal Score Music score of a vocal or choral composition written for orchestral accompaniment, such as an oratorio or cantata. In a
piano-vocal score, the vocal parts are written out in full, but the accompaniment is reduced and adapted for keyboard (usually
piano).
Page 209

Vocal Skills/Technique The abilities that allow a musician or group of musicians to perform with a refined degree of phrasing,
dynamics and style.
Vocal Sound Source Sound created by a voice.
Voicing The manner in which one distributes, or spaces, notes and chords among the various voice parts.
Whole Note The longest note/rest duration in music.

Page 210

References
Arizona Department of Education. (2006). Arizona Music Standards-2006 [PDF document]. Retrieved from
https://www.azed.gov/standards-practices/files/2011/09/music.pdf
Colorado Department of Education. (2009). Music Academic Standards. Retrieved from
http://www2.cde.state.co.us/scripts/allstandards/COStandards.asp?stid=5&glid=0&pgcid=60
Georgia Department of Education. (2009). Georgia Performance Standards Fine Arts ± Music Education [PDF document]. Retrieved
from https://www.georgiastandards.org/standards/gps%20support%20docs/fine-arts-music-gps.pdf
Kelly, S., Williams, D., Oliverio, J., Zack, J., Greenberg, E., Millwater, D., . . . Gordon, M. (2014) RTT Performing Fine Arts
Assessment Project. Retrieved fromhttps://cfaefl.org/assessmentproject/InnerPage.aspx?ID=6
Mississippi Department of Education. (2003). Mississippi Visual and Performing Arts Framework. Retrieved from
http://www.mde.k12.ms.us/docs/curriculum-and-instructions-library/music.pdf?sfvrsn=2
Missoula County Public Schools. Advanced Chamber Ensemble 10, 11, 12 [PDF document]. Retrieved from
http://www.mcpsmt.org/cms/lib03/MT01001940/Centricity/domain/825/music/Advanced%20Chamber%20Ensemble.pdf
National Association of Music Education. (2014). Music Standards Ensemble [PDF document]. Retrieved from
http://www.nafme.org/wp-content/files/2014/11/2014-Music-Standards-Ensemble-Strand.pdf
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
2¶&RQQRU .
A Practical Guide to Vocal Development. Retrieved from http://singwise.com/cgibin/main.pl?section=articles&doc=TipsForPracticingSinging
Overstreet, J., & Ware, M. (2009, Feb 12). Rehearsal Strategies for High School Choirs [PDF document]. Retrieved from
http://foundationformusiceducation.org/wp-content/uploads/2010/08/Rehearsal-Strategies-For-High-School-Choirs.pdf
Public Schools of North Carolina State Board of Education: (North Carolina Arts Education, 2010). Arts Education Instructional
Support Tools. Retrieved from http://ances.ncdpi.wikispaces.net/K-12+Instructional+Tools
Page 211

Public Schools of North Carolina State Board of Education. (n.d.). North Carolina Essential Standards [PDF document]. Retrieved
from http://www.dpi.state.nc.us/docs/acre/standards/new-standards/arts/music/9-12.pdf
Public Schools of North Carolina State Board of Education. (n.d.). North Carolina Essential Standards [PDF document]. Retrieved
from http://www.dpi.state.nc.us/docs/acre/standards/new-standards/arts/music/k-8.pdf
South Carolina American Choral Directors Association. (2016). Elementary, Middle, High School All State Choir. Retrieved from
http://www.scacda.com/
South Carolina Department of Education. (2010). South Carolina Academic Standards for General Music. Retrieved from
http://ed.sc.gov/scdoe/assets/file/agency/ccr/Standards-Learning/documents/AcademicStandards2010DraftGeneralMusic.pdf
South Carolina Department of Education. South Carolina Academic Standards for the Visual and Performing Arts [PDF document].
Retrieved from https://ed.sc.gov/scdoe/assets/file/agency/ccr/StandardsLearning/documents/AcademicStandardsforChoralMusic.pdf
South Carolina Department of Education. (2015). South Carolina College- and Career-Ready Standards for English Language Arts.
Retrieved from http://ed.sc.gov/scdoe/assets/file/programsservices/59/documents/ELA2015SCCCRStandards.pdf
South Carolina Music Educators Association. (2016). High School Choral Performance Assessment. Retrieved from
http://www.scmea.net/divisions/choral-division/hscpa/
State Education Agency Directors of Arts Education: (Music Technology Strand, 2014). National Core Arts Standards. Retrieved from
http://www.nationalartsstandards.org

Page 212

# Genoral Music
## Introduction

The new South Carolina Visual and Performing Arts Standards provide proficiency levels for student expectations. It should be noted
that the nature of general music within a proficiency model demands a balance of each of the artistic processes to provide the South
Carolina student with a comprehensive arts education in music. It should also be noted that further study in choral and instrumental
music is essential to cRPSOHWH RQH¶V PXVLF HGXFDWLRQ H[SHULHQFH DV ZHOO DV VWXG\ LQ PXVLF WKHRU\ FRPSRVLWLRQ DQG PXVLF KLVWRU\ This
proficiency approach to learning music provides a continuum of artistic processes and skills that assist to prepare students from a
novice (beginning) level to college- and career-ready (advanced high) level and builds state and national capacity to improve
knowledge and competencies of future adults as musicians and consumers. Equally, it answers the growing need for the critical skills of
higher order thinking and cultural competencies for relationship building in a safe and authentic way²a keystone for success in global
endeavors and diverse social environments.
Given the research of the importance and contributions of music to the cognitive development (MacDonald, Kreutz, & Mitchell, 2012),
and the positive correlations for student growth (Catterall, 2009), music is a necessary subject for the 21st century. Given the economic
impact of the arts in South Carolina, music should also be viewed as a viable career option IRU 6RXWK &DUROLQD¶V VWXGHQWV +RZHYHU
learners begin new music experiences at different ages and progress toward proficiency at different rates. While the amount of quality
time spent in each course of study is a determining factor in the proficiency level that learners will reach, learners at similar ages still
frequently demonstrate varying proficiency levels. Acknowledging this continuum permits flexibility in how students will
developmentally progress through the artistic processes. The South Carolina Standards for General Music outlines the progression of
learner skills, which makes LW HDVLHU IRU WHDFKHUV WR LGHQWLI\ D OHDUQHU¶V VNLOO OHYHO DQG WR GLIIHUHQWLDWH OHDUQLQJ IRU DOO OHDUQHUV
Demonstrating proficiency in music has potential benefits for learners. Musical knowledge and skill proficiency can be documented
through a variety of assessments and skills to be transferred directly to a career path. Colleges and universities often require at least one
credit of music for entrance. Research supports that prolonged and sustained study in the arts produces not only better artists and
critical thinkers, but also greater contributors to society as a whole (Catterall, 2009).
The rationale for the creation of this document stems from the need to provide a more transparent, learner friendly document that
clearly describes benchmarks for learners at various stages, provide pathways for meeting these benchmarks, and suggest possible
Page 214

strategies for a learning approach. Its intent is to recognize that everyone can learn music, to motivate learning, to increase achievement
through goal setting and self-assessment, and to facilitate building functional knowledge of the role of music in a global society. The
artistic processes, standards, benchmarks, indicators, and learning targets are meant to guide learning and should be shared with
learners and made available to parents and other stakeholders.
The four artistic processes of Creating, Responding, Performing, and Connecting provide the framework for the standards that
represent a comprehensive K±12 course of study and are interconnected and aligned with the National Core Arts Standards. This
document also acknowledges the varied resources available throughout the state and provides possible strategies to meet the standard at
each level of proficiency. The indicators can be viewed as units of study that support the standards. It should be noted that the learning
targets are examples of appropriate lesson material to address the indicators leading to the benchmarks and are meant to serve as
possible suggestions for the construction of lessons. The use of technology as a strategic tool through musical apps, digital recorders,
computers, interactive boards, stereos, keyboards, phones, and Smart TV devices can enhance learning, increase engagement, and
should be a part of the instructional toolbox for addressing the general music standards. The standards document helps motivate
learning by showing how to set achievable goals, self-assess, and chaUW SURJUHVV E\ XVLQJ ³, FDQ´ VWDWHPHQWV WKDW IDFLOLWDWH WKLV SURFHVV
Learners take ownership of their individual development as a musician. The document guides the facilitation of music learning toward
more functional, interactive, and culturally diverse processes. It provides examples of learning targets that can measure student growth
at a defined proficiency level and describes the standard and indicator in terms of individual lessons. This document provides a clearer
understanding of what learners need to know and be able to do to move from one level to the next. The ultimate goal of general music
is to provide foundational support for the development of each student as a musician and leads them to continued participation in choral
and/or instrumental music, as well as expanding their knowledge and interest as composers, theorists, and consumers of music.

Page 215

General Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
GM.CR
NL.1

Benchmark
GM.CR
NM.1

Benchmark
GM.CR
NH.1

Benchmark
GM.CR
IL.1

Benchmark
GM.CR
IM.1

Benchmark
GM.CR
IH.1

Benchmark
GM.CR
AL.1

Benchmark
GM.CR
AM.1

Benchmark
GM.CR
AH.1

I can imitate
a musical
statement by
sight and
sound.

I can answer
a musical
question.

I can
arrange a
musical idea.

I can
compose a
rhythmic and
melodic
phrase.

I can add
harmony to
compose or
arrange
phrases for a
given mood.

I can arrange,
compose, and
explain intent
using
melody,
rhythm, and
harmony.

I can
collaborate
with others to
compose or
arrange a
variety of
musical
styles.

I can compose
music within
expanded
forms.

Indicator
GM.CR
NL.1.1
I can match
sound and
pattern.

Indicator
GM.CR
NM.1.1
I can identify
simple
forms.

Indicator
GM.CR
NH.1.1
I can use
rhythm
patterns,
songs or
words to
create a
musical idea.

I can
combine
musical ideas
to create
phrases for
voice,
instruments,
or body
movement.
Indicator
GM.CR
IL.1.1
I can explain
the use of
ostinato to
arrange a
melodic idea.

Indicator
GM.CR
IM.1.1
I can
organize
rhythmic and
melodic
patterns into
a musical
phrase.

Indicator
GM.CR
IH.1.1
I can identify
key
signatures in
melodic
phrases.

Indicator
GM.CR
AL.1.1
I can use the
circle of
fifths to
explain
transposition
of a written
musical
work.

Indicator
GM.CR
AM.1.1
I can revise a
composition
based on the
feedback
from others
to improve
composed
works.

Indicator
GM.CR
AH.1.1
I can create an
original
composition
independently.

Page 216

Indicator
GM.CR
NL.1.2
I can imitate
a given music
rhythm or
sound using
symbols.

Indicator
GM.CR
NM.1.2
I can identify
same and
different
patterns.

Indicator
GM.CR
NH.1.2
I can create a
musical idea
given
specific
instructions.

Indicator
GM.CR
IL.1.2
I can
construct
arrangements of
simple pieces
for voices or
instruments.

Indicator
GM.CR
IM.1.2
I can create a
melodic
phrase over a
given
rhythmic
idea.

Indicator
GM.CR
IH.1.2
I can
construct a
rhythmic,
melodic and
harmonic
idea for a
given mood.

Indicator
GM.CR
AL.1.2
I can use and
explain
compositional
techniques
to compose
works in a
music form.

Indicator
GM.CR
AM.1.2
I can work
with others to
compose an
original
composition.

Indicator
GM.CR
AH.1.2
I can create a
new
arrangement
from a given
composition.

Benchmark
GM.CR
IM.2
I can
improvise a
rhythm
pattern to
embellish a
given a
harmonic
phrase.
Indicator
GM.CR
IM.2.1
I can
embellish a
bass line
with
improvised
rhythm from
an instrument
or music
software.

Benchmark
GM.CR
IH.2
I can
improvise a
simple
melodic
phrase given
a harmonic
phrase.

Benchmark
GM.CR
AL.2
I can perform
a brief
improvisatio
n given a
chord
progression
and meter.

Benchmark
GM.CRAM.2

Benchmark
GM.CR
AH.2
I can perform
and refine an
extended
spontaneous
improvisation
independently

Indicator
GM.CR
IH.2.1
I can identify
chord
changes.

Indicator
GM.CR
AL.2.1
I can
improvise
harmonizing
parts.

Anchor Standard 2: I can improvise music.
Benchmark
GM.CR
NL.2
I can imitate
simple
rhythm
patterns
within a
given meter.

Benchmark
GM.CRNM.2

Benchmark
GM.CRNH.2

I can imitate
simple tonal
patterns
within a
given key
and tonality.

I can
improvise
responses to
given
rhythmic
patterns.

Indicator
GM.CR
NL.2.1
I can identify
same and
different
rhythms
patterns.

Indicator
GM.CR
NM.2.1
I can identify
same and
different
melodic
patterns.

Indicator
GM.CR
NH.2.1
I can
improvise
simple
ostinati
patterns
within a
given meter.

Benchmark
GM.CR
IL.2
I can
improvise
short melodic
question and
answer
patterns.

Indicator
GM.CR
IL.2.1
I can
improvise
simple tonal
patterns
within a
given key.

I can perform
an
improvisatio
n given a
motive,
chord
progression,
and meter.
Indicator
GM.CR
AM.2.1
I can perform
an
improvisation on a
given
motive.

Indicator
GM.CR
AH.2.1
I can
improvise
responding to
aural cues.

Page 217

Indicator
GM.CR
NL.2.2
I can echo
simple
rhythm
patterns.

Indicator
GM.CR
NM.2.2
I can echo
simple tonal
patterns.

Indicator
GM.CR
NH.2.2
I can
improvise
rhythm
patterns,
songs or
chants to
create a
musical idea.

Indicator
GM.CR
IL.2.2
I can
improvise
rhythmic and
melodic
patterns to
create a
musical
phrase.

Indicator
GM.CR
IM.2.2
I can sing on
a neutral
syllable an
improvised
rhythm.

Indicator
GM.CR
IH.2.2
I can
embellish a
given
melodic
phrase that
corresponds
with simple
chord
changes.

Indicator
GM.CR
AL.2.2
I can
improvise
short
melodies
using
accurate and
consistent
style, meter,
and tonality.

Indicator
GM.CR
AM.2.2
I can
improvise
extended
passages
using
consistent
style, meter,
and tonality.

Indicator
GM.CR
AH.2.2
I can
demonstrate
and refine
musicality
during
improvisational solos.

Artistic Processes: Performing - I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Benchmark
GM.PNL.3
I can use my
voice in
many ways.

Benchmark
GM.P
NM.3
I can use my
singing
voice.

Benchmark
GM.P
NH.3
I can sing,
chant, and
move to
demonstrate
a steady beat.

Benchmark
GM.P
IL.3
I can sing a
variety of
simple part
songs.

Benchmark
GM.P
IM.3
I can sing
with
expression
and technical
accuracy.

Benchmark
GM.P
IH.3
I can sing a
variety of
songs with
expression
and technical
accuracy.

Benchmark
GM.P
AL.3
I can sing
expressively
and apply
technical and
stylistic
criteria in a
variety of
songs alone
and in
various
ensembles.

Benchmark
GM.P
AM.3
I can
collaborate
with others
make
technical and
stylistic
decisions.

Benchmark
GM.P
AH.3
I can make
technical and
stylistic
choices about
my
performance
as a singer
alone and in
various
ensembles.

Page 218

Indicator
GM.P
NL.3.1
I can sing
songs in my
range.

Indicator
GM.P
NM.3.1
I can match
pitch when I
sing.

Indicator
GM.P
NH.3.1
I can sing or
move using a
steady beat.

Indicator
GM.P
IL.3.1
I can sing 2part songs.

Indicator
GM.P
NL.3.2
I can use my
voice to
imitate other
sounds.

Indicator
GM.P
NM.3.2
I can sing
with my
head voice
and chest
voice.

Indicator
GM.P
NH.3.2
I can use
good posture
and breath
support when
I sing.

Indicator
GM.P
IL.3.2
I can sing
with
appropriate
diction and
articulation.

Indicator
GM.P
IM.3.1
I can sing
with proper
intonation
alone and in
different
ensembles.
Indicator
GM.P
IM.3.2
I can apply
dynamics
and
expression
when I sing.

Indicator
GM.P
IH.3.1
I can sing
phrasing
while
responding to
D GLUHFWRU¶V
cues.
Indicator
GM.P
IH.3.2
I can sight
read in
multiple
tonalities and
rhythms.

Indicator
GM.P
AL.3.1
I can sing in
a group with
balance.

Indicator
GM.P
AL.3.2
I can blend
with others in
an ensemble.

Indicator
GM.P
AM.3.1
I can sing in
ensembles.

Indicator
GM.P
AH.3.1
I can use a
variety of
technical and
stylistic
choices in my
performance.
Indicator
Indicator
GM.P
GM.P
AM.3.2
AH.3.2
I can rehearse I can apply a
with an
variety of
ensemble to
musical
improve my
choices for
work.
performance.

Anchor Standard 4: I can play instruments alone and with others.
Benchmark
GM.P
NL.4
I can make
sounds with
classroom
instruments
and other
sound
sources.

Benchmark
GM.P
NM.4
I can imitate
short
rhythmic
patterns.

Benchmark
GM.P
NH.4
I can play
and read
rhythmic,
melodic, and
chord
patterns.

Benchmark
GM.P
IL.4
I can play
accompaniments and
simple songs
on classroom
instruments.

Benchmark
GM.P
IM.4
I can play
and read
complimentary,
contrasting
instrumental
parts
accurately,
and
independently.

Benchmark
GM.P
IH.4
I can play
and read my
part with an
ensemble
using
accurate
technique
and posture.

Benchmark
GM.P
AL.4
I can play an
instrument
expressively
and apply
technical and
stylistic
techniques
in variety of
music alone
and in
various
ensembles.

Benchmark
GM.P
AM.4
I can
collaborate
with others to
apply
technical and
stylistic
techniques
in a variety
of music
alone and in
various
ensembles.

Benchmark
GM.P
AH.4
I can make
technical and
stylistic
choices about
my
performance
as an
instrument-a
list alone and
in various
ensembles.

Page 219

Indicator
GM.P
NL.4.1
I can use my
body to make
sounds.

Indicator
GM.P
NM.4.1
I can echo an
ostinato
rhythm
pattern.

Indicator
GM.P
NH.4.1
I can use
music
notation to
play
instruments.

Indicator
GM.P
IL.4.1
I can play
accompanyments and
songs in
major and
minor
tonalities.

Indicator
GM.P
IM.4.1
I can play my
part
independently in an
ensemble.

Indicator
GM.P
IH.4.1
I can rehearse
for
improvement
in an
ensemble.

Indicator
GM.P
AL.4.1
I can play in
various
musical
styles on
instruments.

Indicator
GM.P
AM.4.1
I can
collaborate
with others to
improve my
ensemble.

Indicator
GM.P
NL.4.2
I can play
pitched and
unpitched
instruments.

Indicator
GM.P
NM.4.2
I can play
melodic
patterns
using steps
and skips.

Indicator
GM.P
NH.4.2
I can play
pentatonic
scales on
instruments.

Indicator
GM.P
IL.4.2
I can play
using proper
technique
and posture.

Indicator
GM.P
IM.4.2
I can play my
instrument
with
technical
accuracy.

Indicator
GM.P
IH.4.2
I can use
proper
technique to
express
music.

Indicator
GM.P
AL.4.2
I can balance
my sound
with others in
an ensemble.

Indicator
GM.P
AM.4.2
I can control
pitch and
tone quality
with proper
dynamics.

Indicator
GM.P
NL.4.3
I can follow
the teacher
when I use
classroom
instruments.

Indicator
GM.P
NM.4.3
I can ask and
answer
musical
questions
using
instruments.

Indicator
GM.P
NH.4.3
I can identify
rhythmic
notation.

Indicator
GM.P
IL.4.3
I can play in
treble and
bass clefs.

Indicator
GM.P
IM.4.3
I can read
from
notation,
songs I play.

Indicator
GM.P
IH.4.3
I can play my
part
independentl
y while
others play.

Indicator
GM.P
AL.4.3
I can sight
read a
musical part.

Indicator
GM.P
AH.4.1
I can control
my instrument
across
expanded
dynamic
ranges using
stylistic
nuances and
expressive
inflections.
Indicator
GM.P
AH.4.2
I can adjust
my
intonation
relative to
chord tones.

Page 220

Anchor Standard 5: I can read and notate music.
Benchmark
GM.P
NL.5
I can read
rhythm
patterns.

Benchmark
GM.P
NM.5
I can read
simple
rhythmic and
melodic
notation.

Benchmark
GM.P
NH.5
I can read,
write simple
rhythmic and
melodic
standard
notation.

Benchmark
GM.P
IL.5
I can explain
note names
and basic
rhythms.

Indicator
GM.P
NL.5.1
I can read
rhythm
patterns with
my voice,
body, and
instruments.

Indicator
GM.P
NM.5.1
I can name
notes in
treble clef.

Indicator
GM.P
NH.5.1
I can read
standard
notation.

Indicator
GM.P
IL.5.1
I can read all
notes in
treble and
bass clefs

Indicator
GM.P
NL.5.2
I can read
basic
rhythms.

Indicator
GM.P
NM.5.2
I can read
simple
quarter,
eighth, half,
whole notes
and rests.

Indicator
GM.P
NH.5.2
I can read
meter in
4/4, 3/4, and
2/4.

Indicator
GM.P
IL.5.2
I can read
basic
rhythms
including
dotted
rhythms.

Benchmark
GM.P
IM.5
I can
interpret
musical
symbols
within
multiple
meters, clefs,
and
expressive
symbols.
Indicator
GM.P
IM.5.1
I can read
alto/tenor
clef.

Indicator
GM.P
IM.5.2
I can identify
compound,
complex, and
syncopated
rhythms.

Benchmark
GM.P
IH.5
I can read
and notate
short musical
works in a
variety of
clefs and
meters.

Indicator
GM.P
IH.5.1
I can read
and use key
signatures.

Benchmark
GM.P
AL.5
I can sight
read a variety
of music at
Grade 2 with
technical
accuracy.

Indicator
GM.P
AL.5.1
I can sight
read musical
works in
simple
meters and
tonalities
with
technical
accuracy.
Indicator
Indicator
GM.P
GM.P
IH.5.2
AL.5.2
I can read
I can respond
and use meter to a director
signatures.
while sightreading.

Benchmark
GM.P
AM.5
I can sight
read a variety
of music at
Grade 3 with
technical
accuracy.

Benchmark
GM.P
AH.5
I can sight
read a variety
of music at
Grade 4 with
technical
accuracy.

Indicator
GM.P
AM.5.1
I can sight
read musical
works in a
variety of
keys and
clefs.

Indicator
GM.P
AH.5.1
I can sight
read musical
works in a
variety of
keys, clefs,
meters.

Indicator
GM.P
AM.5.2
I can apply
tempo and
dynamic
markings to
my sightreading.

Indicator
GM.P
AH.5.2
I can apply
expressive
music
markings to
my sightreading.

Page 221

Artistic Processes: Responding- I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Benchmark
GM.R
NL.6
I can identify
contrasts in
music.

Benchmark
GM.R
NM.6
I can identify
the elements
of music.

Benchmark
GM.R
NH.6
I can describe
how the
music
elements are
used.

Benchmark
GM.R
IL.6
I can explain
how the
elements of
music are
used in a
variety of
genres,
cultures, and
time periods.

Benchmark
GM.R
IM.6
I can
examine how
the elements
of music are
used in a
variety of
genres,
cultures, and
time periods.

Benchmark
GM.R
IH.6
I can find
evidence of
how music is
informed by
the structure,
elements in a
variety of
genres,
cultures, and
time periods.

Indicator
GM.R
NL.6.1
I can identify
dynamics and
steady beat.

Indicator
GM.R
NM.6.1
I can identify
changes in
dynamics,
tempo and
rhythm.

Indicator
GM.R
NH.6.1
I can use
appropriate
vocabulary to
describe
pitch,
tempo, and
dynamics.

Indicator
GM.R
IL.6.1
I can listen,
identify and
explain
pitch,
tempo, and
dynamics to
describe
music of
different
styles.

Indicator
GM.R
IM.6.1
I can
examine the
elements of
pitch,
tempo, and
dynamics in a
variety of
musical
styles
presented
aurally and
visually.

Indicator
GM.R
IH.6.1
I can listen,
identify, and
explain the
elements of
pitch,
tempo,
dynamics,
and style
used in
different
cultures and
time periods.

Benchmark
GM.R
AL.6
I can
examine the
use of
compositional
techniques
within
multiple
musical
works.

Benchmark
GM.R
AM.6
I can
collaborate
with others to
justify the
use of
compositional
techniques
within
musical
works.
Indicator
Indicator
GM.R
GM.R
AL.6.1
AM.6.1
I can describe I can
and use
collaborate
meter,
with others to
tonality,
determine
intervals,
intent of
chords, and
changes in
harmonic
meter,
progressions tonality and
when
harmony
analyzing
that
contribute to
written and
musical style.
aural
compositions
.

Benchmark
GM.R
AH.6
I can analyze
compositional
techniques, to
explain a
FRPSRVHU¶V
intent.

Indicator
GM.R
AH.6.1
I can
determine the
intent of
changes in
meter,
tonality and
harmony in a
variety of
musical
compositions
to create
tension and
emotional
response.

Page 222

Indicator
GM.R
NL.6.2
I can identify
same and
different
sound
sources.

Indicator
GM.R
NM.6.2
I can name
voice types
and
instrument
families.

Indicator
GM.R
NH.6.2
I can identify
by sight and
sound voice
types and
classroom
instruments.

Indicator
GM.R
IL.6.2
I can listen to
and identify
orchestral,
band, and
electronic
instruments
by sight and
sound.

Indicator
GM.R
IM.6.2
I can
examine the
contribution
of timbre in
a variety of
musical
instruments/v
oices to
musical style
and mood.

Indicator
GM.R
IH.6.2
I can
examine the
use of timbre
and texture in
music from a
variety of
different
genres.

Indicator
GM.R
AL.6.2
I can
examine and
discuss
culturally
authentic
practices
found in
musical
works.

Indicator
GM.R
NL.6.3
I can name
same and
different
sections.

Indicator
GM.R
NM.6.3
I can identify
examples of
some basic
musical
forms.

Indicator
GM.R
NH.6.3
I can identify
examples of
complex
musical
forms.

Indicator
GM.R
IL.6.3
I can
examine
musical
forms to
describe a
musical style.

Indicator
GM.R
IM.6.3
I can identify
musical
forms
presented
aurally and
visually.

Indicator
GM.R
IH.6.3
I can
examine the
use of
musical
forms
presented in a
varied
repertoire of
music.

Indicator
GM.R
AL.6.3
I can analyze
and describe
how the use
of expressive
devices and
form are
used in
culturally and
historically
diverse
genres.

Indicator
GM.R
AM.6.2
I can identify
compositional
techniques
used to
achieve
unity,
variety,
tension and
release in
music to
evoke an
emotional
response
from the
listener.
Indicator
GM.R
AM.6.3
I can
examine the
use of
musical form
and
expressive
devices in a
variety of
20th & 21st
Century
compositions.

Indicator
GM.R
AH.6.2
I can examine
timbre and
the use of
voices,
instruments,
and other
sound sources
in a variety of
musical
styles,
cultures, and
genres.

Indicator
GM.R
AH.6.3
I can examine
the use of
musical form
when
analyzing
aural
examples of a
varied
repertoire of
music and
inform my
personal
music
preferences.

Page 223

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
NL.7
I can use my
words to talk
about music.

Benchmark
GM.R
NM.7
I can
demonstrate
how to be an
audience
member in
different
musical
settings.

Benchmark
GM.R
NH.7
I can use
musical
vocabulary to
describe
personal
preference
choices.

Benchmark
GM.R
IL.7
I can use
musical
vocabulary to
critique a
performance.

Benchmark
GM.R
IM.7
I can evaluate
the quality of
musical
performances
and/ or
compositions
of others
using
assessment
tools.

Benchmark
GM.R
IH.7
I can evaluate
the quality of
personal
performances
and/or
compositions
using
assessment
tools.

Benchmark
GM.R
AL.7
I can
collaborate to
develop
strategies for
improvement
of group
performances.

Indicator
GM.R
NL.7.1
I can listen
and respond
to music.

Indicator
GM.R
NM.7.1
I can model
and describe
audience
behavior in
different
settings.

Indicator
GM.R
NH.7.1
I can talk and
write about
music using
musical
vocabulary.

Indicator
GM.R
IL.7.1
I can describe
the quality of
a musical
performance.

Indicator
GM.R
IM.7.1
I can apply
assessment
tools to
evaluate tone
quality,
intonation,
articulation,
rhythmic
accuracy,
musicality,
posture, and
stage
presence to a
live or
recorded
performance.

Indicator
GM.R
IH.7.1
I can apply
assessment
tools to
evaluate tone
quality,
intonation,
articulation,
rhythmic
accuracy,
musicality,
posture, and
stage
presence to
my personal
performance.

Indicator
GM.R
AL.7.1
I can
compare a
group
performance
to a
benchmark to
refine the
performance.

Benchmark
GM.R
AM.7
I can make
critical
evaluations
of
performance,
compositions,
arrangements, and
improvisations.
Indicator
GM.R
AM.7.1
I can listen or
view a
variety of
performances
and offer
suggestions
for improvement.

Benchmark
GM.R
AH.7
I can justify
personal
performance
decisions.

Indicator
GM.R
AH.7.1
I can use
multiple
media sources
to critique my
personal
performances.

Page 224

Artistic Processes: Connecting- I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Benchmark
GM.C
NL.8
I can
recognize
and perform
musical
selections
from my own
culture and
some time
periods.

Benchmark
GM.C
NM.8
I can identify
and perform
musical
selections
from a
culture other
than mine
and a
historical
time period.

Benchmark
GM.C
NH.8
I can identify
and perform
musical
selections
from multiple
cultures
and/or
historical
time periods.

Benchmark
GM.C
IL.8
I can
examine
relationships
among
musical
selections
from multiple
cultures
and/or
historical
time periods.

Benchmark
GM.C
IM.8
I can research
the role of
music within
a specific
culture or
historical
time period
and present
what I
discovered.

Benchmark
GM.C
IH.8
I can perform
and modify a
musical work
using
characteristic
s from a
culture or
time period.

Benchmark
GM.C
AL.8
I can analyze
a diverse
repertoire of
music from a
cultural or
historical
time period.

Benchmark
GM.C
AM.8
I can
examine
contemporary
musical
works to
determine the
influence of
historical and
cultural
traditions.

Benchmark
GM.C
AH.8
I can examine,
create, and
perform music
based on
historical and
cultural
contributions.

Indicator
GM.C
NL.8.1
I can
recognize
that all
cultures and
time periods
use music.

Indicator
GM.C
NM.8.1
I can find
similar
elements of
music within
a culture/time
period.

Indicator
GM.C
NH.8.1
I can find
similar
elements of
music in
different
cultures/time
periods.

Indicator
GM.C
IL.8.1
I can identify
similarities
and
differences in
music from
multiple
cultures and
time periods.

Indicator
GM.C
IM.8.1
I can use
music
vocabulary
terms such as
form, tempo,
dynamics,
etc. to
describe
musical
works from
similar
cultures and
time periods.

Indicator
GM.C
IH.8.1
I can change
a musical
work using
the elements
of music
from a
culture or
time period.

Indicator
GM.C
AL.8.1
I can explain
specific
cultural and
historical
traditions and
infuse these
ideas into my
music.

Indicator
GM.C
AM.8.1
I can select
musical
elements in
contemporary
music that
reflect
cultural and
historical
influences.

Indicator
GM.C
AH.8.1
I can use
historical and
cultural
contributions
to justify my
musical
choices.

Page 225

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Benchmark
GM.C
NL.9
I can explore
general
music
concepts
among arts
disciplines
other content
areas and
related
careers in
familiar
settings.

Benchmark
GM.C
NM.9
I can
recognize
general
music
concepts
among arts
disciplines,
other content
areas and
related
careers.

Benchmark
GM.C
NH.9
I can apply
general
music
concepts to
arts
disciplines,
other content
areas and
related
careers
including
South
Carolina.

Benchmark
GM.C
IL.9
I can explore
a range of
skills shared
among arts
disciplines,
other content
areas and
how they can
be applied to
a career in
music.

Benchmark
GM.C
IM.9
I can
recognize
specific skills
shared
among arts
disciplines,
other content
areas and
how they can
be applied to
a career in
music.

Benchmark
GM.C
IH.9
I can analyze
the tools,
concepts, and
materials
used among
arts
disciplines,
other content
areas and
how they are
used in music
careers.

Indicator
GM.C
NL.9.1
I can identify
the
relationship
between
music and
another
subject in my
school.

Indicator
GM.C
NM.9.1
I can make
connections
between
music and
another
subject in my
school.

Indicator
GM.C
NH.9.1
I can
demonstrate
and describe
the
relationship
between
music and a
concept from
another
subject in my
school.

Indicator
GM.C
IL.9.1
I can apply
music
concepts to
other arts
disciplines
and content
areas.

Indicator
GM.C
IM.9.1
I can
examine the
relationship
between
music and
specific
content from
another arts
discipline
and content
area.

Indicator
GM.C
IH.9.1
I can apply
concepts
from other
arts
disciplines
and content
areas to my
music.

Benchmark
GM.C
AL.9
I can apply
concepts
among arts
disciplines
and other
content areas
to general
music and
analyze how
my interests
and skills
will prepare
me for a
career.
Indicator
GM.C
AL.9.1
I can explain
ideas from
other arts
disciplines
and content
areas through
music.

Benchmark
GM.C
AM.9
I can explain
how
economic
conditions,
cultural
values and
location
influence
music and the
need for
music related
careers.

Benchmark
GM.C
AH.9
I can research
societal
political and
cultural issues
as they relate
to other arts
and content
areas and
apply to my
role as a
musician.

Indicator
GM.C
AM.9.1
I can explain
how my
artistic
choices are
influenced by
cultural and
social values.

Indicator
GM.C
AH.9.1
I can analyze
complex
ideals that
influence my
artistic
perspective
and creative
work.

Page 226

Indicator
GM.C
NL.9.2
I can identify
topics in
music that
interest me.

Indicator
GM.C
NM.9.2
I can identify
life skills
necessary for
a music
career.

Indicator
GM.C
NH.9.2
I can identify
specific
careers in
music.

Indicator
GM.C
IL.9.2
I can
demonstrate
and describe
the skills
needed for
careers in
music.

Indicator
GM.C
IM.9.2
I can
examine the
educational
requirements
needed for a
variety of
careers in
music.

Indicator
GM.C
IH.9.2
I can
compare
similarities
and
differences in
a variety of
music careers
and roles of
musicians in
those careers.

Indicator
GM.C
AL.9.2
I can identify
and describe
traditional
and emerging
careers in
music.

Indicator
GM.C
AM.9.2
I can discuss
the impact of
economic
issues as they
affect the
impact on
music
careers.

Indicator
GM.C
AH.9.2
I can analyze
my personal
career choices
in the arts or
non-arts
disciplines.

Page 227

Novice General Music Standards
Artistic Processes: Creating-I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Novice
Low
Benchmark
GM.CR
NL.1
I can imitate a musical statement by sight and
sound.
Indicator
GM.CR
NL.1.1

Novice
Mid
Benchmark
GM.CR
NM.1
I can answer a musical question.

I can match sound and pattern.

I can identify simple forms.

Sample Learning Targets

Sample Learning Targets

x

I can clap a repeated rhythm.

x

I can imitate sounds using my voice.

x

x

Novice
High
Benchmark
GM.CRNH.1
I can arrange a musical idea.

Indicator
GM.CR
NM.1.1

x

I can identify call and response as a
musical form.

x

I can identify AB/ABA form in simple
songs.

x

I can...

I can use found sounds to create a
composition.

Indicator
GM.CR
NH.1.1
I can use rhythm patterns, songs or words to
create a musical idea.
Sample Learning Targets
x

I can arrange a sound poem.

x

I can arrange a melodic idea on a
barred instrument.

x

I can arrange rhythm patterns with
flashcards.

x

I can use movement to show melodic
contour.

x

, FDQ«

, FDQ«

Page 228

Indicator
GM.CR
NL.1.2
I can imitate a given music rhythm or sound
using symbols.

Indicator
GM.CR
NM.1.2
I can identify same and different patterns.

Indicator
GM.CR
NH.1.2
I can create a musical idea given specific
instructions.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can use symbols to show a rhythm
pattern.

x

I can give a rhythmic response to a
rhythm question.

x

I can create rhythmic compositions
using standard notation.

x

I can compose by drawing icons to
represent music beats.

x

I can identify same/different melodic
and rhythmic patterns.

x

I can use notation technology to
arrange musical patterns.

x

I can...

x

I can identify patterns of
same/different in simple songs and
patterns.

x

I can use movement to share a musical
idea.

x
x

I can...

I can use technology to create musical
ideas.

x

I can...

Anchor Standard 2: I can improvise music.
Novice
Low
Benchmark
GM.CR
NL.2
I can imitate simple rhythm patterns within a
given meter.

Novice
Mid
Benchmark
GM.CR
NM.2
I can imitate simple tonal patterns within a
given key and tonality.

Novice
High
Benchmark
GM.CR
NH.2
I can improvise responses to given rhythmic
patterns.

Page 229

Indicator
GM.CR
NL.2.1
I can identify same and different rhythms
patterns.

Indicator
GM.CR
NM.2.1
I can identify same and different melodic
patterns.

Indicator
GM.CR
NH.2.1
I can improvise simple ostinati patterns
within a given meter.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can use a counting system to decide
same and different.

x

I can listen to two melodic samples
and label same and different.

x

I can improvise an ostinato rhythm
pattern on an instrument.

x

I can listen to two rhythm samples and
label same and different.

x

I can sing a melodic pattern that is the
same and one that is different from the
one I hear.

x

I can use a counting system to
improvise a rhythm pattern.

x

I can...

x

I can...

x

I can...

Indicator
GM.CR
NL.2.2
I can echo simple rhythm patterns.

Indicator
GM.CR
NM.2.2
I can echo simple tonal patterns.

Indicator
GM.CR
NH.2.2
I can improvise rhythm patterns, songs or
chants to create a musical idea.

Sample Learning Targets
x I can use body percussion to echo a
rhythm pattern.

Sample Learning Targets
x I can echo simple tonal patterns using
tonal solfege or pitch name.

Sample Learning Targets
x I can write new words for a familiar
tune.

x

I can echo simple rhythm patterns on
an instrument.

x

I can echo simple tonal patterns on a
neutral syllable.

x

I can improvise a chant for a given
rhythmic pattern.

x

I can use chants to echo a rhythm
pattern.

x

I can echo simple tonal patterns on an
instrument.

x

I can...

x

I can...

x

I can...

Page 230

Artistic Processes: Performing-I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Novice
Low
Benchmark
GM.P
NL.3
I can use my voice in many ways.

Novice
Mid
Benchmark
GM.P
NM.3
I can use my singing voice.

Novice
High
Benchmark
GM.P
NH.3
I can sing, chant, and move to demonstrate a
steady beat.

Indicator
GM.P
NL.3.1
I can sing songs in my range.

Indicator
GM.P
NM.3.1
I can match pitch when I sing.

Indicator
GM.P
NH.3.1
I can sing or move using a steady beat.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can echo, speak, chant, sing and
move to music.

x

I can match pitch with piano or my
teacher.

x

I can sing songs from memory.

x

I can sing songs in unison.

x

I can...

x

I can...

x

I can identify beat/no beat.

x

I can use rhythm sticks to demonstrate
steady beat.

x

I can move to a steady beat following
a conductor.

x

I can...

Page 231

Indicator
GM.P
NL.3.2
I can use my voice to imitate other sounds.

Indicator
GM.P
NM.3.2
I can sing with my head voice and chest voice.

Indicator
GM.P
NH.3.2
I can use good posture and breath support
when I sing.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can make sound effects with my
voice.

x

I can sing with a group.

x

I can explore different sounds with
voice changing software.

x

I can sing patriotic songs.

x

I can sing folk songs.

x

I can echo short melodic patterns.

x

I can...

x

I can demonstrate good singing
posture.

x

I can use basic dynamics (piano and
forte) when I sing.

x

I can...

Anchor Standard 4: I can play instruments alone and with others.
Novice
Low
Benchmark
GM.P
NL.4
I can make sounds with classroom instruments
and other sound sources.
Indicator
GM.P
NL.4.1
I can use my body to make sounds.

Novice
Mid
Benchmark
GM.P
NM.4
I can imitate short rhythmic patterns.
Indicator
GM.P
NM.4.1
I can echo an ostinato rhythm pattern.

Novice
High
Benchmark
GM.P
NH.4
I can play and read rhythmic, melodic, and
chord patterns.
Indicator
GM.P
NH.4.1
I can use music notation to play instruments.

Sample Learning Targets
x I can echo your pattern by clapping.

Sample Learning Targets
x I can play classroom instruments to
follow a rhythm pattern.

Sample Learning Targets
x I can play guitar following a chord
sheet.

x
x

I can patch, clap, and stomp a pattern.
x

I can play an ostinato on a classroom
instrument.

x

I can play recorder from notation B,
A, & G.

x

I can play djembes in a Drum circle.

x

I can...

I can...

Page 232

Indicator
GM.P
NL.4.2
I can play pitched and unpitched instruments.

Indicator
GM.P
NM.4.2
I can play melodic patterns using steps and
skips.

Indicator
GM.P
NH.4.2
I can play pentatonic scales on instruments.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can echo your patterns with my
rhythm sticks.

x

I can play xylophones and other
pitched instruments.

x

I can use instruments to make sound
stories.

x

I can...

x

x

x

x

I can select the correct notes to make a
pentatonic scale on a barred
instrument.

I can use classroom instruments or
sound software to create sound stories.

x

I can play leaps/skips/steps on barred
instruments.

I can identify a skip and step on a
barred instrument.

x

I can...

I can...

Indicator
GM.P
NL.4.3
I can follow the teacher when I use classroom
instruments.

Indicator
GM.P
NM.4.3
I can ask and answer musical questions
using instruments.

Indicator
GM.P
NH.4.3
I can identify rhythmic notation.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can follow my WHDFKHU¶V SDWWHUQ RQ
my rhythm instrument.

x

I can respond to a musical question on
a classroom instrument.

x

I can identify quarter, eighth, half, and
whole notes and their rests.

x

I can start and stop with the conductor.

x

I can write and perform a 4-beat
answer to a 4-beat question.

x

I can play a rhythm pattern from
notation.

x

I can...
x

I can...

x

I can...

Page 233

Anchor Standard 5: I can read and notate music.
Novice
Low
Benchmark
GM.P
NL.5
I can read rhythm patterns.
Indicator
GM.P
NL.5.1
I can read rhythm patterns with my voice,
body, and instruments.
Sample Learning Targets
x

I can read quarter notes/rests.

x

I can identify beat/no beat.

Novice
Mid
Benchmark
GM.P
NM.5
I can read simple rhythmic and melodic
notation.
Indicator
GM.P
NM.5.1
I can name notes in treble clef.

Novice
High
Benchmark
GM.P
NH.5
I can read, write simple rhythmic and melodic
standard notation.
Indicator
GM.P
NH.5.1
I can read standard notation.

Sample Learning Targets

Sample Learning Targets

x

x
x

I can notate one and two sounds to a
beat.
x

x

I can read notes from the staff in treble
clef.

x

I can write simple melodic and
rhythmic notation.

I can use technology to practice
reading and writing simple notation.

x

I can write music notation using
music software.

I can identify a second, third, fifth, and
octave.

x

I can write the notes for ³Twinkle,
Twinkle Little StaU´ on staff paper.

x

I can...

I can...
x

I can...

Page 234

Indicator
GM.P
NL.5.2
I can read basic rhythms.

Indicator
GM.P
NM.5.2
I can read simple quarter, eighth, half, whole
notes, and rests.

Indicator
GM.P
NH.5.2
I can read meter in 4/4, 3/4, and 2/4.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can clap a rhythm pattern from
traditional/non-traditional notation.

x

I can play a rhythm pattern from
notation on a non-pitched instrument.

x

x

I can complete measures using the
correct rhythmic notation for a given
meter signature.

x

I can add missing bar lines for given
meters.

x

I can write simple melodic and
rhythmic notation.

x

I can conduct music in 4/4.

x

I can write the counting for simple
rhythms to include sixteenth notes.

x

I can write music notation using
music software.

x

I can...

, FDQ«
x

I can...

Artistic Processes: Responding-I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Novice
Low
Benchmark
GM.R
NL.6
I can identify contrasts in music.

Novice
Mid
Benchmark
GM.R
NM.6
I can identify the elements of music.

Novice
High
Benchmark
GM.R
NH.6
I can describe how the music elements are
used.

Page 235

Indicator
GM.R
NL.6.1
I can identify dynamics and steady beat.

Indicator
GM.R
NM.6.1
I can identify changes in dynamics, tempo and
rhythm.

Indicator
GM.R
NH.6.1
I can use appropriate vocabulary to describe
pitch, tempo, and dynamics.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can clap or march to a steady beat.

x

I can use movement to express
dynamics in music.

x

I can identify dynamic contrasts by
sight and sound.

x

I can show loud and soft with body
movements.

x

I can identify same and different
rhythm patterns.

x

Given two pitches, I can name the
second as higher, lower, or the same as
the first.

x

I can move to changes of tempo from
a recording or sound source.

x

I can...

x

, FDQ «

x

I can...

Indicator
GM.R
NL.6.2
I can identify same and different sound
sources.

Indicator
GM.R
NM.6.2
I can name voice types and instrument
families.

Indicator
GM.R
NH.6.2
I can identify by sight and sound voice types
and classroom instruments.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can name different sounds from my
environment to create a sound poem.

x

I can identify a child, adult woman,
and adult male voice.

x

I can identify whisper, speaking,
shouting, or singing voice.

x

I can name classroom instruments by
sight and sound.

I can select a class instrument to
imitate the VRXQG RI D FORFN KRUVH¶V
feet, rain, etc.

x

x

x

x

I can classify classroom instruments
into families by sight and sound
(woods, metals, shakers, etc.).

x

I can identify soprano, alto, and tenor,
bass.

x

I can...

I can...

, FDQ«
Page 236

Indicator
GM.R
NL.6.3
I can name same and different sections.

Indicator
GM.R
NM.6.3
I can identify examples of some basic musical
forms.

Indicator
GM.R
NH.6.3
I can identify examples of complex musical
forms.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can show movements to music to
demonstrate same and different.

x

I can use shapes to identify sections of
music.

x

, FDQ «

x

I can identify ABA form.

x

I can identify verse and refrain from
the music and by hearing it.

x

I can identify repeated sections to label
simple forms.

x

I can identify motif, canon, rondo,
AABA, and theme and variations as a
musical form.

x

I can...

x

I can sing the response to a call and
response song.

x

I can...

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
NL.7
I can use my words to talk about music.
Indicator
GM.R
NL.7.1
I can listen and respond to music.

Sample Learning Targets

Benchmark
GM.R
NM.7
I can demonstrate how to be an audience
member in different musical settings.
Indicator
GM.R
NM.7.1
I can model and describe audience behavior in
different settings.

Benchmark
GM.R
NH.7
I can use musical vocabulary to describe
personal preference choices.
Indicator
GM.R
NH.7.1
I can talk and write about music using musical
vocabulary.

Sample Learning Targets

Sample Learning Targets

x

I can talk about music I listen to at
home.

x

I can applaud at appropriate times
during a concert.

x

I can use music vocabulary to describe
what I like in a song.

x

I can use my words to tell you the
music I like. , FDQ«

x

I can sit quietly during a performance.

x

I can talk about a performance using
musical vocabulary. , FDQ«
Page 237

Artistic Processes: Connecting-I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Novice
Low
Benchmark
GM.C
NL.8
I can recognize and perform musical selections
from my own culture and some time periods.

Novice
Mid
Benchmark
GM.C
NM.8
I can identify and perform musical selections
from a culture other than mine and a historical
time period.

Novice
High
Benchmark
GM.C
NH.8
I can identify and perform musical selections
from multiple cultures and/or historical time
periods.

Indicator
GM.C
NL.8.1
I can recognize that all cultures and time
periods use music.

Indicator
GM.C
NM.8.1
I can find similar elements of music within a
culture/time period.

Indicator
GM.C
NH.8.1
I can find similar elements of music in
different cultures/time periods.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can share music ideas with friends.

x

I can sing and talk about folk music.

x

I can name the historical periods of
music.

x

I can name musical titles used for at
least one cultural event.

x

I can name traditions from a culture
other than mine that uses music.

x

I can identify instruments from
multiple cultures.

I can name composers from the
Classical Period (Mozart, Beethoven).

x

I can identify patriotic music and its
purpose.

x

I can define a variety of cultures and
their use of music.

I can...

x

x

I can...

x

x

x

I can describe characteristics of the
Classical Period (balance, order,
following rules).
I can...

Page 238

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Novice
Low
Benchmark
GM.C
NL.9
I can explore general music concepts among
arts disciplines other content areas and related
careers in familiar settings.

Novice
Mid
Benchmark
GM.C
NM.9
I can recognize general music concepts among
arts disciplines, other content areas and related
careers.

Novice
High
Benchmark
GM.C
NH.9
I can apply general music concepts to arts
disciplines, other content areas and related
careers.

Indicator
GM.C
NL.9.1
I can identify the relationship between music
and another subject in my school.

Indicator
GM.C
NM.9.1
I can make connections between music and
another subject in my school.

Indicator
GM.C
NH.9.1
I can demonstrate and describe the relationship
between music and a concept from another
subject in my school.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

I can identify songs that will help me
in another subject (ex. alphabet song).
I can tell a story with found sounds or
computer generated sound.

x

x

, FDQ WDON DERXW PXVLF¶V UHODWLRQVKLS WR
the science of sound.

x

I can use music as a tool to learn about
fractions.

x

I can name some South Carolina
musicians.

x

I can...

I can write a parody to help me learn
the continents.

x

I can sing the Alphabet song to
alphabetize words.

x

I can...

I can...

Page 239

Indicator
GM.C
NL.9.2
I can identify topics in music that interest me.

Indicator
GM.C
NM.9.2
I can identify life skills necessary for a music
career.

Indicator
GM.C
NH.9.2
I can identify specific careers in music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can give examples of careers and
events that use music.

x

I can describe how my musical
performance improved.

x

I can name arts businesses and
organizations that hire musicians.

x

I can describe special types of music
for special events or times of year and
the people involved.

x

I can describe proper performer and
audience behavior for a concert.

x

I can describe music careers of
community members.

x

I can make a list of music careers.

I can...

I can work with others to improve my
performance.

x

x

x

I can...

x

I can...

Page 240

Intermediate General Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Intermediate
Low
Benchmark
GM.CR
IL.1

Intermediate
Mid
Benchmark
GM.CR
IM.1

Intermediate
High
Benchmark
GM.CR
IH.1

I can combine musical ideas to create phrases
for voice, instruments, or body movement.

I can compose a rhythmic and melodic phrase.

I can add harmony to compose or arrange
phrases for a given mood.

Indicator
GM.CR
IL.1.1
I can explain the use of ostinato to arrange a
melodic idea.

Indicator
GM.CR
IM.1.1
I can organize rhythmic and melodic patterns
into a musical phrase.

Indicator
GM.CR
IH.1.1
I can identify key signatures in melodic
phrases.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can use notation technology to
arrange musical patterns.

x

I can write a musical phrase for an
instrument.

x

I can create an ostinato pattern to a
simple song.

x

I can combine patterns to make a
musical phrase.

I can use technology to create musical
ideas.

x

x

x

x

I can add harmony to a familiar tune
to demonstrate knowledge of proper
key structure.

x

I can research appropriate keys for
specific instruments.

x

I can...

I can...

, FDQ«

Page 241

Indicator
GM.CR
IL.1.2
I can construct arrangements of simple pieces
for voices or instruments.

Indicator
GM.CR
IM.1.2
I can create a melodic phrase over a given
rhythmic idea.

Indicator
GM.CR
IH.1.2
I can construct a rhythmic, melodic and
harmonic idea for a given mood.

Sample Learning Targets
x I can arrange a familiar song for an
instrument.

Sample Learning Targets
x I can create a melody using the
pentatonic scale.

Sample Learning Targets
x I can add harmony to a familiar tune
to change the mood.

x

I can use standard notation to
compose an arrangement of a tune.

x

I can use sol, mi, do over an ostinato
rhythm pattern to make a melody.

x

I can...

x

, FDQ«

x

I can write a short chorale.

x

I can use music writing software to
compose or arrange my work.

x

I can...

Anchor Standard 2: I can improvise music.
Intermediate
Low
Benchmark
GM.CR
IL.2
I can improvise short melodic question and
answer patterns.

Intermediate
Mid
Benchmark
GM.CR
IM.2
I can improvise a rhythm pattern to embellish
a given a harmonic phrase.

Intermediate
High
Benchmark
GM.CR
IH.2
I can improvise a simple melodic phrase given
a harmonic phrase.

Page 242

Indicator
GM.CR
IL.2.1
I can improvise simple tonal patterns within a
given key.

Indicator
GM.CR
IM.2.1
I can embellish a bass line with improvised
rhythm from an instrument or music software.

Indicator
GM.CR
IH.2.1
I can identify chord changes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can improvise a scat tune.

x

I can play an improvised bass line on
a barred instrument.

x

I can identify chord changes to
improvise a short melody.

x

I can improvise my own simple tonal
patterns on my instrument.

x

I can improvise a rhythmic bass line
for a folk song on instruments or using
a music app.

x

I can identify different chord patterns
on a staff line.

x
x

I can...

I can write a I, IV, V chord
progression using notation.

x

I can...

x

x

I can improvise a new ending for a
familiar tune.
I can...

Indicator
GM.CR
IL.2.2
I can improvise rhythmic and melodic patterns
to create a musical phrase.

Indicator
GM.CR
IM.2.2
I can sing on a neutral syllable an improvised
rhythm.

Indicator
GM.CR
IH.2.2
I can embellish a given melodic phrase that
corresponds with simple chord changes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can scat to a jazz tune.

x

I can sing a familiar song with new
rhythmic patterns.

x

I can improvise my own simple tonal
patterns on my instrument.

x

I can improvise a new rhythm to end a
song.

x

I can...

x

x

x

I can improvise a melody over a
recorded 12 bar blues progression.

I can improvise a scat tune.
x I can...

I can improvise a new ending for a
familiar tune.
I can...

Page 243

Artistic Processes: Performing-I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Intermediate
Low
Benchmark
GM.P
IL.3
I can sing a variety of simple part songs.

Intermediate
Mid
Benchmark
GM.P
IM.3
I can sing with expression and technical
accuracy.

Intermediate
High
Benchmark
GM.P
IH.3
I can sing a variety of songs with expression
and technical accuracy.

Indicator
GM.P
IL.3.1
I can sing 2-part songs.

Indicator
GM.P
IM.3.1
I can sing with proper intonation alone and in
different ensembles.

Indicator
GM.P
IH.3.1
I can sing phrasing while responding to a
GLUHFWRU¶V FXHV

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can sing partner songs.

x

I can sing my part in an ensemble.

x

I can sing a round or canon.

x

I can sing intervals.

x

I can...

x

I can match pitch with a teacher or
pitch matching app.

x

I can sight read simple songs.

x

I can...

x

, FDQ IROORZ D FRQGXFWRU¶V EHDW DQG
dynamic direction.

x

I can recognize from a score and sing
at various dynamic levels - p, mp, mf,
f.

x

I can...

Page 244

Indicator
GM.P
IL.3.2
I can sing with appropriate diction and
articulation.

Indicator
GM.P
IM.3.2
I can apply dynamics and expression when I
sing.

Indicator
GM.P
IH.3.2
I can sight read in multiple tonalities and
rhythms.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

, FDQ LPLWDWH P\ WHDFKHU¶V
pronunciation.

x

I can respond to director cues to use
dynamics and expression.

x

I can sing legato and staccato styles.

x

I can recognize and respond to
expressive markings in the music.

x

I can...
x

x

I can sight read my part in tune.

x

I can sing in 3-part harmony.

x

I can sight read music selected for
performance and pinpoint areas of
needed improvement.

x

I can...

I can...

Anchor Standard 4: I can play instruments alone and with others.
Intermediate
Low
Benchmark
GM.P
IL.4
I can play accompaniments and simple songs
on classroom instruments.

Intermediate
Mid
Benchmark
GM.P
IM.4
I can play and read complimentary and
contrasting instrumental parts accurately and
independently.

Intermediate
High
Benchmark
GM.P
IH.4
I can play and read my part with an ensemble
using accurate technique and posture.

Page 245

Indicator
GM.P
IL.4.1
I can play accompaniments and songs in
major and minor tonalities.

Indicator
GM.P
IM.4.1
I can play my part independently in an
ensemble.

Indicator
GM.P
IH.4.1
I can rehearse for improvement in an
ensemble.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can play parts using notation.

x

I can read my part with correct rhythm
and tempo.

x

I can practice in an ensemble using
rhythmic and melodic notation.

x

I can identify by sound - major/minor
tonalities.

x

I can explain how to practice my part
for improvement.

x

I can practice with a recording and
improve my ensemble part.

x

, FDQ«

x

, FDQ«

x

I can...

Indicator
GM.P
IL.4.2
I can play using proper technique and posture.

Indicator
GM.P
IM.4.2
I can play my instrument with technical
accuracy.

Indicator
GM.P
IH.4.2
I can use proper technique to express music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can play barred instruments with
correct mallet technique.

x

I can look at myself in the mirror to
play with proper posture.

x

x

I can complete a rubric that includes
posture and intonation about my
playing.

x

I can use proper technique for a
chosen instrument to perform in an
ensemble.

x

I can...

x

I can apply correct articulation to my
music. (legato, staccato, etc.)

x

I can...

I can...

Page 246

Indicator
GM.P
IL.4.3
I can play in treble and bass clefs.

Indicator
GM.P
IM.4.3
I can read from notation, songs I play.

Sample Learning Targets

Sample Learning Targets

Indicator
GM.P
IH.4.3
I can play my part independently while others
play.
Sample Learning Targets

x

I can identify treble and bass clef
notation.

x

I can read my part with correct rhythm
and tempo.

x

I can read rhythmic and melodic
notation.

x

I can identify the correct clef to play a
selected instrument.

x

I can play syncopated patterns from
notation.

x

I can practice in an ensemble.

x

I can...

x

I can...

x

I can...

Anchor Standard 5: I can read and notate music.
Intermediate
Low
Benchmark
GM.P
IL.5
I can explain note names and basic rhythms.

Intermediate
Mid
Benchmark
GM.P
IM.5
I can interpret musical symbols within multiple
meters, clefs, and expressive symbols.

Intermediate
High
Benchmark
GM.P
IH.5
I can read and notate short musical works in a
variety of clefs and meters.

Indicator
GM.P
IL.5.1
I can read all notes in treble and bass clefs.

Indicator
GM.P
IM.5.1
I can read alto/tenor clef.

Indicator
GM.P
IH.5.1
I can read and use key signatures.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can read treble clef notes.

x

I can explain duple and triple meter.

x
x

I can count 6/8 meter.
I can...

x

I can identify the notes in all
traditional clefs.

x

I can rewrite a treble clef passage in
another clef.

x

I can...

x

I can write the circle of fifths. I can
identify appropriate accidentals for a
given key.
I can identify enharmonic pitches.

x

I can...
Page 247

Indicator
GM.P
IL.5.2
I can read basic rhythms including dotted
rhythms.

Indicator
GM.P
IM.5.2
I can identify compound, complex, and
syncopated rhythms.

Indicator
GM.P
IH.5.2
I can read and use meter signatures.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can read treble clef notes.

x

I can notate a syncopated phrase.

x

I can read songs in 5/8, 7/8, 9/8, and
2/2 meters.

x

I can explain duple and triple meter.

x

I can apply a rhythmic counting
system to compound rhythms.

x

I can demonstrate basic conducting
patterns.

x

I can...

x

I can count 6/8 meter.

x

I can...

x

I can...

Artistic Processes: Responding-I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Intermediate
Low
Benchmark
GM.R
IL.6
I can explain how the elements of music are
used in a variety of genres, cultures, and time
periods.

Intermediate
Mid
Benchmark
GM.R
IM.6
I can examine how the elements of music are
used in a variety of genres, cultures, and time
periods.

Intermediate
High
Benchmark
GM.R
IH.6
I can find evidence of how music is informed
by the structure, elements in a variety of
genres, cultures, and time periods.

Page 248

Indicator
Indicator
GM.R
GM.R
IL.6.1
IM.6.1
I can listen, identify, and explain pitch, tempo, I can examine the elements of pitch, tempo,
and dynamics to describe music of different
and dynamics in a variety of musical styles
styles.
presented aurally and visually.

Indicator
GM.R
IH.6.1
I can listen, identify, and explain the elements
of pitch, tempo, dynamics, and style used in
different cultures and time periods.

Sample Learning Targets

Sample Learning Targets

x

I can identify and explain music
characteristics from Native American
and Hispanic cultures.

Sample Learning Targets
x

While listening, I can name
characteristics of two different musical
styles.

x

I can identify and explain music from
the Baroque and Romantic Period.

x

I can compare use of rhythm between
at least 2 different cultures.

x

I can write the common tempi terms in
order.

x

I can compare and contrast how
elements of music are used in rap and
jazz.

x

I can...
x

I can...

x

I can name the characteristics of the
music from the Baroque Period.

x

I can explain the use of polyrhythms in
African music examples.

x

I can identify major and minor
tonality from aural and written
examples.

x

, FDQ«

Page 249

Indicator
GM.R
IL.6.2

Indicator
GM.R
IM.6.2

Indicator
GM.R
IH.6.2

I can listen to and identify orchestral, band,
and electronic instruments by sight and sound.

I can examine the contribution of timbre in a
variety of musical instruments/voices to
musical style and mood.

I can examine the use of timbre and texture in
music from a variety of different genres.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify electronic instruments by
sight and sound.

x

I can choose an instrument to best fit a
musical style and mood.

x

I can compare and contrast the timbre
and texture of folk and opera.

x

I can identify the common orchestra
and band instruments by sight and
sound.

x

I can identify instruments that
contribute to a variety of non-classical
styles such as dobro, mandolin, fiddle.

x

I can identify the differences between
the timbre and texture of a string
versus full orchestra.

x

I can name instruments in each
musical family.

x

I can describe different vocal
techniques that support mood in rap,
country, pop, jazz.

x

I can...

x

I can...
x

I can...

Indicator
GM.R
IL.6.3
I can examine musical forms to describe a
musical style.

Indicator
GM.R
IM.6.3
I can identify musical forms presented aurally
and visually.

Indicator
GM.R
IH.6.3
I can examine the use of musical forms
presented in a varied repertoire of music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify verse and refrain in a
folk song.

x

I can use a labeling system to identify
a musical form.

x

I can use music vocabulary to justify
my choice of music time period.

x

I can determine the form RI WKH ³6WDU
6SDQJOHG %DQQHU´

x

I can identify the musical form from a
musical score.

x

I can study a score to cite examples of
compositional techniques.

x

I can...

x

I can name the musical form of
familiar music.

x

I can...
Page 250

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
IL.7
I can use musical vocabulary to critique a
performance.

Benchmark
GM.R
IM.7
I can evaluate the quality of musical
performances and/ or compositions of others
using assessment tools.

Benchmark
GM.R
IH.7
I can evaluate the quality of personal
performances and/or compositions using
assessment tools.

Indicator
GM.R
IL.7.1
I can describe the quality of a musical
performance.

Indicator
GM.R
IM.7.1
I can apply assessment tools to evaluate tone
quality, intonation, articulation, rhythmic
accuracy, musicality, posture, and stage
presence to a live or recorded performance.

Indicator
GM.R
IH.7.1
I can apply assessment tools to evaluate tone
quality, intonation, articulation, rhythmic
accuracy, musicality, posture, and stage
presence to my personal performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can write a response to a music
performance using music vocabulary.

x

I can apply a rubric/checklist to
evaluate a performance.

x

I can respond appropriately to outside
criticism of my performance.

x

I can name a strength and a weakness
from a performance.

x

I can name and define criteria from a
rubric.

x

I can discuss areas of needed practice
to improve my performance.

x

I can...

x

I can view or listen to recordings to
complete a rubric and suggest ideas for
improvement.

x

I can...

x

I can...

Page 251

Artistic Processes: Connecting- I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Intermediate
Low
Benchmark
GM.C
IL.8
I can examine relationships among musical
selections from multiple cultures and/or
historical time periods.
Indicator
GM.C
IL.8.1
I can identify similarities and differences in
music from multiple cultures and time periods.

Sample Learning Targets

Intermediate
Mid
Benchmark
GM.C
IM.8
I can research the role of music within a
specific culture or historical time period and
present what I discovered.
Indicator
GM.C
IM.8.1
I can use music vocabulary terms such as
form, tempo, dynamics, etc. to describe
musical works from similar cultures and time
periods.
Sample Learning Targets

x

I can describe the difference between
African and Native American music.

x

I can research an historical period and
report it to the class including musical
examples.

x

I can use music vocabulary to describe
a culture (ex. African - polyrhythms,
characteristic instruments).

x

I can describe the music of historical
composers using the elements of
music in my descriptions.

x

I can...

x

x

I can compare and contrast the time
period of Baroque and Classical.

Intermediate
High
Benchmark
GM.C
IH.8
I can perform and modify a musical work
using characteristics from a culture or time
period.
Indicator
GM.C
IH.8.1
I can change a musical work using the
elements of music from a culture or time
period.
Sample Learning Targets
x

I can use technology to arrange ³-LQJOH
%HOOV´ WR UHIOHFW D GLIIHUHQW FXOWXUH

x

I can apply changes to a Classical
piece of music to reflect a different
time period.

x

I can...

I can...

Page 252

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Intermediate
Low
Benchmark
GM.C
IL.9
I can explore a range of skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Intermediate
Mid
Benchmark
GM.C
IM.9
I can recognize specific skills shared among
arts disciplines, other content areas and how
they can be applied to a career in music.

Intermediate
High
Benchmark
GM.C
IH.9
I can analyze the tools, concepts, and materials
used among arts disciplines, other content
areas and how they are used in music careers.

Indicator
GM.C
IL.9.1
I can apply music concepts to other arts
disciplines and content areas.

Indicator
GM.C
IM.9.1
I can examine the relationship between music
and specific content from another arts
discipline and content area.

Indicator
GM.C
IH.9.1
I can apply concepts from other arts disciplines
and content areas to my music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

I can make musical connections to the art
works of Kandinsky.
x I can define words with multiple
meanings in music and other subjects.
x

x

I can use tone color, pattern, texture,
etc.to talk about music and other arts
disciplines.

x

I can compare and contrast mood in
music, art, dance, and drama.

x

I can use music to complete a STEM
project.

x

, FDQ UHODWH PXVLF RI WKH µ V WR LWV
historical context.

x

x

I can trace the connection of the
American Revolution to army bands
and their music.

I can examine the relationship between
an element of music and other
disciplines including other arts
disciplines.

x

I can...

I can...
x

I can...

Page 253

Indicator
GM.C
IL.9.2
I can demonstrate and describe the skills
needed for careers in music.

Indicator
GM.C
IM.9.2
I can examine the educational requirements
needed for a variety of careers in music.

Indicator
GM.C
IH.9.2
I can compare similarities and differences in a
variety of music careers and roles of musicians
in those careers.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

I can research skills needed for a
variety of music careers.

x

I can investigate and report about
music careers in SC.
I can...

x

I can name careers in music and
research the requirements for the
chosen career.
I can examine the requirements of a
music producer.
I can identify college degree programs
for music therapy.

x

I can name and describe skills
necessary for college study of music
leading to a job or profession.

x

, FDQ«

x

I can use technology to compare
required skills for music careers.

x

I can identify common music skills
needed in different music careers.

x

I can...

Page 254

Advanced General Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can arrange and compose music.
Advanced
Low
Benchmark
GM.CR
AL.1
I can arrange, compose, and explain intent
using melody, rhythm, and harmony.

Advanced
Mid
Benchmark
GM.CR
AM.1
I can collaborate with others to compose or
arrange a variety of musical styles.

Advanced
High
Benchmark
GM.CR
AH.1
I can compose music within expanded forms.

Indicator
GM.CR
AL.1.1
I can use the circle of fifths to explain
transposition of a written musical work.

Indicator
GM.CR
AM.1.1
I can revise a composition based on the
feedback from others to improve composed
works.

Indicator
GM.CR
AH.1.1
I can create an original composition
independently.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can use the circle of fifths to
compose a work in complementary
keys.

x

I can use a technology system to
notate and transpose my work.

x

I can transpose a single part for two
different instruments.

x

I can record my composition to revise
based on feedback.

x

I can compose a concerto for my
instrument.

x

I can use anchor compositions from
known composers to compare stylistic
techniques.

x

I can write a song.

x

I can use technology storage systems
to organize my compositions.

x

I can...

x

I can...

Page 255

Indicator
GM.CR
AL.1.2
I can use and explain compositional techniques
to compose works in a musical form.

Indicator
GM.CR
AM.1.2
I can work with others to compose an original
composition.

Indicator
GM.CR
AH.1.2
I can create a new arrangement from a given
composition.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

I can explain common music forms,
(for example: verse-refrain, AB,
ABA).

x

I can use theme and variations to
compose a work with others.

x

I can use technology to collaborate
with team members while composing.

x

I can...

I can compose a short work in a given
music form.

x

I can arrange a work for an ensemble.

x

I can use technology to store and
organize my compositions.

x

I can arrange a composition for an
alternative instrument.

x

I can...

I can...

Anchor Standard 2: I can improvise music.
Advanced
Low
Benchmark
GM.CR
AL.2
I can perform a brief improvisation given a
chord progression and meter.

Advanced
Mid
Benchmark
GM.CR
AM.2
I can perform an improvisation given a
motive, chord progression, and meter.

Advanced
High
Benchmark
GM.CR
AH.2
I can perform and refine an extended
spontaneous improvisation independently.

Page 256

Indicator
GM.CR
AL.2.1
I can improvise harmonizing parts.

Indicator
GM.CR
AM.2.1
I can perform an improvisation on a given
motive.

Indicator
GM.CR
AH.2.1
I can improvise responding to aural cues.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify chord changes in an
unfamiliar piece to improvise on an
instrument.

x

I can vocally improvise harmonic
embellishments over a given melody.

x

I can...

x

I can improvise stylistically
appropriate harmonizing parts.

x

I can improvise rhythmic and melodic
variations on given melodies in
pentatonic, major, and minor keys.

x

I can perform on an instrument, music
apps, or sing an improvised part in an
ensemble.

x

I can improvise with freedom and
expression within a given key,
tonality, meter, and style.

x

I can...

x
Indicator
GM.CRAL.2.2

I can...

I can improvise short melodies using accurate
and consistent style, meter, and tonality.

Indicator
GM.CR
AM.2.2
I can improvise extended passages using
consistent style, meter, and tonality.

Indicator
GM.CRAH.2.2
I can demonstrate and refine musicality during
improvisational solos.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can improvise a short passage using
only a chord progression or lead sheet.

x

I can follow a lead sheet to perform an
extended improvisation pattern.

x

I can show freedom of expression in
my improvisation.

x

I can create a lead sheet for a jazz
tune.

x

I can perform on an instrument, music
apps, or sing an improvised part in an
ensemble.

x

I can critique and provide feedback for
improvisation work.

x

I can...

x

I can...

x

I can...

Page 257

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can sing alone and with others.
Advanced
Low
Benchmark
GM.P
AL.3
I can sing expressively and apply technical and
stylistic criteria in a variety of songs alone and
in various ensembles.

Advanced
Mid
Benchmark
GM.P
AM.3
I can collaborate with others make technical
and stylistic decisions.

Advanced
High
Benchmark
GM.P
AH.3
I can make technical and stylistic choices
about my performance as a singer alone and in
various ensembles.

Indicator
GM.P
AL.3.1
I can sing in a group with balance.

Indicator
GM.P
AM.3.1
I can sing in ensembles.

Indicator
GM.P
AH.3.1
I can use a variety of technical and stylistic
choices in my performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

x

I can identify the melodic line to
balance my part with ensemble
members.
, FDQ UHVSRQG WR D FRQGXFWRU¶V FXHV WR
adjust balance during a performance.

x

I can sing solos, duets, trios, etc.

x

I can choose appropriate music for
myself.

x

I can use feedback of my performance
to improve my skills.

x

I can accept and use criticism of my
personal choices to improve my work.

x

I can...

x

I can...

I can...

Page 258

Indicator
GM.P
AL.3.2
I can blend with others in an ensemble.

Indicator
GM.P
AM.3.2
I can rehearse with an ensemble to improve my
work.

Indicator
GM.P
AH.3.2
I can apply a variety of musical choices for
performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can sing at various dynamic levels p, mp, mf, f. to blend with the group.

x

I can use song form to memorize
lyrics.

x

I can select music appropriate for my
voice and the venue of performance.

x

I can blend harmonic lines above the
bass line to improve intonation.

x

I can demonstrate appropriate
ensemble behaviors to improve my
group.

x

I can apply style decisions to my
personal performance.

x

I can...

x

I can...

x

I can rehearse and polish my part with
a recording in sectionals.

x

I can...

Anchor Standard 4: I can play instruments alone and with others.
Advanced
Low
Benchmark
GM.P
AL.4
I can play an instrument expressively and
apply technical and stylistic techniques in
variety of music alone and in various
ensembles.

Advanced
Mid
Benchmark
GM.P
AM.4
I can collaborate with others to apply technical
and stylistic techniques in a variety of music
alone and in various ensembles.

Advanced
High
Benchmark
GM.P
AH.4
I can make technical and stylistic choices
about my performance as an instrumentalist
alone and in various ensembles.

Page 259

Indicator
GM.P
AL.4.1
I can play in various musical styles on
instruments.

Indicator
GM.P
AM.4.1
I can collaborate with others to improve my
ensemble.

Indicator
GM.P
AH.4.1
I can control my instrument across expanded
dynamic ranges using stylistic nuances and
expressive inflections.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify intonation tendencies
for my instrument.

x

I can respond to musical score
markings in my part to improve
stylistic intentions in the ensemble.

x

x

x

I can listen to a recording of my
ensemble and suggest areas for
additional practice.
I can compare a benchmark
performance of my ensemble selection
WR P\ JURXS¶V SHUIRUPDQFH

x

I can decide how to improve my
performance.

x

I can incorporate vibrato in a
stylistically appropriate manner.

x

I can...

I can...
x

I can...

Indicator
GM.P
AL.4.2
I can balance my sound with others in an
ensemble.

Indicator
GM.P
AM.4.2
I can control pitch and tone quality with
proper dynamics.

Indicator
GM.P
AH.4.2
I can adjust my intonation relative to chord
tones.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can perform the first four SCBDA
Senior Scales (major and relative
minor) for my instrument, in tune.

x

I can perform the SCBDA Clinic
chromatic range for my instrument, in
tune.

x

x

I can play repertoire with a vibrant
tone on my instrument.

x

I can accurately tune my instrument to
other instruments.

x

I can play my part with an
accompaniment app.

x

I can identify the tonic and dominant
to adjust my pitch.

x

I can identify and apply intonation
tendencies for my instrument.

x

I can use a pitch matching app to
improve my practice and intonation.

x

I can...

x

, FDQ«

I can...

Page 260

Indicator
GM.P
AL.4.3
I can sight read a musical part.
Sample Learning Targets
x

I can demonstrate accurate fingering,
bow technique, or the instrument
fingering system for my instrument.

x

I can perform the SCBDA Clinic
chromatic range for my instrument, in
tune.

x

I can sight read with accuracy two
levels below my playing level.

x

I can...

Anchor Standard 5: I can read and notate music.
Advanced
Low
Benchmark
GM.P
AL.5
I can sight read a variety of music at Grade 2
with technical accuracy.

Advanced
Mid
Benchmark
GM.P
AM.5
I can sight read a variety of music at Grade 3
with technical accuracy.

Advanced
High
Benchmark
GM.P
AH.5
I can sight read a variety of music at Grade 4
with technical accuracy.

Page 261

Indicator
GM.P
AL.5.1

Indicator
GM.P
AM.5.1

Indicator
GM.P
AH.5.1

I can sight read musical works in simple
meters and tonalities with technical accuracy.

I can sight read musical works in a variety of
keys and clefs.

I can sight read musical works in a variety of
keys, clefs, meters.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can sight read a musical work in
duple meter.

x

I can sight read a musical work with 2
or more key changes with accuracy.

x

I can sight read a musical work with 2
or more meter changes with accuracy.

x

I can sight read a musical work in
major tonality.

x

I can apply tempo markings when I
sight read.

x

I can apply dynamic markings when I
sight read.

x

I can...

x

I can identify challenging rhythms in
my part during sight-reading.

x

I can use interactive sight-reading
music software to increase my
technical accuracy.

x

I can...
x

I can...

Indicator
GM.P
AL.5.2
I can respond to a director while sight-reading.

Indicator
GM.P
AM.5.2
I can apply tempo and dynamic markings to
my sight-reading.

Indicator
GM.P
AH.5.2
I can apply expressive music markings to my
sight-reading.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can respond to dynamic cues from a
conductor/director while sight-reading.

x

I can apply tempo markings when I
sight read.

x

I can interpret from the page,
expressive cues while I sight read.

x

I can respond to tempo cues from a
conductor/director while sight-reading.

x

I can identify challenging rhythms in
my part during sight-reading.

x

I can apply dynamic markings when I
sight read.

x

I can...

x

I can accurately select a tempo for
sight-reading based on cues in the
score.
, FDQ«

x

I can use interactive sight-reading
music software to increase my
technical accuracy.
, FDQ«
Page 262

x

x

Artistic Processes: Responding-I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Advanced
Low
Benchmark
GM.R
AL.6
I can examine the use of compositional
techniques within multiple musical works.

Advanced
Mid
Benchmark
GM.R
AM.6
I can collaborate with others to justify the use
of compositional techniques within musical
works.

Advanced
High
Benchmark
GM.R
AH.6
I can analyze compositional techniques, to
H[SODLQ D FRPSRVHU¶V LQWHQW

Indicator
GM.R
AL.6.1
I can describe and use meter, tonality,
intervals, chords, and harmonic progressions
when analyzing written and aural
compositions.

Indicator
GM.R
AM.6.1
I can collaborate with others to determine
intent of changes in meter, tonality and
harmony that contribute to musical style.

Indicator
GM.R
AH.6.1
I can determine the intent of changes in meter,
tonality and harmony in a variety of musical
compositions to create tension and emotional
response.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify and write a 12 bar blues
progression.

x

I can listen and respond to musical
texture (thick/thin/ simple/complex).

x

I can justify the aim or purpose of a
FRPSRVHU¶V PXVLFDO ZRUN

x

I can identify intervals from an aural
example.

x

I can define and recognize
monophonic, polyphonic, and
homophonic textures.

x

I can research the background of a
piece to determine possible
FRPSRVHU¶V LQWHQW

x

I can...
x

I can collaborate with others to
identify elements that explain the
musical style of a work.

x

I can...

x

I can...

Page 263

Indicator
GM.R
AL.6.2

Indicator
GM.R
AM.6.2

Indicator
GM.R
AH.6.2

I can examine and discuss culturally authentic
practices found in musical works.

I can identify compositional techniques used
to achieve unity, variety, tension and release in
music to evoke an emotional response from the
listener.

I can examine timbre and the use of voices,
instruments, and other sound sources in a
variety of musical styles, cultures, and genres.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can compare the impact of cultural
influences on musical works.

x

, FDQ GLVFXVV D FRPSRVHU¶V LQWHQW DQG
suggest a purpose for the musical
work.

x

I can research authentic examples of
cultural music and compare it to
current works.

x

I can cite examples of musical tension
within a musical score or performance.

I can...

x

I can...

x

x

I can write a critical analysis of a
musical work or performance.

x

I can analyze the instrument choice of
a calypso band.

x

I can...

Indicator
GM.R
AL.6.3
I can analyze and describe how the use of
expressive devices and form are used in
culturally and historically diverse genres.

Indicator
GM.R
AM.6.3
I can examine the use of musical form and
expressive devices in a variety of 20th & 21st
Century compositions.

Indicator
GM.R
AH.6.3
I can examine the use of musical form when
analyzing aural examples of a varied repertoire
of music and inform my personal music
preferences.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify sonata, concerto, and
sonata allegro forms from various
time periods.

x

I can discuss the impact of musical
form on the overall intent and
expression of a musical work.

x

I can compare multiple works of one
composer as to design, form, and
justify my personal preferences.

x

I can write an analysis of a
composition about the influence of
form on the overall work.

x

I can compare music forms from the
20th and 21st Centuries to earlier
musical examples.

x

x

, FDQ«

x

, FDQ«

x

I can compare and contrast a musical
work from two different composers of
the same time period as to musical
form.
, FDQ«
Page 264

Anchor Standard 7: I can evaluate music.
Benchmark
GM.R
AL.7
I can collaborate to develop strategies for
improvement of group performances.

Benchmark
GM.R
AM.7
I can make critical evaluations of performance,
compositions, arrangements, and
improvisations.

Indicator
GM.R
AL.7.1

Benchmark
GM.R
AH.7
I can justify personal performance decisions.

Indicator
GM.R
AM.7.1

Indicator
GM.R
AH.7.1

I can compare a group performance to a
benchmark to refine the performance.

I can listen or view a variety of performances
and offer suggestions for improvement.

I can use multiple media sources to critique my
personal performances.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can compare the group to a
benchmark offering strategies for
improvement.

x

I can critique the work of others in my
group through collaboration.

x

I can watch and respond to a digital
recording of my performance.

x

I can analyze a score to make
performance decisions.

x
x

I can...

x

I can offer positive feedback and
suggest improvements of musical
performances.

x

I can research multiple examples of
one musical work as points of
comparison to my recorded
performance.

x

I can write a critical analysis of a
work.

x

I can...

I can...

Page 265

Artistic Processes: Connecting- I can relate music ideas to personal meaning, other arts disciplines, and content
areas.
Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Advanced
Low
Benchmark
GM.C
AL.8
I can analyze a diverse repertoire of music
from a cultural or historical time period.

Advanced
Mid
Benchmark
GM.C
AM.8
I can examine contemporary musical works to
determine the influence of historical and
cultural traditions.

Advanced
High
Benchmark
GM.C
AH.8
I can examine, create, and perform music
based on historical and cultural contributions.

Indicator
GM.C
AL.8.1
I can explain specific cultural and historical
traditions and infuse these ideas into my
music.

Indicator
GM.C
AM.8.1
I can select musical elements in contemporary
music that reflect cultural and historical
influences.

Indicator
GM.C
AH.8.1
I can use historical and cultural contributions
to justify my musical choices.

Sample Learning Targets
x I can compare music of the same time
periods from North America and
Europe.

Sample Learning Targets
x I can identify and research a musician
from the last decade and discuss
his/her impact in the music world.

Sample Learning Targets
x I can trace influences from musical
history to contemporary music styles
as they relate to American music.

x

I can trace connections of history to
musical style.

x

I can create a timeline of music history
to other historical events.

x

x

I can name modern composers and
describe their style influences from a
historical context.

x

I can...

x

I can create a set of program notes that
reflect my performance for a current
program using historical and cultural
contributions as points of interest.

x

I can...

I can...

Page 266

Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
Advanced
Low
Benchmark
GM.C
AL.9
I can apply concepts among arts disciplines
and other content areas to general music and
analyze how my interests and skills will
prepare me for a career.

Advanced
Mid
Benchmark
GM.C
AM.9
I can explain how economic conditions,
cultural values and location influence music
and the need for music related careers.

Advanced
High
Benchmark
GM.C
AH.9
I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a musician.

Indicator
GM.C
AL.9.1
I can explain ideas from other arts disciplines
and content areas through music.

Indicator
GM.C
AM.9.1
I can explain how my artistic choices are
influenced by cultural and social values.

Indicator
GM.C
AH.9.1
I can analyze complex ideals that influence my
artistic perspective and creative work.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can explain how the elements of the
arts have similarities and differences.

x

I can select music for a patriotic
celebration.

x

I can analyze the lyrics of music that I
purchase to determine the political and
societal issues of today.

x

I can use a musical parody to explain
the Pythagorean theorem.

x

I can describe the influence of social
values found in selected musical
works.

x

I can research and describe political
and cultural issues influencing
contemporary musical selections.

I can...

x

I can...

x

I can...
x

Page 267

Indicator
GM.C
AL.9.2
I can identify and describe traditional and
emerging careers in music.

Indicator
GM.C
AM.9.2
I can discuss the impact of economic issues as
they affect the impact on music careers.

Indicator
GM.C
AH.9.2
I can analyze my personal career choices in the
arts or non-arts disciplines.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I compare and contrast current
performing artists and discuss their
influences on our society.

x

I can prepare a feasibility study on the
influence of arts investment in my
community.

x

I can investigate a famous musician
and compare his/her career life to a
possible career for myself.

x

I can investigate the cost of live
performances and professional
musicians in a community.

x

I can explore careers in sound
engineering, producing, or music
video production using technology.

x

I can research products designed to be
competitive in the music market.

x

I can...

x

x

I can use research to predict possible
new careers for music.

x

I can evaluate my personal career
choices in the arts or non-arts
discipline.

x

I can...

I can...

Page 268

General Music Glossary
AABA A design sometimes called rounded binary form it was sometimes originally used for short pieces such as dances.Also called
song form, AABA is a variation of ABA in which the first section, A, is played twice before the middle section, B, and a third
time to conclude the piece. During the late eighteenth century, the rounded binary form developed into the sonata form.
AB Binary form A basic musical form consisting of two sections, A and B; usually they are repeated, creating the form AABB.
ABATernary form A basic musical form consisting of three sections (A, B, and A), the third section being virtually identical to the
first. If it is identical, the third section often is not written out, the performer simply being directed to repeat the first section
(usually marked da capo or D.C.), as in the da capo aria and minuet or scherzo with trio.
Accompaniment A musical part that supports or partners a solo instrument, voice, or group.
Arrangement/Arrange Composition based on existing music (e.g., scoring for voices not used in the original piece, adding a
percussion part to the original).
Articulation The manner or style in which the notes in a piece of music are sung. Attacking and releasing. Beginning and ending a
sound clearly and distinctly.
Aural/Aurally By ear; without reference to or memorization of written music.
Blend To merge voices to form a unified resonant sound in which no individual timbre dominates.
Body Percussion The sounds created by using body parts as percussion instruments (for example, clapping hands, stamping feet).
Call and Response The alteration of musical phrases between groups of musicians. Three terms have been used to distinguish between
different forms of call and response: adjacent (the response follows immediately after the call section); overlapping (the
response begins before the call section has concluded; and interlocking (there is a continuous response with a counter solo
passage over it, so thDW WKH FDOO DQG UHVSRQVH DUH ³ORFNHG´ WRJHWKHU
Canon Two or more voices in which one voice enters after another in exact imitation of the first. (See rounds.)

Page 269

Characteristic Tone Quality The particular sound that is characteristic of a specific instrument at all dynamic and pitch levels and
with all articulations.
Chord Three or more pitches sounded simultaneously or functioning as if sounded simultaneously.color (See timbre, definition 1.)
Compositional Techniques Formal melodic, rhythmic, and harmonic techniques used by composers to create music.Combining music
elements to form a whole including style, form, balance, complexity, and continuity.
Contour The shape of a melody or melodic line, contour can be seen by viewing a written piece, as well as heard as to the direction up,
down, or staying the same pitch.
Cut Time Also called alla breve. Used for quick duple time in which the half note, or occasionally the whole note, is given one beat
instead of two. Descants. Harmonizing voice parts added above the melody.
Developmentally Appropriate Taking into account the fact that developmental change is qualitative, sequential, directional,
cumulative, multifactorial, and individual, diatonic A musical scale (major or minor) comprising intervals of five whole steps
and two half steps.
Diction The choice and use of words and phrases in speech or writing.
Dotted Rhythms Rhythm patterns that contain dotted notes (a dot after the note indicates that the note should be extended by half as
PXFK DJDLQ DV WKH QRWH¶V SULQFLSDO WLPH YDOXH
Dynamics Changes in volume; varying degrees of loudness and softness. Adjective form, dynamic.
Elements of Music Seven basic building blocks of music as follows:
1.Rhythm: (beat, meter, tempo, syncopation)
2. Dynamics: (forte, piano, [etc.], crescendo, decrescendo)
3. Melody: (pitch, theme, conjunct, disjunct)
4. Harmony: (chord, progression, consonance, dissonance, key, tonality, atonality)
5. Tone color: (register, range, instrumentation)
Page 270

6. Texture: (monophonic, homophonic, polyphonic, imitation, counterpoint)
7. Form: (binary, ternary, strophic, through-composed)
Embellishments A group of notes or a single note added to a basic melody as ornamentation. In Orff-Schulwerk, embellishment is also
a color part.
Ensemble Skills The abilities that allow a group of musicians to perform together with a refined degree of unanimity of phrasing,
dynamics, and style.
Enunciation The clarity with which words are spoken or sung.
Form The structure or organization of a musical phrase or composition. AB, or binary, form (in which two contrasting sections are
present) is the most basic. ABA, or ternary, form is derived from binary form and results from the repetition of the first section.
Larger musical forms include rondo, theme and variation, sonata, and symphony.
Genre A type or style of music; an established form of musical composition such as ballad, concerto, folk music, lullaby, march,
spiritual.
Good Posture The position of the body for singing. The chin should be parallel to the floor. The shoulders should be held back and
down with the chest held high but not in a strained position. The abdomen should be flat and firm and held in an expandable
position. The hands should be relaxed and still at the sides. Knees should be flexibly loose and never locked. The feet should be
flat on the floor and held shoulder width apart. The weight of the body should be balanced on both feet and the body should be
held slightly forward.
Harmony/Harmonic -(1) The pattern of intervals and chords in a composition. (2) The ways in which chords and intervals are related
to one another and the ways in which one interval or chord can be connected to another. Adjective form, harmonic.
Head Voice A clear, open tone that resonates in the head and not in the throat or chest.
Improvisation/Improvise The creation of music in the course of performance. Verb form, improvise.
Intervals (1) Pairs of notes sounded at the same time. (2) The distances between two pitches. (Skip, steps, leaps).
Intonation The proper production of a musical tone so that it is played or sung in tune with characteristic tone for voice or instrument.
Page 271

Improvise/Improvisation Create and perform (music, drama, or verse) spontaneously or without preparation.
Key Signatures The sharp, flat, or natural signs placed at the beginning of a staff indicating the tonality of the composition.
Major and Minor Tonalities Keys based upon seven-tone diatonic scales and derived from ancient Greek modes and modalities. A
pentatonic tonality is based upon a five-note scale usually made up of the pitches do, re, mi, so, and la.
Match the Pitch To sing (or play) the same pitch given by another instrument or person.
Meter The way beats of music are grouped, often in sets of two or three.
Motive A short tune or musical figure that characterizes and unifies a composition. It can be of any length but is usually only a few
notes long. A motive can be a melodic, harmonic, or rhythmic pattern that is easily recognizable throughout the composition.
Notation/Notate A system used for writing down music showing aspects of music tones such as the tones to be sounded (pitch), the
time each tone should be held in relation to the others (duration), and the degree of loudness (dynamics) at which the tone
should be played. Verb form, notate.
Ostinati Short music patterns that are repeated persistently throughout a performance, composition, or a section of one. (Singular form,
ostinato.)
Partner Songs Two or more different songs that are performed at the same time to create harmony.
Pentatonic A scale made up of five tones (usually do, re, mi, so and la) as opposed to the seven-tone diatonic scale and the twelve-tone
chromatic scale. The pentatonic scale is found in the music of many Asian and African peoples, as well as in some European
folk music. See tonality.
Pitch(1) The property of a musical tone that is determine by the frequency of the sound waves creating it. (2) The highness or lowness
of a tone.
Pitched Adjective describing instruments that produce various tones; includes the families of brass, woodwinds, strings, and
keyboards.
Question-and-Answer Adjective describing a pattern or phrase in which a pair of musical statements complement one another in
rhythmic symmetry and harmonic balance.
Page 272

Rhythm Syllables Musical training involving both ear training and sight singing. Whether the teacher chooses Kodály, Orff, Suzuki, or
another method, it must be used appropriately, sequentially, and consistently.
Rondo The musical form in which the first section, A, recurs after each of several contrasting sections: ABACA.
Rounds Songs or instrumental pieces that begin with a single voice or instrument on the melody, followed at intervals by the other
voices or instruments that enter individually and perform exactly the same melody, thus forming a polyphonic harmony out of a
simple melody. (See canon.)
Solfège $ PXVLF H[HUFLVH LQYROYLQJ ERWK HDU WUDLQLQJ DQG VLJKW VLQJLQJ :KHWKHU WKH WHDFKHU XVHV .RGiO\¶V PHWKRGRORJ\ -RKQ
)HLHUDEHQG¶V Conversational Solfege series, or the Alexander Technique, it must be used appropriately, sequentially, and
consistently.
Style/Stylistic 7KH FRPSRVHU¶V PDQQHU RI WUHDWLQJ WKH YDULRXV HOHPHQWV WKDW PDNH XS D FRPSRVLWLRQ²the overall form, melody,
rhythm, harmony, instrumentation, and so forth²DV ZHOO DV IRU WKH SHUIRUPHU¶V PDQQHU RI SUHVHQWLQJ WKH FRPSRVLWLRQ
Adjective form, stylistic. Adverb form, stylistically. syncopation Stress on a normally unstressed beat.
Tempo(1) A steady succession of units of rhythm; the beat. (2) The speed at which a piece of music is performed or is written to be
performed. Texture. The number and relationship of musical lines in a composition.
Theme and Variation A musical form consisting of a main idea followed by changed versions of that idea.
Timbre(1) The blend of overtones (harmonics) that distinguish a note played on a flute, for example, from the same note played on the
violin. (2) The distinctive tone quality of a particular musical instrument.
Tonality The use of a central note, called the tonic, around which the other tonal material of a composition (notes, intervals, chords) is
built and to which the music returns for a sense of rest and finality. The term tonality refers particularly to harmony and to
chords and their relationships.
Triplets Three notes of equal length that are performed in the duration of two notes of equal length.
Two-and Three-part Songs written for two voices or three voices (for example, soprano and alto; soprano, alto, and baritone).
Unpitched Adjective describing instruments that do not produce various tones; includes such percussion instruments as claves,
maracas, and wood blocks.
Page 273

Verse and Refrain The verse section of the song is the section in which different sets of words are sung to the same repeated melody
and contrasts with a refrain, where the words and melody are both repeated.

Page 274

References
Ammer, C. (1987). The Harper dictionary of music. (2nd ed.). New York: Harper and Row.
Apel, W., & Daniel, R. T. (1961). The Harvard brief dictionary of music. New York: Washington Square Press.
Catterall, J. S. (2009). Doing well and doing good by doing art: A 12-year study of education in the visual and performing arts. Los
Angeles: I-Group Books.
Cole, R. & Schwartz, E. (2009). Virginia Tech multimedia music dictionary. Retrieved from http://www.music.vt.edu/musicdictionary/
Correct Singing Posture. University of Kansas: Department of Music and Dance. Retrieved from
http://web.ku.edu/~cmed/gummposture/posture.html
Kennedy, M. (1985). The Oxford dictionary of music. Oxford: Oxford University Press.
MacDonald, R. A., Kreutz, G., & Mitchell, L. (2012). Music, health, and well-being. New York: Oxford University Press.
Merlot Classics. (2007). Music dictionary. Dolmetsch Organization. http://www.dolmetsch.com/musictheorydefs.htm
National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.org
Randel, D. M. (Ed.). (1986). The new Harvard dictionary of music. Cambridge, MA: Belknap Press.
Sadie, S. (Ed.). (1988). The Grove concise dictionary of music. London: Macmillan.
South Carolina Department of Education. (2010). South Carolina academic standards for general music. Retrieved from
http://ed.sc.gov/scdoe/assets/file/agency/ccr/Standards-Learning/documents/AcademicStandards2010DraftGeneralMusic.pdf

Page 275

# Instrumental Music
## Introduction

The new South Carolina College- and Career-Ready Standards for Instrumental Music Proficiency were written for the learner.
Learners begin new music experiences at different ages and progress toward music proficiency at different rates. The amount of quality
time spent in each course of study is a determining factor in the proficiency level that learners will reach. Learners at similar ages
frequently demonstrate varying proficiency levels. The continuum permits flexibility in acknowledging that students will
developmentally progress through the artistic processes at varying degrees over time.
The 2017 Instrumental Music Standards align with the 2014 National Core Arts Standards for Music and contain some language and
content from the 2010 Instrumental Music Standards. The 2017 standards were written within four artistic processes: creating,
responding, performing, and connecting. Within the artistic processes, the document outlines benchmarks, indicators, and sample
OHDUQLQJ WDUJHWV IHDWXULQJ ³, cDQ´ VWDWHPHQWV designed to place the learner LQ WKH GULYHU¶V VHDW RI WKHLU OHDUQLQJ SURFHVV These standards
cover Band and Orchestra. Standards related to performance contain Indicators and Sample Learnings Targets that are Band and
Orchestra specific.
The ultimate goal of the 2017 Instrumental Music Standards is to provide a document that is learner centered, meeting individual
educational needs and instilling a lifelong appreciation for music.

Page 277

Instrumental Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can compose and arrange music.
Novice
Low

Novice
Mid

Novice
High

Intermediate
Low

Intermediate
Mid

Intermediate
High

Advanced
Low

Advanced
Mid

Advanced
High

Benchmark
IM.CR
NL.1

Benchmark
IM.CR
NM.1

Benchmark
IM.CR
NH.1

Benchmark
IM.CR
IL.1

Benchmark
IM.CR
IM.1

Benchmark
IM.CR
IH.1

Benchmark
IM.CR
AL.1

Benchmark
IM.CR
AM.1

Benchmark
IM.CR
AH.1

I can notate
tonal patterns
using a
defined
selection of
pitches.

I can notate
simple
melodies for
my
instrument

I can
compose and
arrange
melodies for
my
instrument
within simple
forms.

I compose
and arrange
simple
harmonic
accompaniments.

I can
compose and
arrange
melodies
with simple
harmonic
accompaniments.

I can
compose and
arrange
incorporating
expressive
elements.

I can
compose and
arrange short
musical
works for a
small
ensemble.

I can
compose and
arrange short
musical
works for a
large
ensemble.

I can notate
rhythm
patterns
using a
defined
selection of
note values.

Page 278

Indicator
IM.CR
NL.1.1

Indicator
IM.CR
NM.1.1

Indicator
IM.CR
NH.1.1

Indicator
IM.CR
IL.1.1

Indicator
IM.CR
IM.1.1

Indicator
IM.CR
IH.1.1

Indicator
IM.CR
AL.1.1

Indicator
IM.CR
AM.1.1

Indicator
IM.CR
AH.1.1

I can choose
various note
values to
represent
simple
rhythm
patterns.

I can choose
various
pitches to
represent
simple
melodic
patterns.

I can write a
simple
melody
within a
given key,
tonality, and
meter.

I can adapt a
melody for
my
instrument.

I can write
basic chords
in a given
key.

I can
compose
melodies
with simple
chord
progressions.

I can
compose
incorporating
expressive
elements.

I can arrange
a work for
two
instruments.

I can arrange
a work for
large
ensembles
incorporating
elements of
melody,
harmony,
rhythm,
timbre,
texture, form,
and
expression to
communicate
a mood,
emotion,
idea, or
experience.

Indicator
IM.CR
NL.1.2

Indicator
IM.CR
NM.1.2

Indicator
IM.CR
IL.1.2

Indicator
IM.CR
IM.1.2

Indicator
IM.CR
IH.1.2

Indicator
IM.CR
AL.1.2

Indicator
IM.CR
AM.1.2

I can create a
variation on a
given theme.

I can write a
basic chord
progression
in a given
key.

I can arrange
melodies
with simple
chord
progressions.

I can arrange
incorporating
expressive
elements.

I can arrange
a work for
small
ensemble.

I can write
simple
rhythm
patterns
using
standard
music
notation.

I can write
simple
melodic
patterns
using
standard
music
notation.

Page 279

Indicator

Indicator

IM.CR
IL.1.3

IM.CR
IM.1.3

I can
compose
using verserefrain, AB,
ABA, and
theme &
variation
forms.

I can write a
basic
harmonic
accompanyment in a
given key.

Anchor Standard 2: I can improvise music.
Benchmark
IM.CR
NL.2

Benchmark
IM.CR
NM.2

Benchmark
IM.CR
NH.2

Benchmark
IM.CR
IL.2

Benchmark
IM.CR
IM.2

Benchmark
IM.CR
IH.2

Benchmark
IM.CR
AL.2

Benchmark
IM.CR
AM.2

Benchmark
IM.CR
AH.2

I can imitate
simple
rhythm
patterns
within a
given meter.

I can imitate
simple tonal
patterns
within a
given key
and tonality.

I can imitate
simple
melodic
phrases given
simple chord
changes.

I can
improvise
simple
rhythm
patterns
within a
given meter.

I can
improvise
simple tonal
patterns
within a
given key
and tonality.

I can
improvise
simple
melodic
phrases given
chord
changes.

I can perform
a brief
improvisation
given a chord
progression
and meter.

I can perform
an
improvisation
given a
motive, chord
progression,
and meter.

I can perform
an extended
spontaneous
improvisation
with freedom
and
expression
featuring
motivic
development
within a
given key,
tonality,
meter, and
style.

Page 280

Indicator
IM.CR
NL.2.1
I can echo
simple
rhythm
patterns on a
neutral
syllable,
incorporating
movement.

Indicator
IM.CR
NM.2.1
I can echo
simple tonal
patterns on a
neutral
syllable.

Indicator
IM.CR
NH.2.1
I can imitate
simple
melodic
phrases given
simple chord
changes.

Indicator
IM.CR
IL.2.1
I can
improvise my
own simple
rhythm
patterns on a
neutral
syllable,
incorporating
movement.

Indicator
IM.CR
IM.2.1
I can
improvise my
own simple
tonal patterns
on a neutral
syllable.

Indicator
IM.CR
IH.2.1
I can identify
chord
changes.

Indicator
IM.CR
AL.2.1
I can
improvise
short melodic
patterns in
varying
meters.

Indicator
IM.CR
AM.2.1
I can perform
an
improvisation
on a given
motive.

Indicator
IM.CR
AH.2.1
I can
improvise an
extended
unaccompanied solo
within a
given key,
tonality,
meter, and
style.

Indicator
IM.CR
NL.2.2

Indicator
IM.CR
NM.2.2

Indicator
IM.CR
NH.2.2

Indicator
IM.CR
IL.2.2

Indicator
IM.CR
IM.2.2

Indicator
IM.CR
IH.2.2

Indicator
IM.CR
AL.2.2

Indicator
IM.CR
AM.2.2

Indicator
IM.CR
AH.2.2

I can
improvise my
own simple
rhythm
patterns
using
rhythmic
solfege or a
counting
system.

I can
improvise my
own simple
tonal patterns
using tonal
solfege or
pitch names.

I can
improvise a
short passage
using only a
chord
progression
or lead sheet.

I can
improvise an
extended
passage using
only a chord
progression
or lead sheet.

I can
improvise
freely within
a given key,
tonality,
meter, and
style,
responding to
aural cues
from other
members of
an ensemble.

I can echo
simple
rhythm
patterns
using
rhythmic
solfege or a
counting
system.

I can echo
simple tonal
patterns
using tonal
solfege on
pitch names.

I can
embellish a
given
melodic
phrase that
corresponds
with simple
chord
changes on
my
instrument.

I can
improvise
simple
melodic
phrases that
correspond
with chord
changes.

Page 281

Indicator
IM.CR
NL.2.3
I can echo
simple
rhythm
patterns on
my
instrument.

Indicator
IM.CR
NM.2.3
I can echo
simple tonal
patterns on
my
instrument.

Indicator
IM.CR
IL.2.3

Indicator
IM.CR
IM.2.3

I can
improvise my
own simple
rhythm
patterns on
my
instrument.

I can
improvise on
my own
simple tonal
patterns on
my
instrument.

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a characteristic tone.
BAND
Benchmark
IM.B.P
NL.3

Benchmark
IM.B.P
NM.3

Benchmark
IM.B.P
NH.3

Benchmark
IM.B.P
IL.3

Benchmark
IM.B.P
IM.3

Benchmark
IM.B.P
IH.3

Benchmark
IM.B.P
AL.3

Benchmark
IM.B.P
AM.3

Benchmark
IM.B.P
AH.3

I can produce
a steady, free
tone on a
comfortable
pitch.

I can produce
a steady, free
tone within a
limited
range.

I can produce
a steady, free
tone with a
limited
range, in
tune.

I can produce
a centered
tone in a
comfortable
register.

I can produce
a centered
tone in most
registers.

I can produce
a centered
tone in all
registers.

I can produce
a centered
tone in all
registers and
at moderate
dynamic
levels.

I can
consistently
produce a
centered,
vibrant tone
in all
registers and
across the
entire
dynamic
range of my
instrument.

I can adjust
tone color,
vibrato
speed, and
vibrato
width in
response to
stylistic
demands and
the musical
needs of an
ensemble.

Page 282

Indicator
IM.B.P
NL.3.1

Indicator
IM.B.P
NM.3.1

Indicator
IM.B.P
NH.3.1

Indicator
IM.B.P
IL.3.1

Indicator
IM.B.P
IM.3.1

Indicator
IM.B.P
IH.3.1

Indicator
IM.B.P
AL.3.1

Indicator
IM.B.P
AM.3.1

Indicator
IM.B.P
AH.3.1

I can play the
first sounds
on my
instrument
with
characteristic tone
quality.

I can play the
first 3 - 5
pitches on
my
instrument
with
characteristic tone
quality.

I can play all
of the pitches
,¶YH OHDUQHG
with a
characteristic tone and
in tune.

I can play in
tune within a
limited
range.

I can play in
tune within
an expanding
range.

I can
consistently
play in tune.

I can identify
intonation
inaccuracies
and make
adjustments
as needed.

I can play in
tune
individually
with a vibrant
tone.

I can play in
tune
individually
and with an
ensemble
while making
adjustments
as needed.

Indicator
IM.B.P
NL.3.2
I can
demonstrate
correct
posture and
playing
position.

Indicator
IM.B.P
AM.3.2
I can play in
tune across a
range of
dynamics on
my
instrument
with a vibrant
tone.

Page 283

ORCHESTRA
Benchmark
IM.O.P
NL.3

Benchmark
IM.O.P
NM.3

Benchmark
IM.O.P
NH.3

Benchmark
IM.O.P
IL.3

Benchmark
IM.O.P
IM.3

Benchmark
IM.O.P
IH.3

Benchmark
IM.O.P
AL.3

Benchmark
IM.O.P
AM.3

Benchmark
IM.O.P
AH.3

I can produce
a steady, free
tone on a
comfortable
pitch.

I can produce
a steady, free
tone within a
limited
range.

I can produce
a steady, free
tone with a
limited
range, in
tune.

I can produce
a centered
tone in a
comfortable
register.

I can produce
a centered
tone in most
registers.

I can produce
a centered
tone in all
registers.

I can produce
a centered
tone in all
registers and
at moderate
dynamic
levels.

Indicator
IM.O.P
NL.3.1

Indicator
IM.O.P
NM.3.1

Indicator
IM.O.P
NH.3.1

Indicator
IM.O.P
IL.3.1

Indicator
IM.O.P
IM.3.1

Indicator
IM.O.P
IH.3.1

Indicator
IM.O.P
AL.3.1

I can
consistently
produce a
centered,
vibrant tone
in all
registers and
across the
entire
dynamic
range of my
instrument.
Indicator
IM.O.P
AM.3.1

I can adjust
tone color,
vibrato
speed, and
vibrato
width in
response to
stylistic
demands and
the musical
needs of an
ensemble.
Indicator
IM.O.P
AH.3.1

I can hold my
bow
correctly.

I can move
my bow, both
up and down,
while playing
a selection of
notes.

I can play
with my left
hand in
position
using correct
finger
patterns on
the
fingerboard.

I can play in
tune within
an ensemble
on an
appropriate
level of
music.

I can perform
appropriate
scales that
use expanded
registers, in
tune.

I can perform
using
appropriate
finger
placement
associated
with
extensions
and shifting.

I can perform
using
appropriate
bowing
dexterity to
produce
varied
dynamics.

I can perform
with a full,
resonant tone
in all
registers of
my
instrument.

I can play in
tune
individually
and with an
ensemble by
blending into
a uniform
sound while
making
adjustments
as needed.

Page 284

Indicator
IM.O.P
NL.3.2

Indicator
IM.O.P
NM.3.2

Indicator
IM.O.P
NH.3.2

Indicator
IM.O.P
IL.3.2

Indicator
IM.O.P
IM.3.2

Indicator
IM.O.P
IH.3.2

Indicator
IM.O.P
AL.3.2

Indicator
IM.O.P
AM.3.2

Indicator
IM.O.P
AH.3.2

I can move
the bow on
open strings.

I can identify
whole and
half steps,
placing my
fingers on my
strings
accordingly.

I can move
the bow
using
detached and
connected
bow stroke
techniques.

I can identify
notes that are
higher or
lower than
first position
on my
instrument.

I can move
my left hand
position to
execute basic
extensions
and shifting
finger
patterns, in
tune.

I can play in
tune in higher
positions,
making
accurate
shifts.

I can perform
using
appropriate
hand
positions
with precise
shifting
technique
and finger
selections.
Indicator
IM.O.P
AL.3.3

I can perform
using
appropriate
vibrato
width and
speed in all
registers of
my
instrument.

I can perform
with the same
tone,
resonance,
and vibrato
of others in
an ensemble.

Indicator
IM.O.P
NH.3.3

Indicator
IM.O.P
IH.3.3

I can listen
and adjust
my finger
placement to
match a
given pitch.

I can play
using specific
contact
points on my
instrument
and bow to
create
dynamics.
Indicator
IM.O.P
IH .3.4

I can perform
using vibrato
to develop
resonant
tone.

I can move
my left hand
using
primary
vibrato
skills.

Page 285

Anchor Standard 4: I can perform with technical accuracy and expression.
BAND
Benchmark
IM.B.P
NL.4

Benchmark
IM.B.P
NM.4

Benchmark
IM.B.P
NH.4

Benchmark
IM.B.P
IL.4

Benchmark
IM.B.P
IM.4

Benchmark
IM.B.P
IH.4

Benchmark
IM.B.P
AL.4

Benchmark
IM.B.P
AM.4

Benchmark
IM.B.P
AH.4

I can
demonstrate
correct
posture,
guide
position, and
fundamental
fingering/
stick/bow
technique.

I can keep a
steady pulse
in duple and
triple
division and
produce basic
articulations.

I can perform
basic
dynamic
contrasts
and simple
phrases.

I can
demonstrate
increasing
dexterity
across an
expanding
range and at
increasing
tempos.

I can produce
gradual,
controlled
dynamic
changes and
perform
extended
phrases.

I can
demonstrate
fluent
fingering/stic
k/bow
technique
across the
entire range
of my
instrument.

I can perform
with
appropriate
flexibility
within given
meter and
use advanced
articulation
techniques in
a stylistically
appropriate
way.

I can control
pitch and
tone quality
across
expanded
dynamic
range, using
appropriate
stylistic
nuance and
expressive
inflections.

Indicator
IM.B.P
NL.4.1

Indicator
IM.B.P
NM.4.1

Indicator
IM.B.P
NH.4.1

Indicator
IM.B.P
IL.4.1

I can perform
increasingly
complex
rhythms and
meters with
precision and
produce an
expanding
variety of
articulations
with
increasing
facility.
Indicator
IM.B.P
IM.4.1

Indicator
IM.B.P
IH.4.1

Indicator
IM.B.P
AL.4.1

Indicator
IM.B.P
AM.4.1

Indicator
IM.B.P
AH.4.1

I can hold my
instrument
the correct
way at all
times.

I can keep a
steady pulse
in various
meters.

I can
demonstrate
correct
technique for
performing
loud and soft
dynamics.

I can play
scales and/or
rudiments
with
accuracy.

I can play
syncopated
patterns,
quarter,
eighth, and
sixteenth
note
rhythms in
various
meters.

I can
demonstrate
dynamic
contrast and
play four to
eight bar
phrases.

I can
demonstrate
the entire
fingering
system/
position or
rudiment for
my
instrument.

I can
demonstrate
appropriate
tempo
flexibility
within a
given meter.

I can perform
with
technical
ease and
stylistic
integrity.

Page 286

Indicator
IM.B.P
NL.4.2

Indicator
IM.B.P
NM.4.2

Indicator
IM.B.P
NH.4.2

Indicator
IM.B.P
IL.4.2

Indicator
IM.B.P
AM.4.2

I can play
using correct
fingering/
sticking
technique.

I can play
using basic
articulations.

I can shape a
basic musical
phrase.

I can perform
an expanding
variety of
articulations
with
accuracy.

I can play in
a specified
style.

Indicator
IM.B.P
NL.4.3
I can play
simple scale
and/or
rudimental
patterns.

ORCHESTRA
Benchmark
IM.O.P
NL.4

Benchmark
IM.O.P
NM.4

Benchmark
IM.O.P
NH.4

Benchmark
IM.O.P
IL.4

Benchmark
IM.O.P
IM.4

Benchmark
IM.O.P
IH.4

Benchmark
IM.O.P
AL.4

Benchmark
IM.O.P
AM.4

Benchmark
IM.O.P
AH.4

I can
demonstrate
correct
posture,
guide
position, and
fundamental
fingering/
stick/bow
technique.

I can keep a
steady pulse
in duple and
triple
division and
produce basic
articulations.

I can perform
basic
dynamic
contrasts
and simple
phrases.

I can
demonstrate
increasing
dexterity
across an
expanding
range and at
increasing
tempos.

I can perform
increasingly
complex
rhythms and
meters with
precision and
produce an
expanding
variety of
articulations
with
increasing
facility.

I can produce
gradual,
controlled
dynamic
changes and
perform
extended
phrases.

I can
demonstrate
fluent
fingering/
stick/bow
technique
across the
entire range
of my
instrument.

I can perform
with
appropriate
flexibility
within given
meter and
use advanced
articulation
techniques in
a stylistically
appropriate
way.

I can control
pitch and
tone quality
across
expanded
dynamic
range, using
appropriate
stylistic
nuance and
expressive
inflections.

Page 287

Indicator
IM.O.P
NL.4.1

Indicator
IM.O.P
NM.4.1

Indicator
IM.O.P
NH.4.1

Indicator
IM.O.P
IL.4.1

Indicator
IM.O.P
IM.4.1

Indicator
IM.O.P
IH.4.1

Indicator
IM.O.P
AL.4.1

Indicator
IM.O.P
AM.4.1

Indicator
IM.O.P
AH.4.1

I can hold my
instrument
correctly at
all times.

I can perform
basic
rhythms,
keeping a
steady pulse.

I can
demonstrate
loud and soft
dynamics.

I can perform
using correct
hand
positions to
reach
appropriate
registers of
my
instrument.

I can perform
rhythms
using a
developing
knowledge of
note and rest
values.

I can perform
using
dynamic
expression.

I can perform
with
appropriate
and welldefined
bowing
techniques.

I can perform
with
technical
ease and
stylistic
integrity.

Indicator
IM.O.P
NL.4.2

Indicator
IM.O.P
NM.4.2

Indicator
IM.O.P
NH.4.2

Indicator
IM.O.P
IL.4.2

Indicator
IM.O.P
IM.4.2

Indicator
IM.O.P
IH.4.2

I can perform
using
appropriate
hand
positions
with precise
shifting
technique
and
fingerings.
Indicator
IM.O.P
AL.4.2

I can hold my
instrument
with
appropriate
posture.

I can perform
music in
simple duple
and triple
meters.

I can perform
with
increasing
tempo using
precise finger
placement
and bow
movement.

I can perform
a variety of
articulations.

I can perform
lyrically
shaped
dynamics
using
appropriate
bow control.

I can perform
using a
variety of
articulations
with
increasing
dexterity.

Indicator
IM.O.P
NL.4.3
I can pay my
instrument
using correct
bow hold and
bow
movement.

Indicator
IM.O.P
NM.4.3
I can play
using basic
articulations.

I can play
musical
phrases
within my
repertoire.

Indicator
IM.O.P
IM.4.3
I can perform
music
containing
compound
duple and
triple time
signatures.

Page 288

Anchor Standard 5: I can perform using musical notation.
Benchmark
IM.P
NL.5

Benchmark
IM.P
NM.5

Benchmark
IM.P
NH.5

Benchmark
IM.P
IL.5

Benchmark
IM.P
IM.5

Benchmark
IM.P
IH.5

Benchmark
IM.P
AL.5

Benchmark
IM.P
AM.5

Benchmark
IM.P
AH.5

I can identify
music
notation
symbols
representing
simple
familiar tonal
and rhythm
patterns and
tunes.

I can perform
simple
familiar tonal
and rhythm
patterns and
tunes using
music
notation.

I can perform
simple
unfamiliar
tonal and
rhythm
patterns and
tunes using
music
notation.

I can perform
at sight
simple
unfamiliar
musical
works.

I can perform
at sight
moderately
complex
unfamiliar
musical
works.

I can perform
at sight
complex
unfamiliar
musical
works with
accuracy.

I can perform
at sight
complex
unfamiliar
musical
works with
accuracy and
appropriate
expression/
interpretation.

Indicator
IM.P
NL.5.1

Indicator
IM.P
NM.5.1

Indicator
IM.P
NH.5.1

I can identify
music
notation
symbols
representing
an expanded
set of tonal,
rhythmic,
technical,
expressive,
and formal
indications.
Indicator
IM.P
IL.5.1

Indicator
IM.P
IM.5.1

Indicator
IM.P
IH.5.1

Indicator
IM.P
AL.5.1

Indicator
IM.P
AM.5.1

I can perform
at sight
complex
unfamiliar
musical
works with
accuracy,
appropriate
expression/
interpretation, and
fluency.
Indicator
IM.P
AH.5.1

I can identify
the pitches in
the clef
appropriate
to my
instrument.

I can perform
simple
familiar
rhythm
patterns
using music
notation.

I can perform
simple
unfamiliar
rhythm
patterns
using music
notation.

I can identify
advanced key
signatures in
the clef
appropriate
to my
instrument.

I can perform
at sight
simple
unfamiliar
musical
works with
accurate
pitches.

I can perform
at sight
moderately
complex
unfamiliar
musical
works with
accurate
pitches.

I can perform
at sight
complex
unfamiliar
musical
works with
accurate
pitches.

I can perform
at sight
complex
unfamiliar
musical
works with
correct
articulation.

I can perform
at sight
complex
unfamiliar
music works
with fluency.

Page 289

Indicator
IM.P
NL.5.2

Indicator
IM.P
NM.5.2

Indicator
IM.P
NH.5.2

Indicator
IM.P
IL.5.2

Indicator
IM.P
IM.5.2

Indicator
IM.P
IH.5.2

Indicator
IM.P
AL.5.2

Indicator
IM.P
AM.5.2

Indicator
IM.P
AH.5.2

I can identify
accidentals
and simple
key
signatures.

I can perform
simple
familiar tonal
patterns
using music
notation.

I can perform
simple
unfamiliar
tonal patterns
using music
notation.

I can perform
at sight
simple
unfamiliar
musical
works with
accurate
pitches and
rhythms.

I can perform
at sight
moderately
complex
unfamiliar
musical
works with
accurate
pitches and
rhythms.

I can perform
at sight
complex
unfamiliar
musical
works with
accurate
pitches and
rhythms.

I can perform
at sight
complex
unfamiliar
musical
works with
correct
dynamics.

I can perform
at sight
complex
unfamiliar
musical
works with
stylistic
integrity.

Indicator
IM.P
NL.5.3

Indicator
IM.P
NM.5.3

Indicator
IM.P
NH.5.3

I can identify
advanced
note values
and meter
signatures
that represent
syncopation
and smaller
beat
subdivisions
in my music.
Indicator
IM.P
IL.5.3

Indicator
IM.P
IM.5.3

Indicator
IM.P
IH.5.3

Indicator
IM.P
AL.5.3

Indicator
IM.P
AM.5.3

I can identify
note values in
familiar
patterns and
tunes.

I can perform
simple
familiar tunes
using music
notation.

I can perform
simple
unfamiliar
tunes using
music
notation.

I can identify
technical,
expressive,
and formal
indications in
my music.

I can perform
at sight
simple
unfamiliar
musical
works with
accurate
pitches and
rhythms at a
steady
tempo.

I can perform
at sight
moderately
complex
unfamiliar
musical
works with
accurate
pitches and
rhythms at a
steady
tempo.

I can perform
at sight
complex
unfamiliar
musical
works with
accurate
pitches and
rhythms at a
steady
tempo.

I can perform
at sight
complex
unfamiliar
musical
works with
appropriate
phrasing.

Page 290

Indicator
IM.P
NL.5.4
I can identify
simple
familiar
rhythm
patterns with
corresponding notation.

Artistic Processes: Responding- I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Benchmark
IM.R
NL.6

Benchmark
IM.R
NM.6

Benchmark
IM.R
NH.6

Benchmark
IM.R
IL.6

Benchmark
IM.R
IM.6

Benchmark
IM.R
IH.6

Benchmark
IM.R
AL.6

Benchmark
IM.R
AM.6

Benchmark
IM.R
AH.6

I can identify
the elements
of music in
compositions
that I hear.

I can identify
musical
symbols and
describe how
the elements
of music are
used.

I can describe
how
repetition and
contrast are
used in music
and identify
key
signatures.

I can identify
simple music
forms in
compositions
that I hear
and see.

I can identify
how the
elements of
music are
used in
varying
genres.

I can explain
how the
elements of
music are
used in
varying
genres.

I can explain
how the use
of music
elements
impacts
compositions
from
different
historical
periods and
cultures.

I can
examine
musical
forms in
compositions
from varying
genres and
styles.

I can justify
how
structure,
forms, and
performance
decisions
inform
responses to
music based
on the
elements of
music.

Page 291

Indicator
IM.R
NL.6.1

Indicator
IM.R
NM.6.1

Indicator
IM.R
NH.6.1

I can describe
what I hear in
a piece of
music using
musical
vocabulary.

I can explain
how
repetition is
used in
music.

Indicator
IM.R
NL.6.2

Indicator
IM.R
NM.6.2

Indicator
IM.R
NH.6.2

I can identify
tempo and
rhythm.

I can identify
musical
terms in
written music
that I perform
and rehearse.

I can describe
similarities
and
differences in
the music
that I hear.

I can name
the
instruments
that I hear.

Indicator
IM.R
IL.6.1

Indicator
IM.R
IM.6.1

Indicator
IM.R
IH.6.1

Indicator
IM.R
AL.6.1

I can identify
how the
melody,
harmony,
rhythm,
timbre,
texture, form,
and
expressive
elements are
different in
varying
genres of
music.

I can explain
the use of
melody,
harmony,
rhythm,
timbre,
texture, form,
and
expressive
elements in
varying
genres of
music.

I can identify
forms used in
varying
genres and
historical
periods.

Indicator
IM.R
IL.6.2

Indicator
IM.R
IH.6.2

Indicator
IM.R
AL.6.2

Indicator
IM.R
AM.6.2

I can
recognize
form in
music that I
hear and see.

I can describe
how the
elements of
music
function in
different
genres.

I can describe
stylistic
qualities of
music from
different
historical
periods.

I can identify
key
signature
changes and
modulations
in relation to
form.

I can
recognize
melodic
themes in
music that I
hear.

Indicator
IM.R
AM.6.1

Indicator
IM.R
AH.6.1

I can describe I can identify
characteristic harmonic
s of a variety structure.
of musical
forms.

Indicator
IM.R
AH.6.2
I can apply
stylistic
qualities of
music from
different
historical
periods as I
perform on
my
instrument.

Page 292

Indicator
IM.R
NL.6.3

Indicator
IM.R
NM.6.3

Indicator
IM.R
NH.6.3

Indicator
IM.R
IL.6.3

Indicator
IM.R
IM.6.3

Indicator
IM.R
IH.6.3

Indicator
IM.R
AL.6.3

Indicator
IM.R
AM.6.3

Indicator
IM.R
AH.6.3

I can identify
melody,
harmony,
and form.

I can identify
musical
symbols in
my music.

I can
identify key
signatures as
they appear
in music that
I see.

I can
recognize
call and
response in
music that I
hear and see.

I can identify
major and
minor
tonalities.

I can identify
forms used in
varying
genres.

I can describe
musical
works from
different
cultures.

I can describe
stylistic
qualities of
music from
different
historical
periods and
how it
applies to my
instrument.

I can justify
the
performance
decisions in a
variety of
musical
works.

Anchor Standard 7: I can evaluate music.
Benchmark
IM.R
NL.7

Benchmark
IM.R
NM.7

Benchmark
IM.R
NH.7

Benchmark
IM.R
IL.7

Benchmark
IM.R
IM.7

Benchmark
IM.R
IH.7

Benchmark
IM.R
AL.7

Benchmark
IM.R
AM.7

Benchmark
IM.R
AH.7

I can actively
listen to live
or recorded
performances to
identify some
musical
elements

I can describe
my personal
interest in
music
performances using a
given list of
music
terminology.

I can list the
criteria I use
to describe
my interest in
music
performances using
appropriate
music
terminology.

I can describe
the quality of
music
performances using
provided
criteria.

I can explain
my
evaluation of
performances of
others.

I can describe
the quality of
my performances and
compositions.

I can analyze
performance
s and
compositions
, offering
constructive
suggestions
for
improvement
using
provided
criteria.

I can analyze
and critique
compositions
and
performances using
personallydeveloped
criteria.

I can justify
my criteria
for
evaluating
music works
and
performances based
on personal
and
collaborative
research.

Page 293

Indicator
IM.R
NL.7.1
I can actively
listen to
music
performances.

Indicator
IM.R
NM.7.1
I can use
basic music
terminology
to describe
what I am
hearing.
Indicator
IM.R
NM.7.2
I can
summarize
my personal
preferences
of music.

Indicator
IM.R
NH.7.1

Indicator
IM.R
IL.7.1

Indicator
IM.R
IM.7.1

Indicator
IM.R
IH.7.1

Indicator
IM.R
AL.7.1

Indicator
IM.R
AM.7.1

Indicator
IM.R
AH.7.1

I can describe
some of the
elements of
music that I
hear in a
performance

I can describe
what
contributes to
a quality
performance

I can identify
criteria used
to evaluate
performance
of others.

I can
compare my
performance
to
performance
of others.

Indicator
IM.R
IM.7.2

Indicator
IM.R
IH.7.2

I can analyze
personal
compositions
and provide
recommendat
ions for
improvement
Indicator
IM.R
AM.7.2

I can explain
criteria used
for
evaluation.

Indicator
IM.R
NH.7.2

I can
formulate
constructive
feedback for
personal
performances.
Indicator
IM.R
AL.7.2

I can identify
my personal
criteria for
evaluating
music
performances.

I can describe
the elements
of music that
I hear in
performances.

I can evaluate
my
compositions
using specific
criteria.

I can
formulate
constructive
feedback for
the performances of
others.

I can analyze
performance
s and provide
recommendat
ions for
improvement

I can
collaborate
with others to
assess
musical
works and
performances.
Indicator
IM.R
AH.7.3

Indicator
IM.R
AH.7.2

I can research
topics
pertaining to
musical
performance.

Page 294

Artistic Processes: Connecting- I can relate musical ideas to personal experiences, culture, history, and other
disciplines.
Anchor Standard 8:
I can relate musical ideas to personal experiences, culture, and history.
Benchmark
IM.C
NL.8

Benchmark
IM.C
NM.8

Benchmark
IM.C
NH.8

Benchmark
IM.C
IL.8

Benchmark
IM.C
IM.8

Benchmark
IM.C
IH.8

Benchmark
IM.C
AL.8

Benchmark
IM.C
AM.8

Benchmark
IM.C
AH.8

I can talk
about
musical ideas
based on my
personal
experiences.

I can talk
about
musical ideas
based on my
culture.

I can describe
musical ideas
through my
personal
experiences
and my
culture.

I can describe
the purpose
and value of
music in
some
cultures.

I can research
the purpose
and value of
music in a
specific
culture
different
from my
own.

I can
research how
musical ideas
influence
beliefs,
values, or
behaviors in
various
cultures.

I can
synthesize
my research
about other
cultures and
genres to
enhance my
music
performance.

I can justify
the role of
music in a
global
society.

Indicator
IM.C
NL.8.1

Indicator
IM.C
NM.8.1

Indicator
IM.C
NH.8.1

Indicator
IM.C
IL.8.1

Indicator
IM.C
IM.8.1

I can analyze
how musical
ideas
influence
beliefs,
values, or
behaviors in
a specific
culture
different
from my
own.
Indicator
IM.C
IH.8.1

Indicator
IM.C
AL.8.1

Indicator
IM.C
AM.8.1

I can describe
how sound
and music is
used in my
everyday life.

I can
recognize
musical
concepts and
elements
specific to
my culture.

I can describe
how music is
used in my
life and my
community.

I can
recognize the
significance
and intent of
music some
cultures.

I can
describe the
significance
and intent of
music from a
specific
culture.

I can
interpret how
music
preferences
influence
personal
values and
attitudes.

I can analyze
how genres
of music
influence
social
lifestyles and
current
trends.

I can apply
characteristic
expressive
qualities to
my music
performance.

Indicator
IM.C
AH.8.1
I can defend
interpretations of
music
through
appropriate
musical
vocabulary.

Page 295

Indicator
IM.C
NM.8.2

Indicator
IM.C
NH.8.2

Indicator
IM.C
IL.8.2

Indicator
IM.C
IM.8.2

Indicator
IM.C
IH.8.2

Indicator
IM.C
AL.8.2

Indicator
IM.C
AM.8.2

I can
recognize
how music is
used for
occasions
unique to my
culture.

I can describe
how the
elements of
music are
used in my
culture.

I can identify
the
appropriate
music for
particular
events.

I can describe
how music
functions in a
culture.

I can describe
how music is
a vehicle of
expression
that inspires
listener to
think
differently.

I can explain
how music
preferences
influence
group or
social
stereotypes.

I can apply
characteristic
techniques to
my music
performance.

Indicator
IM.C
AH.8.2
I can justify
the role of
music as
having a
common
purpose in
societies
around the
world.

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Benchmark
IM.C
NL.9

Benchmark
IM.C
NM.9

Benchmark
IM.C
NH.9

Benchmark
IM.C
IL.9

Benchmark
IM.C
IM.9

Benchmark
IM.C
IH.9

Benchmark
IM.C
AL.9

Benchmark
IM.C
AM.9

Benchmark
IM.C
AH.9

I can explore
instrumental
music
concepts
among arts
disciplines
and other
content areas.

I can
recognize
and use
instrumental
music
concepts
among arts
disciplines
and other
content areas.

I can apply
instrumental
music
concepts to
arts
disciplines,
other content
areas, and
related
careers.

I can explore
a range of
skills shared
among arts
disciplines,
other content
areas, and
how they can
be applied to
a career in
music.

I can
recognize
specific skills
shared
among arts
disciplines,
other content
areas, and
how they can
be applied to
a career in
music.

I can analyze
the tools,
concepts, and
materials
used among
arts
disciplines,
other content
areas, and
how they are
used in music
careers.

I can analyze
how my
interests and
skills will
prepare me
for a career
in music.

I can create
an
educational
plan for my
career choice
in music.

I can research
societal,
political, and
cultural
issues as they
relate to other
arts and
content areas
and apply to
my role as a
musician.

Page 296

Indicator
IM.C
NL.9.1

Indicator
IM.C
NM.9.1

Indicator
IM.C
NH.9.1

Indicator
IM.C
IL.9.1

Indicator
IM.C
IM.9.1

Indicator
IM.C
IH.9.1

Indicator
IM.C
AL.9.1

I can identify
the
relationship
between
music and
another
subject in my
school.

I can
demonstrate
a relationship
between
music and
another
subject in my
school.

I can describe
the
connection
between
music and a
concept from
another
subject in my
school.

I can apply
music
concepts and
skills to other
arts
disciplines
and content
areas.

I can identify
music skills
that connect
to specific
content from
another arts
discipline
and content
area.

I can apply
concepts
from other
arts
disciplines
and content
areas to my
music.

I can
identify skills
and
knowledge
required from
other content
areas as they
relate to a
career in
music.

Indicator
IM.C
NH.9.2
I can identify
careers in
music.

Indicator
IM.C
IL.9.2

Indicator
IM.C
IM.9.2
I can identify
skills needed
for a career
in music.

Indicator
IM.C
IH.9.2
I can identify
materials and
tools needed
for specific
careers in
music.

Indicator
IM.C
AL.9.2
I can identify
knowledge
and skills
needed in
various
careers in
music.

Indicator
IM.C
AM.9.1

I can research
to set
personal
goals for my
career path.

Indicator
IM.C
AH.9.1
I can analyze
complex
ideas from
other arts
disciplines
and content
areas to
inspire my
creative work
and evaluate
its impact on
my artistic
perspective.

Page 297

Novice Instrumental Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can compose and arrange music.
Novice
Low
Benchmark
IM.CR
NL.1

Novice
Mid
Benchmark
IM.CR
NM.1

Novice
High
Benchmark
IM.CR
NH.1

I can notate rhythm patterns using a defined
selection of note values.
Indicator
IM.CR
NL.1.1
I can choose various note values to represent
simple rhythm patterns.

I can notate tonal patterns using a defined
selection of pitches.
Indicator
IM.CR
NM.1.1
I can choose various pitches to represent
simple melodic patterns.

I can notate simple melodies for my
instrument
Indicator
IM.CR
NH.1.1
I can write a simple melody within a
given key, tonality, and meter.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets
x I can notate a simple melody
that is dictated to me.

x

I can notate common meter signatures in
duple and triple meters on a music staff 3/4, 4/4, and 6/8.

x

I can notate simple rhythm patterns that
are dictated to me.

x

I can use quarter, eighth, half, sixteenth,
whole notes and corresponding rests, and
dotted notes and corresponding rests to
notate my own rhythm patterns in a given
meter.
I can...

x

x

I can notate treble, alto, tenor, and
bass clefs on a musical staff.

x

I can notate common key signatures
using sharps and flats.

x

I can identify basic intervals.

x

I can notate simple tonal patterns that
are dictated to me.

x

I can...

x

I can combine specific pitches
and rhythms to create a unique
musical idea within a given
key, tonality, and meter.

x

I can use music notation
software to notate melodies for
my instrument.

x

I can...
Page 298

Indicator
IM.CR
NL.1.2
I can write simple rhythm patterns using standard
music notation.
Sample Learning Targets

Indicator
IM.CR
NM.1.2
I can write simple melodic patterns using
standard music notation.
Sample Learning Targets

x

I can use notation to represent a rhythm
pattern in 4/4 meter given a selection of
quarter, eighth, half, and whole notes.

x

I can use notation to represent a tonal
pattern in a major tonality given a
selection of pitches.

x

I can use notation to represent a rhythm
pattern in 3/4 meter given a selection of
quarter, eighth, half, and whole notes.

x

I can use notation to represent a tonal
pattern in a minor tonality given a
selection of pitches.

x

I can use notation to represent a rhythm
pattern 4/4 meter given a selection of
quarter, eighth, sixteenth, half, and whole
notes and corresponding rests, and dotted
notes and corresponding rests.

x

I can choose from a selection of
pitches to create my own tonal
patterns in a given key and tonality.

x

I can...

x

I can...

Page 299

Anchor Standard 2: I can improvise music.
Novice
Low
Benchmark
IM.CR
NL.2
I can imitate simple rhythm patterns within a
given meter.
Indicator
IM.CR
NL.2.1
I can echo simple rhythm patterns on a neutral
syllable, incorporating movement.

Novice
Mid
Benchmark
IM.CR
NM.2
I can imitate simple tonal patterns within a
given key and tonality.
Indicator
IM.CR
NM.2.1
I can echo simple tonal patterns on a neutral
syllable.

Novice
High
Benchmark
IM.CR
NH.2
I can imitate simple melodic phrases
given simple chord changes.
Indicator
IM.CR
NH.2.1
I can imitate simple melodic phrases
given simple chord changes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can echo four-beat rhythm patterns on
³EDK´ ZKLOH WDSSLQJ D VWHDG\ pulse in 4/4
meter.

x

I can echo six-beat rhythm patterns on
³EDK´ ZKLOH NHHSLQJ D VWHDG\ pulse in my
heels in 3/4 meter.

x

x

I can echo sign a three-note tonic
tonal pattern in Eb MajRU RQ ³EDK ´

x

x

I can...

x

I can identify by ear the tonic
chord in a familiar song or
piece.

I can echo sing a three-note dominant
WRQDO SDWWHUQ LQ (E 0DMRU RQ ³EDK ´

x

I can echo sing a combination of tonic
and dominant tonal patterns in Eb
0DMRU RQ ³EDK ´

I can identify by ear tonic and
dominant chords in a familiar
song or piece.

x

I can identify by ear tonic,
dominant, and subdominant
chords in a familiar song or
piece.

x

I can...

x

I can echo sing a three-note tonic
WRQDO SDWWHUQ LQ & PLQRU RQ ³EDK ´

x

I can...

Page 300

Indicator
IM.CR
NL.2.2

Indicator
IM.CR
NM.2.2

Indicator
IM.CR
NH.2.2

I can echo simple rhythm patterns using
rhythmic solfege or a counting system.

I can echo simple tonal patterns using tonal
solfege on pitch names.

I can embellish a given melodic phrase
that corresponds with simple chord
changes on my instrument.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can echo four-beat rhythm patterns on
rhythm syllables while tapping a steady
pulse in 4/4 meter.

x

I can echo sing a three-note tonic
tonal pattern in Eb Major on solfege
syllables.

x

I can use rhythmic variations
to embellish a familiar melody
in 2/4 meter on my instrument.

x

I can echo six-beat rhythm patterns on
rhythm syllables while keeping a steady
pulse in my heels in 3/4 meter.

x

I can echo sing a three-note tonic
tonal pattern in Eb major on pitch
names.

x

x

I can...

x

I can echo sing a three-note dominant
tonal pattern in Eb Major on solfege
syllables while fingering the notes on
my instrument.

I can use passing tones to
embellish a familiar melody
over a tonic-dominant-tonic
chord progression on my
instrument.

x

I can...

x

I can echo sing a combination of tonic
and dominant tonal patterns in Eb
major in solfege syllables.

x

I can...

Page 301

Indicator
IM.CR
NL.2.3

Indicator
IM.CR
NM.2.3

I can echo simple rhythm patterns on my
instrument.

I can echo simple tonal patterns on my
instrument.

Sample Learning Targets

Sample Learning Targets

x

I can echo four-beat rhythm patterns on a
single pitch in 4/4 meter.

x

I can echo six-beat rhythm patterns on a
single pitch in 3/4 meter.

x

x

I can echo a three-note tonic tonal
pattern in Eb Major on my
instrument.

x

I can echo a three-note dominant
tonal pattern in Eb Major on my
instrument.

x

I can echo a combination of tonic and
dominant tonal patterns in Eb Major
on my instrument.

x

I can echo a three-note tonic tonal
pattern in C minor on my instrument.

x

I can...

I can...

Page 302

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a characteristic tone.
BAND
Novice
Mid
Benchmark
IM.B.P
NM.3

Novice
Low
Benchmark
IM.B.P
NL.3

Novice
High
Benchmark
IM.B.P
NH.3

I can produce a steady, free tone on a comfortable
pitch.
Indicator
IM.B.P
NL.3.1

I can produce a steady, free tone within a
limited range.
Indicator
IM.B.P
NM.3.1

I can produce a steady, free tone with a
limited range, in tune.
Indicator
IM.B.P
NH.3.1

I can play the first sounds on my instrument with
characteristic tone quality.

I can play the first 3 - 5 pitches on my
instrument with characteristic tone quality.

Sample Learning Targets

Sample Learning Targets

I can play all of the pitches I have
learned with a characteristic tone and
in tune.
Sample Learning Targets

x

I can demonstrate proper breathing
technique.

x

I can demonstrate proper embouchure
formation.

x

x

x

I can play three concert pitches on
my instrument with characteristic
tone quality.

x

I can play a selected range of
pitches with a characteristic
tone, in tune.

x

I can play four concert pitches on my
instrument with characteristic tone
quality.

x

I can adjust my pitch to match
a reference pitch.

x
x

I can play five concert pitches on my
instrument with characteristic tone
quality.

I can perform the first four
SCBDA JuniorScales (and
relative minors), one octave.

x

I can...

x

I can...

I can consistently produce the
appropriate sound on my mouthpiece.
I can...

Page 303

Indicator
IM.B.P
NL.3.2
I can demonstrate correct posture and playing
position.
Sample Learning Targets
x

I can demonstrate proper posture and
playing position.

x

I can demonstrate correct stick hold,
stroke, and playing position.

x

I can...

Page 304

ORCHESTRA
Benchmark
IM.O.P
NM.3

Benchmark
IM.O.P
NL.3
I can produce a steady, free tone on a comfortable
pitch.

I can produce a steady, free tone within a
limited range.

Indicator
IM.O.P
NL.3.1

Benchmark
IM.O.P
NH.3
I can produce a steady, free tone with a
limited range, in tune.

Indicator
IM.O.P
NM.3.1

Indicator
IM.O.P
NH.3.1

I can hold my bow correctly.

I can move my bow, both up and down, while
playing a selection of notes.

I can play with my left hand in position
using correct finger patterns on the
fingerboard.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify correct violin/viola bow
hold.

x

I can play using smooth, separate bow
strokes for each note (detaché).

x

I can play notes on the strings
by pressing, adjusting, and
releasing my fingers using
correct whole and half step
finger patterns.

x

I can identify correct cello/bass (French)
bow hold.

x

I can...

x

I can demonstrate proper bow hold for my
instrument.

x

I can adjust my pitch to match
a reference pitch.

x

I can...

x

I can...

Page 305

Indicator
IM.O.P
NL.3.2

Indicator
IM.O.P
NM.3.2

Indicator
IM.O.P
NH.3.2

I can move the bow on open strings.

I can identify whole and half steps, placing
my fingers on my strings accordingly.

I can move the bow using detached and
connected bow stroke techniques.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can describe how to move the bow on
the strings.

x

I can demonstrate correct left hand
playing position.

x

I can play an open string, keeping the bow
parallel to the bridge and fingerboard.

x

I can identify and play whole and half
step finger patterns.

x

I can...

x

I can...

x

I can play notes using detached
bow strokes from one note to
the next (detaché and
staccato).

x

I can play notes using smooth,
connected bow strokes from
one note to the next (legato and
slurs).

x

I can...
Indicator
IM.O.P
NH.3.3

I can listen and adjust my finger
placement to match a given pitch.
Sample Learning Targets
x

I can sing the given pitch.

x

I can play the pitch with
appropriate finger placement.

x

I can listen and adjust my finger
placement to match a given
pitch.
I can...

x

Page 306

Anchor Standard 4: I can perform with technical accuracy and expression.
Benchmark
IM.B.P
NL.4

BAND
Benchmark
IM.B.P
NM.4

Benchmark
IM.B.P
NH.4

I can demonstrate correct posture, guide position,
and fundamental fingering/stick/bow technique.

I can keep a steady pulse in duple and triple
division and produce basic articulations.

I can perform basic dynamic contrasts
and simple phrases.

Indicator
IM.B.P
NL.4.1
I can hold my instrument the correct way at all
times.
Sample Learning Targets
x

I can demonstrate correct stick hold.

x

I can demonstrate correct hand position.

x

Indicator
IM.B.P
NM.4.1

Indicator
IM.B.P
NH.4.1

I can keep a steady pulse in various meters.

I can demonstrate correct technique for
performing loud and soft dynamics.

Sample Learning Targets
x

I can play in duple meter with a
steady pulse.

x

I can play in triple meter with a
steady pulse.

I can...
x

Sample Learning Targets
x

I can perform a forte dynamic
with appropriate breath support
and embouchure control.

x

I can perform a piano dynamic
with appropriate breath support
and embouchure control.

x

I can perform a forte dynamic
with appropriate stick/stroke
height and playing position.

x

I can perform a piano dynamic
with appropriate stick/stroke
height and playing position.

x

I can...

I can...

Page 307

Indicator
IM.B.P
NL.4.2

Indicator
IM.B.P
NM.4.2

Indicator
IM.B.P
NH.4.2

I can play using correct fingering/sticking
technique.

I can play using basic articulations.

I can shape a basic musical phrase.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can demonstrate correct guide position.

x

I can demonstrate correct legato
articulation technique.

x

I can identify the beginning,
climax, and end of a phrase.

x

I can demonstrate correct finger
placement on my instrument.

x

I can demonstrate correct staccato
articulation technique.

x

I can taper the end of a phrase.

I can play hand-to-hand rhythms with
even stick technique.

x
x

I can...

I can play with dynamic
contrast to provide a sense of
musical direction.

x

I can...

x

x

I can...

Indicator
IM.B.P
NL.4.3
I can play simple scale and/or rudimental
patterns.
Sample Learning Targets
x

I can play a chromatic scale within a
limited range.

x

I can perform the SCBDA Junior Scales.

x

I can perform the SCBDA Junior
Rudiments.

x

I can...

Page 308

ORCHESTRA
Benchmark
IM.O.P
NM.4

Benchmark
IM.O.P
NL.4
I can demonstrate correct posture, guide position,
and fundamental fingering/stick/bow technique.

I can keep a steady pulse in duple and triple
division and produce basic articulations.

Indicator
IM.O.P
NL.4.1

Benchmark
IM.O.P
NH.4
I can perform basic dynamic contrasts
and simple phrases.

Indicator
IM.O.P
NM.4.1

Indicator
IM.O.P
NH.4.1

I can hold my instrument correctly at all times.

I can perform basic rhythms, keeping a
steady pulse.

I can demonstrate loud and soft
dynamics.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can hold my violin/viola parallel to the
floor.

x

I can clap or count aloud with a
steady pulse.

x

I can change bow weight to
create dynamic contrast.

x

I can adjust the length of the endpin on
my cello/bass for the instrument to rest
correctly against my body.

x

I can play basic rhythms of like note
values using whole, half, quarter, and
eighth notes.

x

I can change bow placement to
create dynamic contrast.

x
x

I can...

x

I can play rhythms using
combinations of whole, half, quarter,
eighth, and sixteenth notes.

I can change bow speed to
create dynamic contrast.

x

I can perform with dynamic
contrast between piano and
forte.

x

I can...

x

I can...

Page 309

Indicator
IM.O.P
NL.4.2

Indicator
IM.O.P
NM.4.2

Indicator
IM.O.P
NH.4.2

I can hold my instrument with appropriate
posture.

I can perform music in simple duple and
triple meters.

I can play musical phrases within my
repertoire.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can demonstrate proper sitting and/or
standing posture while I play my
instrument.

x

I can hold my instrument with correct left
wrist position.

x

I can demonstrate proper placement of the
left thumb in first position.

x

x

I can play music that has two beats
per measure, like 2/4 and 2/2.

x

I can play music that has three beats
per measure, like 3/4, 3/2, and 3/8.

x

I can play music that has four beats
per measure, like 4/4, 4/2, and 4/8.

x

I can...

x

I can use bow weight,
placement, and speed to shape a
musical phrase.

x

I can perform with dynamic
contrast to provide a sense of
musical direction.

x

I can...

I can...

Indicator
IM.O.P
NL.4.3
I can pay my instrument using correct bow hold
and bow movement.
Sample Learning Targets

Indicator
IM.O.P
NM.4.3
I can play using basic articulations.

Sample Learning Targets

x

I can maintain the proper thumb contact
point as I play.

x

I can play pizzicato with proper
technique for my instrument.

x

I can demonstrate proper finger curve and
extensions on the stick of the bow.

x

I can demonstrate proper detaché
technique.

x

I can maintain bow movement parallel to
the bridge and fingerboard.

x

I can demonstrate proper hooked
bowing technique.

x

I can...
Page 310

Anchor Standard 5: I can perform using musical notation.
Benchmark
IM.P
NL.5
I can identify music notation symbols representing
simple familiar tonal and rhythm patterns and
tunes.

Benchmark
IM.P
NM.5
I can perform simple familiar tonal and
rhythm patterns and tunes using music
notation.

Indicator
IM.P
NL.5.1

Benchmark
IM.P
NH.5
I can perform simple unfamiliar tonal
and rhythm patterns and tunes using
music notation.

Indicator
IM.P
NM.5.1

Indicator
IM.P
NH.5.1

I can identify the pitches in the clef appropriate to
my instrument.

I can perform simple familiar rhythm
patterns using music notation.

I can perform simple unfamiliar
rhythm patterns using music notation.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can name the lines and spaces in the
treble clef.

x

I can perform a familiar four-beat
rhythm pattern presented in notation.

x

I can name the lines and spaces in the bass
clef.

x

I can perform a series of familiar
four-beat rhythm patterns presented
in notation, in an unfamiliar order.

x

x

I can perform an unfamiliar
four-beat rhythm pattern
presented in notation, in a
familiar meter.

x

I can...

I can...
x

I can...

Page 311

Indicator
IM.P
NL.5.2

Indicator
IM.P
NM.5.2

Indicator
IM.P
NH.5.2

I can identify accidentals and simple key
signatures.

I can perform simple familiar tonal patterns
using music notation.

I can perform simple unfamiliar tonal
patterns using music notation.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can name the sharps in the key of D
Major (for strings).

x

I can perform a familiar three-pitch
tonal pattern presented in notation.

x

I can name the flats in the key of Bb
Major (for band).

x

I can perform a series of familiar
three-pitch tonal patterns presented in
notation, in an unfamiliar order.

x

I can identify, write and construct
beginning level major scales.

x

x

x

I can perform an unfamiliar
three-pitch tonal pattern
presented in notation, in a
familiar key and tonality.

x

I can...

I can...

I can...
Indicator
IM.P
NL.5.3

Indicator
IM.P
NM.5.3

Indicator
IM.P
NH.5.3

I can identify note values in familiar patterns and
tunes.

I can perform simple familiar tunes using
music notation.

I can perform simple unfamiliar tunes
using music notation.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify quarter, eighth, half, and
whole notes in a familiar song.

x

I can perform a familiar tune
presented in notation.

x

I can identify the meter signature of a
familiar song.

x

I can perform a familiar tune
presented in notation, in two or more
different keys.

x

, FDQ«
x

x

I can perform a simple
unfamiliar tune presented in
notation, in a familiar key,
tonality, and meter.

x

I can...

I can...

Page 312

Indicator
IM.P
NL.5.4
I can identify simple familiar rhythm patterns
with corresponding notation.
Sample Learning Targets
x

I can match the correct notation with a
familiar four-beat rhythm pattern
presented to me aurally.

x

I can select the correct notation to
represent a familiar four-beat rhythm
pattern presented aurally.

x

I can...

Page 313

Artistic Processes: Responding- I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Benchmark
IM.R
NL.6
I can identify the elements of music in
compositions that I hear.

Benchmark
IM.R
NM.6
I can identify musical symbols and describe
how the elements of music are used.

Benchmark
IM.R
NH.6

Indicator
IM.R
NM.6.1

I can describe how repetition and
contrast are used in music and identify
key signatures.
Indicator
IM.R
NH.6.1

I can name the instruments that I hear.

I can describe what I hear in a piece of music
using musical vocabulary.

I can explain how repetition is used in
music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

Indicator
IM.R
NL.6.1

x

I can identify the instruments families that
I hear.

x

I can identify instruments being played in
a musical work.

x

x

I can describe tempo using
descriptive words like fast, slow, and
moderate.

x

I can describe dynamics using words
like loud and soft.

x

I can demonstrate a steady pulse and
identify the pulse as fast, slow, or
moderate.

x

I can identify moments of
repetition in music that I hear.

x

I can explain why I think a
composer chose to use
repetition and the affect it has
on their music.

x

I can...

I can...

x

, FDQ«

Page 314

Indicator
IM.R
NL.6.2

Indicator
IM.R
NM.6.2

Indicator
IM.R
NH.6.2

I can identify tempo and rhythm.

I can identify musical terms in written music
that I perform and rehearse.

I can describe similarities and
differences in the music that I hear.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can demonstrate the steady pulse of
music that I hear using body percussion.

x

I can define the musical terms in the
music I perform.

x

I can identify sections that
sound different in the music
that I hear.

x

I can demonstrate the steady pulse of
music that I hear using motion.

x

I can explain how the meaning of the
music terms affect how the music is
performed.

x

I can identify when the
instrumentation or texture
changes in music that I hear.

x

I can discuss how music
changes in different sections of
a piece.

x

I can...

x

I can echo simple rhythms that I hear in a
piece of music.

x

I can identify when rhythms change in
music that I hear.

x

I can...

x

I can...

Indicator
IM.R
NL.6.3

Indicator
IM.R
NM.6.3

I can identify melody, harmony, and form.

I can identify musical symbols in my music.

Sample Learning Targets

Sample Learning Targets

Indicator
IM.R
NH.6.3
I can identify key signatures as they
appear in music.
Sample Learning Targets

x

I can identify high and low pitches in
music that I hear.

x

I can look at a piece of music and
define the musical symbols that I see.

x

I can identify the sharps or
flats in the key signature of a
piece of music I am analyzing.

x

I can identify the instruments that are
playing the melody.

x

I can explain how the musical
symbols affect how the music is
performed.

x

I can identify if they key
signature is major or minor
using clues in the music I am
analyzing.

x

I can identify the instruments that are
playing the harmony.

x

I can«

Page 315

x

, FDQ«

Anchor Standard 7: I can evaluate music.
Benchmark
IM.R
NL.7
I can actively listen to live or recorded
performances to identify some musical elements.

Benchmark
IM.R
NM.7
I can describe my personal interest in music
performances using a given list of music
terminology.

Indicator
IM.R
NL.7.1

Benchmark
IM.R
NH.7
I can list the criteria I use to describe
my interest in music performances
using appropriate music terminology.

Indicator
IM.R
NM.7.1

Indicator
IM.R
NH.7.1

I can actively listen to music performances.

I can use basic music terminology to describe
what I am hearing.

I can describe some of the elements of
music that I hear in a performance.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can complete a listening map while
listening to a musical work.

x

I can use musical terms when I talk
and write about music.

x

I can identify specific musical
occurrences while listening to a
performance.

x

I can explain musical terms and how
those elements are used in
performances.

I can...

x

x

x

I can describe melody in music.

x

I can describe harmony in
music.

x

I can describe the texture of the
music that I hear.

x

I can...

I can...

Page 316

Indicator
IM.R
NM.7.2

Indicator
IM.R
NH.7.2

I can summarize my personal preferences of
music.

I can identify my personal criteria for
evaluating music performances.

Sample Learning Targets

Sample Learning Targets

x

I can talk about my personal
preferences in music.

x

I can write about my personal
preferences in music.

x

x

I can describe why dynamics
are important in a music
performance.

x

I can create a checklist of music
elements to listen for in a
performance.

x

I can create a checklist of things
I like to hear in a music
performance.
I can...

I can...

x

Page 317

Artistic Processes: Connecting- I can relate musical ideas to personal experiences, culture, history, and other
disciplines.
Anchor Standard 8: I can relate musical ideas to personal experiences, culture, and history.
Benchmark
IM.C
NL.8
I can talk about musical ideas based on my
personal experiences.

Benchmark
IM.C
NM.8
I can talk about musical ideas based on my
culture.

Indicator
IM.C
NL.8.1

Benchmark
IM.C
NH.8
I can describe musical ideas through my
personal experiences and my culture.

Indicator
IM.C
NM.8.1

Indicator
IM.C
NH.8.1

I can describe how sound and music is used in my
everyday life.

I can recognize musical concepts and
elements specific to my culture.

I can describe how music is used in my
life and my community.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can talk with others about music.

x

I can write about music in my life.

x

x

I can talk with others about music
groups in my community.

x

I can name instruments used in
my school.

x

I can name instruments I hear in the
music of my culture.

x

I can describe music venues in
my community.

x

I can...

x

I can...

I can«

Page 318

Indicator
IM.C
NM.8.2

Sample Learning Targets

Indicator
IM.C
NH.8.2

I can recognize how music is used for
occasions unique to my culture.

I can describe how the elements of
music are used in my culture.

Sample Learning Targets

Sample Learning Targets

x

I can create a listening map.

x

I can list the occasions where music is
performed in my community.

x

I can describe instruments used
in my culture.

x

I can draw and paint to the mood and
style of music.

x

I can name songs for different
occasions.

x

I can describe playing
techniques for instruments in
my culture.

x

I can...
x

I can«

x

I can ...

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path
choices.
Benchmark
IM.C
NL.9
I can explore instrumental music concepts among
arts disciplines and other content areas.

Benchmark
IM.C
NM.9
I can recognize and use instrumental music
concepts among arts disciplines and other
content areas.

Benchmark
IM.C
NH.9
I can apply instrumental music concepts
to arts disciplines, other content areas,
and related careers.

Page 319

Indicator
IM.C
NL.9.1

Indicator
IM.C
NM.9.1

Indicator
IM.C
NH.9.1

I can identify the relationship between music and
another subject in my school.

I can demonstrate a relationship between
music and another subject in my school.

I can describe the connection between
my knowledge of music and a concept
from another subject in my school.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify how counting in music and
math are related.

x

I can relate fractions to math and the
division of beat in music.

x

I can identify music from other cultures I
learned about in social studies.

x

I can show how sound travels when I
play my instrument.

x

I can...

x

x

I can find repeated patterns in
music and repeated sequences
of numbers in math.

x

I can look at a painting and
identify the kind of music
associated with that time
period.

x

I can...

I can...

Indicator
IM.C
NH.9.2
I can identify careers in music.
Sample Learning Targets
x I can list careers in music that
interest me.
x

I can name people in my
community who have music
related careers.

x

, FDQ«

Page 320

Intermediate Instrumental Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can compose and arrange music.
Intermediate
Low
Benchmark
IM.CR
IL.1
I can compose and arrange melodies for my
instrument within simple forms.

Intermediate
Mid
Benchmark
IM.CR
IM.1
I compose and arrange simple harmonic
accompaniments.

Indicator
IM.CR
IL.1.1

Intermediate
High
Benchmark
IM.CR
IH.1
I can compose and arrange melodies
with simple harmonic accompaniments.

Indicator
IM.CR
IM.1.1

Indicator
IM.CR
IH.1.1

I can adapt a melody for my instrument.

I can write basic chords in a given key.

I can compose melodies with simple
chord progressions.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can arrange a popular tune for
performance on my instrument.

x

I can transpose a melody written for
another instrument for performance on
my instrument.

x

I can...

x

I can write triads in a given key.

x

I can write a chord progression
with a melodic line.

x

I can identify major and minor chords
in a given key.

x

I can write a melodic line
supported by a basic chordal
accompaniment.

x

I can...

x

I can...

Page 321

Indicator
IM.CR
IL.1.2

Indicator
IM.CR
IM.1.2

Indicator
IM.CR
IH.1.2

I can create a variation on a given theme.

I can write a basic chord progression in a
given key.

I can arrange melodies with simple
chord progressions.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can change the meter of a theme to
create a variation.

x

I can identify tonic, dominant, and
subdominant chords in a given key.

x

I can arrange a chord
progression with a melodic line.

x

I can change the tonality of a theme to
create a variation.

x

I can write a chord progression using
tonic, dominant, and subdominant
chords.

x

I can arrange a melody to fit a
given chord progression.

x

I can change the melodic rhythm of a
theme to create a variation.

x

I can...

x

I can...

x

I can add passing tones in a theme to
create a variation.

x

I can...

Page 322

Indicator
IM.CR
IL.1.3

Indicator
IM.CR
IM.1.3

I can compose using verse-refrain, AB, ABA, and
theme & variation forms.

I can write a basic harmonic accompaniment
in a given key.

Sample Learning Targets

Sample Learning Targets

x

I can compose contrasting melodic
phrases.

x

I can identify tonic, dominant, and
subdominant chords.

x

I can compose a simple piece in ABA
form.

x

x

I can...

I can write a simple chordal
accompaniment using tonic and
dominant chords.

x

I can write a simple chordal
accompaniment using tonic,
dominant, and subdominant chords.

x

I can...

Page 323

Anchor Standard 2: I can improvise music.
Intermediate
Low
Benchmark
IM.CR
IL.2
I can improvise simple rhythm patterns within a
given meter.

Intermediate
Mid
Benchmark
IM.CR
IM.2
I can improvise simple tonal patterns within a
given key and tonality.

Intermediate
High
Benchmark
IM.CR
IH.2
I can improvise simple melodic phrases
given chord changes.

Indicator
IM.CR
IL.2.1
I can improvise my own simple rhythm patterns
on a neutral syllable, incorporating movement.

Indicator
IM.CR
IM.2.1
I can improvise my own simple tonal patterns
on a neutral syllable.

Indicator
IM.CR
IH.2.1
I can identify chord changes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can improvise four-beat rhythm patterns
RQ ³EDK´ ZKLOH WDSSLQJ D VWHDG\ pulse in
4/4 meter.

x

I can improvise six-beat rhythm patterns
RQ ³EDK´ ZKLOH NHHSLQJ D VWHDG\ pulse in
my heels in ¾ meter.

x

x

I can improvise a three-note tonic
WRQDO SDWWHUQ LQ (E 0DMRU RQ ³EDK ´

x

I can improvise a three-note dominant
WRQDO SDWWHUQ LQ (E 0DMRU RQ ³EDK ´

x

I can improvise a combination of
tonic and dominant tonal patterns in
(E 0DMRU RQ ³EDK ´

x

I can improvise a three-note tonic
WRQDO SDWWHUQ LQ & PLQRU RQ ³EDK ´

x

I can...

I can...

x

I can identify by ear tonic,
dominant, and subdominant
chords in an unfamiliar song or
piece.

x

I can identify by ear subtonic,
mediant, and sub-mediant
chords in an unfamiliar song or
piece.

x

I can...

Page 324

Indicator
IM.CR
IL.2.2

Indicator
IM.CR
IM.2.2

Indicator
IM.CR
IH.2.2

I can improvise my own simple rhythm patterns
using rhythmic solfege or a counting system.

I can improvise my own simple tonal patterns
using tonal solfege or pitch names.

I can improvise simple melodic phrases
that correspond with chord changes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can improvise four-beat rhythm patterns
on rhythm syllables while tapping a
steady pulse in 4/4 meter.

x

I can improvise a three-note tonic
tonal pattern in Eb Major on solfege
syllables.

x

I can improvise six-beat rhythm patterns
on rhythm syllables while keeping a
steady pulse in my heels in 3/4 meter.

x

I can improvise a three-note tonic
tonal pattern in Eb Major on pitch
names.

I can...

x

x

x

x

I can improvise a three-note dominant
tonal pattern in Eb Major on solfege
syllables while fingering the notes on
my instrument.
I can improvise a combination of
tonic and dominant tonal patterns in
Eb Major on solfege syllables.

x

I can improvise a melodic
phrase over a given chord
progression in a major
tonality.

x

I can improvise a melodic
phrase over a given chord
progression in a minor
tonality.

x

I can improvise a melodic
phrase over a given chord
progression in a dorian
tonality.

x

I can...

I can...

Page 325

Indicator
IM.CR
IL.2.3

Indicator
IM.CR
IM.2.3

I can improvise my own simple rhythm patterns
on my instrument.

I can improvise on my own simple tonal
patterns on my instrument.

Sample Learning Targets

Sample Learning Targets

x

I can improvise four-beat rhythm patterns
on a single pitch in 4/4 meter.

x

I can improve a three-note tonic tonal
pattern in Eb major on my instrument.

x

I can improvise six-beat rhythm patterns
on a single pitch in 3/4 meter.

x

I can improvise a three-note dominant
tonal pattern in Eb major on my
instrument.

x

I can...
x

I can improvise a combination of
tonic and dominant tonal patterns in
Eb Major on my instrument.

x

I can improvise a three-note tonic
tonal pattern in C minor on my
instrument.

x

I can...

Page 326

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a characteristic tone.
BAND
Intermediate
Mid
Benchmark
IM.B.P
IM.3

Intermediate
Low
Benchmark
IM.B.P
IL.3
I can produce a centered tone in a comfortable
register.

I can produce a centered tone in most
registers.

Indicator
IM.B.P
IL.3.1

Intermediate
High
Benchmark
IM.B.P
IH.3
I can produce a centered tone in all
registers.

Indicator
IM.B.P
IM.3.1

Indicator
IM.B.P
IH.3.1

I can play in tune within a limited range.

I can play in tune within an expanding range.

I can consistently play in tune.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can perform selected SCBDA Junior
Scales(and relative minors) for my
instrument, in tune.

x

I can play the SCBDA Junior
chromatic range and perform seven
scales for my instrument in tune.

x

I can play selected exercises in
tune in all registers on my
instrument.

x

I can play a one-octave chromatic scale.

x

I can perform the first four SCBDA
Clinic Scales (and relative minors).

x

x

I can...

I can play the SCBDA Junior
chromatic range for my
instrument, in tune.

x

I can...
x

I can perform all nine SCBDA
Clinic Scales (and relative
minors) for my instrument, in
tune.

x

I can tune by ear and verify my
intonation accuracy.
Page 327

ORCHESTRA
Benchmark
IM.O.P
IM.3

Benchmark
IM.O.P
IL.3
I can produce a centered tone in a comfortable
register.

I can produce a centered tone in most
registers.

Indicator
IM.O.P
IL.3.1

Benchmark
IM.O.P
IH.3
I can produce a centered tone in all
registers.

Indicator
IM.O.P
IM.3.1

Indicator
IM.O.P
IH.3.1

I can play in tune within an ensemble on an
appropriate level of music.

I can perform appropriate scales that use
expanded registers, in tune.

I can perform using appropriate finger
placement associated with extensions
and shifting.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can press, adjust, and release the strings
to smoothly play several measures and
whole lines of music in tune.

x

I can maintain exact contact points to
develop an appropriate tone.

x

I can...

x

I can play multiple one-octave scales,
in tune.

x

I can play a limited number of twooctave scales, in tune.

x

x

I can maintain correct bow direction,
bow speed, and bow weight while
moving the bow in a limited dynamic
range.

x

I can apply introductory skills
of extensions and basic shifting
to scales.

x

I can apply introductory skills
of extensions and basic shifting
to repertoire.

x

I can...

I can...

Page 328

Indicator
IM.O.P
IL.3.2

Indicator
IM.O.P
IM.3.2

Indicator
IM.O.P
IH.3.2

I can identify notes that are higher or lower than
first position on my instrument.

I can move my left hand position to execute
basic extensions and shifting finger patterns,
in tune.

I can play in tune in higher positions,
making accurate shifts.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify notes that are in first
position on my instrument.

x

I can identify aural and visual cues to
anticipate the change of position.

x

I can identify finger patterns that relate to
notes lower than first position.

x

I can listen and adjust finger
placement to guide from one hand
position to the next.

x

I can identify finger patterns that relate to
notes higher than first position.

x

x

I can...
x

I can demonstrate proper technique
when moving from one position to
another.

x

I can glide into desired hand
positions while maintaining
basic extension and shifting
technique.

x

I can glide to and from higher
positions.

x

I can...

I can...

Page 329

Indicator
IM.O.P
IH.3.3
I can play using specific contact points
on my instrument and bow to create
dynamics.
Sample Learning Targets
x

I can demonstrate proper bow
technique to achieve the desired
dynamic level.

x

I can identify the proper contact
point for the bow on the string
to achieve the desired dynamic
level.

x

I can...
Indicator
IM.O.P
IH.3.4

I can move my left hand using primary
vibrato skills.
Sample Learning Targets
x

I can describe the proper left
hand position needed to
produce vibrato.

x

I can demonstrate basic
physical direction and angle of
hand needed to produce
vibrato.

x

I can...
Page 330

Anchor Standard 4: I can perform with technical accuracy and expression.
BAND
Benchmark
IM.B.P
IM.4

Benchmark
IM.B.P
IL.4
I can demonstrate increasing dexterity across an
expanding range and at increasing tempos.

Benchmark
IM.B.P
IH.4

I can perform increasingly complex rhythms
and meters with precision and produce an
expanding variety of articulations with
increasing facility.

I can produce gradual, controlled
dynamic changes and perform extended
phrases.

Indicator
IM.B.P
IM.4.1

Indicator
IM.B.P
IH.4.1

Indicator
IM.B.P
IL.4.1
I can play scales and/or rudiments with regular
accuracy.

I can play syncopated patterns, quarter,
eighth, and sixteenth note rhythms in various
meters.

I can demonstrate dynamic contrast
and play four to eight bar phrases.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can perform the SCBDA Clinic Scales
(and relative minors).

x

I can perform the SCBDA Clinic
Rudiments.

x

I can play scales in thirds.

x

I can play arpeggiated patterns.

x

I can play syncopated rhythms.

x

I can play a crescendo within an
exercise¶s musical phrase.

x

I can play music with changing
meters.

x

I can play a decrescendo within
a musical phrase.

x

I can play appropriate phrases
as determined by the music and
cues from the director.

x

I can...

x

x
x

I can...

I can play music that includes whole,
half, quarter, eighth, and sixteenth
notes and rests.
I can...

Page 331

Indicator
IM.B.P
IL.4.2
I can perform an expanding variety of
articulations with accuracy.
Sample Learning Targets
x I can demonstrate correct tenuto
articulation technique.
x

I can demonstrate correct marcato
articulation technique.

x

I can...

Page 332

ORCHESTRA
Benchmark
IM.O.P
IM.4

Benchmark
IM.O.P
IL.4

Benchmark
IM.O.P
IH.4

I can perform increasingly complex rhythms
and meters with precision and produce an
expanding variety of articulations with
increasing facility.
Indicator
IM.O.P
IM.4.1

I can produce gradual, controlled
dynamic changes and perform extended
phrases.

I can perform using correct hand positions to
reach appropriate registers of my instrument

I can perform rhythms using a developing
knowledge of note and rest values.

I can perform using dynamic
expression.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

I can demonstrate increasing dexterity across an
expanding range and at increasing tempos.

Indicator
IM.O.P
IL.4.1

x

I can perform using correct fingerings
associated with extensions and shifting.

x

I can identify aural and visual cues to
anticipate the change of position.

x

I can play a syncopated rhythm with
effective bow distribution.

I can listen and adjust finger placement to
glide from one hand position to the next.

x

I can...

x

x

I can demonstrate proper technique when
moving from one position to another.

x

I can...

x

I can perform a variety of note value
combinations including triplets,
dotted notes, and syncopation.

Indicator
IM.O.P
IH.4.1

x

I can perform crescendos, using
proper bowing techniques.

x

I can perform decrescendos,
using proper bowing
techniques.

x

I can...

Page 333

Indicator
IM.O.P
IL.4.2

Indicator
IM.O.P
IM.4.2

I can perform with increasing tempo using precise
finger placement and bow movement.

I can perform a variety of articulations.

Sample Learning Targets

Sample Learning Targets

x

x

x

I can perform passages from my
repertoire with efficient and proper finger
movement as the tempo increases.

x

I can demonstrate proper bow distribution
and smooth arm movement at increasing
tempos.

x

I can demonstrate basic spiccato
technique.

x

I can demonstrate basic marcato
technique.

x

I can...

I can...

I can perform using proper bow
control, weight and speed to create
distinct articulations.

Indicator
IM.O.P
IH.4.2
I can perform lyrically shaped
dynamics using appropriate bow
control.
Sample Learning Targets

x

I can perform using correct bow
speed, bow distribution, and
bow weight to produce
advanced dynamic control.

x

I can perform phrasing using
crescendos and decrescendos
with a variety of bowings.

x

I can...

Indicator
IM.O.P
IM.4.3
I can perform music containing compound
duple and triple time signatures.
Sample Learning Targets
x

I can perform music that has two
beats per measure, including 6/8 and
others.

x

I can perform music that has three
beats per measure, including 9/8 and
others.

x

I can perform music that has four
beats per measure, including 12/8 and
others.
Page 334

Anchor Standard 5: I can perform using musical notation.
Benchmark
IM.P
IL.5
I can identify music notation symbols representing
an expanded set of tonal, rhythmic, technical,
expressive, and formal indications.

Benchmark
IM.P
IM.5
I can perform at sight simple unfamiliar
musical works.

Indicator
IM.P
IL.5.1

Benchmark
IM.P
IH.5
I can perform at sight moderately
complex unfamiliar musical works.

Indicator
IM.P
IM.5.1

Indicator
IM.P
IH.5.1

I can identify advanced key signatures in the clef
appropriate to my instrument.

I can perform at sight simple unfamiliar
musical works with accurate pitches.

I can perform at sight moderately
complex unfamiliar musical works with
accurate pitches.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can name the sharps in the key of B
Major (for strings).

x

I can sight-read a grade one piece
with 100percent pitch accuracy.

x

I can sight-read a Grade 2 piece
with 100 percent pitch
accuracy.

x

I can name the flats in the key of Eb, Ab,
and Db Major (for band).

x

I can use instrumental music software
to play and record pieces to determine
accuracy.

x

I can use instrumental music
software to play and record
pieces to determine accuracy.

x

I can...

x

x

I can identify, write, and construct
intermediate level major scales.

x

, FDQ«

I can...

Page 335

Indicator
IM.P
IL.5.2

Indicator
IM.P
IM.5.2

Indicator
IM.P
IH.5.2

I can identify advanced note values and meter
signatures that represent syncopation and
smaller beat subdivisions in my music.

I can perform at sight simple unfamiliar
musical works with accurate pitches and
rhythms.

I can perform at sight moderately
complex unfamiliar musical works with
accurate pitches and rhythms.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify and count syncopated
rhythm patterns.

x

I can identify and count sixteenth note
patterns.

x

x

I can sight-read a Grade 1 piece with
100 percent pitch and rhythm
accuracy.

x

I can sight-read a Grade 2 piece
with 100 percent pitch and
rhythm accuracy.

x

I can use instrumental music software
to play and record pieces to determine
accuracy.

x

I can use instrumental music
software to play and record
pieces to determine accuracy.

x

I can«

x

I can...

I can...

Page 336

Indicator
IM.P
IL.5.3

Indicator
IM.P
IM.5.3

Indicator
IM.P
IH.5.3

I can identify technical, expressive, and formal
indications in my music.

I can perform at sight simple unfamiliar
musical works with accurate pitches and
rhythms at a steady tempo.

I can perform at sight moderately
complex unfamiliar musical works with
accurate pitches and rhythms at a
steady tempo.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify music symbols in my music.

x

I can identify expression markings in my
music.

x

I can...

x

I can sight-read a Grade 1 piece with
100 percent pitch and rhythm
accuracy at a steady tempo.

x

I can identify the time signature and
key signature RI D SLHFH ,¶P DERXW WR
sight read.

x

x

I can sight-read a Grade 2 piece
with 100 percent pitch and
rhythm accuracy at a steady
tempo.

x

I can identify the time
signature, key signature,
accidentals, and tempo of a
SLHFH ,¶P DERXW WR VLJKW UHDG

x

I can...

I can...

Page 337

Artistic Processes: Responding- I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Benchmark
IM.R
IL.6

Benchmark
IM.R
IM.6

Benchmark
IM.R
IH.6

I can identify simple music forms in
compositions that I hear and see.
Indicator
IM.R
IL.6.1

I can identify how the elements of music are
used in varying genres.
Indicator
IM.R
IM.6.1

I can explain how the elements of
music are used in varying genres.
Indicator
IM.R
IH.6.1

I can recognize melodic themes in music that I
hear.

I can identify how the melody, harmony,
rhythm, timbre, texture, form, and expressive
elements are different in varying genres of
music.

I can explain the use of melody,
harmony, rhythm, timbre, texture,
form, and expressive elements in
varying genres of music.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can identify a melodic theme when I
hear it in a piece of music.

x

I can explain the differences between
the elements of music.

x

I can identify types of forms
when presented aurally in
music.

x

I can identify when a melodic theme
returns in a different section of a piece of
music.

x

I can explain how the elements of
music are used in varying genres of
music.

x

I can identify types of forms
when presented visually in
music.

I can recognize a melodic theme when it
appears in different voices in a piece of
music.

x

x

In can explain how smaller
forms (AB, ABA, etc.) inform
larger forms like sonata form
and rondo form.

x

I can...

x

x

I can...

I can...

Page 338

Indicator
IM.R
IL.6.2

Indicator
IM.R
IH.6.2

I can recognize form in music that I hear and see.

I can describe how the elements of
music function in different genres.
Sample Learning Targets
x I can explain how the elements
of music function in music I
play.

Sample Learning Targets
x I can describe the musical differences
between the A and B sections of the
music I hear.
x

I can recognize and label the A section
and B section of a piece of music that I
see.

x

I can describe how harmony is
different in jazz music as
opposed to hip-hop music.

x

I can recognize and identify that a piece
of music is in AB form when I hear it.

x

x

I can...

I can describe how the use of
the elements of music can be
used to determine the genre a
piece of music falls into.

x

I can...

Indicator
IM.R
IL.6.3
I can recognize call and response In music that I
hear and see.
Sample Learning Targets

Indicator
IM.R
IM.6.3
I can identify major and minor tonalities.
Sample Learning Targets

Indicator
IM.R
IH.6.3
I can identify forms used in varying
genres.
Sample Learning Targets

x

I can describe the musical differences
between the call and the response sections
of music that I hear.

x

I can identify whether a piece of
music I hear is in major or minor
tonality.

x

I can explain how musical form
is different in country and
electronic music.

x

I can discuss genres of music in which I
am likely to hear call and response.

x

I can identify musical styles that use
primarily major tonalities.

x

I can identify how ABA form
appears in folk music.

x

I can describe how call and response
differs from other simple forms of music.

x

I can identify when tonalities change
in music that I hear.

x

I can...
Page 339

Anchor Standard 7: I can evaluate music.
Benchmark
IM.R
IL.7

Benchmark
IM.R
IM.7

I can describe the quality of music performances
using provided criteria.

I can explain my evaluation of performances
of others.

Indicator
IM.R
IL.7.1

Indicator
IM.R
IM.7.1

Benchmark
IM.R
IH.7
I can describe the quality of my
performances and compositions.
Indicator
IM.R
IH.7.1

I can describe what contributes to a quality
performance.

I can identify criteria used to evaluate
performance of others.

I can compare my performance to
performance of others.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can critically listen to music
performances.

x

I can list specific criteria used to
evaluate the performance of others.

x

I can describe why a performance was
high-quality using specific analyzed
criteria of the elements of music.

x

I can explain in detail how the
specific criteria are used to evaluated
the performance of others.

I can...

x

x

x

I can evaluate my own
performance in terms of
intonation, rhythmic precision,
etc.

x

I can evaluate the performance
of others in terms of intonation,
rhythmic precision, etc.

x

I can...

I can...

Page 340

Indicator
IM.R
IM.7.2

Indicator
IM.R
IH.7.2

I can describe the elements of music that I
hear in performances.

I can evaluate my compositions using
specific criteria.

Sample Learning Targets
x I can identify specific elements of
music when listening to
performances.

Sample Learning Targets
x I can evaluate the melody of
my compositions.

x

I can describe specific details of the
elements of music of the
performances.

x

I can...

x

I can use the elements of music
to assist with the construction
of the music that I write.

x

I can evaluate the music I
compose using the elements of
music.

x

I can...

Page 341

Artistic Processes: Connecting- I can relate musical ideas to personal experiences, culture, history, and other
disciplines.
Anchor Standard 8: I can relate musical ideas to personal experiences, culture, and history.
Benchmark
IM.C
IL.8
I can describe the purpose and value of music in
some cultures.

Benchmark
IM.C
IM.8

Benchmark
IM.C
IH.8

I can research the purpose and value of music
in a specific culture different from my own.

I can analyze how musical ideas
influence beliefs, values, or behaviors in
a specific culture different from my
own.

Indicator
IM.C
IM.8.1

Indicator
IM.C
IH.8.1

Indicator
IM.C
IL.8.1
I can recognize the significance and intent of
music in some cultures.

I can describe the significance and intent of
music from a specific culture.

I can interpret how music preferences
influence personal values and attitudes.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can name the uses of music in some
cultures.

x

I can name venues where music is used in
some cultures.

x

I can...

x

I can describe the function of music in
Native American religious
ceremonies.

x

I can record in my journal the
influence of jingles in commercials.

x

, FDQ«

x

I can find similarities in themes
in popular country music.

x

I can describe how popular
music sometimes influences
inappropriate behavior.

x

I can«

Page 342

Indicator
IM.C
IL.8.2

Indicator
IM.C
IM.8.2

Indicator
IM.C
IH.8.2

I can identify the appropriate music for particular
events.

I can describe how music functions in a
culture.

I can describe how music is a vehicle of
expression that inspires the listener to
think differently.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can name songs I would hear at a
wedding.

x

I can describe how music can be used
for celebrations.

x

I can write about how music
influences the emotions of the
listener.

x

I can match selections of music to given
events.

x

I can compare and contrast music for
entertainment and music for religious
ceremonies in a given culture.

x

I can summarize how music
effects the scene of a movie or a
short movement sequence.

x

I can...

x

I can...
x

I can...

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path
choices.
Benchmark
IM.C
IL.9
I can explore a range of skills shared among arts
disciplines, other content areas, and how they can
be applied to a career in music.

Benchmark
IM.C
IM.9
I can recognize specific skills shared among
arts disciplines, other content areas, and how
they can be applied to a career in music.

Benchmark
IM.C
IH.9
I can analyze the tools, concepts, and
materials used among arts disciplines,
other content areas, and how they are
used in music careers.

Page 343

Indicator
IM.C
IL.9.1
I can apply music concepts and skills to other arts
disciplines and content areas.

x

x

Sample Learning TargetsI can apply
music practice strategies to steps of
solving a math problem.
I can analyze poetry with relation to
rhythm and tone.

Indicator
IM.C
IM.9.1
I can identify music skills that connect to
specific content from another arts discipline
and content area.

I can apply concepts from other arts
disciplines and content areas to my
music.

Sample Learning Targets

Sample Learning Targets

x

.

x

I can use musical vocabulary in
other subject areas.

x

I can explain how articulation relates
to texture in visual art.

x

I can describe how the Harlem
Renaissance influenced music, art,
and American History.

I can perform music using
historically accurate
ornamentation.

x

I can...

x
x

Indicator
IM.C
IH.9.1

I caQ«

x

I can...

Indicator
IM.C
IM.9.2
I can identify skills needed for a career in
music.

Indicator
IM.C
IH.9.2
I can identify materials and tools
needed for specific careers in music.

Sample Learning Targets

Sample Learning Targets

x

I can list the educational requirements
needed to be a music teacher.

x

I can name current technology
used in a recording studio.

x

I can list the skills required to be a
studio musician.

x

I can list names of music
software used to compose and
arrange music.

x

I can «
x

I can «

Page 344

Advanced Instrumental Music Standards
Artistic Processes: Creating- I can use the elements of music to communicate new musical ideas and works.
Anchor Standard 1: I can compose and arrange music.
Advanced
Low
Benchmark
IM.CR
AL.1

Advanced
Mid
Benchmark
IM.CR
AM.1

Advanced
High
Benchmark
IM.CR
AH.1

I can compose and arrange incorporating
expressive elements.

I can compose and arrange short musical
works for a small ensemble.

I can compose and arrange short musical
works for a large ensemble.

Indicator
IM.CR
AL.1.1
I can compose incorporating expressive
elements.

Indicator
IM.CR
AM.1.1
I can arrange a work for two instruments.

Sample Learning Targets

Sample Learning Targets

Indicator
IM.CR
AH.1.1
I can arrange a work for large ensembles
incorporating elements of melody, harmony,
rhythm, timbre, texture, form, and
expression to communicate a mood, emotion,
idea, or experience.
Sample Learning Targets

x

I can use dynamics and phrasing to
communicate a musical idea.

x

I can compose using articulations to
strengthen a musical idea.

x

x

x

I can...

x

I can write a bass line using chord
tones from tonic, dominant, and
subdominant chords in a given key.

x

I can arrange a work for my band.

x

I can arrange a work for my orchestra.

I can write a countermelody using
chord tones from tonic, dominant,
and subdominant chords in the given
key.

x

I score an arrangement for
transposing instruments.

x

I can...

I can...
Page 345

Indicator
IM.CR
AL.1.2

Indicator
IM.CR
AM.1.2

I can arrange incorporating expressive
elements.

I can arrange a work for a small ensemble.

Sample Learning Targets

Sample Learning Targets

x

I can arrange a work within various
styles.

x

I can use combinations of instruments
to create variations in tone color.

x

I can vary the dynamic contrast and
tonalities in my arrangements.

x

I can arrange a work featuring
homophonic and polyphonic textures.

x

I can...
x

I can score an arrangement for a
variety of instruments.
I can...

Anchor Standard 2: I can improvise music.
Advanced
Low
Benchmark
IM.CR
AL.2
I can perform a brief improvisation given a
chord progression and meter.

Advanced
Mid
Benchmark
IM.CR
AM.2
I can perform an improvisation given a
motive, chord progression, and meter.

Advanced
High
Benchmark
IM.CR
AH.2
I can perform an extended spontaneous
improvisation with freedom and expression
featuring motivic development within a given
key, tonality, meter, and style.

Page 346

Indicator
IM.CR
AL.2.1
I can improvise short melodic patterns in
varying meters.

Indicator
IM.CR
AM.2.1
I can perform an improvisation on a given
motive.

Indicator
IM.CR
AH.2.1
I can improvise an extended unaccompanied
solo within a given key, tonality, meter, and
style.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can take a simple melody and
perform it using multiple meters.

x

I can compare various meters to
determine my personal preference.

x

x

I can use rhythmic variation including augmentation and
diminution, etc. - to improvise on a
given motive.

x

I can use passing tones and use nonharmonic tones to improvise on a
given motive.

I can...

x

x

I can improvise a cadenza
incorporating a composer¶V PHORGLF
and rhythmic motives.

x

I can improvise utilizing modes and
modulations with rhythmic variations.

x

I can improvise while maintaining
thematic unity.

x

I can...

I can«

Page 347

Indicator
IM.CR
AL.2.2

Indicator
IM.CR
AM.2.2

Indicator
IM.CR
AH.2.2

I can improvise a short passage using only a
chord progression or lead sheet.

I can improvise an extended passage using
only a chord progression or lead sheet.

I can improvise freely within a given key,
tonality, meter, and style, responding to
aural cues from other members of an
ensemble.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

x

I can use rhythmic variations,
passing tones, and other nonharmonic tones to improvise a short
passage over a given chord
progression in a major tonality.
I can use rhythmic variations,
passing tones, and other nonharmonic tones to improvise a short
passage over a given chord
progression in a minor tonality.

x

I can use rhythmic variations,
passing tones, and other nonharmonic tones to improvise a short
passage over a given chord
progression in a dorian tonality.

x

I can...

x

I can use rhythmic variations,
passing tones, and other nonharmonic tones to improvise an
extended passage given a lead sheet.

x

I can improvise an extended melody
over a repeated chord progression.

x

I can...

x

I can improvise a solo responding to
musical gestures performed by
another member of the ensemble.

x

I can improvise a solo incorporating
ideas performed by another member
of my ensemble.

x

I can improvise a solo in response to a
metrical shift set up by the drummer.

x

I can...

Page 348

Artistic Processes: Performing- I can perform a variety of music with fluency and expression.
Anchor Standard 3: I can produce a characteristic tone.
BAND
Advanced
Mid
Benchmark
IM.B.P
AM.3

Advanced
High
Benchmark
IM.B.P
AH.3

I can consistently produce a centered, vibrant
tone in all registers and across the entire
dynamic range of my instrument.
Indicator
IM.B.P
AM.3.1

I can adjust tone color, vibrato speed, and
vibrato width in response to stylistic demands
and the musical needs of an ensemble.
Indicator
IM.B.P
AH.3.1

I can identify intonation inaccuracies and
make adjustments as needed.

I can play in tune individually with a vibrant
tone.

Sample Learning Targets

Sample Learning Targets

I can play in tune individually and with an
ensemble while making adjustments as
needed.
Sample Learning Targets

Advanced
Low
Benchmark
IM.B.P
AL.3
I can produce a centered tone in all registers
and at moderate dynamic levels.
Indicator
IM.B.P
AL.3.1

x

I can identify intonation tendencies
for my instrument.

x

I can play repertoire with a vibrant
tone on my instrument.

x

I can adjust my intonation relative to
chord tones.

x

I can perform the SCBDA Clinic
chromatic range for my instrument,
in tune.

x

I can practice long tones to improve
my tone quality.

x

I can incorporate vibrato in a
stylistically appropriate manner.

x

I can...

x

x

I can perform the first four SCBDA
Senior Scales (and relative minors)
for my instrument, in tune.

I can perform all SCBDA Senior
Scales and relative minors) for my
instrument, in tune.

x

I can...

x

I can...

Page 349

Indicator
IM.B.P
AM.3.2
I can play in tune across a range of dynamics
on my instrument with a vibrant tone.
Sample Learning Targets
x

I can demonstrate a controlled
decrescendo from forte to mezzo
piano with a vibrant tone.

x

I can perform a controlled crescendo
with a vibrant tone.

x

I can...

Page 350

ORCHESTRA
Benchmark
IM.O.P
AM.3

Benchmark
IM.O.P
AH.3

I can consistently produce a centered, vibrant
tone in all registers and across the entire
dynamic range of my instrument.
Indicator
IM.O.P
AM.3.1

I can adjust tone color, vibrato speed, and
vibrato width in response to stylistic demands
and the musical needs of an ensemble.
Indicator
IM.O.P
AH.3.1

I can perform using appropriate bowing
dexterity to produce varied dynamics.

I can perform with a full, resonant tone in all
registers of my instrument.

I can play in tune individually and with an
ensemble by blending into a uniform sound
while making adjustments as needed.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

Benchmark
IM.O.P
AL.3
I can produce a centered tone in all registers
and at moderate dynamic levels.
Indicator
IM.O.P
AL.3.1

x

I can perform using proper bow
control, pressure, and speed to create
a quality sound.

x

I can demonstrate proper bow control
and placement when playing various
dynamic levels.

x

x

I can maintain natural resonance
within the lower register of my
instrument.
I can maintain natural resonance
within the upper register of my
instrument. I can maintain natural
resonance across the entire range of
my instrument.

I can...
x

x

I can perform with the same
articulation, contact point, and bow
distribution to create a well-rounded
tone within an ensemble.

x

I can adjust to match the articulation
of other sections in the ensemble.

x

I can...

I can...

Page 351

Indicator
IM.O.P
AL.3.2

Indicator
IM.O.P
AM.3.2

Indicator
IM.O.P
AH.3.2

I can perform using appropriate hand
positions with precise shifting technique and
finger selections.

I can perform using appropriate vibrato width
and speed in all registers of my instrument.

I can perform with the same tone, resonance,
and vibrato of others in an ensemble.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can glide into desired hand
positions using conditioned
movements to reach all registers of
my instrument.

x

I can choose appropriate fingerings
for the repertoire I am playing.

x

I can...

x

I can maintain a balanced, firm
vibrato motion while smoothly
moving the bow.

x

I can play in tune while making subtle
finger adjustments to resonate sound
appropriate to the ensemble.

x

I can maintain vibrato at all dynamic
levels.

x

I can develop fingerings that match
the tone and intent of the original
selection.

x

I can...
x

I can...

Indicator
IM.O.P
AL.3.3
I can perform using vibrato to develop
resonant tone.
Sample Learning Targets
x

I can maintain the speed, weight, and
sounding point of vibrato on my
instrument while smoothly moving
the bow.

x

I can use vibrato in stylistically
correct places in the repertoire.

x

I can...
Page 352

Anchor Standard 4: I can perform with technical accuracy and expression.
BAND
Benchmark
IM.B.P
AM.4

Benchmark
IM.B.P
AL.4
I can demonstrate fluent fingering/stick/bow
technique across the entire range of my
instrument.

I can perform with appropriate flexibility
within given meter and use advanced
articulation techniques in a stylistically
appropriate way.

Indicator
IM.B.P
AL.4.1

Benchmark
IM.B.P
AH.4
I can control pitch and tone quality across
expanded dynamic range, using appropriate
stylistic nuance and expressive inflections.

Indicator
IM.B.P
AM.4.1

Indicator
IM.B.P
AH.4.1

I can demonstrate the entire fingering system/
position or rudiment for my instrument.

I can demonstrate appropriate tempo
flexibility within a given meter.

I can perform with technical ease and stylistic
integrity.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can play rudimental with fluency.

x

I can use rubato to play expressively.

x

I can perform the SCBDA Senior
Scales (and relative minors).

x

I can use appropriate tempo changes
to play expressively.

x

I can perform the SCBDA Senior
Rudiments.

x

I can...

x

I can...

x

I can perform using well-defined
dynamic contrast and advanced
phrasing with stylistic fluency.

x

I can perform with well-developed
resonance, vibrato, and tone color
with stylistic fluency.

x

I can interpret a piece of music,
synthesizing dynamic contrast, tone
color, and stylistic qualities.

x

I can...

Page 353

Indicator
IM.B.P
AM.4.2
I can play in a specified style.
Sample Learning Targets
x

I can modify note lengths to play
within a specified style.

x

I can modify articulations to play
within a specified style.

x

I can...

Page 354

ORCHESTRA
Benchmark
IM.O.P
AM.4

Benchmark
IM.O.P
AL.4
I can demonstrate fluent fingering/stick/bow
technique across the entire range of my
instrument.
Indicator
IM.O.P
AL.4.1
I can perform using appropriate hand
positions with precise shifting technique and
fingerings.
Sample Learning Targets
x

I can glide into desired hand
positions using conditioned
movements to reach all registers of
my instrument.

x

I can perform precise and efficient
fingerings throughout the range of
my instrument.

x

I can play in tune, making subtle
finger adjustments while shifting.

x

Benchmark
IM.O.P
AH.4

I can perform with appropriate flexibility
within given meter and use advanced
articulation techniques in a stylistically
appropriate way.
Indicator
IM.O.P
AM.4.1

I can control pitch and tone quality across
expanded dynamic range, using appropriate
stylistic nuance and expressive inflections.

I can perform with appropriate and welldefined bowing techniques.

I can perform with technical ease and stylistic
integrity.

Sample Learning Targets

Sample Learning Targets

Indicator
IM.O.P
AH.4.1

x

I can perform complex rhythmic
patterns, including polyrhythms,
hemiola, and cross-rhythms.

x

I can perform using well-defined
dynamic contrast and advanced
phrasing with stylistic fluency.

x

I can perform proper articulations in
a manner that reflects the composer¶V
intent.

x

I can perform with well-developed
resonance, vibrato, and tone color
with stylistic fluency.

x

I can...

x

I can perform music using polished
bow movements.

x

I can interpret a piece of music,
synthesizing dynamic contrast, tone
color, and stylistic qualities.

x

I can...

I can...

Page 355

Indicator
IM.O.P
AL.4.2
I can perform using a variety of articulations
with increasing dexterity.
Sample Learning Targets
x

I can play using proper bow control,
weight and speed to create distinct
articulations.

x

I can demonstrate spiccato technique
with increasing facility.

x

I can demonstrate marcato technique
with increasing facility.

x

I can demonstrate basic ricochet
technique.

x

I can...

Page 356

Anchor Standard 5: I can perform using musical notation.
Benchmark
IM.P
AL.5
I can perform at sight complex unfamiliar
musical works with accuracy.

Benchmark
IM.P
AM.5
I can perform at sight complex unfamiliar
musical works with accuracy and appropriate
expression/interpretation.

Indicator
IM.P
AL.5.1

Benchmark
IM.P
AH.5
I can perform at sight complex unfamiliar
musical works with accuracy, appropriate
expression/interpretation, and fluency.

Indicator
IM.P
AM.5.1

Indicator
IM.P
AH.5.1

I can perform at sight complex unfamiliar
musical works with accurate pitches.

I can perform at sight complex unfamiliar
musical works with correct articulations.

I can perform at sight complex unfamiliar
music works with fluency.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can sight read a Grade 3 piece with
100 percent pitch accuracy.

x

I can identify a variety of symbols
related to articulations.

x

I can play and record using current
and emerging technologies to selfassess for accuracies.

x

I can sight-read a Grade 3 piece with
correct articulations.

x
x

I can...

x

I can sight-read Grade 4 or higher
music with accurate pitches and
rhythms.

x

I can sight-read Grade 4 or higher
music with accurate articulations.

x

I can...

I can...

Page 357

Indicator
IM.P
AL.5.2

Indicator
IM.P
AM.5.2

Indicator
IM.P
AH.5.2

I can perform at sight complex unfamiliar
musical works with accurate pitches and
rhythms.

I can perform at sight complex unfamiliar
musical works with correct dynamics.

I can perform at sight complex unfamiliar
musical works with stylistic integrity.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can sight-read a Grade 3 piece with
100 percent pitch and rhythm
accuracy.

x

I can play and record using current
and emerging technologies to selfassess for accuracies.

x

x

I can identify a variety of symbols
related to dynamics.

x

I can sight-read Grade 4or higher
music using stylistic integrity.

x

I can sight-read a Grade 3 piece using
correct dynamics.

x

I can sight-read performing dynamic,
meter, and tempo changes.

x

I can...

x

I can...

, FDQ«
Indicator
IM.P
AL.5.3

I can perform at sight complex unfamiliar
musical works with accurate pitches and
rhythms at a steady tempo.
Sample Learning Targets
x

I can sight-read a Grade 3 piece with
100percentpitch and rhythm
accuracy.

x

I can sight-read a Grade 3 piece while
maintaining a steady tempo.
I can...

x

Indicator
IM.P
AM.5.3
I can perform at sight complex unfamiliar
musical works with appropriate phrasing.
Sample Learning Targets
x

I can identify phrases in a musical
work.

x

I can perform at sight phrases from a
musical work with expression.

x

I can sight-read a Grade 3 piece using
appropriate phrasing.

x

I can...
Page 358

Artistic Processes: Responding- I can respond to musical ideas as a performer and listener.
Anchor Standard 6: I can analyze music.
Benchmark
IM.R
AL.6
I can explain how the use of music elements
impacts compositions from different
historical periods and cultures.

Benchmark
IM.R
AM.6
I can examine musical forms in compositions
from varying genres and styles.

Indicator
IM.R
AL.6.1

Benchmark
IM.R
AH.6
I can justify how structure, forms, and
performance decisions inform responses to
music based on the elements of music.

Indicator
IM.R
AM.6.1

Indicator
IM.R
AH.6.1

I can identify forms used in varying genres
and historical periods.

I can describe characteristics of a variety of
musical forms.

I can identify harmonic structure.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can explain the components of
sonata form.

x

I can explain the characteristics of
musical forms and how they are
similar and different.

x

I can describe how sonata form
differs in symphonic works versus
instrumental works.

x

I can describe how a particular form
appears in different genres of music.

x

I can describe how various forms
have evolved over time.

x

I can...

x

I can...

x

I can identify harmonic structure of
the music I hear and perform.

x

I can describe how harmonic
structure changes the mood of a
piece of music.

x

I can identify specific performance
decisions of different performers.

x

I can describe how performance
decisions highlight the form and
harmonic structure set forth by the
composer.

x

I can...
Page 359

Indicator
IM.R
AL.6.2

Indicator
IM.R
AM.6.2

Indicator
IM.R
AH.6.2

I can describe stylistic qualities of music from
different historical periods.

I can identify key signature changes and
modulations in relation to form.

I can apply stylistic qualities of music from
different historical periods as I perform on my
instrument.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can explain how modulations affect
harmonic structure.

x

I can apply historically appropriate
musical ornamentation to my
performance on my instrument.

x

I can describe how a modulation
bridges between sections in sonata
form.

x

I can justify my performance
decisions based on my analysis of the
elements of music and their use in
the appropriate historical period.

x

I can...

x

I can listen to music from different
time periods and describe the
differences in their styles.

x

I can explain how music changed
through history.

x

I can list qualities of the music of
various historical periods.

x

I can explain how modulations and
tonality unify a musical work.

x

I can describe how qualities of
different historical periods from
current music today.

x

I can...

x

I can...

Page 360

Indicator
IM.R
AL.6.3

Indicator
IM.R
AM.6.3

Indicator
IM.R
AH.6.3

I can describe musical works from different
cultures.

I can describe stylistic qualities of music from
different historical periods and how it applies
to my instrument.

I can justify the performance decisions in a
variety of musical works.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can describe the similarities and
differences of music from different
cultures.

x

I can describe how my instrument
appears in music from different
historical periods.

x

I can identify instrumental,
expressive, and tonal qualities that
make the music from a specific
culture unique.

x

I can identify stylistic qualities of
Baroque performance technique.

x

I can demonstrate Baroque
performance technique on my
instrument.

x

I can describe how techniques from
different historical periods inform
performance decisions.

x

I can...

x

x

I can describe how music from
different cultures informs current
music today.
I can...

x

I can defend performance decisions I
make on my instrument.

x

I can rationalize performance
decisions I make on my instrument
within an ensemble.

x

I can...

Page 361

Anchor Standard 7: I can evaluate music.
Benchmark
IM.R
AL.7

Benchmark
IM.R
AM.7

I can analyze performances and
compositions, offering constructive
suggestions for improvement using provided
criteria.
Indicator
IM.R
AL.7.1

I can analyze and critique compositions and
performances using personally-developed
criteria.

I can formulate constructive feedback for
personal performances.
Sample Learning Targets

I can analyze personal compositions and
provide recommendations for improvement.
Sample Learning Targets

I can justify my criteria for evaluating music
works and performances based on personal
and collaborative research.

Indicator
IM.R
AM.7.1

x

I can evaluate my own performance.

x

I can evaluate my personal
compositions.

x

I can formulate constructive feedback
for my own personal performances
using the elements of music as the
basis for criteria of evaluation.

x

I can provide criteria for improvement
based on composition rules.

x
x

Benchmark
IM.R
AH.7

Indicator
IM.R
AH.7.1
I can explain criteria used for evaluation.
Sample Learning Targets
x

I can develop criteria for the
evaluation of music works and
performances.

x

I can explain how the criterion for the
evaluation of music works and
performances was developed.

x

I can...

I can...

I can...

Page 362

Indicator
IM.R
AL.7.2
I can formulate constructive feedback for the
performances of others.
Sample Learning Targets

Indicator
IM.R
AM.7.2
I can analyze performances and provide
recommendations for improvement.
Sample Learning Targets

x

I can evaluate the performance of
others.

x

I can evaluate my personal
performances and those of others.

x

I can formulate constructive feedback
for the performance of others using
the elements of music as the basis for
criteria of evaluation.

x

I can provide criteria for improvement
of my personal performances and
those of others using the elements of
music a basis for criteria of
evaluation.

x

Indicator
IM.R
AH.7.2
I can collaborate with others to assess musical
works and performances.
Sample Learning Targets
x

I can collaborate with others to
develop criteria for the evaluation of
music works and performances.

x

I can collaborate with others to
explain how the criteria for the
evaluation of music works and
performances were developed.

x

I can...

I can...
x

I can...

Indicator
IM.R
AH.7.3
I can research topics pertaining to musical
performance.
Sample Learning Targets
x

I can extend my personal learning of
music performance by conducting
research.

x

I can explain how my understanding
of music performance was improved
by conducting research.

x

I can...

Page 363

Artistic Processes: Connecting- I can relate musical ideas to personal experiences, culture, history, and other
disciplines.
Anchor Standard 8: I can relate musical ideas to personal experiences, culture, and history.

Benchmark
IM.C
AL.8
I can research how musical ideas influence
beliefs, values, or behaviors in various
cultures.

Benchmark
IM.C
AM.8
I can synthesize my research about other
cultures and genres to enhance my music
performance.

Indicator
IM.C
AL.8.1

Benchmark
IM.C
AH.8
I can justify the role of music in a global
society.

Indicator
IM.C
AM.8.1

Indicator
IM.C
AH.8.1

I can analyze how genres of music influence
social lifestyles and current trends.

I can apply characteristic expressive qualities
to my music performance.

I can defend interpretations of music through
appropriate musical vocabulary.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can compare genres of music.

x

I can perform in a historically
accurate Baroque style. .

x

I can describe how music influences
social trends.

x

I can perform tiered dynamics in
Renaissance era music.

x

I can«
x

x

I can compile information using
appropriate vocabulary to describe
musical interpretations.

I can explain why trills start on the note above
in the Mozart Clarinet Concerto.

I can...
x

I can...

Page 364

Indicator
IM.C
AL.8.2

Indicator
IM.C
AM.8.2

I can explain how music preferences influence
group or social stereotypes.

I can apply characteristic techniques to my
music performance.

Sample Learning Targets

Sample Learning Targets

Indicator
IM.C
AH.8.2
I can justify the role of music as having a
common purpose in societies around the
world.
Sample Learning Targets

x

I can conduct surveys to determine
music preferences of my peers

x

I can perform swung eighth notes in
bebop music.

x

I can compare and contrast the
influence of popular music in Korea
and America.

x

I can describe the musical preferences
of different generations of people.

x

I can perform appropriate
articulations for the musical time
period.

x

I can list events that all countries have
in common where music plays a
significant role in the purpose of the
event.

x

I can...

x

, FDQ«

x

I can...

Anchor Standard 9: I can relate music to other arts disciplines, other content areas, and career path choices.
Benchmark
IM.C
AL.9

Benchmark
IM.C
AM.9

I can analyze how my interests and skills will
prepare me for a career in music.

I can create an educational plan for my career
choice in music.

Benchmark
IM.C
AH.9
I can research societal, political, and cultural
issues as they relate to other arts and content
areas and apply to my role as a musician.

Page 365

Indicator
IM.C
AL.9.1

Indicator
IM.C
AM.9.1

Indicator
IM.C
AH.9.1

I can identify skills and knowledge required
from other content areas as they relate to a
career in music.

I can research to set personal goals for my
career path.

I can analyze complex ideas from other arts
disciplines and content areas to inspire my
creative work and evaluate its impact on my
artistic perspective.

Sample Learning Targets

Sample Learning Targets

Sample Learning Targets

x

I can create a budget for a recording
studio.

x

I can design a website to promote my
brass quintet.

x

x

I can interview personnel from
community ensembles to examine
their educational background.

x

I can research the socio political
atmosphere of the time period of the
piece I am performing.

x

I can research colleges that offer
programs in music that I am
interested in.

x

I can compare and contrast music
from East and West Germany during
WWI to determine underlying themes
of music propaganda.

x

I can...
x

I can...

I can...

Indicator
IM.C
AL.9.2
I can identify knowledge and skills needed in
various careers in music.
Sample Learning Targets
x

I can list the certification needed to
become a music software engineer.

x

I can describe the skills needed to
become and instrument repair
technician.

x

I can«
Page 366

Instrumental Music Glossary
AB Form TKH RYHUDOO VWUXFWXUH RI D PXVLFDO SLHFH WKDW XVHV ³VDPH´ DQG ³GLIIHUHQW´ VHFWLRQV.
ABA Form TKH RYHUDOO VWUXFWXUH RI D PXVLFDO SLHFHV WKDW XVHV ³VDPH,´ ³GLIIHUHQW,´ ³VDPH.´
Analysis (of music) The study of music that focuses on the form or structure of the music itself. There are several methods of analysis,
including analysis by harmonic structure, theme, by form, and by phrase.
Appropriate Sound Characteristic tone of an instrument.
Arco Directive to play with the bow as opposed to plucked or pizzicato.
Articulation The way an individual note or group of notes should be performed based on the context of the music or the intention of
the performer.
Aural Skills Skills wherein musicians learn to identify musical elements solely by hearing.
Aural Cues Musical directive given not using words.
Bow Articulations Direction or performance technique which effects the sound of a single note or multiple notes.
Bow Direction Up bow or down bow.
Bow Speed How fast or slow the bow moves.
Bow Weight Pressure applied to bow.
Cadenza An ornamentalpassageperformed near the close of a composition, usually improvised, and usually performed by a soloist.
Cadenzas are mostly to be found in arias or a concerto.
Call and Response Succession of two distinct phrases usually played by different musicians, where the second phrase is heard as a
direct commentary on or response to the first.
Characteristic Tone The sound the instrument is intended to make.
Page 367

Chromatic Range Scope of the instrument or composition in half steps.
Compose To create an original work.
Composer A person who writes music.
Composition Any musical work or production.
Compound Time Signature Meter in which each beat is divisible by three rather than two.
Cross Rhythms Effect produced when two conflicting rhythms are heard together.
Detaché Detached bow stroke.
Detached Bow Strokes Bow movement that move up and down.
Duple Meter A rhythmic pattern with the measure being divisible by two.
Dynamic Contrast Difference between the loudness and softness in music.
Dynamic Range Scope of the range of loudness and softness in a composition.
Dynamics The loudness or softness of a composition.
Elements of Music Pitch, timbre, texture, volume, duration, and form.
Ensembles A group of musicians that perform as a unit.
Expression When a composition is trying to represent something extra-musical.
Expression Markings Printed directives for the musician to perform a specific way.
Expressive Elements Dynamics, tempo, and articulations.
Expressive Indications Directives in music to performer to play with a specific expressive element.
Flat Lowering of pitch by a half step.
Page 368

Genres A unique category of composition with similar style, form, emotion, or subject.
Harmonic Structure The structure of a work or passage as considered from the point of view of its chordal characteristics and
relationships between those chords.
Harmony The combination of notes sounded simultaneously to produce chords.
Hemiola A rhythmic pattern of syncopated beats.
Instrument Family Grouping of several different but related instruments.
Key Tonality of a piece of music.
Key Signature The sharp, flats, or naturals signs placed at the beginning of a staff indicating the tonality of the composition.
Macrobeat Long beats.
Major tonality A sequence of notes that define the tonality of the major scale.
Marcato Marked, accented, emphatic, stress.
Markings Printed directives for the musician to perform a specific way.
Melodic Theme Subject material in which part or all of the composition is based.
Melody A succession of tones comprised of mode, rhythm, and pitches arranged to achieve musical shape.
Meter A recurring pattern of stresses or accents that provide the pule or beat of music.
Meter Signatures Notational convention used to specify how many beats are to be in each bar and which note value is to be given one
beat.
Microbeat Division of the large beat.
Minor Tonality A series of tones that defines a minor tonality.
Modulation The process of changing from one key to another.
Page 369

Musical Phrases Grouping of consecutive melodic notes.
Musical Styles Genres of music.
Music Form Overall structure or plan of a piece of music which describes the layout of a composition.
Ornamentation Decorative notes of short duration to compositions to emphasize certain notes.
Performance An event that features the realizations of a composition.
Personal Preferences and Interests in Music $ PXVLFLDQ¶V RZQ WDVWH LQ PXVLF.
Phrasing The art of performing music in a way that allows each phrase to be conceived as a single unit.
Pitch The specific quality of a sound that makes it a recognizable tone.
Pizzicato Directive to play plucked with the fingers rather than bowed or arco.
Playing Position How an instrument is held.
Polyrhythms The use of several patterns or meters simultaneously.
Position The placement of the hand or slide.
Pulse Consistent beats.
Range The scope of notes that an instrument can produce or scope of notes in a composition from the lowest note to the highest note.
Register A division of the range of an instrument or singing voice.
Rhythm The subdivision of a space of time into a defined, repeated pattern.
Rondo Form Musical structure ABACABA.
Rudiment Small rhythmic patterns which are later extended into more complex patterns.
Scales Set of musical notes ordered by fundamental frequency or pitch.
Page 370

SCBDA 6RXWK &DUROLQD %DQG 'LUHFWRU¶V $VVRFLDWLRQ.
Sections (of music) Complete but not independent idea.
Sharp Raising a pitch by a half set.
Sonata Form Musical structure with three sections, exposition, development, and recapitulation.
Spiccato A directive to perform the indicated passage of a composition separated or detached.
Staccato A style of playing notes in a detached, separated, distinct manner, as opposed to legato. Staccato is indicated as a dot directly
above or below the notehead.
Stick/Bow Technique Proper grip and control of the stick or bow.
Style (music) Characteristic manner of presentation of musical elements.
Syncopation Deliberate upsetting of the meter or pulse of a composition by means of a temporary shifting of the accent to a weak beat
or an off beat.
Tempo The speed of the rhythm of a composition.
Tonality The principle of organization of a composition around a tonic based upon a major or minor scale.
Tone Steady, consistent sound.
Tone Color Element of sound that distinguishes different types of sound production.
Triple Meter A metrical pattern having three beats to a measure.
Vibrato The pulsating or vibrating element of some sounds that is produced by a full resonant quality of tone.

Page 371

References
ArtsEdge, T. K. C. (1996). ARTSEDGE: $UWV LQWHJUDWLRQ 7KH .HQQHG\ &HQWHU¶V perspective. Retrieved from https://artsedge.kennedycenter.org/educators/how-to/series/arts-integration/arts-integration

ArtsEdge, T. K. C. (1996). ARTSEDGE: Lessons. Retrieved from https://artsedge.kennedy-center.org/educators/lessons
Azzara, C. D., &Grunow, R. F. (2006). Developing musicianship through improvisation (1st ed.). Chicago, IL: GIA Publications.
Benham, S. J., Publishing, A., Wagner, M. L., Aten, J. L., Evans, J. L., &Odegaard, D. (2011). ASTA string curriculum 2011:
Standards, goals, and learning sequences for essential skills and knowledge in K-12 string programs. Fairfax, VA: American
String Teacher Association.
Brenner, B., & Strand, K. (2013). A Case Study of Teaching Musical Expression to Young Performers. Journal of Research in Music
Education, 61, 80-96.
Byo, J. (2014). Applying score analysis to a rehearsal pedagogy of expressive performance, Music Educators Journal, 101(2), 76-82.
Collins, P. (n.d.). 7KH YLROLQLVW¶V JXLGH WR WKH $OH[DQGHU 7HFKQLTXH Retrieved from http://www.alexandertechnique.com/articles/violinist/
Eccles, D. (2013). String speak for the Non-String Player [PowerPoint slides]. Retrieved from
https://flmusiced.org/flmusicapps/Sessions/Handouts/2013/fmeastringspeakslideshow.pdf

Firth, V. (2016). 40 essential snare drum rudiments. Vic Firth Company. Retrieved from http://vicfirth.com/40-essential-rudiments/
Gorow, R. (2002). +HDULQJ DQG ZULWLQJ PXVLF 3URIHVVLRQDO WUDLQLQJ IRU WRGD\¶V PXVLFLDQ (2nd ed. revised and expanded). United
States: September Publishing.
Hopkins, M. (2016). String Pedagogy Notebook. Retrieved from http://stringtechnique.com/rehearsal-strategies.html
Inc, H. V. (2013). Harmonic vision home page. Retrieved from http://harmonicvision.com
Jazz at Lincoln Center. (2016). Retrieved from http://www.jazz.org/

Jazz, J. A. (1997). Jamey Aebersold jazz. Retrieved from http://jazzbooks.com/mm5/#
Konowitz, B. (1973). Music improvisation as a classroom method: A new approach to teaching music. NY: Alfred Publishers.
Laux, C. (2016). Ideas and resources for the 21st century string/orchestra classroom. Retrieved from http://www.orchestrateacher.net/
Page 372

Making Music (2015). A simple guide to transposing. Retrieved from http://makingmusicmag.com/a-simple-guide-to-transposing/
Moran, P. (2012). Bringing composing into schools. Journal of Music. Retrieved from http://journalofmusic.com/focus/bringingcomposing-schools
National Association for Music Education. (2014). Music Standards for High School Traditional and Emerging Ensembles [PDF
document]. Retrieved from http://www.nafme.org/wp-content/files/2014/11/2014-Music-Standards-Ensemble-Strand.pdf
National Association for Music Education. (2014). Music Standards for Music-Composition and Theory [PDF document]. Retrieved
from http://www.nationalartsstandards.org/sites/default/files/Music%20Composition%20and%20Theory%20at%20a%20Glance%203-415.pdf

National Coalition for Core Arts Standards. (2014). National Core Arts Standards. Dover, DE: State Education Agency Directors of
Arts Education. Retrieved from www.nationalartsstandards.orgNorth Carolina Department of Education. (2012). North
Carolina Arts Education K-12 Instructional Tools [PDF document]. Retrieved from
http://ances.ncdpi.wikispaces.net/file/view/Music.I%20Can%20Statements.pdf
Ohio Department of Education. (2012). Ohio Department of Education Music Standards Grades 9-12 [PDF document]. Retrieved from
http://education.ohio.gov/getattachment/Topics/Ohio-s-New-Learning-Standards/Fine-Arts/Ohio-MusicStandards-Final2.pdf.aspx
Pearson, B., &Nowlin, R. (2011). Teaching band with excellence- A comprehensive curricular, pedagogical, and administrative
resource. San Diego, CA: Neil A. Kjos Music Company.
Pflaum, J. (2012). EDUTOPIA: 8VLQJ ³PXVLF ZULWLQJ´ WR WULJJHU FUHDWLYLW\ DZDUHQHVV PRWLYDWLRQ DQG SRHWU\. Retrieved from
http://www.edutopia.org/blog/music-writing-trigger-creativity-jeffrey-pflaum
Rusbult, C. (1998). Musical improvisation (creativity + music theory). Whole-Person Education. Retrieved from
http://www.asa3.org/ASA/education/teach/music.htm
Rush, S. (2006). Habits of a successful band director. Chicago, IL: GIA Publications.
Russo, W., Ainis, J., & Stevenson, D. (1988). Composing music: A new approach. Chicago, IL: University of Chicago Press.
Selby, C., Rush, S., & Moon, R. (2014). Habits of a successful string musician. Chicago, IL: GIA Publications.
Silverman, T. (2015). How to improvise on the violin. The Strad. Retrieved from http://www.thestrad.com/cpt-latests/how-toimprovise-on-the-violin/
Page 373

Skobe, M. (2012). ([SORULQJ FKLOGUHQ¶V PHDQLQJ-making of their own musical performance. Retrieved
fromhttp://www.academia.edu/3982551/Exploring_Childrens_Meaning-Making_of_Their_Own_Musical_Performance
South Carolina Band Directors Association. (2016). Audition Requirements. Retrieved from http://www.bandlink.org/all-stateband/auditions/audition-requirements/
South Carolina Department of Education. (2010). South Carolina Academic Standards for Instrumental Music. Retrieved from
http://ed.sc.gov/scdoe/assets/file/agency/ccr/Standards-Learning/documents/AcademicStandardsforInstrumentalMusic.pdf
South Carolina Department of Education. (2010). South Carolina Academic Standards for the Visual and Performing Arts [PDF
document]. Retrieved from http://ed.sc.gov/scdoe/assets/file/agency/ccr/StandardsLearning/documents/AcademicStandardsforInstrumentalMusic.pdf

Stewart, D. (2012). Arranging for Strings: Part 1. Sound On Sound. Retrieved from http://www.soundonsound.com/techniques/arrangingstrings-part-1

Systems, N., & Foundations, S. P. (2004). The Sussmannshaus Tradition for Violin Playing. Retrieved from
http://www.violinmasterclass.com/
Taylor, B. (2000). The Art of Improvisation (1.0 ed.)[PDF document].Taylor-James Publication. Retrieved from http://notybratstvo.org/sites/default/files/instr-improvisation-b-taylor-.pdf
University Rochester (2015, March 17). The art of improvisation: Inspiration and meaning for music [Online Video]. Retrieved from
https://www.youtube.com/watch?v=SzTaU5YI3oo
Williams, R., & King, J. (1998). Foundations for superior performance: Warm-ups and technique for band. San Diego, CA: Neil A.
Kjos Music Company.

Page 374


