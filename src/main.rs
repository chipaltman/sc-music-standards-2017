#![allow(unused_imports)]

/// External crate `pdf_extract`
use pdf_extract;
/// Filesystem manipulating operations.
use std::fs;
/// Native module for fixing messy text files.
mod clean;


fn main() {
    clean::nl_after_per();
    println!("Hello, world!");
}
