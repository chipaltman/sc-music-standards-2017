/// # Write logic for formatting a text file.
/// -[ ] for TXT
/// -[ ] for MD
///
/// ## Bad Formatting:
/// -[ ] ubiquitous newlines
///     -[ ] leftover from tables
///     -[ ] mid-sentence
///     -[ ] mid-word
/// -[ ] old pagination
/// -[ ] artifacts (e.g. ^L)
/// -[ ] duplication

/// Newlines only after periods.
pub fn nl_after_per() {}
