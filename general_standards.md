# General Music Standards
+ Creating - *I can use the elements of music to communicate new musical ideas and works.*
    + Anchor Standard 1: I can arrange and compose music.
+ Performing - *I can perform a variety of music with fluency and expression.*
    + Anchor Standard 2: I can improvise music.
    + Anchor Standard 3: I can sing alone and with others.
    + Anchor Standard 4: I can play instruments alone and with others.
    + Anchor Standard 5: I can read and notate music.
+ Responding- *I can respond to musical ideas as a performer and listener.*
    + Anchor Standard 6: I can analyze music.
    + Anchor Standard 7: I can evaluate music.
+ Connecting - *I can relate music ideas to personal meaning, other arts disciplines, and content areas.*
    + Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
    + Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.
